## Introduction

AAA test automation framework will be used mostly for rest/graphql integration tests and UI E2E tests. 

---
## Prerequisites
* gradle 6.8.2
* Open JDK 15
* Setup Allure on local machine if you want to generate report or install plugin for Jenkins
* IntelliJ IDEA - with lombok, cucumber plugins (set settings/gradle to Java 15)

## Clone a repository
https://bitbucket.org/parkjockey/aaa-automation

## Dependencies
* For running tests - JUnit runner
* For BDD - cucumber(core, java15, JVM, JUnit, gherkin), groovy-all
* For sending rest requests - rest-assured 
* For preparing the body and asserting response - Gson
* For preparing graphQl - test-graphql-java
* For UI - selenium and webdrivermanager
* For access IAM DB - jdbc
* For access HMT neo4j DB - neo4j-java-driver
* For Kafka integration - kafka-clients and other
* Replacement getters and setter - Lombok
* Accessing yaml files - snake-yaml
* Logging - slf4j
* For testing Redis - jedis
* For obtaining email - javax mail
* For integration with optimazely - optimizely libraries
* For cognito integration - cognitoidentityprovider
* For aws SQS integration to obtain email - aws-java-sdk (reefuato+someRandomNumber@reefplatform.com)
* Reporting - allure (cucumber4-jvm, rest-assured), testrail-api-java-client, cukedoctor

## Project structure:
   * src/test/java/common - will be used for storing general helpers in common packages 
   * src/test/java/data_containers - will use for PicoContainer DI
   * src/test/java/graphql - will be used for storing graphql steps, requests, responses and helpers
   * src/test/ui - will be used for storing selenium E2E tests
   * src/test/java/runners - will store runner class
   * src/test/resources -  will store features in the features package. Then feature package will have packages related to each functionality with specific features files. Also,  stores yaml for different environments (URL, credentials etc…) and graphql body files

## How to run tests: 
  * By Junit runner - just run class CucumberRunner in runners package. Default it will run tests on dev environment.
  * By terminal type command: 
  gradle cucumber -Denv={envName} -Ddriver={driverName} -Dcucumber.options="-t @tag1,@tag2 -t ~@igonredTag"  
    
  Note: envName is stored in properties.yml file.
  If you have some tests that are currently failing because they are waiting future implementation, annotate them with @failing so we can ignore them during our run.
  
## Reporting:   
  * If you want to generate Allure report type in terminal: allure generate <yourPathToAllureReport/allure-results> --clear. 
  After the task is finished check the allure-report package and open with the preferred browser open index.html file. 
  (If you run as Junit run Class it allure-result will be generated in the root package if it is run via terminal/Gradle 
  run it is stored in root/build/allure-result)
  * Adoc file is generated at the end of run
  * TestRail run report is also generated in every run
