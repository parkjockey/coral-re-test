package common.helpers.aws_helpers.cognito;

import common.helpers.aws_helpers.AWSCreds;
import data_containers.GeneralData;
import software.amazon.awssdk.auth.credentials.AwsBasicCredentials;
import software.amazon.awssdk.auth.credentials.AwsCredentials;
import software.amazon.awssdk.auth.credentials.StaticCredentialsProvider;
import software.amazon.awssdk.regions.Region;
import software.amazon.awssdk.services.cognitoidentityprovider.CognitoIdentityProviderClient;
import software.amazon.awssdk.services.cognitoidentityprovider.model.AdminDeleteUserRequest;

public class CognitoHelpers {
    public static String userPoolId;
    static GeneralData generalData;
    static CognitoIdentityProviderClient identityProviderClient = cognitoIdentityProviderClient(generalData.countryCode);

    public CognitoHelpers(GeneralData generalData) {
        this.generalData = generalData;
    }

    public static CognitoIdentityProviderClient cognitoIdentityProviderClient(String countryCode) {
        String region = countryCode.equals("us") ?  "us-east-2" : "ca-central-1";

        AwsCredentials credentials = AwsBasicCredentials.create(AWSCreds.accessKey, AWSCreds.secretKey);
        return CognitoIdentityProviderClient.builder()
                .credentialsProvider(StaticCredentialsProvider.create(credentials))
                .region(Region.of(region))
                .build();
    }

    public static void removeUser(String username) {
        var request = AdminDeleteUserRequest.builder()
                .userPoolId(userPoolId)
                .username(username)
                .build();
        identityProviderClient.adminDeleteUser(request);
    }


}
