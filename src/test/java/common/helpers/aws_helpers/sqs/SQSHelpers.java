package common.helpers.aws_helpers.sqs;

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.sqs.AmazonSQS;
import com.amazonaws.services.sqs.AmazonSQSClientBuilder;
import com.amazonaws.services.sqs.model.DeleteMessageRequest;
import com.amazonaws.services.sqs.model.Message;
import com.amazonaws.services.sqs.model.ReceiveMessageRequest;
import com.google.gson.Gson;
import common.helpers.aws_helpers.AWSCreds;
import common.helpers.aws_helpers.sqs.response.SQSMessageResponse;
import common.helpers.aws_helpers.sqs.response.SQSResponse;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

public class SQSHelpers extends AWSCreds {

    private static Logger logger = LoggerFactory.getLogger(SQSHelpers.class);
    private static AWSCredentials credentials = new BasicAWSCredentials(accessKey, secretKey);
    private static AmazonSQS sqs = AmazonSQSClientBuilder.standard()
            .withCredentials(new AWSStaticCredentialsProvider(credentials))
            .withRegion(Regions.US_WEST_2)
            .build();

    /**
     * Method will fetch all messages from the que, while looping it will delete read message and put in new list
     *
     * @return List of SQS messages
     * @author dino.rac
     */
    public static List<Message> getSQSMessages() {
        List<Message> messagelist = new ArrayList<>();

        try {
            boolean flag = true;
            while (flag) {
                ReceiveMessageRequest receiveMessageRequest = new ReceiveMessageRequest(sqsUrl);
                receiveMessageRequest.setMaxNumberOfMessages(10);
                receiveMessageRequest.withMaxNumberOfMessages(10).withWaitTimeSeconds(20);
                List<Message> messages = sqs.receiveMessage(receiveMessageRequest).getMessages();

                for (Message message : messages) {
                    //Add message to list
                    messagelist.add(message);
                    //Delete message from que so we can fetch new message
                    String messageReceiptHandle = message.getReceiptHandle();
                    sqs.deleteMessage(new DeleteMessageRequest().withQueueUrl(sqsUrl).withReceiptHandle(messageReceiptHandle));
                }
                if (messages.size() == 0) {
                    flag = false;
                }
            }

        } catch (AmazonServiceException ase) {
            ase.printStackTrace();
        } catch (AmazonClientException ace) {
            ace.printStackTrace();
        } catch (NullPointerException e) {
            e.printStackTrace();
        }finally {
            return messagelist;
        }

    }

    /**
     * Find desired Mail from SQS message list
     *
     * @param recipient
     * @param subject
     * @return String that contains email
     */
    public static String getSpecificMessage(String recipient, String subject) {
        String email = null;
        try {
            //Obtain list of all messages
            List<Message> messageList = getSQSMessages();
            //Find Message for specific user with desired subject
            for (Message m : messageList) {
                SQSResponse response = new Gson().fromJson(m.getBody(), SQSResponse.class);
                SQSMessageResponse message = new Gson().fromJson(response.getMessage(), SQSMessageResponse.class);
                if (message.getContent().contains("Subject: " + subject) && message.getContent().contains("To: " + recipient)) {
                    email = message.getContent();
                    logger.info("Email is found: " + message.getContent());
                    break;
                }
            }
        } catch (NullPointerException e) {
            logger.error("Email not found");
        } finally {
            return email;
        }

    }

    public static void deleteAllMessages() {
        //Fetch emails from SQS
        List<Message> messageList = getSQSMessages();

        //delete all messages
        if (messageList == null) {
            for (Message m : messageList) {
                //Delete
                sqs.deleteMessage(new DeleteMessageRequest()
                        .withQueueUrl(sqsUrl)
                        .withReceiptHandle(m.getReceiptHandle()));
            }
        }
    }

    @Test
    public void deleteManualSQS() {
        deleteAllMessages();

    }
}
