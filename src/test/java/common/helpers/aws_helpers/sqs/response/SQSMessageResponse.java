package common.helpers.aws_helpers.sqs.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import lombok.Getter;

@Getter
public class SQSMessageResponse {
    @SerializedName("content")
    @Expose
    private String content;
}
