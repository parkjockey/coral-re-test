package common.helpers.aws_helpers.sqs.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import lombok.Getter;

@Getter
public class SQSResponse {
    @SerializedName("Type")
    @Expose
    private String type;
    @SerializedName("MessageId")
    @Expose
    private String messageId;
    @SerializedName("TopicArn")
    @Expose
    private String topicArn;
    @SerializedName("Subject")
    @Expose
    private String subject;
    @SerializedName("Message")
    @Expose
    private String message;
    @SerializedName("Timestamp")
    @Expose
    private String timestamp;
    @SerializedName("SignatureVersion")
    @Expose
    private String signatureVersion;
    @SerializedName("Signature")
    @Expose
    private String signature;
    @SerializedName("SigningCertURL")
    @Expose
    private String signingCertURL;
    @SerializedName("UnsubscribeURL")
    @Expose
    private String unsubscribeURL;
}
