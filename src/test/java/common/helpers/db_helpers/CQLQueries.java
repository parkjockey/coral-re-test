package common.helpers.db_helpers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CQLQueries {

    private static Logger logger = LoggerFactory.getLogger(CQLQueries.class);

    public static String getRealEstateId(String realEstateId) throws Exception {
        return Neo4JHelpers.queryRequest("MATCH (n:RealEstateLocation) WHERE n.realEstateId = '"
                + realEstateId + "' RETURN n.realEstateId;");
    }

    public static String createLocation(String realEstateId, String locationName) throws Exception {
        return Neo4JHelpers.queryRequest("CREATE (n:RealEstateLocation { realEstateId: '"+realEstateId+"'," +
                " fullAddress: '1301 ASSEMBLY STREET, 1, COLUMBIA, 29201, SC, US', locationName: '"+locationName+"'," +
                " city: 'COLUMBIA'}) RETURN n.realEstateId;");
    }

    public static void deleteLocation(String realEstateId) throws Exception {
             Neo4JHelpers.deleteRequest(
                     "MATCH (n:RealEstateLocation {realEstateId: '"+realEstateId+"'})" +
                    "DETACH DELETE n"
             );
       logger.info("LOCATION IS DELETED");
    }
    public static Integer getOrganizationId(Integer orgId) throws Exception {
        return Neo4JHelpers.queryRequestInt("MATCH (n:Organization) WHERE n.orgId ="
                + orgId + "RETURN n.orgId;");
    }



}
