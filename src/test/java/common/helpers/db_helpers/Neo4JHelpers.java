package common.helpers.db_helpers;

import org.neo4j.driver.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static org.neo4j.driver.Values.parameters;

public class Neo4JHelpers {
    static Driver driver;
    static String neo4JUrl = null;
    static String neo4JUser = null;
    static String neo4JPass = null;
    private static Logger logger = LoggerFactory.getLogger(Neo4JHelpers.class);
//    static  String query = "MATCH (n:RealEstateLocation)\n" +
//            "WHERE n.realEstateId = 'US-CA-RE-1198791'\n" +
//            "RETURN n.realEstateId;";


    public static void setNeo4JCredentials(String url, String user, String pass) {
        neo4JUrl = url;
        neo4JUser = user;
        neo4JPass = pass;
    }

    /**
     * Method will return desired value from query
     *
     * @param query
     * @return String with desired value
     * @throws Exception
     * @author dino.rac
     */
    public static String queryRequest(String query) throws Exception {

        driver = GraphDatabase.driver(neo4JUrl, AuthTokens.basic(neo4JUser, neo4JPass));
        String queryResult = null;
        try (Session session = driver.session()) {
            queryResult = session.writeTransaction(new TransactionWork<String>() {
                @Override
                public String execute(Transaction tx) {
                    Result result = tx.run(query);
                    return result.single().get(0).asString();
                }
            });
            logger.info("Query result: " + queryResult);
            driver.close();
        } catch (Exception e) {
            driver.close();
            e.printStackTrace();
        } finally {
            return queryResult;
        }
    }

    /**
     * Method will execute create/update and return desired value
     *
     * @param query
     * @return String with desired value
     * @throws Exception
     * @author dino.rac
     */
    public static String executableRequest(String query, String key, String value) throws Exception {

        driver = GraphDatabase.driver(neo4JUrl, AuthTokens.basic(neo4JUser, neo4JPass));
        String queryResult = null;
        try (Session session = driver.session()) {
            queryResult = session.writeTransaction(new TransactionWork<String>() {
                @Override
                public String execute(Transaction tx) {
                    Result result = tx.run(query, parameters(key, value));
                    return result.single().get(0).asString();
                }
            });
            logger.info("Query result: " + queryResult);
            driver.close();
        } catch (Exception e) {
            driver.close();
            e.printStackTrace();
        } finally {
            return queryResult;
        }
    }

    /**
     * Method will return desired value from query
     *
     * @param query
     * @return String with desired value
     * @throws Exception
     * @author dino.rac
     */
    public static Result deleteRequest(String query) throws Exception {

        driver = GraphDatabase.driver(neo4JUrl, AuthTokens.basic(neo4JUser, neo4JPass));
         Result queryResult = null;
        try (Session session = driver.session()) {
            queryResult = session.writeTransaction(new TransactionWork<Result>() {
                @Override
                public Result execute(Transaction tx) {
                    Result result = tx.run(query);
                    tx.commit();
                    tx.close();

                    return result;
                }

            });
            logger.info("Delete query executed");
            driver.close();
        } catch (Exception e) {
            e.printStackTrace();
            driver.session().close();
            driver.close();
        } finally {
             return queryResult;
        }
    }

    public static Integer queryRequestInt(String query) throws Exception {

        driver = GraphDatabase.driver(neo4JUrl, AuthTokens.basic(neo4JUser, neo4JPass));
        Integer queryResult = null;
        try (Session session = driver.session()) {
            queryResult = session.writeTransaction(new TransactionWork<Integer>() {
                @Override
                public Integer execute(Transaction tx) {
                    Result result = tx.run(query);
                    return result.single().get(0).asInt();
                }
            });
            logger.info("Query result: " + queryResult);
            driver.close();
        } catch (Exception e) {
            driver.close();
            e.printStackTrace();
        } finally {
            return queryResult;
        }
    }

}

