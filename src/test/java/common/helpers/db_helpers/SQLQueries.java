package common.helpers.db_helpers;

import java.util.ArrayList;

public class SQLQueries {
    public static ArrayList<String> insertTestModule = new ArrayList<>(){{

        add("INSERT INTO modules (name, level, created_date, last_modified_date)\n" +
                "VALUES ('Test module', 1, now(), now());");
        add("INSERT INTO modules (name, level, parent_id, created_date, last_modified_date)\n" +
                "VALUES ('Test child module', 2, (SELECT id FROM modules WHERE name = 'Test module'), now(), now());");
        add("INSERT INTO modules (name, level, parent_id, created_date, last_modified_date)\n" +
                "VALUES ('Test grandchild module', 3,\n" +
                "        (SELECT id FROM modules WHERE name = 'Test child module'),\n" +
                "        now(), now());");
        add("INSERT INTO organization_modules (organization_id, module_id)\n" +
                "VALUES ((SELECT id from organization where type = 'ENTERPRISE'), (SELECT id FROM modules WHERE name = 'Test module'));");
    }};

    public static ArrayList<String> deleteTestModule = new  ArrayList<>(){{
        add("DELETE FROM organization_modules WHERE module_id in (SELECT id FROM modules WHERE name in ('Test module', 'Test child module', 'Test grandchild module'));");
        add("DELETE FROM roles_modules WHERE module_id in (SELECT id FROM modules WHERE name in ('Test module', 'Test child module', 'Test grandchild module'));");
        add("DELETE FROM organization_modules WHERE organization_id in (SELECT id from organization where type = 'ENTERPRISE') AND module_id in (SELECT id FROM modules WHERE name = 'Test module');");
        add("DELETE FROM modules where name = 'Test grandchild module';");
        add("DELETE FROM modules where name = 'Test child module';");
        add("DELETE FROM modules where name = 'Test module';");
    }};

    public static Integer findModuleId(String moduleName){
        return JdbcHelpers.getIntValueFromDb("select id from modules where name ='" + moduleName + "';", "id");
    }

    public static Integer findUserId(String username){
        return  JdbcHelpers.getIntValueFromDb("select id from users where username='"+username+"';", "id");
    }

    public static ArrayList<String> deleteConsumer(String username){
        return  new  ArrayList<>(){{
            add("delete from consumer_users where user_id in (select id from users where username ='"+username+"');");
            add("delete from users where username ='"+username+"';");
        }};
    }

    public static Integer findRoleId(Integer organizationId){
        return  JdbcHelpers.getIntValueFromDb("select id from roles where organization_id ='"+organizationId+"';", "id");
    }

    public static String getResetPasswordVerificationCode(Integer userId){
        return  JdbcHelpers.getStringValueFromDb("select reset_password_verification_code from users where id ='"+userId+"';", "reset_password_verification_code");
    }

    public static String getUserInvitationCode(String userName){
        String dbQuery = "select invitation_code from organization_users\n" +
                "inner join users\n" +
                "on users.id =  organization_users.user_id\n" +
                "where users.username = '" +
                userName + "';";
        return JdbcHelpers.getStringValueFromDb(dbQuery, "invitation_code");
    }

    public static void setReefOrgName(){
        JdbcHelpers.executeUpdate("update organization set name = 'REEF', description = 'REEF Enterprise organization' where type = 'ENTERPRISE';");
    }




}
