package common.helpers.environment;

public class Env {

    public String name;
    public String reefWebApp;
    public String gqlBaseUri;
    public String dbUrl;
    public String dbUser;
    public String dbPass;
    public String loginWebApp;
    public String loginRedirectUrl;
    public String azureTenantId;
    public String azureClientId;
    public String azureSecret;
    public String azureUserDomain;
    public String redis;
    public String optimizelyKey;
    public String userPoolId;
    public String restBaseUri;
    public String neo4JUrl;
    public String neo4JUser;
    public String neo4JPass;
    public String hmtGqlUri;
    public String mediatorGql;

}
