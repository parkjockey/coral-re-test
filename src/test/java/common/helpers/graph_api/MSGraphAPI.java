package common.helpers.graph_api;

import common.helpers.graph_api.request.assign_user.AssignUerToAppBody;
import common.helpers.graph_api.request.create_user.CreateUserBody;
import common.helpers.rest.HelperRest;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;

import static org.hamcrest.core.IsNull.nullValue;

/**
 * Class contains helpers for MSGraphAPI
 *
 * @author dino.rac
 */
public class MSGraphAPI {

    public static final String MS_LOGIN_URI = "https://login.microsoftonline.com";
    public static final String MS_GRAPH_URI = "https://graph.microsoft.com/v1.0";
    public static Logger logger = LoggerFactory.getLogger(MSGraphAPI.class);
    public static  String tenantId;
    public static String clientId;
    public static final String SCOPES = "https://graph.microsoft.com/.default";
    public static  String clientSecret;
    public static String azureUserDomain;


    /**
     * Method will return Azure Active Directory access token
     *
     * @return token as String
     * @author dino.rac
     */
    public static String getADToken() {
        RequestSpecBuilder builder = new RequestSpecBuilder();

        builder.setBaseUri(MS_LOGIN_URI);
        builder.setBasePath("/" + tenantId + "/oauth2/v2.0/token");
        builder.setContentType("application/x-www-form-urlencoded");
        builder.addFormParam("client_id", clientId);
        builder.addFormParam("scope", SCOPES);
        builder.addFormParam("grant_type", "client_credentials");
        builder.addFormParam("client_secret", clientSecret);

        RequestSpecification rSpec = builder.build();

        Response response = HelperRest.sendPostRequest(rSpec);

        if (response.toString().equals(nullValue()) || response.toString().contains("error"))
            logger.error("\nTOKEN ISN'T GENERATED: " + response.toString());

        String token = response.getBody().jsonPath().get("access_token");
        logger.info("\nTOKEN IS GENERATED: " + token);
        return token;

    }

    /**
     * Method will generate random AD user
     *
     * @param token
     * @return HashMap with user details such as: mail, password, displayName, mailNickname
     * @author dino.rac
     */
    public static HashMap<String, String> createADUser(String token) {
        HashMap<String, String> user = new HashMap<>() {{
            put("displayName", "Auto User");
            put("mail", "auto"+ HelperRest.getRandomNumber()+"@"  + azureUserDomain);
            put("password", "Test1234!");
            put("mailNickName", "AutoUser");
            put("givenName", "Auto");
            put("surname", "User");
        }};

        CreateUserBody body = new CreateUserBody();

        body.setAccountEnabled(true);
        body.setDisplayName(user.get("displayName"));
        body.setUserPrincipalName(user.get("mail"));
        body.setPasswordPolicies("DisablePasswordExpiration");
        body.getPasswordProfile().setForceChangePasswordNextSignIn(false);
        body.getPasswordProfile().setPassword(user.get("password"));
        body.setMailNickname(user.get("mailNickName"));
        body.setMail(user.get("mail"));
        body.setGivenName(user.get("givenName"));
        body.setSurname(user.get("surname"));

        RequestSpecBuilder builder = new RequestSpecBuilder();
        builder.setBaseUri(MS_GRAPH_URI);
        builder.setBasePath("/users");
        builder.setContentType(ContentType.JSON);
        builder.setBody(body);
        builder.addHeader("Authorization", token);

        RequestSpecification rSpec = builder.build();
        Response response = HelperRest.sendPostRequest(rSpec);
        user.put("id", response.jsonPath().getString("id"));

        if (user.get("id").equals(nullValue()) || user.get("id").isEmpty())
            logger.error("USER ISN'T GENERATED");

        return user;
    }

    /**
     * Method will delete user from AD
     *
     * @param token
     * @param id    can be AD objectID or user email
     * @return blank response
     * @author dino.rac
     */
    public static Response deleteADUser(String token, String id) {
        RequestSpecBuilder builder = new RequestSpecBuilder();
        builder.setBaseUri(MS_GRAPH_URI);
        builder.setBasePath("/users/" + id);
        builder.addHeader("Authorization", token);

        RequestSpecification rSpec = builder.build();
        Response response = HelperRest.sendDeleteRequest(rSpec);

        return response;
    }

    /**
     * Method will assign Active Directory user to desired APP
     * @param token
     * @param userId user object id
     * @param appObjId
     * @param appRoleId
     * @return Response for successful assignment
     * @author dino.rac
     */
    public static Response assignADUserToApp(String token, String userId, String appObjId, String appRoleId){
        RequestSpecBuilder builder = new RequestSpecBuilder();
        AssignUerToAppBody body = new AssignUerToAppBody();

        body.setPrincipalId(userId);
        body.setResourceId(appObjId);
        body.setAppRoleId(appRoleId);

        builder.setBaseUri(MS_GRAPH_URI);
        builder.setBasePath("/users/" + userId+"/appRoleAssignments");
        builder.setBody(body);
        builder.addHeader("Authorization", token);
        builder.setContentType(ContentType.JSON);

        RequestSpecification rSpec = builder.build();
        Response response = HelperRest.sendPostRequest(rSpec);

        if (response.toString().equals(nullValue()) || response.toString().contains("error")) {
            logger.error("\nUser ins't assigned to application : " + response.toString());
        }
        return  response;
    }

    /**
     * Method will generate random active or inactive AD user
     *
     * @param token
     * @param isActive
     * @return HashMap with user details such as: mail, password, displayName, mailNickname
     * @author dino.rac
     */
    public static HashMap<String, String> createADUser(String token, boolean isActive) {
        HashMap<String, String> user = new HashMap<>() {{
            put("displayName", "Auto User");
            put("mail", "auto"+ HelperRest.getRandomNumber() + "@" +azureUserDomain);
            put("password", "Test1234!");
            put("mailNickName", "AutoUser");
        }};

        CreateUserBody body = new CreateUserBody();

        body.setAccountEnabled(isActive);
        body.setDisplayName(user.get("displayName"));
        body.setUserPrincipalName(user.get("mail"));
        body.setPasswordPolicies("DisablePasswordExpiration");
        body.getPasswordProfile().setForceChangePasswordNextSignIn(false);
        body.getPasswordProfile().setPassword(user.get("password"));
        body.setMailNickname(user.get("mailNickName"));
        body.setMail(user.get("mail"));

        RequestSpecBuilder builder = new RequestSpecBuilder();
        builder.setBaseUri(MS_GRAPH_URI);
        builder.setBasePath("/users");
        builder.setContentType(ContentType.JSON);
        builder.setBody(body);
        builder.addHeader("Authorization", token);

        RequestSpecification rSpec = builder.build();
        Response response = HelperRest.sendPostRequest(rSpec);
        user.put("id", response.jsonPath().getString("id"));

        if (user.get("id").equals(nullValue()) || user.get("id").isEmpty())
            logger.error("USER ISN'T GENERATED");

        return user;
    }


}
