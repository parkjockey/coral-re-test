package common.helpers.graph_api.request.assign_user;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AssignUerToAppBody {
    @SerializedName("principalId")
    @Expose
    private String principalId;
    @SerializedName("resourceId")
    @Expose
    private String resourceId;
    @SerializedName("appRoleId")
    @Expose
    private String appRoleId;
}
