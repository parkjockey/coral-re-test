package common.helpers.graph_api.request.create_user;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CreateUserBody {
    @SerializedName("accountEnabled")
    @Expose
    private Boolean accountEnabled;
    @SerializedName("displayName")
    @Expose
    private String displayName;
    @SerializedName("mailNickname")
    @Expose
    private String mailNickname;
    @SerializedName("passwordPolicies")
    @Expose
    private String passwordPolicies;
    @SerializedName("passwordProfile")
    @Expose
    private PasswordProfile passwordProfile = new PasswordProfile();
    @SerializedName("userPrincipalName")
    @Expose
    private String userPrincipalName;
    @SerializedName("mail")
    @Expose
    private String mail;
    @SerializedName("givenName")
    @Expose
    private String givenName;
    @SerializedName("surname")
    @Expose
    private String surname;
}
