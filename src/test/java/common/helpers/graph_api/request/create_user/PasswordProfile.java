package common.helpers.graph_api.request.create_user;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PasswordProfile {
    @SerializedName("password")
    @Expose
    private String password;
    @SerializedName("forceChangePasswordNextSignIn")
    @Expose
    private Boolean forceChangePasswordNextSignIn;
}
