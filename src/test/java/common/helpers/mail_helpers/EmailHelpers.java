package common.helpers.mail_helpers;


import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.mail.*;
import javax.mail.search.SearchTerm;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

/**
 * Class will store methods for email helpers
 */
public class EmailHelpers {

    public static Logger logger = LoggerFactory.getLogger(EmailHelpers.class);

    public static void main(String[] args) {

    }

    /**
     * Method will find email when all parameters are inserted
     *
     * @param host        email host
     * @param userName    email userName for login
     * @param password    password
     * @param mailSubject subject
     * @param recipient   recipient mail since we are using +randomNumber in email (availtesterkovic+1234@gamil.com)
     * @return Message type email for desired user
     * @throws MessagingException
     * @author Dino
     */
    public static Message searchEmailsBySubject(String host, String userName,
                                         String password, final String mailSubject, String recipient) throws MessagingException {
        //Define protocol
        Properties sysProps = System.getProperties();

        sysProps.setProperty("mail.store.protocol", "imaps");
        //System.setProperty("javax.net.ssl.trustStore", "C://Program Files (x86)//java//java-se-8u40-ri//jre//lib//security");
        Message[] foundMessages = new Message[0];
        Message mail = null;
        Session session;
        Store store = null;
        Folder folderInbox = null;

        try {
            //Create session to server
             session = Session.getInstance(sysProps, null);

            //Store yours messages:
             store = session.getStore("imaps");

            store.connect(host, userName, password);

            // opens the inbox folder
            folderInbox = store.getFolder("INBOX");
            folderInbox.open(Folder.READ_ONLY);

            // creates a search criterion
            SearchTerm searchCondition = new SearchTerm() {
                @Override
                public boolean match(Message message) {
                    try {
                        if (message.getSubject().contains(mailSubject)) {
                            return true;
                        }
                    } catch (MessagingException ex) {
                        ex.printStackTrace();
                    }
                    return false;
                }
            };

            // performs search through the folder
            foundMessages = folderInbox.search(searchCondition);

            for (int i = 0; i < foundMessages.length; i++) {
                Message message = foundMessages[i];
                String subject = message.getSubject();
                logger.info("Found message #" + i + ": " + subject);
            }

           
        } catch (NoSuchProviderException ex) {
            logger.error("No provider.");
            ex.printStackTrace();
        } catch (MessagingException ex) {
            logger.error("Could not connect to the message store.");
            ex.printStackTrace();
        }

        for (int i = 0; i < foundMessages.length; i++) {
            if (foundMessages[i].getRecipients(Message.RecipientType.TO)[0].toString().equals(recipient)) {
                mail = foundMessages[i];
            }
        }
        // disconnect
//        folderInbox.close(false);
//        store.close();
        return mail;
    }

    /**
     * Method will wait for desired mail to be received
     *
     * @param emailHostName email host
     * @param emailUserName email userName for login
     * @param emailPassword password
     * @param mailSubject   subject
     * @param mailRecipient recipient mail since we are using +randomNumber in email (availtesterkovic+1234@gamil.com)
     * @param waitTime      set waiting period in milliseconds
     * @return Message when email is received
     * @throws MessagingException
     * @author Dino
     */
    public static Message waitMailToBeObtained(String emailHostName, String emailUserName, String emailPassword,

                                               String mailSubject, String mailRecipient, long waitTime) throws MessagingException, InterruptedException {
        Message welcomeMail = null;
        Thread.sleep(2000);

        long t = System.nanoTime();
        long end = t + TimeUnit.SECONDS.toNanos(waitTime);
        while ((welcomeMail == null || welcomeMail.getRecipients(Message.RecipientType.TO) == null || !welcomeMail.getRecipients(Message.RecipientType.TO)[0].toString().equals(mailRecipient)) && end > System.nanoTime()) {
            welcomeMail = searchEmailsBySubject(emailHostName, emailUserName, emailPassword,
                    mailSubject, mailRecipient);

        }
        if (welcomeMail == null) {
            logger.error("Mail is not obtained");
        }
        return welcomeMail;
    }

    /**
     * Email can be obtained with just email and subject, other necessary properties are set
     * @param email
     * @param subject
     * @return
     * @throws MessagingException
     * @throws InterruptedException
     */
    public static Message  waitMailToBeObtained(String email, String subject) throws MessagingException, InterruptedException {
        Message message = waitMailToBeObtained("imap.gmail.com",
                "reefuato@gmail.com", "Test1234!", subject, email, 2);

       return message;
    }

    public static String getOTP(String mail){
        String first = StringUtils.substringAfter(mail, "<div class=3D\"code\"><span>");
        String otp = StringUtils.substringBefore(first,"</span></div>");
        return otp;
    }
}
