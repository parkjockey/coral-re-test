package common.helpers.optimizely;

import com.optimizely.ab.Optimizely;
import com.optimizely.ab.OptimizelyFactory;

public class OptimizelyHelpers {


    public static Boolean enableFeature(String sdkKey, String feature, String userId) {
        Optimizely optimizelyClient = OptimizelyFactory.newDefaultInstance(sdkKey);
        return optimizelyClient.isFeatureEnabled(feature, userId);

    }
}
