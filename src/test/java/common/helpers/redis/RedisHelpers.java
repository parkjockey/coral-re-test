package common.helpers.redis;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import redis.clients.jedis.Jedis;

import java.util.Map;

public class RedisHelpers {

    public static Logger logger = LoggerFactory.getLogger(RedisHelpers.class);
    public static String prefixUserAuthData = "IA_UserAuthorizationData:";
    public static String prefixBlackList = "IA_TokenBlackList:";
    public static String redisHost = null;
    static Integer redisPort = 6379;
    static String redisPass = "REEFTechnologyCloud123+";

    public static Map<String, String> getFromRedis(String redisKey) {
        //Connecting to Redis server on localhost
        Jedis jedis = new Jedis(redisHost, redisPort, true);
        jedis.connect();
        jedis.auth(redisPass);
        logger.info("Connection to server successfully");
        logger.info("Extracted from redis: " + jedis.hgetAll(redisKey).toString());
        return jedis.hgetAll(redisKey);
    }
}
