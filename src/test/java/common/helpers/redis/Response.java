package common.helpers.redis;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Response {
    @SerializedName("id")
    @Expose
    private Integer id;

}
