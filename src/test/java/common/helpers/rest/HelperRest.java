package common.helpers.rest;

import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.apache.commons.lang3.RandomStringUtils;

import java.time.Instant;
import java.util.Random;

import static io.restassured.RestAssured.given;

/**
 * Class provide helpers for rest testing such as send get, post, put, delete requests
 *
 * @author: dino.rac
 */
public class HelperRest {

    /**
     * Generic method for sending Post request when request specification object
     * is inserted as parameter
     *
     * @param rSpec request specification object
     * @return response which can be used for assertion
     * @author: dino.rac
     */
    public static Response sendPostRequest(RequestSpecification rSpec) {

        return given().spec(rSpec).log().all().relaxedHTTPSValidation().when()
                .post().then().log().all().extract().response();
    }

    /**
     * Generic method for sending Get request when request specification object
     * is inserted as parameter
     *
     * @param rSpec request specification object
     * @return response which can be used for assertion
     * @author: dino.rac
     */
    public static Response sendGetRequest(RequestSpecification rSpec) {
        return given().spec(rSpec).log().all().relaxedHTTPSValidation().when()
                .get().then().log().all().extract().response();
    }

    /**
     * Generic method for sending Put request when request specification object
     * is inserted as parameter
     *
     * @param rSpec request specification object
     * @return response which can be used for assertion
     * @author: dino.rac
     */
    public static Response sendPut(RequestSpecification rSpec) {

        return given().spec(rSpec).log().all().relaxedHTTPSValidation().when()
                .put().then().log().all().extract().response();

    }

    /**
     * Generic method for sending Delete request when request specification object
     * is inserted as parameter
     *
     * @param rSpec request specification object
     * @return response which can be used for assertion
     * @author: dino.rac
     */

    public static Response sendDeleteRequest(RequestSpecification rSpec) {
        return given().spec(rSpec).log().all().relaxedHTTPSValidation().when()
                .delete().then().log().all().extract().response();

    }

    public static Integer getRandomNumber() {
        int max = 10000000;
        int min = 1;
        Random randomNum = new Random();

        return min + randomNum.nextInt(max);
    }

    public static Integer getFiveDigitRanNumber() {

        Random r = new Random(System.currentTimeMillis());
        return ((1 + r.nextInt(2)) * 10000 + r.nextInt(10000));

    }

    public static Instant getCurrentDateTime() {
        long now = System.currentTimeMillis();
        now = now / 1000;
        return Instant.ofEpochSecond(now);

    }

    public static String getRandomEmail() {

        String email = HelperRest.getAlphaNumericString(7) + "@" + HelperRest.getAlphaNumericString(5) + ".com";
        return email;
    }

    /**
     * Method will response when request specification and request method are inserted
     *
     * @param rSpec         RequestSpecification
     * @param requestMethod Name of request, check switch statement for desired value
     * @return Response
     * @author: dino.rac
     */
    public static Response sendRequest(RequestSpecification rSpec, String requestMethod) {
        Response response;

        switch (requestMethod) {
            case "GET":
                response = given().spec(rSpec).log().all().relaxedHTTPSValidation().when()
                        .get().then().log().all().extract().response();
                break;

            case "POST":
                response = given().spec(rSpec).log().all().relaxedHTTPSValidation().when()
                        .post().then().log().all().extract().response();
                break;

            case "PUT":
                response = given().spec(rSpec).log().all().relaxedHTTPSValidation().when()
                        .put().then().log().all().extract().response();
                break;

            case "DELETE":
                response = given().spec(rSpec).log().all().relaxedHTTPSValidation().when()
                        .delete().then().log().all().extract().response();
                break;

            default:
                throw new IllegalStateException(
                        "Unexpected value: " + requestMethod);

        }
        return response;
    }

    /**
     * Genererate random alphanumeric string
     *
     * @param n number of characters
     * @return
     */
    public static String getAlphaNumericString(int n) {
        return RandomStringUtils.randomAlphanumeric(n).toUpperCase();
    }


    public static String getRandomUatoMail(){
        return "reefuato+"+ HelperRest.getRandomNumber()+"@reefplatform.com";
    }

}

