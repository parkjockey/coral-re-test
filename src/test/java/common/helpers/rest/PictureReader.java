package common.helpers.rest;

import org.apache.commons.codec.binary.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

public class PictureReader {
    private static Logger logger = LoggerFactory.getLogger(PictureReader.class);

    public static String encodeFileToBase64Binary(File file){
        String encodedfile = null;
        try {
            FileInputStream fileInputStreamReader = new FileInputStream(file);
            byte[] bytes = new byte[(int)file.length()];
            fileInputStreamReader.read(bytes);
            encodedfile = new String(Base64.encodeBase64(bytes), "UTF-8");
        } catch (FileNotFoundException e) {
            logger.error("\nCant find picture");
                 e.printStackTrace();
        } catch (IOException e) {
            logger.error("\nPicture is not encoded");
                e.printStackTrace();
        }

        return encodedfile;
    }


}
