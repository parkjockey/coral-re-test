package common.helpers.rest;

import com.auth0.jwt.JWT;
import com.auth0.jwt.interfaces.Claim;
import com.auth0.jwt.interfaces.DecodedJWT;

import java.util.Date;
import java.util.Map;

public class TokenProvider {
    private static final String USERNAME = "username";
    private static final String JTI = "jti";
    private final String DECODING_TOKEN_ERROR = "Error while decoding token %s!";
    private final String MISSING_CLAIM_ERROR = "Claim %s is not present in token!";

    public static String getUsernameClaim(String token) {
        DecodedJWT decodedToken = decodeToken(token);
        return getClaim(decodedToken, USERNAME).asString();
    }
    public Date getExpirationTime(String token) {
        return decodeToken(token).getExpiresAt();
    }
    private static DecodedJWT decodeToken(String token) {
            return JWT.decode(token);

    }
    private static Claim getClaim(DecodedJWT decodedToken, String claim) {
        Map<String, Claim> claims = decodedToken.getClaims();
               return claims.get(claim);
    }

    public static String getJtiClaim(String token) {
        DecodedJWT decodedToken = decodeToken(token);
        return getClaim(decodedToken, JTI).asString();
    }
}
