package common.helpers.twilio;


import com.google.gson.Gson;
import common.helpers.rest.HelperRest;
import common.helpers.twilio.getMessagesResponse.GetMessagesResponse;
import common.helpers.twilio.getMessagesResponse.Message;
import io.restassured.authentication.PreemptiveBasicAuthScheme;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.apache.commons.lang3.StringUtils;
import org.junit.Test;

import java.util.ArrayList;

public class TwilioHelpers {
    public static String baseUri = "https://api.twilio.com/2010-04-01/Accounts/";
    public static String twilioAccountId = "ACd651da7cc91ad855e6d7eeddb64cd552";
    public static String twilioPassword = "96e4200269f501b0df45f233b5127f29";
    public static String phoneReceiver = "+17028669865";
    public static String phoneSender = "+13072434486";


    public static PreemptiveBasicAuthScheme setAuthScheme() {
        PreemptiveBasicAuthScheme auth = new PreemptiveBasicAuthScheme();
        auth.setUserName(twilioAccountId);
        auth.setPassword(twilioPassword);
        return auth;
    }

    public static Response getMessages() {
        RequestSpecBuilder builder = new RequestSpecBuilder();
        //Prepare request
        builder.setBaseUri(baseUri);
        builder.setBasePath(twilioAccountId + "/Messages.json");
        builder.setAuth(setAuthScheme());
        builder.setContentType("application/json");
        builder.addFormParam("To", phoneReceiver);
        builder.addFormParam("From", phoneSender);
        RequestSpecification rSpec = builder.build();
        //Send request
        return HelperRest.sendGetRequest(rSpec);
    }

    public static ArrayList<String>getSidIds(){
        Gson gson = new Gson();
        ArrayList<String> sidList = new ArrayList<>();
        GetMessagesResponse response = gson.fromJson(getMessages().asString(), GetMessagesResponse.class);
        for (Message m : response.getMessages()){
            sidList.add(m.getSid());
        }
        return sidList;
    }

    public static String getOtpSignUp(){
        Gson gson = new Gson();

        GetMessagesResponse response = gson.fromJson(getMessages().asString(), GetMessagesResponse.class);

        String first = StringUtils.substringAfter(response.getMessages().get(0).getBody(), "Use this verification code ");
        String otp = StringUtils.substringBefore(first," to create REEF account.");
        return otp;
    }

    public static String getOtpLogin(){
        Gson gson = new Gson();

        GetMessagesResponse response = gson.fromJson(getMessages().asString(), GetMessagesResponse.class);

        String first = StringUtils.substringAfter(response.getMessages().get(0).getBody(), "Use this verification code ");
        String otp = StringUtils.substringBefore(first," to log in to the REEF Cloud.");
        return otp;
    }

    public static String getPhoneUpdateOtp(){
        Gson gson = new Gson();

        GetMessagesResponse response = gson.fromJson(getMessages().asString(), GetMessagesResponse.class);

        String first = StringUtils.substringAfter(response.getMessages().get(0).getBody(), "Use  this verification code ");
        String otp = StringUtils.substringBefore(first," to verify your new mobile phone number.");
        return otp;
    }



    public static Response deleteMessage(String sid) {
        RequestSpecBuilder builder = new RequestSpecBuilder();
        //Prepare request
        builder.setBaseUri(baseUri);
        builder.setBasePath(twilioAccountId + "/Messages/"+sid+".json");
        builder.setAuth(setAuthScheme());
        builder.addFormParam("To", phoneReceiver);
        builder.addFormParam("From", phoneSender);
        RequestSpecification rSpec = builder.build();
        //Send request
        return HelperRest.sendDeleteRequest(rSpec);
    }

    public static void deleteAllMessages(){
        for (String sid: getSidIds()){
            deleteMessage(sid);
        }
    }

    @Test
    public void execute() {
        deleteAllMessages();
        System.out.println(getSidIds());
    }
}

