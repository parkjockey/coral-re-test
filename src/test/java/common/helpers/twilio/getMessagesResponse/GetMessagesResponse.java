package common.helpers.twilio.getMessagesResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import lombok.Getter;

import java.util.List;

@Getter
public class GetMessagesResponse {
    @SerializedName("first_page_uri")
    @Expose
    private String firstPageUri;
    @SerializedName("end")
    @Expose
    private Integer end;
    @SerializedName("previous_page_uri")
    @Expose
    private Object previousPageUri;
    @SerializedName("messages")
    @Expose
    private List<Message> messages = null;
    @SerializedName("uri")
    @Expose
    private String uri;
    @SerializedName("page_size")
    @Expose
    private Integer pageSize;
    @SerializedName("start")
    @Expose
    private Integer start;
    @SerializedName("next_page_uri")
    @Expose
    private Object nextPageUri;
    @SerializedName("page")
    @Expose
    private Integer page;
}
