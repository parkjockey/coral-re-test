package common.helpers.twilio.getMessagesResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import lombok.Getter;

@Getter
public class Message {
    @SerializedName("body")
    @Expose
    private String body;
    @SerializedName("num_segments")
    @Expose
    private String numSegments;
    @SerializedName("direction")
    @Expose
    private String direction;
    @SerializedName("from")
    @Expose
    private String from;
    @SerializedName("date_updated")
    @Expose
    private String dateUpdated;
    @SerializedName("price")
    @Expose
    private String price;
    @SerializedName("error_message")
    @Expose
    private Object errorMessage;
    @SerializedName("uri")
    @Expose
    private String uri;
    @SerializedName("account_sid")
    @Expose
    private String accountSid;
    @SerializedName("num_media")
    @Expose
    private String numMedia;
    @SerializedName("to")
    @Expose
    private String to;
    @SerializedName("date_created")
    @Expose
    private String dateCreated;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("sid")
    @Expose
    private String sid;
    @SerializedName("date_sent")
    @Expose
    private String dateSent;
    @SerializedName("messaging_service_sid")
    @Expose
    private Object messagingServiceSid;
    @SerializedName("error_code")
    @Expose
    private Object errorCode;
    @SerializedName("price_unit")
    @Expose
    private String priceUnit;
    @SerializedName("api_version")
    @Expose
    private String apiVersion;
    @SerializedName("subresource_uris")
    @Expose
    private SubresourceUris subresourceUris;
}
