package common.helpers.twilio.getMessagesResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import lombok.Getter;

@Getter
public class SubresourceUris {
    @SerializedName("media")
    @Expose
    private String media;
    @SerializedName("feedback")
    @Expose
    private String feedback;
}
