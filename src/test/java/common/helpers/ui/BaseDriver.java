package common.helpers.ui;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.net.MalformedURLException;
import java.net.URL;

import static java.util.concurrent.TimeUnit.SECONDS;

public class  BaseDriver {

    public static RemoteWebDriver driver;


    /**
     * Method will initialize driver depends on desired operating system
     *
     * @param desiredDriver operating system or docker env, check switch statement
     * @return initialized driver
     * @throws MalformedURLException
     * @author dino.racd
     */
    public static void initializeDriver(String desiredDriver) throws MalformedURLException {
        DesiredCapabilities dr;

        switch (desiredDriver) {

            case "headlessChrome":
                WebDriverManager.chromedriver().setup();
                driver = new ChromeDriver(chromeOptions(true));
                driver.manage().timeouts().implicitlyWait(5, SECONDS);
                break;

            case "headlessFirefox":
                WebDriverManager.firefoxdriver().setup();
                driver = new FirefoxDriver(firefoxOptions(true));
                driver.manage().timeouts().implicitlyWait(5, SECONDS);
                break;

            case "chrome":
                //  WebDriverManager.chromedriver().config().setForceDownload(true);
                WebDriverManager.chromedriver().setup();
                driver = new ChromeDriver(chromeOptions(false));
                driver.manage().timeouts().implicitlyWait(5, SECONDS);
                break;

            case "chromeGrid":
                //  WebDriverManager.chromedriver().config().setForceDownload(true);
                WebDriverManager.chromedriver().setup();
                driver = new RemoteWebDriver(new URL("https://selgrid.co.reefplatform.com/wd/hub"), chromeOptions(false));
                driver.manage().timeouts().implicitlyWait(5, SECONDS);
                break;

            case "firefox":
                WebDriverManager.firefoxdriver().setup();
                driver = new FirefoxDriver(firefoxOptions(false));
                driver.manage().timeouts().implicitlyWait(5, SECONDS);
                break;


            default:
                throw new IllegalStateException(
                        "Unexpected value: " + desiredDriver);
        }

    }

    public static ChromeOptions chromeOptions(boolean headless){
        ChromeOptions chromeOptions = new ChromeOptions();
        chromeOptions.setHeadless(headless);
        chromeOptions.addArguments("start-maximized");
        chromeOptions.addArguments("--disable-extensions");
        chromeOptions.addArguments("--disable-infobars");
        chromeOptions.addArguments("--ignore-certificate-errors");
   //     chromeOptions.addArguments("window-size=1920,1080");
        return  chromeOptions;
    }

    public static FirefoxOptions firefoxOptions(boolean headless){
        FirefoxOptions firefoxOptions = new FirefoxOptions();
        firefoxOptions.addArguments("start-maximized");
        firefoxOptions.addArguments("--disable-extensions");
        firefoxOptions.addArguments("--ignore-certificate-errors");
        firefoxOptions.addArguments("--disable-infobars");
        firefoxOptions.addArguments("--disable-gpu");
        firefoxOptions.setAcceptInsecureCerts(headless);
        return  firefoxOptions;
    }




}


