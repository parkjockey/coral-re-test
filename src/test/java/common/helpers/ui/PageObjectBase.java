package common.helpers.ui;

import io.qameta.allure.Allure;
import org.apache.commons.io.FileUtils;
import org.joda.time.DateTime;
import org.openqa.selenium.*;
import org.openqa.selenium.html5.LocalStorage;
import org.openqa.selenium.html5.WebStorage;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.awt.*;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.concurrent.TimeUnit;

import static org.awaitility.Awaitility.await;


public class PageObjectBase extends BaseDriver {
    public static WebDriverWait wait;
    private static String screenshotPath = "build/allure-results/";
    private static JavascriptExecutor js = (JavascriptExecutor) driver;

    /**
     * Method will take screenshot
     *
     * @param scenarioName
     * @throws IOException
     * @author dino.rac
     */
    public static void takeScreenshot(String scenarioName) throws IOException {
        File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
        FileUtils.copyFile(scrFile, new File(screenshotPath + scenarioName + ".png"));
    }

    /**
     * Method will add screenshot to allure report
     *
     * @param scenarioName
     * @throws IOException
     */
    public static void addScreenshotAllure(String scenarioName) throws IOException {
        Path content = Paths.get(screenshotPath + scenarioName + ".png");
        try (InputStream is = Files.newInputStream(content)) {
            Allure.addAttachment(scenarioName, is);
        }
    }

    /**
     * Wait for element to be clickable
     *
     * @param element
     * @return WebElement after wait
     * @author dino.rac
     */
    public static WebElement waitToBeClickable(WebElement element) {
        wait = new WebDriverWait(driver, 15);
        return wait.until(ExpectedConditions.elementToBeClickable(element));
    }

    /**
     * Wait to finish page loanding after JavaScript executor is finished
     *
     * @param waitTime
     * @author dino.rac
     */
    public static void waitForLoad(long waitTime) {
        ExpectedCondition<Boolean> expectation = new
                ExpectedCondition<Boolean>() {
                    public Boolean apply(WebDriver driver) {
                        return ((JavascriptExecutor) driver).executeScript("return document.readyState").toString().equals("complete");
                    }
                };
        try {
            Thread.sleep(2000);
            WebDriverWait wait = new WebDriverWait(driver, waitTime);
            wait.until(expectation);
        } catch (Throwable error) {
        }
    }


    /**
     * Method will try to find clickable element 3 times
     *
     * @param by
     * @return WebElement
     * @author dino.rac
     */
    public static WebElement retryFindElement(By by) {
        WebElement el = null;
        int attempts = 0;
        while (attempts < 6) {
            try {
                el = waitToBeClickable(driver.findElement(by));
                if (el != null) {
                    break;
                }
            } catch (org.openqa.selenium.StaleElementReferenceException e) {
                el = waitToBeClickable(driver.findElement(by));
            }
            attempts++;
        }
        return el;
    }

    /**
     * Method will return vale from local storage when key is inserted
     *
     * @param key
     * @return String with value for specific key
     * @author dino.rac
     */
    public static String getItemFromLocalStorage(String key) {
        return (String) js.executeScript(String.format(
                "return localStorage.getItem('%s');", key));
    }


    /**
     * Method will perform Click on Element via JavaScript executor
     *
     * @param element
     * @author dino.rac
     */
    public static void performClick(WebElement element) {
        WebElement ele = element;
        JavascriptExecutor executor = (JavascriptExecutor) driver;
        executor.executeScript("arguments[0].click();", ele);
    }


    /**
     * Method perform scroll down via JavaScript executor
     *
     * @param scrollableElement, waitBeforeNextScroll, scrollTimes
     * @author Vladimir Papuga
     */
    public static void performScrollDown(WebElement scrollableElement, int waitBeforeNextScroll, int scrollTimes) {
        WebElement div = scrollableElement;
        JavascriptExecutor jse = (JavascriptExecutor) driver;
        int i = 1;
        while (i <= scrollTimes) {
            jse.executeScript("arguments[0].scrollBy(0,document.body.scrollHeight)", div, waitBeforeNextScroll, scrollTimes);
            waitForLoad(waitBeforeNextScroll);
            i++;
        }
    }

    /**
     * Method wait to loader disappear
     *
     * @author Vladimir Papuga
     */
    public static void waitToLoaderDisappear() {
        new WebDriverWait(driver, 20).until(ExpectedConditions.invisibilityOfAllElements(driver.findElements(By.xpath("//img[contains(@src,'/static/media/loader_normal.9232c5a4.svg')]"))));
    }


    /**
     * Wait for 5 seconds that condition become true
     * @param condition desired condition
     * @author dino.rac
     */
    public static void waitToBeTrue(Boolean condition) {
        waitSpecificTimeToBeTrue(5, condition);
    }

    /**
     * Wait for 5 seconds that condition become true
     * @param condition desired condition
     * @param time waitng period
     * @author dino.rac
     */
    public static void waitSpecificTimeToBeTrue(Integer time, Boolean condition) {
        await("Wait condition to be true")
                .atMost(time, TimeUnit.SECONDS).until(() -> condition);
    }

    /**
     * Method wait to toastify disappear
     *
     * @author Vladimir Papuga
     */
    public static void waitToToastifyDisappear() {
        new WebDriverWait(driver, 20).until(ExpectedConditions.invisibilityOfAllElements(driver.findElements(By.xpath("//div[@class='Toastify__toast Toastify__toast--default']"))));
    }

    /**
     * Method is Element Present
     *
     * @author Vladimir Papuga
     */
    public static boolean isElementPresent(By selector) {
        try {
            driver.findElement(selector);
            return true;
        } catch (NoSuchElementException e) {
            return false;
        }
    }

    /**
     * Method will perform Clear on input field
     *
     * @param element
     * @author Vladimir Papuga
     */
    public static void performClear(WebElement element) {
        WebElement ele = element;
        ele.sendKeys(Keys.CONTROL + "A" + Keys.BACK_SPACE);

    }

    /**
     * Method will perform mouse hover on element
     *
     * @param element
     * @author Vladimir Papuga
     */
    public static void performMouseHover(WebElement element) {
        Actions actions = new Actions(driver);
        WebElement ele = element;
        actions.moveToElement(ele);

    }

    /**
     * Method will handle OS side for upload file
     *
     * @param
     * @author Vladimir Papuga
     */
    public static void uploadFileOSSide(String picturePath) throws AWTException {
        //put path to your image in a clipboard
        StringSelection ss = new StringSelection(picturePath);
        Toolkit.getDefaultToolkit().getSystemClipboard().setContents(ss, null);
        //imitate mouse events like ENTER, CTRL+C, CTRL+V
        Robot robot = new Robot();
        robot.keyPress(KeyEvent.VK_ENTER);
        robot.keyRelease(KeyEvent.VK_ENTER);
        robot.keyPress(KeyEvent.VK_CONTROL);
        robot.keyPress(KeyEvent.VK_V);
        robot.keyRelease(KeyEvent.VK_V);
        robot.keyRelease(KeyEvent.VK_CONTROL);
        robot.keyPress(KeyEvent.VK_ENTER);
        robot.keyRelease(KeyEvent.VK_ENTER);
    }

    public static void addTokenToLocalStorage(String rcAuthValue){
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("localStorage.setItem(arguments[0],arguments[1])", "rc_auth", rcAuthValue);
        LocalStorage local = ((WebStorage) driver).getLocalStorage();

        local.setItem("rc_auth", rcAuthValue);
        local.setItem("organizationId", "1");

        System.out.println("Local storage: " +local.getItem("rc_auth"));
    }


    /**
     * Method will return previous month in format MMM
     *
     * @author Vladimir Papuga
     */
    public static String previousMonthMMM() {
        DateTime date = DateTime.now();
        DateTime previousMonth = date.minusMonths(1);
        String month = previousMonth.toString("MMM");
        return month;
    }

    /**
     * Method will return current month in format MMM
     *
     * @author Vladimir Papuga
     */
    public static String currentMonthMMM() {
        DateTime date = DateTime.now();
        String month = date.toString("MMM");
        return month;
    }

    /**
     * Method will return current day in format d
     *
     * @author Vladimir Papuga
     */
    public static String getCurrentDay() {
        DateTime date = DateTime.now();
        String day = date.toString("d");
        return day;
    }

    /**
     * Method will perform Double click
     *
     * @param element
     * @author Vladimir Papuga
     */
    public static void performDoubleClick(WebElement element) {
        int i = 0;
        for(i=0;i<2;i++){
        waitToBeClickable(element).click();
        }
    }

    /**
     * Method will convert RGBA color format to Hexadecimal color format
     *
     * @param element
     * @author Vladimir Papuga
     */
    public static String rgbaToHexColor(WebElement element) {
        String color = element.getCssValue("color");
        String[] hexValue = color.replace("rgba(", "").replace(")", "").split(",");
        hexValue[0] = hexValue[0].trim();
        int hexValue1 = Integer.parseInt(hexValue[0]);
        hexValue[1] = hexValue[1].trim();
        int hexValue2 = Integer.parseInt(hexValue[1]);
        hexValue[2] = hexValue[2].trim();
        int hexValue3 = Integer.parseInt(hexValue[2]);
        String actualColor = String.format("#%02x%02x%02x", hexValue1, hexValue2, hexValue3);
        return actualColor.toUpperCase();
        }


}



