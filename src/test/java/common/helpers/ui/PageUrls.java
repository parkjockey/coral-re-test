package common.helpers.ui;

/**
 * Class will be used for storing IAM page urls since another team is side bar owner
 */
public class PageUrls {

    public enum Pages {
        HOME("/identity-access/home"),
        USERS("/identity-access/organizations/users?page=0&size=20"),
        ROLES("/identity-access/roles"),
        ORGANIZATIONS("/identity-access/organizations?page=0&size=20");

        public final String path;

        private Pages(String path) {
            this.path = path;
        }
    }
}
