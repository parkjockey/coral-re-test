package common.responses;

import io.restassured.response.Response;

public class ErrorMessageResponse {

    public static String getErrorMessage(Response response){
        return response.getBody().jsonPath().get("messageKey");
    }

    public static String getValidationMessage(Response response, Integer position){
        return response.getBody().jsonPath().get("validationErrors["+position+"].messageKey");
    }
}
