package common.steps;


import common.helpers.rest.HelperRest;
import common.helpers.testrail.TestRailHelpers;
import common.responses.ErrorMessageResponse;
import data_containers.GeneralData;
import graphql.responses.ErrorResponses;
import io.cucumber.java8.En;
import org.assertj.core.api.SoftAssertions;
import org.junit.Assert;
import runners.CucumberRunner;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

public class CommonSteps implements En {

    public GeneralData generalData;
    public SoftAssertions assertions;


    public CommonSteps(GeneralData generalData) {
        this.generalData = generalData;
        assertions = new SoftAssertions();

        Then("Response status code is {int}", (Integer statusCode) -> {
            assertThat("Response has status code ", generalData.response.getStatusCode(), is(statusCode));
        });

        When("Invalid method {string} is sent", (String method) -> {
           generalData.response = HelperRest.sendRequest(generalData.rSpec, method);
        });

        Then("Response contains error message {string}", (String errorMessage) -> {
            Assert.assertThat("Error message is shown", ErrorMessageResponse.getErrorMessage(generalData.response), is(errorMessage));
        });

        Given("Test with id {int} added to Testrail run", (Integer caseId)  -> {
            if (CucumberRunner.TEST_RAIL_FLAG) {
                generalData.caseId = caseId;
                TestRailHelpers.updateRun(caseId);
                TestRailHelpers.addResult("Test", caseId);
            }

        });

        When("login requirements request is sent for non-sso user", () ->{

        });

        Then("Response contains validation error messages: {string}, {string}", (String firstMessage, String secondMessage) -> {
            ArrayList <String> expectedErrors = new ArrayList<>();
            expectedErrors.add(firstMessage);
            expectedErrors.add(secondMessage);
            List<String> sortedEE = expectedErrors.stream().sorted().collect(Collectors.toList());

            ArrayList<String> actualErrors = new ArrayList<>();
            actualErrors.add(ErrorMessageResponse.getValidationMessage(generalData.response, 0));
            actualErrors.add(ErrorMessageResponse.getValidationMessage(generalData.response, 1));
            List<String> sortedAE = actualErrors.stream().sorted().collect(Collectors.toList());

            sortedEE.get(0).equals(sortedAE.get(0));
            sortedEE.get(1).equals(sortedAE.get(1));
        });

        Then("Response contains validation error message: {string}", (String errorMessage) -> {
            Assert.assertThat("Error message is shown", ErrorMessageResponse.getValidationMessage(generalData.response, 0),
                    is(errorMessage));
        });

        Then("Response contains error messageKey {string}", (String messageKey) -> {
            assertions.assertThat(ErrorResponses.getErrorMessageKey(generalData.response)).isEqualTo(messageKey);
            assertions.assertAll();
        });

        And("Response contains error messageKeys {string}", (String messageKey) -> {
            assertions.assertThat(ErrorResponses.getErrorMessageKeys(generalData.response)).isEqualTo(messageKey);
            assertions.assertAll();
        });

    }
}
