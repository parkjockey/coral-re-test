package common.steps;

import common.helpers.aws_helpers.cognito.CognitoHelpers;
import common.helpers.aws_helpers.sqs.SQSHelpers;
import common.helpers.db_helpers.CQLQueries;
import common.helpers.db_helpers.JdbcHelpers;
import common.helpers.db_helpers.SQLQueries;
import common.helpers.rest.TokenProvider;
import common.helpers.testrail.TestRailHelpers;
import common.helpers.twilio.TwilioHelpers;
import common.helpers.ui.BaseDriver;
import common.helpers.ui.PageObjectBase;
import data_containers.GeneralData;
import data_containers.UserData;
import data_containers.hmt.LocationData;
import io.cucumber.core.api.Scenario;
import io.cucumber.java.After;
import io.cucumber.java.AfterStep;
import io.cucumber.java.Before;

import java.io.IOException;


public class Hooks {

    private GeneralData generalData;
    private UserData userData;
    private LocationData locationData;

    public Hooks(GeneralData generalData, UserData userData, LocationData locationData) {
        this.generalData = generalData;
        this.userData = userData;
        this.locationData = locationData;
    }

    @Before
    public void beforeTest(Scenario scenario) throws IOException {
        generalData.adNoSSOUsername = "reefuato@gmail.com";
       if(generalData.envName.contains("dev")) generalData.adSSOUsername = "global-reef-admin@devreeftechnology.onmicrosoft.com";
        if(generalData.envName.contains("qa")) generalData.adSSOUsername = "global-reef-admin@uatreeftechnology.onmicrosoft.com";
        generalData.adPassword = "Test1234!";
        //Selenium Driver will be initialized only for scenarios with @ui tag
        if (scenario.getSourceTagNames().contains("@ui")) {
            BaseDriver.initializeDriver(generalData.driverType);
            BaseDriver.driver.get(generalData.loginWebApp+generalData.loginRedirectUrl);
            PageObjectBase.waitForLoad(2);
        }
    }

    @AfterStep
    public void afterMethod(Scenario scenario) throws IOException {
        if (scenario.isFailed() && (generalData.caseId != null)) {
            TestRailHelpers.addStatusForCase(5, generalData.caseId);

        } else {
            if (generalData.caseId != null) {
                TestRailHelpers.addStatusForCase(1, generalData.caseId);
            }
        }

    }

    @After()
    public void afterTest(Scenario scenario) throws Exception {
        if (scenario.isFailed() && PageObjectBase.driver != null) {
            PageObjectBase.takeScreenshot(scenario.getName());
            PageObjectBase.addScreenshotAllure(scenario.getName());
            PageObjectBase.driver.manage().deleteAllCookies();
            PageObjectBase.driver.quit();
        }

        if (PageObjectBase.driver != null && scenario.getSourceTagNames().contains("@ui")) {
            PageObjectBase.driver.quit();
        }

        if(scenario.getSourceTagNames().contains("@twilio")) {
           TwilioHelpers.deleteAllMessages();
         if (!scenario.getSourceTagNames().contains("@p#signup")) {
             String username = TokenProvider.getUsernameClaim(userData.token);
             CognitoHelpers.removeUser(username);
             for (String s : SQLQueries.deleteConsumer(username)) {
                 JdbcHelpers.executeUpdate(s);
             }
         }
         //delete sqs message
            SQSHelpers.deleteAllMessages();
        }

        if(scenario.getSourceTagNames().contains("@hmt") && locationData.realEstateId != null) {
            //delete location after test
            CQLQueries.deleteLocation(locationData.realEstateId);
        }

    }


}

