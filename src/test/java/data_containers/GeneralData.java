package data_containers;

import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class GeneralData {
    public Response response;
    public static String envName;
    public RequestSpecification rSpec;
    public Integer caseId;
    public static String driverType;
    public static String reefWebApp;
    public static String adSSOUsername;
    public static String adNoSSOUsername;
    public static String adPassword;
    public static String token;
    public static String gqlBaseUri;
    public static String redirectUrl = "http://localhost/callback";
    public static String loginWebApp;
    public static String loginRedirectUrl;
    public static Boolean optimizelyBoolean;
    public static String restBaseUri;
    public static String countryCode = "us";
    public static String hmtGqlUri;
    public static String mediatorGql;
    public static String apiKey = "a4b7ef47-04ba-493b-8104-8679bedd1002";

}
