package data_containers;

import data_containers.org.OrganizationEntity;

import java.util.ArrayList;

public class OrganizationData {
    public static String name;
    public static String description;
    public static Integer id;
    public static ArrayList<OrganizationEntity> listOfOrganizations;
}

