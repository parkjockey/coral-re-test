package data_containers;

public class UserData {
    public static Integer id;
    public static String email;
    public static String firstName;
    public static String lastName;
    public static String password = "Test1234!";
    public static String invitationCode;
    public static String token;
    public static String updatedFirstName;
    public static String updatedLastName;
    public static String updatedPassword;
    public static String session;
    public static String otpCode;
    public static String userName;
    public static String phone;
    public static String code;
    public static String updatedEmail;
    public static String updatedPhoneNumber;
    public static String invalidEmail;
    public static String invalidPhoneNumber;
}
