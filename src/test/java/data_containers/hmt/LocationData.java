package data_containers.hmt;

public class LocationData {
    public String realEstateId;
    public String labels;
    public Integer identity;
    public String fullAddress;
    public String locationName;
    public String city;
}
