package graphql.helpers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.gson.Gson;
import com.vimalselvam.graphql.GraphqlTemplate;
import common.helpers.environment.Env;
import common.helpers.environment.YamlReader;
import common.helpers.graph_api.MSGraphAPI;
import common.helpers.rest.GraphQlCoreHelpers;
import common.helpers.rest.HelperRest;
import common.helpers.testrail.TestRailHelpers;
import common.helpers.ui.BaseDriver;
import common.helpers.ui.PageObjectBase;
import data_containers.GeneralData;
import data_containers.OrganizationData;
import data_containers.RoleData;
import data_containers.org.OrganizationEntity;
import graphql.variables.GetAllOrganizationsV2Variables;
import graphql.variables.UpdateOrganizationVariables;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import ui.page_object.LoginPage;
import ui.page_object.MicrosoftLoginPage;
import ui.page_object.SideBar;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class GraphQLHelpers {

    public static File organizationUsersPageFile = new File("src/test/resources/graphql/organizationUsersPage.graphql");
    static File getOrgFile = new File("src/test/resources/graphql/findOrganizationById.graphql");
    static File addOrgFile = new File("src/test/resources/graphql/addOrganization.graphql");
    static File getAllOrgsFile = new File("src/test/resources/graphql/findAllOrganizations.graphql");
    static File getOrgUserRolesFile = new File("src/test/resources/graphql/organizationUserRolesPage.graphql");
    static File inviteUsersFile = new File("src/test/resources/graphql/inviteUsers.graphql");
    static File getUserByEmailFile = new File("src/test/resources/graphql/findOrganizationUserByEmail.graphql");
    static File addOrgRoleFile = new File("src/test/resources/graphql/addOrganizationRole.graphql");
    static File updateOrgRoleFile = new File("src/test/resources/graphql/updateOrganizationRole.graphql");
    static File deleteOrgRoleFile = new File("src/test/resources/graphql/deleteOrganizationRole.graphql");
    static File getOrgRolesFile = new File("src/test/resources/graphql/organizationRolesPage.graphql");
    static File assignRolesFile = new File("src/test/resources/graphql/assignRoles.graphql");
    static File organizationUserData = new File("src/test/resources/graphql/organizationUserData.graphql");
    static File loginWithUsernameAndPasswordFile = new File("src/test/resources/graphql/authentication.graphql");
    static File findOrganizationByNameFile = new File("src/test/resources/graphql/findOrganizationName.graphql");
    static File updateOrgFile = new File("src/test/resources/graphql/updateOrganization.graphql");


    static ObjectNode variables = new ObjectMapper().createObjectNode();
    static GeneralData generalData;
    static OrganizationData organizationData;
    static RoleData roleData;
    static Gson gson = new Gson();


    public GraphQLHelpers(GeneralData generalData, OrganizationData organizationData, RoleData roleData) {
        this.generalData = generalData;
        this.organizationData = organizationData;
        this.roleData = roleData;
    }


    private static Response buildAndSendRequest(String graphqlPayload) {

        RequestSpecBuilder builder = new RequestSpecBuilder();

        //Prepare request
        builder.setBaseUri(generalData.gqlBaseUri);
        builder.setBasePath("/authorization/graphql");
        builder.setContentType("application/json");
        builder.setBody(graphqlPayload);
        builder.addHeader("Authorization", generalData.token);
        RequestSpecification rSpec = builder.build();

        return HelperRest.sendPostRequest(rSpec);
    }

    private static String GetUserToken() throws InterruptedException, IOException, ParseException {

        TestRailHelpers.createTestRailInstance();
        TestRailHelpers.setProjectSuiteId("AAA-automation", "Automation");
        TestRailHelpers.createRun();

        generalData.envName = System.getProperty("env");
        if (generalData.envName == null || generalData.envName.isEmpty()) generalData.envName = "dev";

        generalData.driverType = System.getProperty("driver");
        if (generalData.driverType == null || generalData.driverType.isEmpty()) generalData.driverType = "chrome";

        Env testEnvironment = YamlReader.getEnvironmentFromYaml(generalData.envName);
        generalData.reefWebApp = testEnvironment.reefWebApp;
        generalData.gqlBaseUri = testEnvironment.gqlBaseUri;

        HashMap<String, String> adUser = MSGraphAPI.createADUser(MSGraphAPI.getADToken());
        generalData.adSSOUsername = adUser.get("mail");
        generalData.adPassword = adUser.get("password");
        //We must wait to Microsoft process all created data
        Thread.sleep(7000);

        BaseDriver.initializeDriver(generalData.driverType);
        BaseDriver.driver.get(generalData.reefWebApp);
        PageObjectBase.waitForLoad(5);
        LoginPage.getMicrosoftButton().click();
        MicrosoftLoginPage.loginMicrosoftAccount(generalData.adSSOUsername, generalData.adPassword);
        SideBar.getSideBar().isDisplayed();
        generalData.token = PageObjectBase.getItemFromLocalStorage("accessToken");
        BaseDriver.driver.quit();

        return generalData.token;
    }

    public static Response getOrganizationById(int id) throws IOException {

        variables.removeAll();
        variables.put("id", id);

        String graphqlPayload = GraphqlTemplate.parseGraphql(getOrgFile, variables);

        return buildAndSendRequest(graphqlPayload);
    }

    public static Response GetAllOrganizationsRequest() throws IOException {

        variables.removeAll();
        variables.put("pageSize", 1000);
        String graphqlPayload = GraphqlTemplate.parseGraphql(getAllOrgsFile, variables);

        return buildAndSendRequest(graphqlPayload);
    }

    public static Response addOrganization(String name, String description) throws IOException {

        variables.removeAll();
        variables.put("name", name);
        variables.put("description", description);

        String graphqlPayload = GraphqlTemplate.parseGraphql(addOrgFile, variables);

        return buildAndSendRequest(graphqlPayload);
    }

    public static Response GetOrganizationUsers(Integer organizationId, String email, Integer pageSize, Integer pageNumber) throws IOException {

        variables.removeAll();

        if (organizationId == null) {
            variables.put("organizationId", 1);
        } else variables.put("organizationId", organizationId);
        if (email == null) {
            variables.put("email", "");
        } else variables.put("email", email);
        if (pageSize == null) {
            variables.put("pageSize", 20);
        } else variables.put("pageSize", pageSize);
        if (pageNumber == null) {
            variables.put("pageNumber", 0);
        } else variables.put("pageNumber", pageNumber);


//        if(organizationId == null){
//            variables.put("organizationId",1);
//        } else variables.put("organizationId", organizationId == null ? 1 : organizationId);
//        if(pageSize == null){
//            variables.put("pageSize", 5);
//        } else variables.put("pageSize", pageSize == null ? 5 : pageSize);

        String graphqlPayload = GraphqlTemplate.parseGraphql(organizationUsersPageFile, variables);

        return buildAndSendRequest(graphqlPayload);
    }

    public static Response GetOrganizationUserRoles(Integer organizationId, Integer userId) throws IOException {

        variables.removeAll();
        variables.put("organizationId", organizationId == null ? 1 : organizationId);
        variables.put("userId", userId == null ? 11 : userId);

        String graphqlPayload = GraphqlTemplate.parseGraphql(getOrgUserRolesFile, variables);

        return buildAndSendRequest(graphqlPayload);
    }

    public static Response GenericRequest(String graphqlPayload) throws InterruptedException, ParseException, IOException {
        RequestSpecBuilder builder = new RequestSpecBuilder();

        String token = GetUserToken();

        //Prepare request
        builder.setBaseUri(generalData.gqlBaseUri);
        builder.setBasePath("/authorization/graphql");
        builder.setContentType("application/json");
        builder.setBody(graphqlPayload);
        builder.addHeader("Authorization", token);

        return buildAndSendRequest(graphqlPayload);
    }

    public static Response GetOrganizationById401() throws IOException, ParseException, InterruptedException {

        variables.removeAll();
        variables.put("id", 1);

        String graphqlPayload = GraphqlTemplate.parseGraphql(addOrgFile, variables);

        return GenericRequest(graphqlPayload);
    }

    public static ArrayList<String> createOrganizations(int numberOfNewOrganizations) throws IOException {

        ArrayList<String> newOrgNames = new ArrayList<>();

        for (int i = 0; i < numberOfNewOrganizations; i++) {

            newOrgNames.add(
                    addOrganization("Test " + HelperRest.getRandomNumber(), "Desc " + HelperRest.getRandomNumber())
                            .jsonPath().get("data.addOrganization.name").toString());
        }
        return newOrgNames;
    }

    public static Integer getCreatedOrganizationsId() throws IOException {

        return Integer.parseInt(GraphQLHelpers.addOrganization("Test " + HelperRest.getRandomNumber(), "Desc " + HelperRest.getRandomNumber())
                .jsonPath().get("data.addOrganization.id"));

    }


    public static Response addRoleToOrganization() throws IOException {

        roleData.roleName = "TestRole" + HelperRest.getRandomNumber();
        roleData.roleDescription = "TestDesc" + HelperRest.getRandomNumber();

        variables.removeAll();
        variables.put("organizationId", organizationData.id);
        variables.put("name", roleData.roleName);
        variables.put("description", roleData.roleDescription);

        String graphqlPayload = GraphqlTemplate.parseGraphql(addOrgRoleFile, variables);

        return buildAndSendRequest(graphqlPayload);
    }

    public static Response updateOrganizationRole() throws IOException {

        roleData.roleName = "TestRole" + HelperRest.getRandomNumber();
        roleData.roleDescription = "TestDesc" + HelperRest.getRandomNumber();

        variables.removeAll();
        variables.put("organizationId", organizationData.id);
        variables.put("roleId", roleData.roleId);
        variables.put("name", roleData.roleName);
        variables.put("description", roleData.roleDescription);

        String graphqlPayload = GraphqlTemplate.parseGraphql(updateOrgRoleFile, variables);

        return buildAndSendRequest(graphqlPayload);
    }

    public static Response deleteOrganizationRole() throws IOException {

        variables.removeAll();
        variables.put("organizationId", organizationData.id);
        variables.put("roleId", roleData.roleId);

        String graphqlPayload = GraphqlTemplate.parseGraphql(deleteOrgRoleFile, variables);

        return buildAndSendRequest(graphqlPayload);
    }

    public static Response getOrganizationRoles() throws IOException {

        variables.removeAll();
        variables.put("organizationId", organizationData.id);

        String graphqlPayload = GraphqlTemplate.parseGraphql(getOrgRolesFile, variables);

        return buildAndSendRequest(graphqlPayload);
    }

    public static List<Object> getOrganizationRoleIds() throws IOException {

        variables.removeAll();
        variables.put("organizationId", organizationData.id);

        String graphqlPayload = GraphqlTemplate.parseGraphql(getOrgRolesFile, variables);

        buildAndSendRequest(graphqlPayload);

        return generalData.response.jsonPath().getList("data.organizationRolesPage.content.id");
    }

    public static Response getOrganizationUserData(String baseUri, String token, String organizationId) throws IOException {

        String graphqlPayload = GraphQlCoreHelpers.parseGraphql(organizationUserData, "{\"organizationId\":\"" + organizationId + "\"}");

        return GraphQlCoreHelpers.sendRequestWithToken(baseUri, token, graphqlPayload);
    }

    public static Response findOrganizationByName(String token, String organizationName) throws IOException {
        String graphqlPayload = GraphQlCoreHelpers.parseGraphql(findOrganizationByNameFile, "{\"name\":\"" + organizationName + "\"}");

        return GraphQlCoreHelpers.sendRequestWithToken(generalData.gqlBaseUri, token, graphqlPayload);
    }

    public static Response updateOrganization(String baseUri, String token,
                                              String orgId, String orgName, String orgDesc) throws IOException {
        UpdateOrganizationVariables variables = new UpdateOrganizationVariables();
        variables.setId(orgId);
        variables.setName(orgName);
        variables.setDescription(orgDesc);

        String graphqlPayload = GraphQlCoreHelpers.parseGraphql(updateOrgFile, gson.toJson(variables));

        return GraphQlCoreHelpers.sendRequestWithToken(baseUri, token, graphqlPayload);
    }

    public static Response findAllOrganizationV2(String token, String organizationName, Integer pageNum, Integer pageSize) throws IOException {
        GetAllOrganizationsV2Variables variables = new GetAllOrganizationsV2Variables();
        variables.setName(organizationName);
        variables.setPageNumber(pageNum != null ? pageNum : 0);
        variables.setPageSize(pageSize != null ? pageSize : 40);

        String graphqlPayload = GraphQlCoreHelpers.parseGraphql(getAllOrgsFile, gson.toJson(variables));
        return GraphQlCoreHelpers.sendRequestWithToken(generalData.gqlBaseUri, token, graphqlPayload);
    }


    public static HashMap<String, String> createRandomOrganization() throws IOException {
        HashMap<String, String> orgData = new HashMap<>();
        orgData.put("name", "AutoOrg" + HelperRest.getRandomNumber());
        orgData.put("description", "Desc" + HelperRest.getRandomNumber());
        orgData.put("id", GraphQLHelpers.addOrganization(orgData.get("name"), orgData.get("description"))
                .jsonPath().get("data.addOrganization.id"));
        return orgData;

    }

    public static ArrayList<OrganizationEntity> createRandomOrgs(Integer numb) throws IOException {
        ArrayList<OrganizationEntity> list = new ArrayList<>();
        for (int i = 0; i < numb; i++) {
            OrganizationEntity org = new OrganizationEntity();
            org.name = "AutoOrg" + HelperRest.getRandomNumber();
            org.description = "Desc" + HelperRest.getRandomNumber();
            String id = GraphQLHelpers.sendAddOrgRequest(org.name, org.description)
                    .jsonPath().get("data.addOrganization.id");
            org.id = Integer.parseInt(id);
            list.add(org);
        }


        return list;
    }

    public static Response sendAddOrgRequest(String name, String description) throws IOException {

        variables.removeAll();
        variables.put("name", name);
        variables.put("description", description);

        String graphqlPayload = GraphqlTemplate.parseGraphql(addOrgFile, variables);

        RequestSpecBuilder builder = new RequestSpecBuilder();

        //Prepare request
        builder.setBaseUri(generalData.gqlBaseUri);
        builder.setBasePath("/authorization/graphql");
        builder.setContentType("application/json");
        builder.setBody(graphqlPayload);
        builder.addHeader("Authorization", generalData.token);
        RequestSpecification rSpec = builder.build();

        return HelperRest.sendPostRequest(rSpec);
    }


}

