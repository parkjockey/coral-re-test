package graphql.helpers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.gson.Gson;
import com.vimalselvam.graphql.GraphqlTemplate;
import common.helpers.rest.GraphQlCoreHelpers;
import common.helpers.rest.HelperRest;
import data_containers.GeneralData;
import data_containers.UserData;
import graphql.variables.AssignRolesVariables;
import graphql.variables.invite_users.InviteUserVariables;
import graphql.variables.invite_users.UserVariablres;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static graphql.helpers.GraphQLHelpers.*;

public class InviteUsersHelprs {

    static GeneralData generalData;
    static UserData userData;

    static ObjectMapper mapper = new ObjectMapper();
    static ObjectNode variables = new ObjectMapper().createObjectNode();
    private static Gson gson = new Gson();
    static File organizationUserInviteDataFile = new File("src/test/resources/graphql/organizationUserInviteData.graphql");
    static File assignRolesFile = new File("src/test/resources/graphql/assignRoles.graphql");
    public InviteUsersHelprs(GeneralData generalData, UserData userData){
        this.generalData = generalData;
        this.userData = userData;
    }

    private static Response buildAndSendRequest(String graphqlPayload) {

        RequestSpecBuilder builder = new RequestSpecBuilder();

        //Prepare request
        builder.setBaseUri(generalData.gqlBaseUri);
        builder.setBasePath("/authorization/graphql");
        builder.setContentType("application/json");
        builder.setBody(graphqlPayload);
        builder.addHeader("Authorization", generalData.token);
        RequestSpecification rSpec = builder.build();

        return HelperRest.sendPostRequest(rSpec);
    }

    public static ArrayList<String> getInvitedUsersEmails(Integer numberOfUsers, Integer organizationId) throws IOException {

        ArrayNode usersList = mapper.createArrayNode();
        ArrayList<String> invitedUsersEmailList = new ArrayList<>();

        for (int i = 0; i < numberOfUsers; i++) {

            ObjectNode user = mapper.createObjectNode();
            userData.email = HelperRest.getRandomEmail();
            user.put("email", userData.email);
            usersList.add(user);
            invitedUsersEmailList.add(userData.email);
        }

        variables.removeAll();
        variables.put("usersList", usersList);
        variables.put("organizationId", organizationId);
        variables.put("roleId", roleData.roleId);

        String graphqlPayload = GraphqlTemplate.parseGraphql(inviteUsersFile, variables);

        buildAndSendRequest(graphqlPayload);

        return invitedUsersEmailList;
    }

    public static Response inviteUsers(Integer numberOfUsers, boolean assignRole) throws IOException {

        ArrayNode usersList = mapper.createArrayNode();

        for (int i = 0; i < numberOfUsers; i++) {

            ObjectNode user = mapper.createObjectNode();
            userData.email = HelperRest.getRandomEmail();
            user.put("email", userData.email);
            user.put("sourceId", "1");
            user.put("userSourceIdentifier", "Test");
            usersList.add(user);
        }

        variables.removeAll();
        variables.put("usersList", usersList);
        variables.put("organizationId", organizationData.id);
        if (assignRole) {
            variables.put("roleId", roleData.roleId);
        }

        String graphqlPayload = GraphqlTemplate.parseGraphql(inviteUsersFile, variables);

        return buildAndSendRequest(graphqlPayload);
    }

    public static Response assignRoles() throws IOException {

        variables.removeAll();
        variables.put("userId", userData.id);
        variables.put("organizationId", organizationData.id);

        ArrayNode rolesList = mapper.createArrayNode();
        rolesList.add(500);
        rolesList.add(501);
        variables.put("rolesList", rolesList);

        String graphqlPayload = GraphqlTemplate.parseGraphql(assignRolesFile, variables);

        return buildAndSendRequest(graphqlPayload);
    }

    public static Response assignRoles(Integer userId, Integer organizationId, List<Integer> roleIds) throws IOException {
        AssignRolesVariables variables = new AssignRolesVariables();
        variables.setUserId(userId);
        variables.setOrganizationId(organizationId);
        variables.rolesList = roleIds;

        String payload = GraphQlCoreHelpers.parseGraphql(assignRolesFile, gson.toJson(variables));

        return GraphQlCoreHelpers.sendRequestWithToken(generalData.gqlBaseUri, generalData.token, payload);
    }

    public static  Response reinviteUsers(ArrayList<String> userEmails, ArrayList<String> usernameList, Integer organizationId, Integer roleId) throws IOException {

        ArrayNode usersList = mapper.createArrayNode();

        for (int i = 0; i < userEmails.size(); i++) {

            ObjectNode user = mapper.createObjectNode();
            user.put("email", userEmails.get(i));
            user.put("username", usernameList.get(i));
            usersList.add(user);
        }

        variables.removeAll();
        variables.put("usersList", usersList);
        variables.put("organizationId", organizationId);
        variables.put("roleId", roleId);

        String graphqlPayload = GraphqlTemplate.parseGraphql(inviteUsersFile, variables);

        return buildAndSendRequest(graphqlPayload);
    }

    public static Response getUserByEmail(Integer organizationId, String email) throws IOException {

        variables.removeAll();
        variables.put("organizationId", organizationId);
        variables.put("email", email);

        String graphqlPayload = GraphqlTemplate.parseGraphql(getUserByEmailFile, variables);

        return buildAndSendRequest(graphqlPayload);
    }

    public static String getUsernameByEmail(Integer organizationId, String email) throws IOException {

        variables.removeAll();
        variables.put("organizationId", organizationId);
        variables.put("email", email);

        String graphqlPayload = GraphqlTemplate.parseGraphql(getUserByEmailFile, variables);

        //generalData.response = buildAndSendRequest(graphqlPayload);

        return  buildAndSendRequest(graphqlPayload).jsonPath().get("data.findOrganizationUserByEmail.username").toString();
    }

    public static Response inviteSpecificUsers(String baseUri, String token, ArrayList<UserVariablres>usersLists, Integer organizationId, Integer roleId) throws IOException {
        InviteUserVariables variables = new InviteUserVariables();
        variables.usersList = new ArrayList<>(usersLists);
        variables.setOrganizationId(organizationId);
        variables.setRoleId(roleId);

        String graphqlPayload = GraphQlCoreHelpers.parseGraphql(inviteUsersFile, gson.toJson(variables));

        return GraphQlCoreHelpers.sendRequestWithToken(baseUri, token, graphqlPayload);
    }


    public static Response organizationUserInviteData(String inviteCode) throws IOException {
        String graphqlPayload = GraphQlCoreHelpers.parseGraphql(organizationUserInviteDataFile, "{\"inviteCode\":\""+inviteCode+"\"}");

        return GraphQlCoreHelpers.sendRequestWithoutToken(generalData.gqlBaseUri, graphqlPayload);
    }

}
