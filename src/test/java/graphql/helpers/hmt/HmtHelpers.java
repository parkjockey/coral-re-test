package graphql.helpers.hmt;

import com.google.gson.Gson;
import common.helpers.rest.GraphQlCoreHelpers;
import data_containers.GeneralData;
import io.restassured.response.Response;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;

public class HmtHelpers {

    static File hierarchyRootLevelsFile = new File("src/test/resources/graphql/hmt/hierarchyRootLevels.graphql");

    public static Logger logger = LoggerFactory.getLogger(HmtHelpers.class);
    static GeneralData generalData;

    public HmtHelpers(GeneralData generalData) {
        this.generalData = generalData;
    }

    public static Response hierarchyRootLevels(String token, Integer orgId) throws IOException {
        HashMap<String, Integer> var = new HashMap<>();
        var.put("organizationId", orgId);

        String graphqlPayload = GraphQlCoreHelpers.parseGraphql(hierarchyRootLevelsFile, new Gson().toJson(var));

        return GraphQlCoreHelpers.sendRequestWithToken(generalData.hmtGqlUri, token, graphqlPayload);
    }
}
