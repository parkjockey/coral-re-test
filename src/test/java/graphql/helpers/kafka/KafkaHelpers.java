package graphql.helpers.kafka;

import com.google.gson.Gson;
import data_containers.GeneralData;
import io.confluent.kafka.serializers.KafkaAvroDeserializer;
import org.apache.avro.generic.GenericRecord;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.Duration;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Properties;

public class KafkaHelpers {
    private static final String KAFKA_URL = "- b-1.msk-dev-1.cq34ds.c4.kafka.us-east-2.amazonaws.com:9092";
    static Gson gson = new Gson();
    private static Logger logger = LoggerFactory.getLogger(KafkaHelpers.class);
    private static GeneralData generalData;

    public KafkaHelpers(GeneralData generalData) {
        this.generalData = generalData;
    }

    private static Properties kafkaProperties() {
        Properties props = new Properties();
        props.put("bootstrap.servers", KAFKA_URL);
        props.put("key.deserializer", StringDeserializer.class);
        props.put("value.deserializer", KafkaAvroDeserializer.class);
        props.put("schema.registry.url", "https://schema-registry-dev.co.reefplatform.com/");
        props.put(ConsumerConfig.GROUP_ID_CONFIG, "iamGroup1");
        return props;
    }


    public static ConsumerUserDataKafkaResponse consumeConsumerDataTopic(String username) {
        KafkaConsumer<String, GenericRecord> consumer = new KafkaConsumer<String, GenericRecord>(kafkaProperties());
        String topicName = null;
        ConsumerUserDataKafkaResponse response = null;
        //Subscribing to topic
        if (generalData.envName.contains("dev")) topicName = "dev-iam-consumer-data";
        if (generalData.envName.contains("qa")) topicName = "qa-iam-consumer-data";
        consumer.subscribe(Arrays.asList(topicName));
        logger.info("Successfully subscribed to iam-consumer-data topic");

        //polling message
        try {
            ConsumerRecords<String, GenericRecord> records = consumer.poll(Duration.ofMillis(20000));
            for (ConsumerRecord<String, GenericRecord> record : records) {
                logger.info("Your key: " + record.key() + ", Value:" + record.value());
                logger.info("Partition:" + record.partition() + ",Offset:" + record.offset());
                String s = record.value().toString();
                response = gson.fromJson(s, ConsumerUserDataKafkaResponse.class);
                if (response.getUsername().equals(username)) break;
            }
        } catch (NullPointerException e) {
            logger.info("Message isn't obtained");
        }

        consumer.close();
        return response;

    }

    public static GenericRecord consumeUserDataTopic(String username) {
        KafkaConsumer<Long, GenericRecord> consumerUser = new KafkaConsumer<>(kafkaProperties());
        String topicName = null;
        GenericRecord userRecord = null;
        //Subscribing to topic
        if (generalData.envName.contains("dev")) topicName = "dev-iam-user-data";
        if (generalData.envName.contains("qa")) topicName = "qa-iam-user-data";
        consumerUser.subscribe(Arrays.asList(topicName));
        logger.info("Successfully subscribed to iam-user-data topic");

        //polling message
        try {
            ConsumerRecords<Long, GenericRecord> records = consumerUser.poll(Duration.ofMillis(20000));
            for (ConsumerRecord<Long, GenericRecord> record : records) {
                logger.info("Your key: " + record.key() + ", Value:" + record.value());
                logger.info("Partition:" + record.partition() + ",Offset:" + record.offset());
                if (record.value().get("username").toString().contains(username)) {
                    userRecord = record.value();
                    break;
                }

            }
        }  catch (NullPointerException e){
            logger.info("Message isn't obtained");
        }
        consumerUser.close();
        return userRecord;

    }

    public static GenericRecord consumeNotificationTopic(String username) {
        KafkaConsumer<Long, GenericRecord> consumer = new KafkaConsumer<>(kafkaProperties());
        String topicName = null;
        GenericRecord userRecord = null;
        //Subscribing to topic
        if (generalData.envName.contains("dev")) topicName = "dev-re-notification-service";
        if (generalData.envName.contains("qa")) topicName = "qa-re-notification-service";
        consumer.subscribe(Arrays.asList(topicName));
        logger.info("Successfully subscribed to re-notification-service topic");

        //polling message
        try {

            ConsumerRecords<Long, GenericRecord> records = consumer.poll(Duration.ofMillis(20000));
            for (ConsumerRecord<Long, GenericRecord> record : records) {
                logger.info("Your key: " + record.key() + ", Value:" + record.value());
                logger.info("Partition:" + record.partition() + ",Offset:" + record.offset());
                if (record.value().get("receiverId").toString().contains(username)) {
                    userRecord = record.value();
                    break;
                }

            }
        }  catch (NullPointerException e){
            logger.info("Message isn't obtained");
        }
        consumer.close();

        return userRecord;

    }

    public static ArrayList<GenericRecord> pickSpecificMessageConsumeUserDataTopic(String username, Integer messageNumber) {
        KafkaConsumer<Long, GenericRecord> consumerUser = new KafkaConsumer<>(kafkaProperties());
        String topicName = null;
        GenericRecord userRecord = null;
        ArrayList<GenericRecord> list = new ArrayList<>();
        //Subscribing to topic
        if (generalData.envName.contains("dev")) topicName = "dev-iam-user-data";
        if (generalData.envName.contains("qa")) topicName = "qa-iam-user-data";
        consumerUser.subscribe(Arrays.asList(topicName));
        logger.info("Successfully subscribed to iam-user-data topic");

        //polling message
        try {
            ConsumerRecords<Long, GenericRecord> records = consumerUser.poll(Duration.ofMillis(20000));
            int i = 0;
            while ( i<messageNumber) {
                for (ConsumerRecord<Long, GenericRecord> record : records) {
                    logger.info("Your key: " + record.key() + ", Value:" + record.value());
                    logger.info("Partition:" + record.partition() + ",Offset:" + record.offset());
                    if (record.value().get("username").toString().contains(username)) {
                        list.add(record.value());
                        if (list.size()==messageNumber)
                        break;
                    }
                    i++;

                }
            }
        }  catch (NullPointerException e){
            logger.info("Message isn't obtained");
        }
        consumerUser.close();
        return list;

    }


}
