package graphql.helpers.organization;

import com.google.gson.Gson;
import common.helpers.rest.GraphQlCoreHelpers;
import data_containers.GeneralData;
import graphql.variables.FindOrganizationRoleByIdVariables;
import graphql.variables.FindOrganizationRoleNameVariables;
import io.restassured.response.Response;

import java.io.File;
import java.io.IOException;

public class OrganizationRoleHelpers {
    public static File findOrganizationRoleByIdFile = new File("src/test/resources/graphql/findOrganizationRoleById.graphql");
    public static File findOrganizationRoleNameFile = new File("src/test/resources/graphql/findOrganizationRoleName.graphql");
    static GeneralData generalData;
    static Gson gson = new Gson();

    public OrganizationRoleHelpers(GeneralData generalData) {
        this.generalData = generalData;
    }

    public static Response findOrganizationRoleById(String token, Integer organizationId, Integer roleId) throws IOException {
        FindOrganizationRoleByIdVariables variables = new FindOrganizationRoleByIdVariables();
        variables.setOrganizationId(organizationId);
        variables.setRoleId(roleId);

        String graphqlPayload = GraphQlCoreHelpers.parseGraphql(findOrganizationRoleByIdFile, gson.toJson(variables));

        return GraphQlCoreHelpers.sendRequestWithToken(generalData.gqlBaseUri, token, graphqlPayload);
    }

    public static Response findOrganizationRoleName(String token, Integer organizationId, String roleName) throws IOException {
        FindOrganizationRoleNameVariables variables = new FindOrganizationRoleNameVariables();
        variables.setOrganizationId(organizationId);
        variables.setName(roleName);

        String graphqlPayload = GraphQlCoreHelpers.parseGraphql(findOrganizationRoleNameFile, gson.toJson(variables));

        return GraphQlCoreHelpers.sendRequestWithToken(generalData.gqlBaseUri, token, graphqlPayload);
    }
}
