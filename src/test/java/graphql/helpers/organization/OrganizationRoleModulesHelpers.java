package graphql.helpers.organization;

import com.google.gson.Gson;
import common.helpers.rest.GraphQlCoreHelpers;
import data_containers.GeneralData;
import graphql.responses.organization.role_modules.Module;
import graphql.responses.organization.role_modules.RoleModulesResponse;
import graphql.responses.organization.role_modules.SubModules;
import graphql.variables.AssignModulesVariables;
import graphql.variables.FindAllModulesVariables;
import graphql.variables.OrganizationModulesVariables;
import graphql.variables.RoleModulesVariables;
import graphql.variables.update_org_role_modules.UpdateOrganizationRoleModulesVariables;
import io.restassured.response.Response;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

public class OrganizationRoleModulesHelpers {
    public static File roleModulesFile = new File("src/test/resources/graphql/organizationRoleModules.graphql");
    public static File updateModulesFile = new File("src/test/resources/graphql/updateOrganizationRoleModules.graphql");
    public static File findAllModulesFile = new File("src/test/resources/graphql/modulePages.graphql");
    public static File assignModulesFile = new File("src/test/resources/graphql/assignModules.graphql");
    public static File organizationModulesFile = new File("src/test/resources/graphql/organizationModules.graphql");
    static GeneralData generalData;
    static Gson gson = new Gson();

    public OrganizationRoleModulesHelpers(GeneralData generalData) {
        this.generalData = generalData;
    }

    public static Response roleModules(String token, Integer organizationId, Integer roleId) throws IOException {
        RoleModulesVariables variables = new RoleModulesVariables();
        variables.setOrganizationId(organizationId.toString());
        variables.setRoleId(roleId.toString());

        String graphqlPayload = GraphQlCoreHelpers.parseGraphql(roleModulesFile, gson.toJson(variables));

        return GraphQlCoreHelpers.sendRequestWithToken(generalData.gqlBaseUri, token, graphqlPayload);
    }

    public static ArrayList<String> getAllModulesIds(Response response) {
        RoleModulesResponse modulesResponse = gson.fromJson(response.asString(), RoleModulesResponse.class);
        ArrayList<String> idsList = new ArrayList<>();
        for (Module m : modulesResponse.getData().getOrganizationRoleModules().getModules()) {
            idsList.add(m.getId());
        }
        return idsList;
    }

    public static ArrayList<String> getAllModulesNames(Response response) {
        RoleModulesResponse modulesResponse = gson.fromJson(response.asString(), RoleModulesResponse.class);
        ArrayList<String> idsList = new ArrayList<>();
        for (Module m : modulesResponse.getData().getOrganizationRoleModules().getModules()) {
            idsList.add(m.getName());
        }
        return idsList;
    }

    public static ArrayList<SubModules> getSubmodulesListByModuleName(Response response, String moduleName) {
        RoleModulesResponse modulesResponse = gson.fromJson(response.asString(), RoleModulesResponse.class);
        ArrayList<SubModules> subModulesList = new ArrayList<>();
        for (Module m : modulesResponse.getData().getOrganizationRoleModules().getModules()) {
            if (moduleName.equals(m.getName())) {
                subModulesList = m.getSubModules();
            }
        }
        return subModulesList;
    }

    public static Module getModuleByName(Response response, String moduleName) {
        RoleModulesResponse modulesResponse = gson.fromJson(response.asString(), RoleModulesResponse.class);
        Module desiredModule = null;
        for (Module m : modulesResponse.getData().getOrganizationRoleModules().getModules()) {
            if (moduleName.equals(m.getName())) {
                desiredModule = m;
            }
        }
        return desiredModule;
    }

    public static Response updateRoleModules(String token, Integer organizationId, Integer roleId,
                                             ArrayList<graphql.variables.update_org_role_modules.Module> moduleList) throws IOException {
        UpdateOrganizationRoleModulesVariables variables = new UpdateOrganizationRoleModulesVariables();
        variables.setOrganizationId(organizationId);
        variables.setRoleId(roleId);
        variables.modules = new ArrayList<>(moduleList);

        String graphqlPayload = GraphQlCoreHelpers.parseGraphql(updateModulesFile, gson.toJson(variables));

        return GraphQlCoreHelpers.sendRequestWithToken(generalData.gqlBaseUri, token, graphqlPayload);
    }

    public static Response findAllModules(String token, String name, Integer pageNumber, Integer pageSize) throws IOException {
        FindAllModulesVariables variables = new FindAllModulesVariables();
        if (name != null) variables.setName(name);
        if (pageNumber != null){
            variables.setPageNumber(pageNumber);
        }else {
            variables.setPageNumber(0);
        }
        if (pageSize != null){
            variables.setPageSize(pageSize);
        }else {
            variables.setPageSize(100);
        }
        String graphqlPayload = GraphQlCoreHelpers.parseGraphql(findAllModulesFile, gson.toJson(variables));

        return GraphQlCoreHelpers.sendRequestWithToken(generalData.gqlBaseUri, token, graphqlPayload);
    }

    public static Response assignModulesToOrg(String token, ArrayList<Integer> modulesList, Integer orgId) throws IOException {
        AssignModulesVariables variables = new AssignModulesVariables();
        variables.modulesList = modulesList;
        variables.setOrganizationId(orgId);

        String graphqlPayload = GraphQlCoreHelpers.parseGraphql(assignModulesFile, gson.toJson(variables));

        return GraphQlCoreHelpers.sendRequestWithToken(generalData.gqlBaseUri, token, graphqlPayload);
    }

    public static Response organizationModules(String token, String organizationId, Integer pageNumber, Integer pageSize) throws IOException {
        OrganizationModulesVariables variables = new OrganizationModulesVariables();
        variables.setOrganizationId(organizationId);
        if (pageNumber != null){
            variables.setPageNumber(pageNumber);
        }else {
            variables.setPageNumber(0);
        }
        if (pageSize != null){
            variables.setPageSize(pageSize);
        }else {
            variables.setPageSize(100);
        }
        String graphqlPayload = GraphQlCoreHelpers.parseGraphql(organizationModulesFile, gson.toJson(variables));

        return GraphQlCoreHelpers.sendRequestWithToken(generalData.gqlBaseUri, token, graphqlPayload);
    }


}
