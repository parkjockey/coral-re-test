package graphql.helpers.users;

import com.google.gson.Gson;
import common.helpers.rest.GraphQlCoreHelpers;
import data_containers.GeneralData;
import graphql.responses.AuthenticationResponse;
import graphql.responses.users.getTokens.GetTokens;
import graphql.responses.users.getTokens.GetTokensResponse;
import graphql.variables.AuthenticateVariables;
import graphql.variables.GetTokensVariables;
import graphql.variables.LoginVariables;
import io.restassured.response.Response;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;

public class AuthenticationHelpers {
    public static Logger logger = LoggerFactory.getLogger(AuthenticationHelpers.class);
    public static Gson gson = new Gson();
    public static File authenticationGraphQLFile = new File("src/test/resources/graphql/authentication.graphql");
    public static File loginGQLFile = new File("src/test/resources/graphql/login.graphql");
    public static File getTokensGQLFile = new File("src/test/resources/graphql/getTokens.graphql");
    public static File refreshTokens = new File("src/test/resources/graphql/refreshTokens.graphql");
    public static File logoutFile = new File("src/test/resources/graphql/logout.graphql");
    private static GeneralData generalData;

    public AuthenticationHelpers(GeneralData generalData) {
        this.generalData = generalData;
    }

    public static Response authenticationQuery(String uri, String userName, String password) throws IOException {
        AuthenticateVariables authenticateVariables = new AuthenticateVariables();
        authenticateVariables.setUsername(userName);
        authenticateVariables.setPassword(password);

        String payload = GraphQlCoreHelpers.parseGraphql(authenticationGraphQLFile, gson.toJson(authenticateVariables));

        return GraphQlCoreHelpers.sendRequestWithoutToken(uri, payload);
    }

    /**
     * Metdho will return accessCode which is necessary for getTokensQuery method
     *
     * @param userName
     * @param password
     * @return access code
     * @throws IOException
     * @author dino
     */
    public static String getAccessCode(String userName, String password) throws IOException {
        LoginVariables variables = new LoginVariables();
        variables.setUsername(userName);
        variables.setPassword(password);
        variables.setRedirectUrl(generalData.redirectUrl);

        String payload = GraphQlCoreHelpers.parseGraphql(loginGQLFile, gson.toJson(variables));

        Response response = GraphQlCoreHelpers.sendRequestWithoutToken(generalData.gqlBaseUri, payload);

        return response.jsonPath().get("data.login.accessCode");
    }

    /**
     * Method will call login query and return rest assured Response
     *
     * @param userName
     * @param password
     * @return
     * @throws IOException
     */
    public static Response loginQuery(String userName, String password) throws IOException {
        LoginVariables variables = new LoginVariables();
        variables.setUsername(userName);
        variables.setPassword(password);
        variables.setRedirectUrl(generalData.redirectUrl);

        String payload = GraphQlCoreHelpers.parseGraphql(loginGQLFile, gson.toJson(variables));

        Response response = GraphQlCoreHelpers.sendRequestWithoutToken(generalData.gqlBaseUri, payload);

        return response;
    }

    /**
     * Method will return response with all tokens if all parameters are inserted
     *
     * @param accessCode
     * @param redirectUrl
     * @return
     * @throws IOException
     */
    public static Response getTokensQuery(String accessCode, String redirectUrl) throws IOException {
        GetTokensVariables variables = new GetTokensVariables();
        variables.setAccessCode(accessCode);
        variables.setRedirectUrl(redirectUrl);

        String payload = GraphQlCoreHelpers.parseGraphql(getTokensGQLFile, gson.toJson(variables));

        return GraphQlCoreHelpers.sendRequestWithoutToken(generalData.gqlBaseUri, payload);

    }

    /**
     *Method will return Response with all tokens when username and password are provided
     * @param userName
     * @param password
     * @return
     * @throws IOException
     */
    public static Response getTokens(String userName, String password) throws IOException {
        String accessCode = getAccessCode(userName, password);
        return getTokensQuery(accessCode, generalData.redirectUrl);
    }

    /**
     *Method will obtain access token
     * @param userName
     * @param password
     * @return
     * @throws IOException
     */
    public static String getToken(String userName, String password) throws IOException {
        String accessCode = getAccessCode(userName, password);
        Response response = getTokensQuery(accessCode, generalData.redirectUrl);
        return AuthenticationResponse.accessToken(response);
    }

    public static Response refreshTokens(String refreshToken) throws IOException {
        String payload =
                GraphQlCoreHelpers.parseGraphql(refreshTokens, "{\"refreshToken\":\""+refreshToken+"\"}");

        return GraphQlCoreHelpers.sendRequestWithoutToken(generalData.gqlBaseUri, payload);

    }

    public static Response logout(String token) throws IOException {
        String payload = GraphQlCoreHelpers.parseGraphql(logoutFile, null);

        return GraphQlCoreHelpers.sendRequestWithToken(generalData.gqlBaseUri, token, payload);

    }

    public static String getRCAuthJson(String userName, String password) throws IOException {
        String accessCode = getAccessCode(userName, password);
        Response response = getTokensQuery(accessCode, generalData.redirectUrl);
        GetTokensResponse getTokensResponse = new Gson().fromJson(response.asString(), GetTokensResponse.class);
        GetTokens tokens = getTokensResponse.getData().getGetTokens();
        return new Gson().toJson(tokens);
    }
}
