package graphql.helpers.users;

import com.google.gson.Gson;
import common.helpers.rest.GraphQlCoreHelpers;
import graphql.variables.ConfirmNotSSOUserVariables;
import io.restassured.response.Response;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;

public class ConfirmUserHelpers {
    public static Logger logger = LoggerFactory.getLogger(AuthenticationHelpers.class);
    public static Gson gson = new Gson();

    public static Response confirmNotSSOUser(String uri, String inviteCode, String firstName, String lastName, String password)
            throws IOException {
        ConfirmNotSSOUserVariables variables = new ConfirmNotSSOUserVariables();
        variables.setInviteCode(inviteCode);
        variables.setFirstName(firstName);
        variables.setLastName(lastName);
        variables.setPassword(password);

        String payload = GraphQlCoreHelpers.parseGraphql(new File("src/test/resources/graphql/confirmOrganizationUserInvite.graphql"),
                gson.toJson(variables));

        return GraphQlCoreHelpers.sendRequestWithoutToken(uri, payload);
    }

    public static Response confirmSSOUser(String uri, String inviteCode)
            throws IOException {
        ConfirmNotSSOUserVariables variables = new ConfirmNotSSOUserVariables();
        variables.setInviteCode(inviteCode);

        String payload = GraphQlCoreHelpers.parseGraphql(new File("src/test/resources/graphql/confirmOrganizationSsoUserInvite.graphql"),
                gson.toJson(variables));

        return GraphQlCoreHelpers.sendRequestWithoutToken(uri, payload);
    }

}
