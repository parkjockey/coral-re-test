package graphql.helpers.users;

import com.google.gson.Gson;
import common.helpers.rest.GraphQlCoreHelpers;
import data_containers.GeneralData;
import io.restassured.response.Response;

import java.io.File;
import java.io.IOException;

public class ConsumerUserEmailExistsHelpers {
   static GeneralData generalData;
    Gson gson = new Gson();
    static File checkEmailFile = new File("src/test/resources/graphql/consumerUserEmailExists.graphql");

    public ConsumerUserEmailExistsHelpers(GeneralData generalData) {
        this.generalData = generalData;
    }

    public static Response checkEmailExists(String email) throws IOException {
        String graphqlPayload = GraphQlCoreHelpers.parseGraphql(checkEmailFile, "{\"email\":\""+email+"\"}");

        return GraphQlCoreHelpers.sendRequestWithoutToken(generalData.gqlBaseUri, graphqlPayload);
    }
}
