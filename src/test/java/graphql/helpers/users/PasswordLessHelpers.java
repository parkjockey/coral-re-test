package graphql.helpers.users;

import com.google.gson.Gson;
import common.helpers.rest.GraphQlCoreHelpers;
import data_containers.GeneralData;
import graphql.variables.ConfirmPasswordLessUserVariables;
import io.restassured.response.Response;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;

public class PasswordLessHelpers {
    static GeneralData generalData;
    static Gson gson = new Gson();
    static File passwordLessConsumerUserSignUpFile = new File("src/test/resources/graphql/passwordlessConsumerUserSignUp.graphql");
    static File passwordLessUserConfirmFile = new File("src/test/resources/graphql/confirmPasswordlessConsumerUserSignUp.graphql");
    static File passwordLessConsumerLoginFile = new File("src/test/resources/graphql/passwordlessConsumerUserLogin.graphql");
    static File confirmPasswordLessConsumerUserLoginFile = new File("src/test/resources/graphql/confirmPasswordlessConsumerUserLogin.graphql");
    static File passwordlessConsumerUserLoginMediatorFile = new File("src/test/resources/graphql/consumers/passwordlessConsumerUserLogin.graphql");
    static File confirmPasswordlessConsumerUserLoginMediatorFile = new File("src/test/resources/graphql/consumers/confirmPasswordlessConsumerUserLogin.graphql");

    public PasswordLessHelpers(GeneralData generalData) {
        this.generalData = generalData;
    }

    public static Response passwordLessConsumerUserSignUp(String email) throws IOException {
        HashMap<String, String> var = new HashMap<>();
        var.put("email", email);
        var.put("countryCode",generalData.countryCode);

        String graphqlPayload = GraphQlCoreHelpers.parseGraphql(passwordLessConsumerUserSignUpFile, gson.toJson(var));

        return GraphQlCoreHelpers.sendRequestWithoutToken(generalData.gqlBaseUri, graphqlPayload);
    }

    public static Response passwordLessConsumerUserSignUpPhone(String phoneNumber) throws IOException {
        HashMap<String, String> var = new HashMap<>();
        var.put("phoneNumber", phoneNumber);
        var.put("countryCode",generalData.countryCode);
        String graphqlPayload = GraphQlCoreHelpers.parseGraphql(passwordLessConsumerUserSignUpFile, gson.toJson(var));

        return GraphQlCoreHelpers.sendRequestWithoutToken(generalData.gqlBaseUri, graphqlPayload);
    }

    public static Response confirmPasswordLessUserSignUp(String email, String otp, String session) throws IOException {
        ConfirmPasswordLessUserVariables variables = new ConfirmPasswordLessUserVariables();
        variables.setEmail(email);
        variables.setCode(otp);
        variables.setSession(session);

        String graphqlPayload = GraphQlCoreHelpers.parseGraphql(passwordLessUserConfirmFile, gson.toJson(variables));

        return GraphQlCoreHelpers.sendRequestWithoutToken(generalData.gqlBaseUri, graphqlPayload);
    }

    public static Response passwordLessConsumerLogin(String email) throws IOException {
        String graphqlPayload = GraphQlCoreHelpers.parseGraphql(passwordLessConsumerLoginFile, "{\"email\":\"" + email + "\"}");

        return GraphQlCoreHelpers.sendRequestWithoutToken(generalData.gqlBaseUri, graphqlPayload);
    }

    public static Response passwordlessConsumerLoginMediator(String email, String phone)throws IOException{
        String graphqlPayload = GraphQlCoreHelpers.parseGraphql(passwordlessConsumerUserLoginMediatorFile, "{\"email\":\"" + email + "\", \"phoneNumber\":\"" + phone + "\"}");

        return GraphQlCoreHelpers.sendRequestWithoutTokenMediator(generalData.mediatorGql, graphqlPayload, generalData.apiKey);
    }

    public static Response confirmPasswordLessConsumerUserLogin(String email, String otp, String session) throws IOException {
        ConfirmPasswordLessUserVariables variables = new ConfirmPasswordLessUserVariables();
        variables.setEmail(email);
        variables.setCode(otp);
        variables.setSession(session);

        String graphqlPayload = GraphQlCoreHelpers.parseGraphql(confirmPasswordLessConsumerUserLoginFile, gson.toJson(variables));

        return GraphQlCoreHelpers.sendRequestWithoutToken(generalData.gqlBaseUri, graphqlPayload);
    }

    public static Response confirmPasswordlessConsumerLoginMediator(String email, String phone, String code) throws IOException{
        ConfirmPasswordLessUserVariables variables = new ConfirmPasswordLessUserVariables();
        variables.setEmail(email);
        variables.setPhoneNumber(phone);
        variables.setCode(code);

        String graphqlPayload = GraphQlCoreHelpers.parseGraphql(confirmPasswordlessConsumerUserLoginMediatorFile, gson.toJson(variables));

        return GraphQlCoreHelpers.sendRequestWithoutTokenMediator(generalData.mediatorGql, graphqlPayload, generalData.apiKey);
    }

    public static Response confirmPasswordLessUserSignUpPhone(String phone, String otp, String session) throws IOException {
        ConfirmPasswordLessUserVariables variables = new ConfirmPasswordLessUserVariables();
        variables.setPhoneNumber(phone);
        variables.setCode(otp);
        variables.setSession(session);

        String graphqlPayload = GraphQlCoreHelpers.parseGraphql(passwordLessUserConfirmFile, gson.toJson(variables));

        return GraphQlCoreHelpers.sendRequestWithoutToken(generalData.gqlBaseUri, graphqlPayload);
    }

    public static Response passwordLessConsumerLoginPhone(String phoneNumber) throws IOException {
        String graphqlPayload = GraphQlCoreHelpers.parseGraphql(passwordLessConsumerLoginFile, "{\"phoneNumber\":\"" + phoneNumber + "\"}");

        return GraphQlCoreHelpers.sendRequestWithoutToken(generalData.gqlBaseUri, graphqlPayload);
    }


    public static Response confirmPasswordLessConsumerUserLoginPhone(String phoneNumber, String otp, String session) throws IOException {
        ConfirmPasswordLessUserVariables variables = new ConfirmPasswordLessUserVariables();
        variables.setPhoneNumber(phoneNumber);
        variables.setCode(otp);
        variables.setSession(session);

        String graphqlPayload = GraphQlCoreHelpers.parseGraphql(confirmPasswordLessConsumerUserLoginFile, gson.toJson(variables));

        return GraphQlCoreHelpers.sendRequestWithoutToken(generalData.gqlBaseUri, graphqlPayload);
    }
}
