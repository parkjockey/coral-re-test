package graphql.helpers.users;

import com.google.gson.Gson;
import common.helpers.rest.GraphQlCoreHelpers;
import data_containers.GeneralData;
import io.restassured.response.Response;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;

public class ResetPasswordHelpers {
    static GeneralData generalData;
    static Gson gson = new Gson();
    static File resetPasswordFile = new File("src/test/resources/graphql/open/mutations/resetPassword.graphql");
    static File confirmResetPasswordFile = new File("src/test/resources/graphql/open/mutations/confirmResetPassword.graphql");


    public ResetPasswordHelpers(GeneralData generalData) {
        this.generalData = generalData;
    }

    public static Response updatePassword(String email)
            throws IOException {

        HashMap<String, String>variables = new HashMap<>();
        variables.put("email", email);

        String payload = GraphQlCoreHelpers.parseGraphql(resetPasswordFile, gson.toJson(variables));

        return GraphQlCoreHelpers.sendRequestWithoutToken(generalData.gqlBaseUri, payload);
    }

    public static Response confirmResetPassword(String newPassword, String verificationCode)
            throws IOException {

        HashMap<String, String>variables = new HashMap<>();
        variables.put("newPassword", newPassword);
        variables.put("verificationCode", verificationCode);

        String payload = GraphQlCoreHelpers.parseGraphql(confirmResetPasswordFile, gson.toJson(variables));

        return GraphQlCoreHelpers.sendRequestWithoutToken(generalData.gqlBaseUri, payload);
    }


}
