package graphql.helpers.users;

import com.google.gson.Gson;
import common.helpers.rest.GraphQlCoreHelpers;
import data_containers.GeneralData;
import graphql.variables.UpdatePasswordVariables;
import io.restassured.response.Response;

import java.io.File;
import java.io.IOException;

public class UpdatePasswordHelpers {
    private static Gson gson = new Gson();
    private static GeneralData generalData;
    private static File updatePasswordFile = new File("src/test/resources/graphql/updatePassword.graphql");

    public UpdatePasswordHelpers(GeneralData generalData) {
        this.generalData = generalData;
    }

    public static Response updatePassword(String token, String currentPassword, String newPassword)
            throws IOException {
        UpdatePasswordVariables variables = new UpdatePasswordVariables();
        variables.setCurrentPassword(currentPassword);
        variables.setNewPassword(newPassword);

        String payload = GraphQlCoreHelpers.parseGraphql(updatePasswordFile, gson.toJson(variables));

        return GraphQlCoreHelpers.sendRequestWithToken(generalData.gqlBaseUri, token, payload);
    }
}
