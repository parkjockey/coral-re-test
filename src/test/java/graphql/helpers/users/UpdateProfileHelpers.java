package graphql.helpers.users;

import com.google.gson.Gson;
import common.helpers.rest.GraphQlCoreHelpers;
import data_containers.GeneralData;
import graphql.variables.UpdateProfileVariables;
import io.restassured.response.Response;

import java.io.File;
import java.io.IOException;

public class UpdateProfileHelpers {
    private static Gson gson = new Gson();
    private static GeneralData generalData;
    private static File updateProfileFile = new File("src/test/resources/graphql/updateProfile.graphql");
    private static File updateProfilePictureFile = new File("src/test/resources/graphql/updateProfilePicture.graphql");
    private static File profilePictureFile = new File("src/test/resources/graphql/profilePicture.graphql");
    private static File updateEmailFile = new File("src/test/resources/graphql/updateEmail.graphql");
    private static File confirmConsumerUserEmailWithOtpFile = new File("src/test/resources/graphql/confirmConsumerUserEmailWithOtp.graphql");

    public UpdateProfileHelpers(GeneralData generalData) {
        this.generalData = generalData;
    }

    public static Response updateProfile(String token, String firstName, String lastName, String email)
            throws IOException {
        UpdateProfileVariables variables = new UpdateProfileVariables();
        try {
            variables.setFirstName(firstName);
            variables.setLastName(lastName);
            variables.setEmail(email);
        } catch (NullPointerException nullPointer) {
            variables.setFirstName(firstName);
            variables.setLastName(lastName);
        }


        String payload = GraphQlCoreHelpers.parseGraphql(updateProfileFile, gson.toJson(variables));

        return GraphQlCoreHelpers.sendRequestWithToken(generalData.gqlBaseUri, token, payload);
    }

    public static Response updateProfilePicture(String token, String picture, String extension)
            throws IOException {

        String var = "{\"content\":\"" + picture + "\",\"extension\":\"" + extension+"\"}";

    String payload = GraphQlCoreHelpers.parseGraphql(updateProfilePictureFile, var);

        return GraphQlCoreHelpers.sendRequestWithToken(generalData.gqlBaseUri, token, payload);
}

    public static Response profilePicture(String token)
            throws IOException {

        String payload = GraphQlCoreHelpers.parseGraphql(profilePictureFile, null);

        return GraphQlCoreHelpers.sendRequestWithToken(generalData.gqlBaseUri, token, payload);
    }

    public static Response consumerUpdateEmail(String token, String email)
            throws IOException {

        String var = "{\"email\":\"" + email + "\"}";
        String payload = GraphQlCoreHelpers.parseGraphql(updateEmailFile, var);
        return GraphQlCoreHelpers.sendRequestWithToken(generalData.gqlBaseUri, token, payload);
    }

    public static Response confirmConsumerUserEmailWithOtp(String token, String verificationEmailCode, String tempEmail)
            throws IOException {

        String var = "{\"verificationEmailCode\":\"" + verificationEmailCode + "\",\"temporaryEmail\":\"" + tempEmail+"\"}";

        String payload = GraphQlCoreHelpers.parseGraphql(confirmConsumerUserEmailWithOtpFile, var);

        return GraphQlCoreHelpers.sendRequestWithToken(generalData.gqlBaseUri, token, payload);
    }
}
