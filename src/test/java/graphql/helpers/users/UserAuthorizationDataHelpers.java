package graphql.helpers.users;

import com.google.gson.Gson;
import common.helpers.rest.GraphQlCoreHelpers;
import data_containers.GeneralData;
import io.restassured.response.Response;

import java.io.File;
import java.io.IOException;

public class UserAuthorizationDataHelpers {

    static GeneralData generalData;
    Gson gson = new Gson();
    static File authorizationDataFile = new File("src/test/resources/graphql/userAuthorizationData.graphql");

    public UserAuthorizationDataHelpers(GeneralData generalData) {
        this.generalData = generalData;
    }

    public static Response userAuthorizationData(String token) throws IOException {
        String graphqlPayload = GraphQlCoreHelpers.parseGraphql(authorizationDataFile, null);

        return GraphQlCoreHelpers.sendRequestWithToken(generalData.gqlBaseUri, token,  graphqlPayload);
    }
}
