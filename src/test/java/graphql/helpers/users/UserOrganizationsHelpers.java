package graphql.helpers.users;

import com.google.gson.Gson;
import common.helpers.rest.GraphQlCoreHelpers;
import data_containers.GeneralData;
import graphql.variables.UserOrganizationsVariables;
import io.restassured.response.Response;

import java.io.File;
import java.io.IOException;

public class UserOrganizationsHelpers {
    public static Gson gson = new Gson();
    public static File userOrganizationsFile = new File("src/test/resources/graphql/userOrganizations.graphql");
    private static GeneralData generalData;

    public UserOrganizationsHelpers(GeneralData generalData) {
        this.generalData = generalData;
    }

    public static Response userOrganizationsQueryWithParam(String token, String organizationName, Integer pageNumber, Integer pageSize)
            throws IOException {
        UserOrganizationsVariables variables = new UserOrganizationsVariables();
        variables.setName(organizationName);
        if (pageNumber == null) {
            variables.setPageNumber(0);
        } else {
            variables.setPageNumber(pageNumber);
        }
        if (pageSize == null) {
            variables.setPageSize(40);
        } else {
            variables.setPageSize(pageSize);
        }
        String payload = GraphQlCoreHelpers.parseGraphql(userOrganizationsFile, gson.toJson(variables));

        return GraphQlCoreHelpers.sendRequestWithToken(generalData.gqlBaseUri, token, payload);
    }
}
