package graphql.helpers.users;

import com.google.gson.Gson;
import common.helpers.rest.GraphQlCoreHelpers;
import data_containers.GeneralData;
import graphql.helpers.GraphQLHelpers;
import graphql.variables.users.OrganizationUsersPageVariables;
import graphql.variables.users.UpdateUserStatusVariables;
import io.restassured.response.Response;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

public class UsersGqlHelpers {

    public static Gson gson = new Gson();
    public static File userStatusesFile = new File("src/test/resources/graphql/users/userStatuses.graphql");
    public static File updateUserStatusesFile = new File("src/test/resources/graphql/users/updateUserStatus.graphql");
    static GeneralData generalData;

    public UsersGqlHelpers(GeneralData generalData) {
        this.generalData = generalData;
    }

    public static Response userStatuses(String token)
            throws IOException {
        String payload = GraphQlCoreHelpers.parseGraphql(userStatusesFile, null);

        return GraphQlCoreHelpers.sendRequestWithToken(generalData.gqlBaseUri, token, payload);
    }

    public static Response filterOrganizationUsersPageByEmail(String token, Integer organizationId, String email, Integer pageNumber)
            throws IOException {
        OrganizationUsersPageVariables variables = new OrganizationUsersPageVariables();
        variables.setOrganizationId(organizationId);
        variables.setEmail(email);
        variables.setPageNumber(pageNumber);
        variables.setPageSize(40);

        String payload = GraphQlCoreHelpers.parseGraphql(GraphQLHelpers.organizationUsersPageFile, new Gson().toJson(variables));

        return GraphQlCoreHelpers.sendRequestWithToken(generalData.gqlBaseUri, token, payload);
    }

    public static Response filterOrganizationUsersPageByName(String token, Integer organizationId, String name, Integer pageNumber)
            throws IOException {
        OrganizationUsersPageVariables variables = new OrganizationUsersPageVariables();
        variables.setOrganizationId(organizationId);
        variables.setName(name);
        variables.setPageNumber(pageNumber);
        variables.setPageSize(40);

        String payload = GraphQlCoreHelpers.parseGraphql(GraphQLHelpers.organizationUsersPageFile, new Gson().toJson(variables));

        return GraphQlCoreHelpers.sendRequestWithToken(generalData.gqlBaseUri, token, payload);
    }

    public static Response filterOrganizationUsersPageByRoleIds(String token, Integer organizationId, ArrayList<Integer> roleIds, Integer pageNumber)
            throws IOException {
        OrganizationUsersPageVariables variables = new OrganizationUsersPageVariables();
        variables.setOrganizationId(organizationId);
        variables.roleIds= (ArrayList<Integer>) roleIds.clone();
        variables.setPageNumber(pageNumber);
        variables.setPageSize(40);

        String payload = GraphQlCoreHelpers.parseGraphql(GraphQLHelpers.organizationUsersPageFile, new Gson().toJson(variables));

        return GraphQlCoreHelpers.sendRequestWithToken(generalData.gqlBaseUri, token, payload);
    }

    public static Response filterOrganizationUsersPageByStatuses(String token, Integer organizationId, ArrayList<String> statuses, Integer pageNumber)
            throws IOException {
        OrganizationUsersPageVariables variables = new OrganizationUsersPageVariables();
        variables.setOrganizationId(organizationId);
        variables.statuses= (ArrayList<String>) statuses.clone();
        variables.setPageNumber(pageNumber);
        variables.setPageSize(10);

        String payload = GraphQlCoreHelpers.parseGraphql(GraphQLHelpers.organizationUsersPageFile, new Gson().toJson(variables));

        return GraphQlCoreHelpers.sendRequestWithToken(generalData.gqlBaseUri, token, payload);
    }

    public static Response filterOrganizationUsersPageByOrderBy(String token, Integer organizationId, ArrayList<String> orderBy, Integer pageNumber)
            throws IOException {
        OrganizationUsersPageVariables variables = new OrganizationUsersPageVariables();
        variables.setOrganizationId(organizationId);
        variables.orderBy= (ArrayList<String>) orderBy.clone();
        variables.setPageNumber(pageNumber);
        variables.setPageSize(10);

        String payload = GraphQlCoreHelpers.parseGraphql(GraphQLHelpers.organizationUsersPageFile, new Gson().toJson(variables));

        return GraphQlCoreHelpers.sendRequestWithToken(generalData.gqlBaseUri, token, payload);
    }

    public static Response filterOrganizationUsersPageSize(String token, Integer organizationId, Integer pageSize, Integer pageNumber)
            throws IOException {
        OrganizationUsersPageVariables variables = new OrganizationUsersPageVariables();
        variables.setOrganizationId(organizationId);
        variables.setPageNumber(pageNumber);
        variables.setPageSize(pageSize);

        String payload = GraphQlCoreHelpers.parseGraphql(GraphQLHelpers.organizationUsersPageFile, new Gson().toJson(variables));

        return GraphQlCoreHelpers.sendRequestWithToken(generalData.gqlBaseUri, token, payload);
    }


    public static Response updateUserStatus(String token, Integer orgId, Integer userId, String userStatus)
            throws IOException {
        UpdateUserStatusVariables variables = new UpdateUserStatusVariables();
        variables.setOrganizationId(orgId);
        variables.setUserId(userId);
        variables.setUserStatus(userStatus);

        String payload = GraphQlCoreHelpers.parseGraphql(updateUserStatusesFile, gson.toJson(variables));

        return GraphQlCoreHelpers.sendRequestWithToken(generalData.gqlBaseUri, token, payload);
    }


}
