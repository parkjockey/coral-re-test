package graphql.helpers.users.consumers;

import com.google.gson.Gson;
import common.helpers.rest.GraphQlCoreHelpers;
import data_containers.GeneralData;
import io.restassured.response.Response;

import java.io.File;
import java.io.IOException;

public class ConsumerEmailVerificationDataHelpers {
   static GeneralData generalData;
    Gson gson = new Gson();
   static File consumerUserEmailVerificationDataFile = new File("src/test/resources/graphql/consumerUserEmailVerificationData.graphql");

    public ConsumerEmailVerificationDataHelpers(GeneralData generalData) {
        this.generalData = generalData;
    }

    public static Response consumerVerificationData(String verificationEmailCode) throws IOException {
        String graphqlPayload = GraphQlCoreHelpers
                .parseGraphql(consumerUserEmailVerificationDataFile,
                        "{\"verificationEmailCode\":\""+verificationEmailCode+"\"}");

        return GraphQlCoreHelpers.sendRequestWithoutToken(generalData.gqlBaseUri, graphqlPayload);
    }
}
