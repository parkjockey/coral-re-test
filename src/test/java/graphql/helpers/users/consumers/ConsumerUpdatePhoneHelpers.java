package graphql.helpers.users.consumers;

import com.google.gson.Gson;
import common.helpers.rest.GraphQlCoreHelpers;
import data_containers.GeneralData;
import io.restassured.response.Response;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;

public class ConsumerUpdatePhoneHelpers {

    static GeneralData generalData;
    static File updatePhoneFile = new File("src/test/resources/graphql/updatePhoneNumber.graphql");
    static File confirmConsumerUserPhoneNumberWithOtpFile = new File("src/test/resources/graphql/confirmConsumerUserPhoneNumberWithOtp.graphql");

    public ConsumerUpdatePhoneHelpers(GeneralData generalData) {
        this.generalData = generalData;
    }

    public static Response updatePhoneNumber(String token, String phoneNumber) throws IOException {
        HashMap<String, String> vars = new HashMap<>();
        vars.put("phoneNumber", phoneNumber);

        String graphqlPayload = GraphQlCoreHelpers
                .parseGraphql(updatePhoneFile, new Gson().toJson(vars));

        return GraphQlCoreHelpers.sendRequestWithToken(generalData.gqlBaseUri, token, graphqlPayload);
    }

    public static Response confirmConsumerUserPhoneNumberWithOtp(String token, String verificationCode, String temporaryPhoneNumber) throws IOException {
        HashMap<String, String> vars = new HashMap<>();
        vars.put("verificationCode", verificationCode);
        vars.put("temporaryPhoneNumber", temporaryPhoneNumber);

        String graphqlPayload = GraphQlCoreHelpers
                .parseGraphql(confirmConsumerUserPhoneNumberWithOtpFile, new Gson().toJson(vars));

        return GraphQlCoreHelpers.sendRequestWithToken(generalData.gqlBaseUri, token, graphqlPayload);
    }
}
