package graphql.helpers.users.consumers;

import com.google.gson.Gson;
import common.helpers.rest.GraphQlCoreHelpers;
import data_containers.GeneralData;
import io.restassured.response.Response;

import java.io.File;
import java.io.IOException;

public class ConsumerUserCheckPhoneNumberHelpers {

    static GeneralData generalData;
    Gson gson = new Gson();
    static File checkPhoneFile = new File("src/test/resources/graphql/consumerUserPhoneNumberExists.graphql");

    public ConsumerUserCheckPhoneNumberHelpers(GeneralData generalData) {
        this.generalData = generalData;
    }

    public static Response checkPhone(String phoneNumber) throws IOException {
        String graphqlPayload = GraphQlCoreHelpers.parseGraphql(checkPhoneFile, "{\"phoneNumber\":\""+phoneNumber+"\"}");

        return GraphQlCoreHelpers.sendRequestWithoutToken(generalData.gqlBaseUri, graphqlPayload);
    }
}
