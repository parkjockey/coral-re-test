package graphql.helpers.users.consumers;

import common.helpers.rest.GraphQlCoreHelpers;
import data_containers.GeneralData;
import data_containers.UserData;
import io.restassured.response.Response;

import java.io.File;
import java.io.IOException;

public class ConsumerUserVehiclesHelpers {
    static GeneralData generalData;
    static UserData userData;

    static File createVehicleFile = new File("src/test/resources/graphql/consumers/createVehicleForUser.graphql");

    public ConsumerUserVehiclesHelpers(GeneralData generalData, UserData userData){
        this.generalData = generalData;
        this.userData = userData;
    }

    public static Response createVehicleForUser() throws IOException {
        String graphqlPayload = GraphQlCoreHelpers.parseGraphql(createVehicleFile, null);
        return GraphQlCoreHelpers.sendRequestWithTokenMediator(generalData.mediatorGql, graphqlPayload, generalData.apiKey, userData.token);
    }
}
