package graphql.helpers.users.consumers;

import com.google.gson.Gson;
import common.helpers.rest.GraphQlCoreHelpers;
import data_containers.GeneralData;
import graphql.variables.consumers.ConsumerUserDataByIdVariables;
import graphql.variables.consumers.UpdateConsumerStatusVariables;
import graphql.variables.consumers.UpdateConsumerVariables;
import io.restassured.response.Response;

import java.io.File;
import java.io.IOException;

public class ConsumerUsersHelpers {

    static GeneralData generalData;
    static File consumerUserDataFile = new File("src/test/resources/graphql/consumerUserData.graphql");
    static File consumerUserPoolsFile = new File("src/test/resources/graphql/consumers/consumerUserPools.graphql");
    static File updateConsumerUserFile = new File("src/test/resources/graphql/consumers/updateConsumerUser.graphql");
    static File updateConsumerStatusFile = new File("src/test/resources/graphql/consumers/updateConsumerStatus.graphql");
    static File consumerUserDataById = new File("src/test/resources/graphql/consumers/consumerUserDataById.graphql");


    public ConsumerUsersHelpers(GeneralData generalData) {
        this.generalData = generalData;
    }

    public static Response consumerUserData(String token) throws IOException {
        String graphqlPayload = GraphQlCoreHelpers.parseGraphql(consumerUserDataFile,null);

        return GraphQlCoreHelpers.sendRequestWithToken(generalData.gqlBaseUri, token,graphqlPayload);
    }

    public static Response consumerUserPools(String token) throws IOException {
        String graphqlPayload = GraphQlCoreHelpers.parseGraphql(consumerUserPoolsFile,null);

        return GraphQlCoreHelpers.sendRequestWithToken(generalData.gqlBaseUri, token,graphqlPayload);
    }

    public static Response updateConsumerUser(String token, UpdateConsumerVariables consumerVariables) throws IOException {

        String graphqlPayload = GraphQlCoreHelpers.parseGraphql(updateConsumerUserFile, new Gson().toJson(consumerVariables));

        return GraphQlCoreHelpers.sendRequestWithToken(generalData.gqlBaseUri, token,graphqlPayload);
    }

    public static Response updateConsumerStatus(String token, Integer userId, String userStatus) throws IOException {
        UpdateConsumerStatusVariables vars = new UpdateConsumerStatusVariables();
        vars.setUserId(userId);
        vars.setUserStatus(userStatus);

        String graphqlPayload = GraphQlCoreHelpers
                .parseGraphql(updateConsumerStatusFile, new Gson().toJson(vars));

        return GraphQlCoreHelpers.sendRequestWithToken(generalData.gqlBaseUri, token, graphqlPayload);
    }

    public static Response consumerUserDataById(String token, Integer id) throws IOException {
        ConsumerUserDataByIdVariables vars = new ConsumerUserDataByIdVariables();
        vars.setId(id);

        String graphqlPayload = GraphQlCoreHelpers.parseGraphql(consumerUserDataById,new Gson().toJson(vars));

        return GraphQlCoreHelpers.sendRequestWithToken(generalData.gqlBaseUri, token,graphqlPayload);
    }
}
