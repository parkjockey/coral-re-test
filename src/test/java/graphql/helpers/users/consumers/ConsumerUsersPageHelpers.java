package graphql.helpers.users.consumers;

import com.google.gson.Gson;
import common.helpers.rest.GraphQlCoreHelpers;
import data_containers.GeneralData;
import io.restassured.response.Response;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;

public class ConsumerUsersPageHelpers {

    static GeneralData generalData;
    static File consumerUsersPageFile = new File("src/test/resources/graphql/consumers/consumerUsersPage.graphql");

    public ConsumerUsersPageHelpers(GeneralData generalData) {
        this.generalData = generalData;
    }

    public static Response getConsumerByEmail(String token, String userPoolKey, String email) throws IOException {
        HashMap<String, String> vars = new HashMap<>();
        vars.put("userPoolKey", userPoolKey);
        vars.put("email", email);

        String graphqlPayload = GraphQlCoreHelpers
                .parseGraphql(consumerUsersPageFile, new Gson().toJson(vars));

        return GraphQlCoreHelpers.sendRequestWithToken(generalData.gqlBaseUri, token, graphqlPayload);
    }

    public static Response getConsumerByPhone(String token, String userPoolKey, String $phoneNumber) throws IOException {
        HashMap<String, String> vars = new HashMap<>();
        vars.put("userPoolKey", userPoolKey);
        vars.put("phoneNumber", $phoneNumber);

        String graphqlPayload = GraphQlCoreHelpers
                .parseGraphql(consumerUsersPageFile, new Gson().toJson(vars));

        return GraphQlCoreHelpers.sendRequestWithToken(generalData.gqlBaseUri, token, graphqlPayload);
    }

}
