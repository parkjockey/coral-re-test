package graphql.request_objects;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;

@Getter
@Setter
public class OrganizationsResponse {

    @SerializedName("totalItems")
    @Expose
    private Integer totalItems;
    @SerializedName("totalPages")
    @Expose
    private Integer totalPages;
    @SerializedName("content")
    @Expose
    private ArrayList<Organization> content = new ArrayList<>();
}
