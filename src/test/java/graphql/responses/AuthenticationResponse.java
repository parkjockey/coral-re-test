package graphql.responses;

import io.restassured.response.Response;

public class AuthenticationResponse {

    public static String accessToken (Response response){
        return response.jsonPath().get("data.getTokens.accessToken");
    }

    public static String idToken (Response response){
        return response.jsonPath().get("data.getTokens.idToken");
    }

    public static String refreshToken (Response response){
        return response.jsonPath().get("data.getTokens.refreshToken");
    }

    public static String tokenType (Response response){
        return response.jsonPath().get("data.getTokens.tokenType");
    }

    public static Integer expiresIn (Response response){
        return response.jsonPath().get("data.getTokens.expiresIn");
    }
}
