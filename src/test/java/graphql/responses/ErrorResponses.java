package graphql.responses;

import io.restassured.response.Response;

public class ErrorResponses {

    public static String getErrorMessageKey(Response response){
        return response.jsonPath().get("errors[0].extensions.response.body.messageKey").toString();
    }
    public static String getErrorMessageKeys(Response response){
        return response.jsonPath().get("messageKey").toString();
    }
}
