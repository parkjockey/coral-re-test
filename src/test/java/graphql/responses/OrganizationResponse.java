package graphql.responses;

import data_containers.GeneralData;
import lombok.AllArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@AllArgsConstructor
public class OrganizationResponse {
    GeneralData generalData;

    public String AddOrganizationGetName(){
        return generalData.response.jsonPath().get("data.addOrganization.name").toString();
    }

    public String AddOrganizationGetDescription(){
        return generalData.response.jsonPath().get("data.addOrganization.description").toString();
    }

    public String AddOrganizationGetId(){
        return generalData.response.jsonPath().get("data.addOrganization.id").toString();
    }

    public String FindOrganizationGetName(){
        return generalData.response.jsonPath().get("data.findOrganizationById.name").toString();
    }

    public String FindOrganizationGetDescription(){
        return generalData.response.jsonPath().get("data.findOrganizationById.description").toString();
    }

    public Boolean invitedUserHasUsername(){
        return generalData.response.jsonPath().get("data.findOrganizationUserByEmail.username") != null;
    }
    public Boolean inviteUserHasSourceSystemName(){
        return generalData.response.jsonPath().get("data.findOrganizationUserByEmail.sourceSystemName") !=null;
    }
    public Boolean inviteUserHasSourceSystemUserIdentifier(){
        return generalData.response.jsonPath().get("data.findOrganizationUserByEmail.sourceSystemUserIdentifier") !=null;
    }


    public int GetResponseStatus(){
        return generalData.response.statusCode();
    }

    public List<Object> getOrganizationRoles(){
        return generalData.response.jsonPath().getList("data.organizationRolesPage.content.name");
    }

    public int getNumberOfInvitedAndAssignedUsers(){
        return generalData.response.jsonPath().get("data.inviteUsers.numberOfInvitedAndAssignedToRole");
    }

    public List<Object> getListOfUsersRoles(){

        return generalData.response.jsonPath().getList("data.organizationUserRolesPage.content.name");
    }

    public List<Object> getOrganizationUserId(){

        return generalData.response.jsonPath().get("data.organizationUsersPage.content.id");
    }

}

