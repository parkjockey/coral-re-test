package graphql.responses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import io.cucumber.java8.Id;

import java.util.ArrayList;

public class OrganizationUser {

    @SerializedName("id")
    @Expose
    private Id id;
    @SerializedName("fullName")
    @Expose
    private String fullName;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("userStatus")
    @Expose
    private String userStatus;
    @SerializedName("invitedOnDate")
    @Expose
    private String invitedOnDate;
    @SerializedName("acceptedOnDate")
    @Expose
    private String acceptedOnDate;
    @SerializedName("roles")
    @Expose
    private ArrayList<Role> roles = new ArrayList<>();


}
