package graphql.responses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import graphql.request_objects.Organization;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;

    @Getter
    @Setter
    public class OrganizationUsersPage {
        @SerializedName("content")
        @Expose
        private ArrayList<Organization> content = new ArrayList<>();
        @SerializedName("totalPages")
        @Expose
        private Integer totalPages;
        @SerializedName("totalItems")
        @Expose
        private Integer totalItems;
    }


