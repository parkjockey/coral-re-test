package graphql.responses;


import data_containers.GeneralData;
import graphql.request_objects.Organization;
import lombok.AllArgsConstructor;




import java.util.ArrayList;
import java.util.List;

@AllArgsConstructor
public class OrganizationsResponse {
    GeneralData generalData;


    //private ArrayList<OrganizationData> content;

    public List<Object> getOrganizationsNames() {

        List<Object> orgNames;

        orgNames = generalData.response.jsonPath().getList("data.findAllOrganizations.content.name");
        return orgNames;
    }

    public int getTotalOrganizations() {

        return generalData.response.jsonPath().get("data.findAllOrganizations.totalItems");
    }

    public List<Object> getOrganizationUsersNames() {

        //List<Object> orgUsers;
        return generalData.response.jsonPath().getList("data.organizationUsersPage.content.fullName");
    }

    public int getNumberOfOrganizationUsers(){

        List<Object> orgUserIds;

        orgUserIds = generalData.response.jsonPath().getList("data.organizationUsersPage.content.fullName");
        return  orgUserIds.size();
    }

    public Integer GetTotalUsers(){
        return generalData.response.jsonPath().get("data.organizationUsersPage.totalItems");
    }

    public List<Object> getListOfUsersRoles(){

        return generalData.response.jsonPath().getList("data.organizationUserRolesPage.content.name");
    }

    public List<Object> getOrganizationRoles() {

        return generalData.response.jsonPath().getList("data.organizationRolesPage.content.name");

    }

    public ArrayList<Organization> getAllOrganizations(){

        ArrayList<Organization> orgList = generalData.response.jsonPath().get("data.findAllOrganizations.content");
        System.out.println(orgList.size());

        return orgList;
    }

    public boolean accessTokenReceived(){

        return generalData.response.jsonPath().get("data.authentication.accessToken") != null;
    }

}
