package graphql.responses.consumers;

import io.restassured.response.Response;

public class PasswordLessConsumerUserSignUpResponse {

    public static String getSession(Response response){
     return    response.jsonPath().getString("data.passwordlessConsumerUserSignUp.session");
    }
}
