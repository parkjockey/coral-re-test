package graphql.responses.consumers.confirm_passless_login_consumer_response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import lombok.Getter;

@Getter
public class AuthenticationResponse {
    @SerializedName("idToken")
    @Expose
    private String idToken;
    @SerializedName("accessToken")
    @Expose
    private String accessToken;
    @SerializedName("refreshToken")
    @Expose
    private String refreshToken;
    @SerializedName("expiresIn")
    @Expose
    private Integer expiresIn;
    @SerializedName("tokenType")
    @Expose
    private String tokenType;
}
