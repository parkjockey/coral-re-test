package graphql.responses.consumers.confirm_passless_login_consumer_response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import lombok.Getter;

@Getter
public class ConfirmPasswordLessConsumerLoginResponse {
    @SerializedName("data")
    @Expose
    private Data data;
}
