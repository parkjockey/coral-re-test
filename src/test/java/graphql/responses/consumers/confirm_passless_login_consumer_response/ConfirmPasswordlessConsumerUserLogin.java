package graphql.responses.consumers.confirm_passless_login_consumer_response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import lombok.Getter;

@Getter
public class ConfirmPasswordlessConsumerUserLogin {
    @SerializedName("successfulAuthentication")
    @Expose
    private Boolean successfulAuthentication;
    @SerializedName("authenticationResponse")
    @Expose
    private AuthenticationResponse authenticationResponse;
    @SerializedName("session")
    @Expose
    private Object session;
}
