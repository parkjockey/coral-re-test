package graphql.responses.consumers.confirm_passless_login_consumer_response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import lombok.Getter;

@Getter
public class Data {
    @SerializedName("confirmPasswordlessConsumerUserLogin")
    @Expose
    private ConfirmPasswordlessConsumerUserLogin confirmPasswordlessConsumerUserLogin;
}
