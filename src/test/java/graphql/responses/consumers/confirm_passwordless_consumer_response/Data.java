package graphql.responses.consumers.confirm_passwordless_consumer_response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import lombok.Getter;

@Getter
public class Data {
    @SerializedName("confirmPasswordlessConsumerUserSignUp")
    @Expose
    private ConfirmPasswordlessConsumerUserSignUp confirmPasswordlessConsumerUserSignUp;
}
