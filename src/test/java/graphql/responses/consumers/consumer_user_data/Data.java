package graphql.responses.consumers.consumer_user_data;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import lombok.Getter;

@Getter
public class Data {
    @SerializedName("consumerUserData")
    @Expose
    private ConsumerUserData consumerUserData;
}
