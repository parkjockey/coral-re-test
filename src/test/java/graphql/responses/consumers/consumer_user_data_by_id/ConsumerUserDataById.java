package graphql.responses.consumers.consumer_user_data_by_id;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import lombok.Getter;

@Getter
public class ConsumerUserDataById {
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("consumerId")
    @Expose
    private Integer consumerId;
    @SerializedName("username")
    @Expose
    private String username;
    @SerializedName("isSsoUser")
    @Expose
    private Object isSsoUser;
    @SerializedName("givenName")
    @Expose
    private Object givenName;
    @SerializedName("familyName")
    @Expose
    private Object familyName;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("temporaryEmail")
    @Expose
    private Object temporaryEmail;
    @SerializedName("phoneNumber")
    @Expose
    private Object phoneNumber;
    @SerializedName("temporaryPhoneNumber")
    @Expose
    private Object temporaryPhoneNumber;
    @SerializedName("userStatus")
    @Expose
    private String userStatus;
    @SerializedName("lastModifiedDate")
    @Expose
    private String lastModifiedDate;
}
