package graphql.responses.consumers.consumer_user_data_by_id;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import lombok.Getter;
 @Getter
public class ConsumerUserDataByIdResponse {
     @SerializedName("data")
     @Expose
     private Data data;
}
