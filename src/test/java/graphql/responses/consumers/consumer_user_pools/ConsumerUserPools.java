package graphql.responses.consumers.consumer_user_pools;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import lombok.Getter;

import java.util.ArrayList;

@Getter
public class ConsumerUserPools {
    @SerializedName("userPools")
    @Expose
    private ArrayList<UserPool> userPools = null;
}
