package graphql.responses.consumers.consumer_user_pools;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import lombok.Getter;

@Getter
public class Data {
    @SerializedName("consumerUserPools")
    @Expose
    private ConsumerUserPools consumerUserPools;
}
