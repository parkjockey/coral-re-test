package graphql.responses.consumers.consumer_users_page;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import lombok.Getter;

@Getter
public class ConsumerUserPageResponse {
    @SerializedName("data")
    @Expose
    private Data data;
}
