package graphql.responses.consumers.consumer_users_page;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import lombok.Getter;

import java.util.ArrayList;

@Getter
public class ConsumerUsersPage {
    @SerializedName("content")
    @Expose
    private ArrayList<Content> content = null;
    @SerializedName("paginationToken")
    @Expose
    private Object paginationToken;
}
