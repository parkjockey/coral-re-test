package graphql.responses.consumers.passwordless_consumer_login_mediator;

import io.restassured.response.Response;

public class PasswordlessConsumerLoginResponse {

    public static String getCode (Response response){
        return response.jsonPath().getString("data.passwordlessConsumerUserLogin.code");
    }

    public static String getSession (Response response){
        return response.jsonPath().getString("data.passwordlessConsumerUserLogin.session");
    }
}
