package graphql.responses.consumers.passwordless_consumer_login_mediator;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class confirmPasswordlessConsumerUserLogin {
    @SerializedName("successfulAuthentication")
    @Expose
    private Boolean successfulAuthentication;
    @SerializedName("authenticationResponse")
    @Expose
    private authenticationResponse authenticationResponse;
    @SerializedName("session")
    @Expose
    private Object session;
}
