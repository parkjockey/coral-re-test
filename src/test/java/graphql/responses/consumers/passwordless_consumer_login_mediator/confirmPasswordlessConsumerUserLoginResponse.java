package graphql.responses.consumers.passwordless_consumer_login_mediator;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import lombok.Getter;

@Getter
public class confirmPasswordlessConsumerUserLoginResponse {
    @SerializedName("data")
    @Expose
    private data data;
}
