package graphql.responses.organization.add_org_role;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import lombok.Getter;

@Getter
public class AddOrganizationRole {
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("numberOfAssignedUsers")
    @Expose
    private Object numberOfAssignedUsers;
    @SerializedName("numberOfAssignedModules")
    @Expose
    private Object numberOfAssignedModules;
}
