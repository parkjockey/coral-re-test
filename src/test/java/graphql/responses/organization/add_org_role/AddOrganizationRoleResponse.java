package graphql.responses.organization.add_org_role;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import lombok.Getter;

@Getter
public class AddOrganizationRoleResponse {
    @SerializedName("data")
    @Expose
    private Data data;
}
