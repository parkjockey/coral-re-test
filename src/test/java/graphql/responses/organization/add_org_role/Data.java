package graphql.responses.organization.add_org_role;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import lombok.Getter;

@Getter
public class Data {
    @SerializedName("addOrganizationRole")
    @Expose
    private AddOrganizationRole addOrganizationRole;
}
