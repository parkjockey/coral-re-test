package graphql.responses.organization.find_all_modules;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import lombok.Getter;

@Getter
public class ModulesPageResponse {
    @SerializedName("data")
    @Expose
    private Data data;
}
