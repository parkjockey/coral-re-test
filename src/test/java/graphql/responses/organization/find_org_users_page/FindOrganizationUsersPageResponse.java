package graphql.responses.organization.find_org_users_page;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import lombok.Getter;

@Getter
public class FindOrganizationUsersPageResponse {
    @SerializedName("data")
    @Expose
    private Data data;
}
