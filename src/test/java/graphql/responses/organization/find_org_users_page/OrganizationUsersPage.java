package graphql.responses.organization.find_org_users_page;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import lombok.Getter;

import java.util.ArrayList;

@Getter
public class OrganizationUsersPage {
    @SerializedName("totalPages")
    @Expose
    private Integer totalPages;
    @SerializedName("totalItems")
    @Expose
    private Integer totalItems;
    @SerializedName("content")
    @Expose
    private ArrayList<Content> content = null;
}
