package graphql.responses.organization.find_role_by_id;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import lombok.Getter;

@Getter
public class Data {
    @SerializedName("findOrganizationRoleById")
    @Expose
    private FindOrganizationRoleById findOrganizationRoleById;
}
