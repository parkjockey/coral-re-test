package graphql.responses.organization.find_role_by_id;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import lombok.Getter;

@Getter
public class FindOrganizationRoleByIdResponse {
    @SerializedName("data")
    @Expose
    private Data data;
}
