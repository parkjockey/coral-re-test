package graphql.responses.organization.get_all_org_v2;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import lombok.Getter;

import java.util.List;

@Getter
public class Content {
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("logoUrl")
    @Expose
    private Object logoUrl;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("modules")
    @Expose
    private List<Module> modules = null;
}
