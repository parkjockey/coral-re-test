package graphql.responses.organization.get_all_org_v2;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import lombok.Getter;

import java.util.List;

@Getter
public class FindAllOrganizations {
    @SerializedName("totalPages")
    @Expose
    private Integer totalPages;
    @SerializedName("totalItems")
    @Expose
    private Integer totalItems;
    @SerializedName("content")
    @Expose
    private List<Content> content;
}
