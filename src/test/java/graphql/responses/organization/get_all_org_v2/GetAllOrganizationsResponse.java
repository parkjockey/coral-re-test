package graphql.responses.organization.get_all_org_v2;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import lombok.Getter;

@Getter
public class GetAllOrganizationsResponse {
    @SerializedName("data")
    @Expose
    private Data data;
}
