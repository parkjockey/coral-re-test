package graphql.responses.organization.get_org_roles;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import lombok.Getter;

@Getter
public class Content {
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("numberOfAssignedUsers")
    @Expose
    private Integer numberOfAssignedUsers;
    @SerializedName("numberOfAssignedModules")
    @Expose
    private Integer numberOfAssignedModules;
}
