package graphql.responses.organization.get_org_roles;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import lombok.Getter;

@Getter
public class Data {
    @SerializedName("organizationRolesPage")
    @Expose
    private OrganizationRolesPage organizationRolesPage;
}
