package graphql.responses.organization.get_org_roles;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import lombok.Getter;

import java.util.ArrayList;

@Getter
public class OrganizationRolesPage {
    @SerializedName("totalItems")
    @Expose
    private Integer totalItems;
    @SerializedName("totalPages")
    @Expose
    private Integer totalPages;
    @SerializedName("content")
    @Expose
    private ArrayList<Content> content = null;
}
