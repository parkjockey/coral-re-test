package graphql.responses.organization.org_user_data;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import lombok.Getter;

@Getter
public class Data {
    @SerializedName("organizationUserData")
    @Expose
    private OrganizationUserData organizationUserData;
}
