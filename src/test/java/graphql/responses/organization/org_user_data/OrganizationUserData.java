package graphql.responses.organization.org_user_data;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import lombok.Getter;

import java.util.ArrayList;

@Getter
public class OrganizationUserData {
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("username")
    @Expose
    private String username;
    @SerializedName("givenName")
    @Expose
    private String givenName;
    @SerializedName("familyName")
    @Expose
    private String familyName;
    @SerializedName("fullName")
    @Expose
    private String fullName;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("userStatus")
    @Expose
    private String userStatus;
    @SerializedName("invitedOnDate")
    @Expose
    private String invitedOnDate;
    @SerializedName("acceptedOnDate")
    @Expose
    private String acceptedOnDate;
    @SerializedName("roles")
    @Expose
    private ArrayList<Role> roles = null;
    @SerializedName("phoneNumber")
    @Expose
    private String phoneNumber;
    @SerializedName("profilePictureUrl")
    @Expose
    private String profilePictureUrl;
}
