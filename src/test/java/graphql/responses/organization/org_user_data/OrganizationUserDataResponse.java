package graphql.responses.organization.org_user_data;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import lombok.Getter;

@Getter
public class OrganizationUserDataResponse {
    @SerializedName("data")
    @Expose
    private Data data;
}
