package graphql.responses.organization.organization_modules;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import lombok.Getter;

@Getter
public class Data {
    @SerializedName("organizationModules")
    @Expose
    private OrganizationModules organizationModules;
}
