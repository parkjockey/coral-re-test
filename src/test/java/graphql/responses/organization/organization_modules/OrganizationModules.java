package graphql.responses.organization.organization_modules;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import lombok.Getter;

import java.util.ArrayList;

@Getter
public class OrganizationModules {
    @SerializedName("totalPages")
    @Expose
    private Integer totalPages;
    @SerializedName("totalItems")
    @Expose
    private Integer totalItems;
    @SerializedName("content")
    @Expose
    private ArrayList<Content> content = null;
}
