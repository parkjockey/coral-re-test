package graphql.responses.organization.organization_modules;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import lombok.Getter;

@Getter
public class OrganizationModulesResponse {
    @SerializedName("data")
    @Expose
    private Data data;
}
