package graphql.responses.organization.role_modules;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import lombok.Getter;

import java.util.ArrayList;

@Getter
public class Module {
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("numberOfAssignedModules")
    @Expose
    private Object numberOfAssignedModules;
    @SerializedName("numberOfModules")
    @Expose
    private Integer numberOfModules;
    @SerializedName("permissionLevel")
    @Expose
    private String permissionLevel;
    @SerializedName("subModules")
    @Expose
    private ArrayList<SubModules> subModules;
}
