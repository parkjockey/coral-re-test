package graphql.responses.organization.role_modules;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import lombok.Getter;

import java.util.List;

@Getter
public class OrganizationRoleModules {
    @SerializedName("modules")
    @Expose
    private List<Module> modules = null;
}
