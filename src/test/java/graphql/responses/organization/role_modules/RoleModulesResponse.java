package graphql.responses.organization.role_modules;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import lombok.Getter;

@Getter
public class RoleModulesResponse {
    @SerializedName("data")
    @Expose
    private Data data;
}
