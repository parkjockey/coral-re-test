package graphql.responses.organization.role_modules;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import lombok.Getter;

@Getter
public class SubSubModules {
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("permissionLevel")
    @Expose
    private String permissionLevel;
    @SerializedName("numberOfAssignedSubModules")
    @Expose
    private Integer numberOfAssignedSubModules;
    @SerializedName("numberOfModules")
    @Expose
    private Integer numberOfModules;
}
