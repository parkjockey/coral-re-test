package graphql.responses.organization.update_org_role;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import lombok.Getter;

@Getter
public class UpdateOrganizationRoleResponse {
    @SerializedName("data")
    @Expose
    private Data data;
}
