package graphql.responses.users;

import io.restassured.response.Response;

public class FindOrgUserByEmailResponse {

    public static String getUserId (Response response){
        return response.jsonPath().get("data.findOrganizationUserByEmail.id").toString();
    }

    public static String getUserName (Response response){
        return response.jsonPath().get("data.findOrganizationUserByEmail.username").toString();
    }
}
