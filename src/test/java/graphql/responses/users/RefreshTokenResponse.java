package graphql.responses.users;

import io.restassured.response.Response;

public class RefreshTokenResponse {

    public static String accessToken (Response response){
        return response.jsonPath().get("data.refreshTokens.accessToken");
    }

    public static String idToken (Response response){
        return response.jsonPath().get("data.refreshTokens.idToken");
    }

    public static String refreshToken (Response response){
        return response.jsonPath().get("data.refreshTokens.refreshToken");
    }

    public static String tokenType (Response response){
        return response.jsonPath().get("data.refreshTokens.tokenType");
    }

    public static Integer expiresIn (Response response){
        return response.jsonPath().get("data.refreshTokens.expiresIn");
    }
}
