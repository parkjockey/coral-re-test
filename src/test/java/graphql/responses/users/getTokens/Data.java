package graphql.responses.users.getTokens;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import lombok.Getter;

@Getter
public class Data {
    @SerializedName("getTokens")
    @Expose
    private GetTokens getTokens;

    public GetTokens getGetTokens() {
        return getTokens;
    }
}
