package graphql.responses.users.getTokens;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import lombok.Getter;

@Getter
public class GetTokensResponse {
    @SerializedName("data")
    @Expose
    private Data data;
}
