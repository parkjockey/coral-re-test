package graphql.responses.users.invite_users;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class InviteUserResponse {
    @SerializedName("data")
    @Expose
    private Data data;
}
