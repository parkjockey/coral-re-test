package graphql.responses.users.invite_users;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class InviteUsers {
    @SerializedName("numberOfInvited")
    @Expose
    private Integer numberOfInvited;
    @SerializedName("numberOfIgnored")
    @Expose
    private Integer numberOfIgnored;
    @SerializedName("numberOfInvitedAndAssignedToRole")
    @Expose
    private Integer numberOfInvitedAndAssignedToRole;
    @SerializedName("numberOfAssignedToRole")
    @Expose
    private Integer numberOfAssignedToRole;
}
