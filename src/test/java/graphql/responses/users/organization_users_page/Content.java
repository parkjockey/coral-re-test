package graphql.responses.users.organization_users_page;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import lombok.Getter;

import java.util.List;

@Getter
public class Content {
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("username")
    @Expose
    private String username;
    @SerializedName("isSsoUser")
    @Expose
    private Object isSsoUser;
    @SerializedName("givenName")
    @Expose
    private String givenName;
    @SerializedName("familyName")
    @Expose
    private String familyName;
    @SerializedName("fullName")
    @Expose
    private String fullName;
    @SerializedName("profilePictureUrl")
    @Expose
    private Object profilePictureUrl;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("temporaryEmail")
    @Expose
    private Object temporaryEmail;
    @SerializedName("phoneNumber")
    @Expose
    private Object phoneNumber;
    @SerializedName("userStatus")
    @Expose
    private String userStatus;
    @SerializedName("invitedOnDate")
    @Expose
    private String invitedOnDate;
    @SerializedName("acceptedOnDate")
    @Expose
    private String acceptedOnDate;
    @SerializedName("lastModifiedDate")
    @Expose
    private String lastModifiedDate;
    @SerializedName("roles")
    @Expose
    private List<Role> roles = null;
    @SerializedName("sourceSystemName")
    @Expose
    private String sourceSystemName;
    @SerializedName("sourceSystemUserIdentifier")
    @Expose
    private String sourceSystemUserIdentifier;
}
