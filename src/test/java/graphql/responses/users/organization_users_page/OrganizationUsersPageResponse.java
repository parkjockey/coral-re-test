package graphql.responses.users.organization_users_page;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import lombok.Getter;

@Getter
public class OrganizationUsersPageResponse {
    @SerializedName("data")
    @Expose
    private Data data;
}
