package graphql.responses.users.userAuthorizationData;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import lombok.Getter;

@Getter
public class Data {
    @SerializedName("userAuthorizationData")
    @Expose
    private UserAuthorizationData userAuthorizationData;
}
