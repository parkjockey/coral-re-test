package graphql.responses.users.userAuthorizationData;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import lombok.Getter;

@Getter
public class ModulePermission {
    @SerializedName("moduleName")
    @Expose
    private String moduleName;
    @SerializedName("permission")
    @Expose
    private String permission;
}
