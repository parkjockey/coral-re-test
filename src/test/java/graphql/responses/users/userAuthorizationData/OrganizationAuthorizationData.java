package graphql.responses.users.userAuthorizationData;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import lombok.Getter;

import java.util.List;

@Getter
public class OrganizationAuthorizationData {

    @SerializedName("organizationId")
    @Expose
    private String organizationId;
    @SerializedName("modulePermissions")
    @Expose
    private List<ModulePermission> modulePermissions = null;
}
