package graphql.responses.users.userAuthorizationData;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import lombok.Getter;

import java.util.List;

@Getter
public class UserAuthorizationData {
    @SerializedName("globalReefAdmin")
    @Expose
    private Boolean globalReefAdmin;
    @SerializedName("organizationAuthorizationData")
    @Expose
    private List<OrganizationAuthorizationData> organizationAuthorizationData = null;
}
