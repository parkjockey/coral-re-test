package graphql.responses.users.userAuthorizationData;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import lombok.Getter;

@Getter
public class UserAuthorizationDataResponse {
    @SerializedName("data")
    @Expose
    private Data data;
}
