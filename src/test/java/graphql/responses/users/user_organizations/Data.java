package graphql.responses.users.user_organizations;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import lombok.Getter;

@Getter
public class Data {
    @SerializedName("userOrganizations")
    @Expose
    private UserOrganizations userOrganizations;
}
