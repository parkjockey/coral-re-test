package graphql.responses.users.user_organizations;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import lombok.Getter;

import java.util.List;
@Getter
public class UserOrganizations {
    @SerializedName("content")
    @Expose
    private List<Content> content = null;
    @SerializedName("totalPages")
    @Expose
    private Integer totalPages;
    @SerializedName("totalItems")
    @Expose
    private Integer totalItems;
}
