package graphql.steps;

import common.helpers.aws_helpers.sqs.SQSHelpers;
import common.helpers.db_helpers.JdbcHelpers;
import common.helpers.mail_helpers.EmailHelpers;
import data_containers.GeneralData;
import data_containers.OrganizationData;
import data_containers.RoleData;
import data_containers.UserData;
import graphql.helpers.users.AuthenticationHelpers;
import io.cucumber.java8.En;
import org.assertj.core.api.SoftAssertions;

import java.util.concurrent.TimeUnit;

import static org.awaitility.Awaitility.await;

public class CommonGQLSteps implements En {
    GeneralData generalData;
    UserData userData;
    OrganizationData organizationData;
    RoleData roleData;
    SoftAssertions assertions;

    public CommonGQLSteps(GeneralData generalData, UserData userData, OrganizationData organizationData, RoleData roleData) {
        this.generalData = generalData;
        this.userData = userData;
        this.organizationData = organizationData;
        this.roleData = roleData;
        assertions = new SoftAssertions();

        Given("Reef organization id is obtained", () -> {
            organizationData.id = JdbcHelpers.getIntValueFromDb("select id from organization where name = 'REEF';", "id");
        });

        Given("admin accessToken is obtained", () -> {
            generalData.token = AuthenticationHelpers.getToken(generalData.adNoSSOUsername, generalData.adPassword);
            assertions.assertThat(generalData.token).isNotNull();
            assertions.assertAll();
        });

        Given("REEF Consumers organization id is obtained", () -> {
            organizationData.id = JdbcHelpers.getIntValueFromDb("select id from organization where name = 'REEF Consumers';", "id");
        });

        Then("Email is received with subject: {string}", (String subject) -> {
            await("Wait condition to be true")
                    .atMost(30, TimeUnit.SECONDS)
                    .pollDelay(5, TimeUnit.SECONDS)
                    .pollInterval(5, TimeUnit.SECONDS)
                    .until(() ->
                 //   EmailHelpers.waitMailToBeObtained(userData.email, subject).getSubject().equals(subject));
                            SQSHelpers.getSpecificMessage(userData.email, subject).contains(subject));

        });

        Then("Email is received with subject: {string}, body contains otp code", (String subject) -> {

//            final Message[] mail = {null};
//            await("Wait condition to be true")
//                    .atMost(20, TimeUnit.SECONDS)
//                    .pollDelay(2, TimeUnit.SECONDS)
//                    .pollInterval(2, TimeUnit.SECONDS)
//                    .until(() -> {
//                        mail[0] = EmailHelpers.waitMailToBeObtained(userData.email, subject);
//                        return !mail[0].equals(null);
//                    });
//            userData.otpCode = EmailHelpers.getOTP(mail[0].getContent().toString());

            userData.otpCode = EmailHelpers.getOTP(SQSHelpers.getSpecificMessage(userData.email, subject));

            assertions.assertThat(userData.otpCode).isNotNull();
            assertions.assertAll();
        });
    }
}
