package graphql.steps;

import com.google.gson.Gson;
import common.helpers.db_helpers.CQLQueries;
import common.helpers.redis.RedisHelpers;
import common.helpers.rest.HelperRest;
import data_containers.*;
import graphql.helpers.GraphQLHelpers;
import graphql.helpers.InviteUsersHelprs;
import graphql.responses.OrganizationResponse;
import graphql.responses.OrganizationUsersPage;
import graphql.responses.OrganizationsResponse;
import graphql.responses.organization.find_org_users_page.Content;
import graphql.responses.organization.find_org_users_page.FindOrganizationUsersPageResponse;
import graphql.responses.organization.get_all_org_v2.GetAllOrganizationsResponse;
import graphql.responses.organization.org_user_data.OrganizationUserData;
import graphql.responses.organization.org_user_data.OrganizationUserDataResponse;
import io.cucumber.java8.En;
import org.assertj.core.api.SoftAssertions;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;


public class GraphQlSteps implements En {
    GeneralData generalData;
    UserData userData;
    RoleData roleData;
    OrganizationData organizationData;
    OrganizationsData organizationsData;
    Gson gson;

    private SoftAssertions softAssertions = new SoftAssertions();
    private OrganizationResponse organizationResponse;
    private OrganizationsResponse organizationsResponse;
    private OrganizationUsersPage organizationUsersPage;


    public GraphQlSteps(GeneralData generalData, OrganizationsData organizationsData, OrganizationData organizationData, UserData userData, RoleData roleData) {

        this.generalData = generalData;
        this.userData = userData;
        this.roleData = roleData;
        this.organizationsData = organizationsData;
        this.organizationResponse = new OrganizationResponse(generalData);
        this.organizationsResponse = new OrganizationsResponse(generalData);
        this.organizationData= organizationData;
        organizationData.name = "OrgAuto"+ HelperRest.getRandomNumber();
        organizationData.description = "Description"+HelperRest.getRandomNumber();
        gson = new Gson();


        When("Get Organization by id GraphQl request with Reef id is sent", () -> {

            //GraphQLHelpers.CreateOrganizations(2);
            generalData.response = GraphQLHelpers.getOrganizationById(organizationData.id);
        });

        When("Get all Organizations GraphQl request is sent", ()-> {
            generalData.response = GraphQLHelpers.GetAllOrganizationsRequest();

//            System.out.println(generalData.response.asString());
//
//            System.out.println(generalData.response.jsonPath().getString("data.findAllOrganizations"));
//
//            graphql.request_objects.OrganizationsResponse or = gson.fromJson(generalData.response.jsonPath().getString("data.findAllOrganizations[0]"),
//                    graphql.request_objects.OrganizationsResponse.class);
//
//             System.out.println(or.getTotalPages());

        });

        When("An unauthorized get Organization by id GraphQl request is sent", () -> {
            generalData.response = GraphQLHelpers.GetOrganizationById401();
        });

        When("Add Organization GraphQl request is sent", () -> {
            generalData.response = GraphQLHelpers.addOrganization(organizationData.name, organizationData.description);
            organizationData.id = Integer.parseInt(organizationResponse.AddOrganizationGetId());
            softAssertions.assertThat(organizationData.id).isNotNull();
        });
        When("Get Organization Users GQL request is sent", () -> {

            generalData.response = GraphQLHelpers.GetOrganizationUsers(1,null, null, null);
            organizationsData.totalItems = organizationsResponse.GetTotalUsers();
        });

        When("Get Organization User Roles request is sent", () -> {

            generalData.response = GraphQLHelpers.GetOrganizationUserRoles(null, null);

        });

        When("New organization is created", () -> {

            organizationData.id = GraphQLHelpers.getCreatedOrganizationsId();
        });

        When("Get user by email request is sent", () -> {
            generalData.response = InviteUsersHelprs.getUserByEmail(organizationData.id, userData.email);
            userData.id = Integer.parseInt(generalData.response.jsonPath().get("data.findOrganizationUserByEmail.id"));
        });

        When("New role is added to the new organization", () -> {

            generalData.response = GraphQLHelpers.addRoleToOrganization();
            roleData.roleId = Integer.parseInt(generalData.response.jsonPath().get("data.addOrganizationRole.id"));

        });

        When("Get organization roles request is sent", () -> {

            generalData.response = GraphQLHelpers.getOrganizationRoles();
        });


        Then("Response contains the new role", () -> {

            softAssertions.assertThat(organizationResponse.getOrganizationRoles()).as("Organization contains the new role")
                    .contains(roleData.roleName);
            softAssertions.assertAll();
        });


//        Then("The response contains the expected role", () -> {
//
//            softAssertions.assertThat(organizationsResponse.getListOfUsersRoles()).as("list contains expected role")
//                    .contains("Global REEF Administrator");
//            softAssertions.assertAll();
//
//        });

        Then("Response contains a username", () -> {

            softAssertions.assertThat(organizationResponse.invitedUserHasUsername()).isTrue();
            softAssertions.assertThat(organizationResponse.inviteUserHasSourceSystemName()).isTrue();
            softAssertions.assertThat(organizationResponse.inviteUserHasSourceSystemUserIdentifier()).isTrue();
            softAssertions.assertAll();

        });



        Then("Add Organization response contains the same data as the request", () -> {
            organizationData.id = Integer.parseInt(organizationResponse.AddOrganizationGetId());
            softAssertions.assertThat(organizationResponse.AddOrganizationGetId()).as("Id is present")
                    .isNotNull();
            softAssertions.assertThat(organizationResponse.AddOrganizationGetName())
                    .as("Response name is the same as name in Request").isEqualTo(organizationData.name);
            softAssertions.assertThat(organizationResponse.AddOrganizationGetDescription())
                    .as("Response description is the same as description in Request").isEqualTo(organizationData.description);
            softAssertions.assertAll();
        });

        Then("Find Organization response name is {string}", (String name) -> {
            assertThat("Response name is ", organizationResponse.FindOrganizationGetName(), is(name));
        });

        Then("Find Organization response description is {string}", (String description) -> {
            assertThat("Response description is ", organizationResponse.FindOrganizationGetDescription(), is(description));
        });



        Then("Get all Organizations Response contains new Organization", () -> {

            List<Object> orgNames = organizationsResponse.getOrganizationsNames();

            softAssertions.assertThat(orgNames).as("New organization exists!")
                    .contains(organizationData.name);
            softAssertions.assertAll();
        });

        Then("The Response contains what is expected", () -> {
            softAssertions.assertThat(organizationsResponse.getOrganizationUsersNames().contains("Test - High level admin"));
        });
//        Users should first be assigned to the organization

        Then("The response is paginated correctly", () -> {

            for (int i = 1; i < 10; i++) {

                generalData.response = GraphQLHelpers.GetOrganizationUsers(1,null, i, null);
                softAssertions.assertThat(i).as("Page size is " + i)
                        .isEqualTo(organizationsResponse.getNumberOfOrganizationUsers());
            }
            softAssertions.assertAll();
        });


        Then("GraphQl Response status code is {int}", (Integer statusCode) -> {
            assertThat("Response has status code is ", organizationResponse.GetResponseStatus(), is(statusCode));
        });

        Then("GraphQl response message is {string}", (String message) -> {
            assertThat("Response message is ", generalData.response.jsonPath().get("errors.message").toString(), is(message));
        });

        When("OrganizationUserData query is called with obtained token( again)", () -> {
            generalData.response = GraphQLHelpers.getOrganizationUserData(generalData.gqlBaseUri, userData.token, organizationData.id.toString());
        });

        Then("Response contains user's data and desired role for user which token is used in request", () -> {
            OrganizationUserDataResponse userDataResponse = gson.fromJson(generalData.response.asString(), OrganizationUserDataResponse.class);

            OrganizationUserData data = userDataResponse.getData().getOrganizationUserData();
            userData.userName = data.getUsername();
            softAssertions.assertThat(data.getId()).as("Id is not null").isNotNull();
            softAssertions.assertThat(data.getUsername()).as("Usename is not null").isNotNull();
            softAssertions.assertThat(data.getGivenName()).as("First name is correct").isEqualTo(userData.firstName);
            softAssertions.assertThat(data.getFamilyName()).as("Last name is correct").isEqualTo(userData.lastName);
            softAssertions.assertThat(data.getFullName()).as("Full name is correct").isEqualTo(userData.firstName+" "+userData.lastName);
            softAssertions.assertThat(data.getEmail()).as("Email is correct").isEqualTo(userData.email.toLowerCase());
            softAssertions.assertThat(data.getUserStatus()).as("Status is correct").isEqualTo("ACTIVE");
            softAssertions.assertThat(data.getInvitedOnDate()).as("Invitation date is not null").isNotNull();
            softAssertions.assertThat(data.getAcceptedOnDate()).as("Accepted date is not null").isNotNull();
            softAssertions.assertThat(data.getProfilePictureUrl()).isNull();
            softAssertions.assertThat(data.getRoles().get(0).getId()).as("RoleId is correct").isEqualTo(roleData.roleId.toString());
            softAssertions.assertThat(data.getRoles().get(0).getName()).as("RoleName is notNull").isNotNull();
            softAssertions.assertThat(data.getPhoneNumber()).as("Phone number field is introduced but it is empty").isNull();
            softAssertions.assertAll();
        });

        When("OrganizationUserData query is called by Reef user for newly created organization", () -> {
            generalData.response = GraphQLHelpers.getOrganizationUserData(generalData.gqlBaseUri, userData.token, organizationData.id.toString());
        });

        When("Get Organization Users GQL request is sent with newly created user", () -> {
          generalData.response = GraphQLHelpers.GetOrganizationUsers(organizationData.id, userData.email, null, null);
        });

        Then("The Response contains userData", () -> {
            FindOrganizationUsersPageResponse resp = gson.fromJson(generalData.response.asString(), FindOrganizationUsersPageResponse.class);
            Integer totalPages = resp.getData().getOrganizationUsersPage().getTotalPages();
            Integer totalItems = resp.getData().getOrganizationUsersPage().getTotalItems();
            Content user = resp.getData().getOrganizationUsersPage().getContent().get(0);
            //Assert user data
            softAssertions.assertThat(totalPages).isEqualTo(1);
            softAssertions.assertThat(totalItems).isEqualTo(1);
            softAssertions.assertThat(user.getEmail()).isEqualTo(userData.email.toLowerCase());
            softAssertions.assertThat(user.getId()).isNotNull();
            softAssertions.assertThat(user.getUsername()).isNotNull();
            softAssertions.assertThat(user.getFamilyName()).isEqualTo(userData.lastName);
            softAssertions.assertThat(user.getFullName()).isEqualTo(userData.firstName+" "+userData.lastName);
            softAssertions.assertThat(user.getIsSsoUser()).isNull();
            softAssertions.assertThat(user.getProfilePictureUrl()).isNull();
            softAssertions.assertThat(user.getUserStatus()).isEqualTo("ACTIVE");
            softAssertions.assertThat(user.getInvitedOnDate()).isNotNull();
            softAssertions.assertThat(user.getAcceptedOnDate()).isNotNull();
            softAssertions.assertThat(user.getRoles().get(0).getId()).isEqualTo(roleData.roleId.toString());
            softAssertions.assertThat(user.getRoles().get(0).getName()).isEqualTo(roleData.roleName);
            softAssertions.assertAll();


        });

        When("OrganizationUserData query is called by Reef Global Admin user for newly created organization", () -> {
            generalData.response = GraphQLHelpers.getOrganizationUserData(generalData.gqlBaseUri, generalData.token, organizationData.id.toString());
        });

        Then("User data is stored in redis without permission level {string} or {string}", (String level1, String level2) -> {
            Map<String, String> redisMap = RedisHelpers.getFromRedis(RedisHelpers.prefixUserAuthData+userData.userName);
            softAssertions.assertThat(userData.userName).isEqualTo(redisMap.get("username"));
            softAssertions.assertThat(userData.id.toString()).isEqualTo(redisMap.get("userId"));
            softAssertions.assertThat(redisMap.toString()).doesNotContain(level1);
            softAssertions.assertThat(redisMap.toString()).doesNotContain(level2);
            softAssertions.assertAll();
        });

        Then("Response contains userName", () -> {
            OrganizationUserDataResponse userDataResponse = gson.fromJson(generalData.response.asString(), OrganizationUserDataResponse.class);

            OrganizationUserData data = userDataResponse.getData().getOrganizationUserData();
            userData.userName = data.getUsername();
        });

        Then("User data is stored in redis without permission level {string} or {string} and without {string} module", (String level1, String level2, String module) -> {
            Map<String, String> redisMap = RedisHelpers.getFromRedis(RedisHelpers.prefixUserAuthData+userData.userName);
            softAssertions.assertThat(userData.userName).isEqualTo(redisMap.get("username"));
            softAssertions.assertThat(userData.id.toString()).isEqualTo(redisMap.get("userId"));
            softAssertions.assertThat(redisMap.toString()).doesNotContain(level1);
            softAssertions.assertThat(redisMap.toString()).doesNotContain(level2);
            softAssertions.assertThat(redisMap.toString()).doesNotContain(module);
            softAssertions.assertAll();
        });

        When("FindAllOrganizations query is called for that organization", () -> {
            generalData.response = GraphQLHelpers.findAllOrganizationV2(generalData.token, organizationData.name, null, null);

        });

        When("Random organization is created", () -> {
            HashMap<String, String> org = GraphQLHelpers.createRandomOrganization();

            organizationData.name = org.get("name");
            organizationData.description = org.get("description");
            organizationData.id = Integer.parseInt(org.get("id"));
        });

        Then("Response contains all organization information and assigned parent modules", () -> {
            GetAllOrganizationsResponse response = gson.fromJson(generalData.response.asString(), GetAllOrganizationsResponse.class);
            graphql.responses.organization.get_all_org_v2.Content content = response.getData().getFindAllOrganizations().getContent().get(0);

            softAssertions.assertThat(content.getId()).isEqualTo(organizationData.id.toString());
            softAssertions.assertThat(content.getName()).isEqualTo(organizationData.name);
            softAssertions.assertThat(content.getDescription()).isEqualTo(organizationData.description);
            softAssertions.assertThat(content.getType()).isEqualTo("TENANT");
            softAssertions.assertAll();
        });

        Then("Organization is stored in HMT db via kafka", () -> {
            Integer hmtOrgId = CQLQueries.getOrganizationId(organizationData.id);
            softAssertions.assertThat(hmtOrgId).isEqualTo(organizationData.id);
            softAssertions.assertAll();
        });

    }
}
