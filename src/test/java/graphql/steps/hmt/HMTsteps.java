package graphql.steps.hmt;

import common.helpers.db_helpers.CQLQueries;
import data_containers.GeneralData;
import data_containers.OrganizationData;
import data_containers.hmt.LocationData;
import graphql.helpers.hmt.HmtHelpers;
import io.cucumber.java8.En;
import org.assertj.core.api.SoftAssertions;

public class HMTsteps implements En {
    LocationData locationData;
    GeneralData generalData;
    OrganizationData organizationData;
    SoftAssertions assertions;

    public HMTsteps(LocationData locationData, GeneralData generalData, OrganizationData organizationData) {
        this.locationData = locationData;
        this.generalData = generalData;
        this.organizationData = organizationData;
        assertions = new SoftAssertions();

        Given("HMT test method", () -> {
            locationData.realEstateId = "US-SC-RE-1202800";
            locationData.locationName = "IamLocation";
            System.out.println("Created location " + CQLQueries.createLocation(locationData.realEstateId, locationData.locationName));
            System.out.println("Get location " + CQLQueries.getRealEstateId(locationData.realEstateId));

        });

        When("HierarchyRootLevels query is sent", () -> {
            generalData.response = HmtHelpers.hierarchyRootLevels(generalData.token, organizationData.id);
        });

        Then("Response contains all root levels hierarchies", () -> {
            assertions.assertThat(generalData.response.jsonPath().get("data.hierarchyRootLevels[0].id").toString()).isNotNull();
            assertions.assertThat(generalData.response.jsonPath().get("data.hierarchyRootLevels[0].name").toString()).isEqualTo("Parking Level Hierarchy");
            assertions.assertAll();
        });
    }
}
