package graphql.steps.organization;

import com.google.gson.Gson;
import data_containers.GeneralData;
import data_containers.OrganizationData;
import data_containers.RoleData;
import data_containers.UserData;
import graphql.helpers.GraphQLHelpers;
import io.cucumber.java8.En;
import org.assertj.core.api.SoftAssertions;

public class FindOrganizationByNameSteps implements En {
    GeneralData generalData;
    UserData userData;
    OrganizationData organizationData;
    RoleData roleData;
    SoftAssertions assertions;
    Gson gson;

    public FindOrganizationByNameSteps(GeneralData generalData, UserData userData, OrganizationData organizationData, RoleData roleData) {
        this.generalData = generalData;
        this.userData = userData;
        this.organizationData = organizationData;
        this.roleData = roleData;
        assertions = new SoftAssertions();
        gson = new Gson();

        When("FindOrganizationByName query is called", () -> {
            generalData.response = GraphQLHelpers.findOrganizationByName(generalData.token, organizationData.name);
        });

        Then("Response contains organization name", () -> {
            assertions.assertThat(generalData.response
                    .jsonPath().get("data.findOrganizationName.name").toString()).isEqualTo(organizationData.name);
            assertions.assertAll();
        });
    }
}
