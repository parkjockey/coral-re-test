package graphql.steps.organization;

import com.google.gson.Gson;
import common.helpers.db_helpers.SQLQueries;
import data_containers.GeneralData;
import data_containers.OrganizationData;
import data_containers.RoleData;
import data_containers.UserData;
import graphql.helpers.organization.OrganizationRoleModulesHelpers;
import graphql.responses.organization.find_all_modules.Content;
import graphql.responses.organization.find_all_modules.ModulesPageResponse;
import graphql.responses.organization.organization_modules.OrganizationModulesResponse;
import graphql.responses.organization.role_modules.SubModules;
import graphql.variables.update_org_role_modules.Module;
import graphql.variables.update_org_role_modules.SubSubModules;
import io.cucumber.java8.En;
import org.assertj.core.api.SoftAssertions;

import java.util.ArrayList;

public class OrganizationRoleModulesSteps implements En {
    GeneralData generalData;
    UserData userData;
    OrganizationData organizationData;
    RoleData roleData;
    SoftAssertions assertions;
    Gson gson;
    ArrayList<String> moduleIds;
    ArrayList<Integer> moduleIdsInt;
    SubModules testSubModule;
    graphql.responses.organization.role_modules.Module testModule;

    public OrganizationRoleModulesSteps(GeneralData generalData, UserData userData, OrganizationData organizationData, RoleData roleData) {
        this.generalData = generalData;
        this.userData = userData;
        this.organizationData = organizationData;
        this.roleData = roleData;
        assertions = new SoftAssertions();
        gson = new Gson();
        moduleIdsInt = new ArrayList<>();

        When("OrganizationRoleModules query is called", () -> {
            generalData.response = OrganizationRoleModulesHelpers.roleModules(generalData.token, organizationData.id,
                    roleData.roleId);
        });

        Then("Response contains list of modules and submodules and their names and id are not null", () -> {
            moduleIds = OrganizationRoleModulesHelpers.getAllModulesIds(generalData.response);
            ArrayList<String> moduleName = OrganizationRoleModulesHelpers.getAllModulesNames(generalData.response);
            //Check if all module ids are not null
            for (String id : moduleIds) {
                assertions.assertThat(id).isNotNull();
            }
            //Check if all module names are not null
            for (String name : moduleName) {
                assertions.assertThat(name).isNotNull();
            }

            //Check Test submodule and test sub submodule
            testSubModule = OrganizationRoleModulesHelpers
                    .getSubmodulesListByModuleName(generalData.response, "Test module").get(0);
            assertions.assertThat(testSubModule.getId()).isNotNull();
            assertions.assertThat(testSubModule.getName()).isNotNull();
            assertions.assertThat(testSubModule.getSubModules().get(0).getId()).isNotNull();
            assertions.assertThat(testSubModule.getSubModules().get(0).getName()).isNotNull();

            //Check Real Estate
            for (SubModules s : OrganizationRoleModulesHelpers
                    .getSubmodulesListByModuleName(generalData.response, "Real Estate")) {
                assertions.assertThat(s.getName()).isNotNull();
                assertions.assertThat(s.getId()).isNotNull();
            }
        });

        Given("All module ids are obtained and ids for Test submodule and sub sub module", () -> {
            generalData.response = OrganizationRoleModulesHelpers.roleModules(generalData.token, organizationData.id,
                    roleData.roleId);
            moduleIds = OrganizationRoleModulesHelpers.getAllModulesIds(generalData.response);
            testModule = OrganizationRoleModulesHelpers
                    .getModuleByName(generalData.response, "Test module");
        });

        When("UpdateOrganizationRoleModules mutation is called tu update all modules and test submodule and test sub sub module to {string} level", (String level) -> {
            ArrayList<graphql.variables.update_org_role_modules.Module> modules = new ArrayList<>();
            //add all module ids
            for (String id : moduleIds) {
                graphql.variables.update_org_role_modules.Module m = new Module();
                m.setId(Integer.parseInt(id));
                m.setPermissionLevel(level);
                modules.add(m);

            }
            //Remove Test module so we can insert it with all submodules
            modules.removeIf(t -> t.getId() == Integer.parseInt(testModule.getId()));
            //Set OWNER status for Test sub modules and sub submodule
            graphql.variables.update_org_role_modules.Module testModuleVariables = new Module();
            testModuleVariables.setId(Integer.parseInt(testModule.getId()));
            testModuleVariables.setPermissionLevel(level);
            graphql.variables.update_org_role_modules.SubModules sub = new graphql.variables.update_org_role_modules.SubModules();
            sub.setId(testModule.getSubModules().get(0).getId());
            sub.setPermissionLevel(level);
            graphql.variables.update_org_role_modules.SubSubModules subsub = new SubSubModules();
            subsub.setId(testModule.getSubModules().get(0).getSubModules().get(0).getId());
            subsub.setPermissionLevel(level);
            sub.subModules.add(subsub);
            testModuleVariables.subModules.add(sub);

            modules.add(testModuleVariables);

            generalData.response = OrganizationRoleModulesHelpers
                    .updateRoleModules(generalData.token, organizationData.id, roleData.roleId, modules);

        });
        Then("Response contains boolean true", () -> {
            assertions.assertThat(generalData.response.jsonPath().get("data.updateOrganizationRoleModules").toString()).isEqualTo("true");
            assertions.assertAll();
        });

        When("Module Pages query is called", () -> {
            generalData.response = OrganizationRoleModulesHelpers.findAllModules(generalData.token, null, null, null);
        });

        Then("Response contains modules list", () -> {
            ModulesPageResponse response = gson.fromJson(generalData.response.asString(), ModulesPageResponse.class);

            assertions.assertThat(response.getData().getModulesPage().getTotalItems()).isNotNull();
            assertions.assertThat(response.getData().getModulesPage().getTotalPages()).isNotNull();

            for (Content c : response.getData().getModulesPage().getContent()) {
                assertions.assertThat(c.getId()).isNotNull();
                assertions.assertThat(c.getName()).isNotNull();
                if (c.getName().equals("Test module")) {
                    c.getSubModules().get(0).getName().equals("Test child module");
                    assertions.assertThat(c.getSubModules().get(0).getId()).isNotNull();
                    c.getSubModules().get(0).getSubModules().get(0).getName().equals("Test grandchild module");
                    assertions.assertThat(c.getSubModules().get(0).getSubModules().get(0).getId()).isNotNull();
                }
            }
            assertions.assertAll();

        });

        Given("All level one module ids are obtained", () -> {
            //Call find all modules to collect ids
            generalData.response = OrganizationRoleModulesHelpers.findAllModules(generalData.token, null, null, null);
            moduleIdsInt = new ArrayList<>();
            ModulesPageResponse response = gson.fromJson(generalData.response.asString(), ModulesPageResponse.class);
            //collect level one ids
            for (Content c : response.getData().getModulesPage().getContent()) {
               if(!c.getName().equals("Organizations")) moduleIdsInt.add(Integer.parseInt(c.getId()));
            }
            assertions.assertThat(moduleIdsInt.size() > 0).isTrue();
        });

        When("AssignModules mutation is called", () -> {
            generalData.response = OrganizationRoleModulesHelpers.assignModulesToOrg(generalData.token, moduleIdsInt, organizationData.id);
        });

        Then("AssignModules Response contains boolean true", () -> {
            assertions.assertThat(generalData.response.jsonPath().get("data.assignModules").toString()).isEqualTo("true");

        });

        When("Organization modules query is called", () -> {
            generalData.response = OrganizationRoleModulesHelpers.organizationModules(generalData.token, organizationData.id.toString(), null, null);
        });

        Then("Organization module response contains modules list", () -> {
            OrganizationModulesResponse response = gson.fromJson(generalData.response.asString(), OrganizationModulesResponse.class);

            assertions.assertThat(response.getData().getOrganizationModules().getTotalItems()).isNotNull();
            assertions.assertThat(response.getData().getOrganizationModules().getTotalPages()).isNotNull();

            for (graphql.responses.organization.organization_modules.Content c : response.getData().getOrganizationModules().getContent()) {
                assertions.assertThat(c.getId()).isNotNull();
                assertions.assertThat(c.getName()).isNotNull();
                if (c.getName().equals("Test module")) {
                    c.getSubModules().get(0).getName().equals("Test child module");
                    assertions.assertThat(c.getSubModules().get(0).getId()).isNotNull();
                    c.getSubModules().get(0).getSubModules().get(0).getName().equals("Test grandchild module");
                    assertions.assertThat(c.getSubModules().get(0).getSubModules().get(0).getId()).isNotNull();
                }
            }
            assertions.assertAll();
        });

        Given("{string} module is assigned to organization", (String moduleName) -> {
            Integer moduleId = SQLQueries.findModuleId(moduleName);
            assertions.assertThat(moduleId).isNotNull();
            assertions.assertAll();
            moduleIdsInt.clear();
            moduleIdsInt.add(moduleId);
            generalData.response = OrganizationRoleModulesHelpers.assignModulesToOrg(generalData.token, moduleIdsInt, organizationData.id);
            assertions.assertThat(generalData.response).isNotNull();

        });


        Given("{string} module is assigned to role with permission level {string}", (String moduleName, String permisssionLevel) -> {
            //Obtain module id
            Integer moduleId = SQLQueries.findModuleId(moduleName);
            assertions.assertThat(moduleId).isNotNull();
            assertions.assertAll();
            //Set modules with permission level
            ArrayList<graphql.variables.update_org_role_modules.Module> modules = new ArrayList<>();
            graphql.variables.update_org_role_modules.Module m = new Module();
            m.setId(moduleId);
            m.setPermissionLevel(permisssionLevel);
            modules.add(m);

            generalData.response = OrganizationRoleModulesHelpers
                    .updateRoleModules(generalData.token, organizationData.id, roleData.roleId, modules);

        });


        Given("Organizations module id is obtained", () -> {
            //Call find all modules to collect ids
            generalData.response = OrganizationRoleModulesHelpers.findAllModules(generalData.token, null, null, null);
            moduleIdsInt = new ArrayList<>();
            ModulesPageResponse response = gson.fromJson(generalData.response.asString(), ModulesPageResponse.class);
            //collect Organizations module id
            for (Content c : response.getData().getModulesPage().getContent()) {
                if(c.getName().equals("Organizations")) moduleIdsInt.add(Integer.parseInt(c.getId()));
            }
            assertions.assertThat(moduleIdsInt.size() > 0).isTrue();
        });

        Given("{string} module with {string} and {string} submodules is assigned to role with permission level {string}", (String moduleName, String subModule1, String subModule2, String permisssionLevel) -> {
            //Obtain module id and submodules
            Integer moduleId = SQLQueries.findModuleId(moduleName);
            Integer submodule1Id = SQLQueries.findModuleId(subModule1);
            Integer submodule2Id = SQLQueries.findModuleId(subModule2);
            assertions.assertThat(moduleId).isNotNull();
            assertions.assertThat(submodule1Id).isNotNull();
            assertions.assertThat(submodule2Id).isNotNull();
            assertions.assertAll();
            //Set modules with permission level
            ArrayList<graphql.variables.update_org_role_modules.Module> modules = new ArrayList<>();
            graphql.variables.update_org_role_modules.Module m = new Module();
            m.setId(moduleId);
            m.setPermissionLevel(permisssionLevel);
            graphql.variables.update_org_role_modules.SubModules sub1 = new graphql.variables.update_org_role_modules.SubModules();
            sub1.setId(submodule1Id);
            sub1.setPermissionLevel(permisssionLevel);
            graphql.variables.update_org_role_modules.SubModules sub2 = new graphql.variables.update_org_role_modules.SubModules();
            sub2.setId(submodule2Id);
            sub2.setPermissionLevel(permisssionLevel);
            m.getSubModules().add(sub1);
            m.getSubModules().add(sub2);
            modules.add(m);

            generalData.response = OrganizationRoleModulesHelpers
                    .updateRoleModules(generalData.token, organizationData.id, roleData.roleId, modules);
        });
    }


}
