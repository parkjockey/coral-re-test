package graphql.steps.organization;

import com.google.gson.Gson;
import common.helpers.db_helpers.JdbcHelpers;
import data_containers.GeneralData;
import data_containers.OrganizationData;
import data_containers.RoleData;
import data_containers.UserData;
import graphql.helpers.GraphQLHelpers;
import graphql.helpers.organization.OrganizationRoleHelpers;
import graphql.helpers.organization.OrganizationRoleModulesHelpers;
import graphql.helpers.users.AuthenticationHelpers;
import graphql.responses.OrganizationResponse;
import graphql.responses.OrganizationsResponse;
import graphql.responses.organization.add_org_role.AddOrganizationRole;
import graphql.responses.organization.add_org_role.AddOrganizationRoleResponse;
import graphql.responses.organization.find_role_by_id.FindOrganizationRoleById;
import graphql.responses.organization.find_role_by_id.FindOrganizationRoleByIdResponse;
import graphql.responses.organization.get_org_roles.Content;
import graphql.responses.organization.get_org_roles.GetOrganizationPageResponse;
import graphql.responses.organization.update_org_role.UpdateOrganizationRole;
import graphql.responses.organization.update_org_role.UpdateOrganizationRoleResponse;
import graphql.variables.update_org_role_modules.Module;
import io.cucumber.java8.En;
import org.assertj.core.api.SoftAssertions;

import java.util.ArrayList;

public class OrganizationRolesSteps implements En {
    GeneralData generalData;
    RoleData roleData;
    UserData userData;
    OrganizationData organizationData;
    OrganizationResponse organizationResponse;
    graphql.responses.organization.role_modules.Module orgModule;
    private SoftAssertions softAssertions = new SoftAssertions();
    private Gson gson;

    public OrganizationRolesSteps(GeneralData generalData, OrganizationData organizationData, OrganizationsResponse organizationsResponse, RoleData roleData, UserData userData) {

        this.generalData = generalData;
        this.roleData = roleData;
        this.userData = userData;
        this.organizationData = organizationData;
        this.organizationResponse = new OrganizationResponse(generalData);
        gson = new Gson();


        When("Get organization roles request is sent for Reef organization", () -> {

            generalData.response = GraphQLHelpers.getOrganizationRoles();
        });

        When("New role is added to Reef Enterprise", () -> {
            organizationData.id = JdbcHelpers.getIntValueFromDb("select id from organization where name = 'REEF';", "id");
                     generalData.response = GraphQLHelpers.addRoleToOrganization();
            roleData.roleId = Integer.parseInt(generalData.response.jsonPath().get("data.addOrganizationRole.id"));
        });

        When("{int} new roles are added to the new organization", (Integer numberOfRoles) -> {

            for (int i = 0; i < numberOfRoles; i++) {

                generalData.response = GraphQLHelpers.addRoleToOrganization();
            }
        });

        When("New role is updated", () -> {
            generalData.response = GraphQLHelpers.updateOrganizationRole();
        });

        When("New role is deleted", () -> {
            generalData.response = GraphQLHelpers.deleteOrganizationRole();
        });

        Then("Response contains deleteOrganizationRole true", () -> {

            softAssertions.assertThat(generalData.response.jsonPath().get("data.deleteOrganizationRole").toString()).isEqualTo("true");
            softAssertions.assertAll();
        });

        Then("Response contains the correct role details", () -> {
            AddOrganizationRoleResponse response = gson.fromJson(generalData.response.asString(), AddOrganizationRoleResponse.class);
            AddOrganizationRole addedRole = response.getData().getAddOrganizationRole();
            softAssertions.assertThat(addedRole.getName()).as("Organization contains the new role")
                    .contains(roleData.roleName);
            softAssertions.assertThat(addedRole.getId()).isNotNull();
            softAssertions.assertThat(addedRole.getDescription()).isEqualTo(roleData.roleDescription);
            softAssertions.assertThat(addedRole.getNumberOfAssignedModules()).isNull();
            softAssertions.assertThat(addedRole.getNumberOfAssignedUsers()).isNull();
            softAssertions.assertAll();
        });

        Then("Response contains updated role", () -> {
            UpdateOrganizationRoleResponse response = gson.fromJson(generalData.response.asString(), UpdateOrganizationRoleResponse.class);
            UpdateOrganizationRole updatedRole = response.getData().getUpdateOrganizationRole();
            softAssertions.assertThat(updatedRole.getName()).as("Organization contains the new role")
                    .contains(roleData.roleName);

            softAssertions.assertThat(updatedRole.getId()).isEqualTo(roleData.roleId.toString());
            softAssertions.assertThat(updatedRole.getDescription()).isEqualTo(roleData.roleDescription);
            softAssertions.assertThat(updatedRole.getNumberOfAssignedModules()).isNull();
            softAssertions.assertThat(updatedRole.getNumberOfAssignedUsers()).isNull();
            softAssertions.assertAll();
        });


        Then("The response contains the expected role", () -> {

            softAssertions.assertThat(organizationsResponse.getListOfUsersRoles()).as("list contains expected role")
                    .contains("Global REEF Administrator");
            softAssertions.assertAll();

        });

        Given("Activated user obtains token", () -> {
            userData.token = AuthenticationHelpers.getToken(userData.email, userData.password);
            softAssertions.assertThat(userData.token).isNotNull();
            softAssertions.assertAll();
         });

        Then("Response list of roles with all desired fields", () -> {
            GetOrganizationPageResponse response = gson.fromJson(generalData.response.asString(), GetOrganizationPageResponse.class);
            ArrayList<Content> rolesList = response.getData().getOrganizationRolesPage().getContent();

            //Check that every role has not null values for all all fields
            for (Content role : rolesList) {
                softAssertions.assertThat(role.getId()).isNotNull();
                softAssertions.assertThat(role.getDescription()).isNotNull();
                softAssertions.assertThat(role.getName()).isNotNull();
                softAssertions.assertThat(role.getStatus()).isEqualTo("ACTIVE");
                softAssertions.assertThat(role.getNumberOfAssignedModules()).isNotNull();
                softAssertions.assertThat(role.getNumberOfAssignedUsers()).isNotNull();
                softAssertions.assertAll();
            }

            //Check if response contains total number of items and pages
            softAssertions.assertThat(response.getData().getOrganizationRolesPage().getTotalPages()).isNotNull();
            softAssertions.assertThat(response.getData().getOrganizationRolesPage().getTotalItems()).isNotNull();
            softAssertions.assertAll();

        });

        Given("Users module is assigned to the role", () -> {
            //Obtain Users module information
            generalData.response = OrganizationRoleModulesHelpers.roleModules(generalData.token, organizationData.id,
                    roleData.roleId);
            orgModule = OrganizationRoleModulesHelpers
                    .getModuleByName(generalData.response, "Organizations");

            //Assign module to the role
            ArrayList<graphql.variables.update_org_role_modules.Module> modules = new ArrayList<>();
            graphql.variables.update_org_role_modules.Module m = new Module();
            m.setId(Integer.parseInt(orgModule.getId()));
            m.setPermissionLevel("OWNER");
            modules.add(m);

            generalData.response = OrganizationRoleModulesHelpers
                    .updateRoleModules(generalData.token, organizationData.id, roleData.roleId, modules);
        });

        When("FindOrganizationRoleById query is called", () -> {
            generalData.response = OrganizationRoleHelpers
                    .findOrganizationRoleById(generalData.token, organizationData.id, roleData.roleId);
        });

        Then("response contains all role detail with correct number of assigned modules and users", () -> {
            FindOrganizationRoleByIdResponse response =
                    gson.fromJson(generalData.response.asString(), FindOrganizationRoleByIdResponse.class);
            FindOrganizationRoleById role = response.getData().getFindOrganizationRoleById();

            softAssertions.assertThat(role.getId()).isEqualTo(roleData.roleId.toString());
            softAssertions.assertThat(role.getName()).isEqualTo(roleData.roleName);
            softAssertions.assertThat(role.getDescription()).isEqualTo(roleData.roleDescription);
            softAssertions.assertThat(role.getStatus()).isEqualTo("ACTIVE");
            softAssertions.assertThat(role.getNumberOfAssignedModules()).isEqualTo(1);
            softAssertions.assertThat(role.getNumberOfAssignedUsers()).isEqualTo(1);
            softAssertions.assertAll();
        });

        When("FindOrganizationRoleName query is called", () -> {
            generalData.response = OrganizationRoleHelpers.findOrganizationRoleName(generalData.token,
                    organizationData.id, roleData.roleName);
        });

        Then("Response contains role name", () -> {
            softAssertions.assertThat(generalData.response
                    .jsonPath().get("data.findOrganizationRoleName.name").toString()).isEqualTo(roleData.roleName);
            softAssertions.assertAll();
        });


    }
}
