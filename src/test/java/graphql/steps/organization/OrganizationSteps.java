package graphql.steps.organization;

import common.helpers.db_helpers.SQLQueries;
import data_containers.GeneralData;
import data_containers.OrganizationData;
import data_containers.RoleData;
import graphql.helpers.GraphQLHelpers;
import graphql.helpers.organization.OrganizationRoleModulesHelpers;
import io.cucumber.java8.En;
import org.assertj.core.api.SoftAssertions;

import java.util.ArrayList;

public class OrganizationSteps implements En {
    GeneralData generalData;
    OrganizationData organizationData;
    RoleData roleData;
    SoftAssertions assertions;
    ArrayList<Integer> moduleIdsInt;

    public OrganizationSteps(GeneralData generalData, OrganizationData organizationData, RoleData roleData) {
        this.generalData = generalData;
        this.organizationData = organizationData;
        this.roleData = roleData;
        assertions = new SoftAssertions();
        moduleIdsInt = new ArrayList<>();

        Given("{int} random organizations are created", (Integer numberOfOrganizations) -> {
         organizationData.listOfOrganizations = GraphQLHelpers.createRandomOrgs(numberOfOrganizations);


        });

        Given("{string} module is assigned to {int} organizations", (String moduleName, Integer organizationNumber) -> {
            Integer moduleId = SQLQueries.findModuleId(moduleName);
            assertions.assertThat(moduleId).isNotNull();
            assertions.assertAll();
            moduleIdsInt.clear();
            moduleIdsInt.add(moduleId);
            for (int i = 0; i < organizationNumber; i++) {
                generalData.response = OrganizationRoleModulesHelpers
                        .assignModulesToOrg(generalData.token, moduleIdsInt,
                                organizationData.listOfOrganizations.get(i).id);
                assertions.assertThat(generalData.response).isNotNull();
            }

        });
    }
}
