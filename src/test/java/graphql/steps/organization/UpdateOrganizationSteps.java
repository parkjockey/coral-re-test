package graphql.steps.organization;

import common.helpers.rest.HelperRest;
import data_containers.GeneralData;
import data_containers.OrganizationData;
import data_containers.RoleData;
import data_containers.UserData;
import graphql.helpers.GraphQLHelpers;
import io.cucumber.java8.En;
import org.assertj.core.api.SoftAssertions;

public class UpdateOrganizationSteps implements En {
    GeneralData generalData;
    UserData userData;
    RoleData roleData;
    OrganizationData organizationData;
    SoftAssertions assertions;
    String newOrgName;
    String newOrgDesc;

    public UpdateOrganizationSteps(GeneralData generalData, UserData userData, RoleData roleData, OrganizationData organizationData) {
        this.generalData = generalData;
        this.userData = userData;
        this.roleData = roleData;
        this.organizationData = organizationData;
        assertions = new SoftAssertions();
        newOrgName = "NewOrgAuto"+ HelperRest.getRandomNumber();
        newOrgDesc = "newOrgDesc";

        When("Update Organization GraphQl request is sent", () -> {

          generalData.response =
                  GraphQLHelpers
                          .updateOrganization(generalData.gqlBaseUri,
                                  generalData.token, organizationData.id.toString(), newOrgName,newOrgDesc);
        });

        Then("Response contains updateOrganization {string}", (String status) -> {
           assertions.assertThat(generalData.response.jsonPath().get("data.updateOrganization").toString()).isEqualTo(status);
        });

        When("Update Organization GraphQl request is sent with not existing organization", () -> {
            generalData.response =
                    GraphQLHelpers
                            .updateOrganization(generalData.gqlBaseUri,
                                    generalData.token, "999999999", newOrgName,newOrgDesc);
        });
    }
}
