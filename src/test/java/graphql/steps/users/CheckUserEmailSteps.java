package graphql.steps.users;

import common.helpers.rest.HelperRest;
import data_containers.GeneralData;
import data_containers.OrganizationData;
import data_containers.RoleData;
import data_containers.UserData;
import graphql.helpers.users.ConsumerUserEmailExistsHelpers;
import io.cucumber.java8.En;
import org.assertj.core.api.SoftAssertions;

public class CheckUserEmailSteps implements En {
    GeneralData generalData;
    UserData userData;
    OrganizationData organizationData;
    RoleData roleData;
    SoftAssertions assertions;

    public CheckUserEmailSteps(GeneralData generalData, UserData userData, OrganizationData organizationData, RoleData roleData) {
        this.generalData = generalData;
        this.userData = userData;
        this.organizationData = organizationData;
        this.roleData = roleData;
        assertions = new SoftAssertions();

        When("ConsumerUserEmailExists query is called", () -> {
           generalData.response = ConsumerUserEmailExistsHelpers.checkEmailExists(userData.email);
        });

        When("ConsumerUserEmailExists query is called with not existing user", () -> {
            userData.email = "notexisting"+ HelperRest.getRandomNumber()+"@not.com";
            generalData.response = ConsumerUserEmailExistsHelpers.checkEmailExists(userData.email);
        });

        Then("Response contains consumerUserEmailExists {string}", (String status) -> {
           assertions.assertThat(generalData.response
                   .jsonPath().get("data.consumerUserEmailExists").toString()).isEqualTo(status);
           assertions.assertAll();
        });
    }
}
