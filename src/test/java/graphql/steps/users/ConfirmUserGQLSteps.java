package graphql.steps.users;

import common.helpers.db_helpers.JdbcHelpers;
import common.helpers.db_helpers.SQLQueries;
import common.helpers.graph_api.MSGraphAPI;
import common.helpers.rest.HelperRest;
import common.helpers.rest.TokenProvider;
import data_containers.GeneralData;
import data_containers.OrganizationData;
import data_containers.RoleData;
import data_containers.UserData;
import graphql.helpers.GraphQLHelpers;
import graphql.helpers.InviteUsersHelprs;
import graphql.helpers.kafka.KafkaHelpers;
import graphql.helpers.users.ConfirmUserHelpers;
import graphql.variables.invite_users.UserVariablres;
import io.cucumber.java8.En;
import org.apache.avro.generic.GenericRecord;
import org.apache.commons.lang3.StringUtils;
import org.assertj.core.api.SoftAssertions;

import java.util.ArrayList;
import java.util.HashMap;

public class ConfirmUserGQLSteps implements En {

    GeneralData generalData;
    UserData userData;
    OrganizationData organizationData;
    RoleData roleData;
    SoftAssertions assertions;


    public ConfirmUserGQLSteps(GeneralData generalData, UserData userData, OrganizationData organizationData,
                               RoleData roleData) {
        this.generalData = generalData;
        this.userData = userData;
        this.organizationData = organizationData;
        this.roleData = roleData;
        assertions = new SoftAssertions();

        Given("Invitation code is obtained", () -> {
            //Go to DB and find invitation code
            String dbQuery = "select invitation_code from organization_users\n" +
                    "inner join users\n" +
                    "on users.id =  organization_users.user_id\n" +
                    "where users.username = '" + generalData.response.jsonPath().get("data.findOrganizationUserByEmail.username").toString() + "';";

            userData.invitationCode = JdbcHelpers.getStringValueFromDb(dbQuery, "invitation_code");
        });

        When("ConfirmOrganizationUserInvite mutation is called", () -> {
            userData.firstName = "AutoFN" + HelperRest.getRandomNumber();
            userData.lastName = "AutoLN" + HelperRest.getRandomNumber();
            generalData.response =
                    ConfirmUserHelpers
                            .confirmNotSSOUser(generalData.gqlBaseUri, userData.invitationCode, userData.firstName, userData.lastName, userData.password);
        });

        Then("Response contains confirmOrganizationUserInvite with value true", () -> {
            assertions.assertThat(generalData.response.jsonPath().get("data.confirmOrganizationUserInvite").toString()).isEqualTo("true");
            assertions.assertAll();
        });

        Given("SSO user is created", () -> {
            HashMap<String, String> adUser = MSGraphAPI.createADUser(MSGraphAPI.getADToken());
            userData.email = adUser.get("mail");
            userData.password = adUser.get("password");
            userData.firstName = adUser.get("givenName");
            userData.lastName = adUser.get("surname");
        });

        Given("Invite SSO user to Reef organization as Global Reef Admin", () -> {
            UserVariablres userVariablres = new UserVariablres();
            userVariablres.setEmail(userData.email);
            userVariablres.setSourceId("1");
            userVariablres.setUserSourceIdentifier("Test");
            ArrayList<UserVariablres> userList = new ArrayList<>() {{
                add(userVariablres);
            }};
            organizationData.id = JdbcHelpers.getIntValueFromDb("select id from organization where name = 'REEF';", "id");
            //Find Global Reef Admin Id
            roleData.roleId = JdbcHelpers.getIntValueFromDb("select id from roles where roles.name = 'Global REEF Administrator';", "id");
            generalData.response = InviteUsersHelprs
                    .inviteSpecificUsers(generalData.gqlBaseUri, generalData.token, userList, organizationData.id, roleData.roleId);
        });

        When("ConfirmOrganizationSsoUserInvite mutation is called", () -> {
            generalData.response =
                    ConfirmUserHelpers
                            .confirmSSOUser(generalData.gqlBaseUri, userData.invitationCode);
        });

        Then("Response contains confirmOrganizationSsoUserInvite with value true", () -> {
            assertions.assertThat(generalData.response.jsonPath().get("data.confirmOrganizationSsoUserInvite").toString()).isEqualTo("true");
            assertions.assertAll();
        });

        Given("Invite Not SSO user to Reef organization as Global Reef Admin", () -> {
            UserVariablres userVariablres = new UserVariablres();
            userData.email ="reefuato+" + HelperRest.getRandomNumber() + "@reefplatform.com";
            userVariablres.setEmail(userData.email);
            userVariablres.setSourceId("1");
            userVariablres.setUserSourceIdentifier("Testing");

            ArrayList<UserVariablres> userList = new ArrayList<>() {{
                add(userVariablres);
            }};
            organizationData.id = JdbcHelpers.getIntValueFromDb("select id from organization where name = 'REEF';", "id");
            //Find Global Reef Admin Id
            roleData.roleId = JdbcHelpers.getIntValueFromDb("select id from roles where roles.name = 'Global REEF Administrator';", "id");
            roleData.roleName = "Global REEF Administrator";
            generalData.response = InviteUsersHelprs
                    .inviteSpecificUsers(generalData.gqlBaseUri, generalData.token, userList, organizationData.id, roleData.roleId);
        });

        Given("Invite Not SSO user to random organization with random role", () -> {
            UserVariablres userVariablres = new UserVariablres();
            userData.email = "reefuato+" + HelperRest.getRandomNumber() + "@reefplatform.com";
            userVariablres.setEmail(userData.email);
            userVariablres.setSourceId("1");
            userVariablres.setUserSourceIdentifier("QA");
            ArrayList<UserVariablres> userList = new ArrayList<>() {{
                add(userVariablres);
            }};
            //Create random organization and obtain id
            organizationData.id = GraphQLHelpers.getCreatedOrganizationsId();

            //Create random role and obtain id
            generalData.response = GraphQLHelpers.addRoleToOrganization();
            roleData.roleId = generalData.response.jsonPath().getInt("data.addOrganizationRole.id");

            //Invite user
            generalData.response = InviteUsersHelprs
                    .inviteSpecificUsers(generalData.gqlBaseUri, generalData.token, userList, organizationData.id, Integer.parseInt(String.valueOf(roleData.roleId)));
        });

        Given("Invite Not SSO user to Reef organization with newly created role", () -> {
            UserVariablres userVariablres = new UserVariablres();
            userData.email = HelperRest.getRandomEmail();
            userVariablres.setEmail(userData.email);
            userVariablres.setSourceId("1");
            userVariablres.setUserSourceIdentifier("Testing");
            ArrayList<UserVariablres> userList = new ArrayList<>() {{
                add(userVariablres);
            }};
            generalData.response = InviteUsersHelprs
                    .inviteSpecificUsers(generalData.gqlBaseUri, generalData.token, userList, organizationData.id, roleData.roleId);
        });

        Given("Invite Not SSO user to Reef organization with random role", () -> {
            UserVariablres userVariablres = new UserVariablres();
            userData.email = HelperRest.getRandomEmail();
            userVariablres.setEmail(userData.email);
            userVariablres.setSourceId("1");
            userVariablres.setUserSourceIdentifier("Test");
            ArrayList<UserVariablres> userList = new ArrayList<>() {{
                add(userVariablres);
            }};
            organizationData.id = JdbcHelpers.getIntValueFromDb("select id from organization where name = 'REEF';", "id");

            generalData.response = InviteUsersHelprs
                    .inviteSpecificUsers(generalData.gqlBaseUri, generalData.token, userList, organizationData.id, roleData.roleId);
        });

        Given("Invite user to new organization", () -> {
            UserVariablres userVariablres = new UserVariablres();
            userVariablres.setEmail(userData.email);
            userVariablres.setSourceId("1");
            userVariablres.setUserSourceIdentifier("test");
            ArrayList<UserVariablres> userList = new ArrayList<>() {{
                add(userVariablres);
            }};

            generalData.response = InviteUsersHelprs
                    .inviteSpecificUsers(generalData.gqlBaseUri, generalData.token, userList, organizationData.id, roleData.roleId);
        });

        Then("Kafka message is obtained for newly created user", () -> {
            String username = TokenProvider.getUsernameClaim(userData.token);
            GenericRecord userDataKafka = KafkaHelpers.consumeUserDataTopic(username);

            assertions.assertThat(userDataKafka.get("userId")).isNotNull();
            assertions.assertThat(userDataKafka.get("username")).isEqualTo(username);
            assertions.assertAll();
        });

        Then("Kafka message is obtained for notification topic", () -> {
            String emailPrefix = StringUtils.substringBefore(userData.email, "@");

            GenericRecord notificationServiceData = KafkaHelpers.consumeNotificationTopic(emailPrefix);

            assertions.assertThat(notificationServiceData.get("receiverId").toString()).contains(emailPrefix);
            assertions.assertThat(notificationServiceData.get("senderId").toString()).isEqualTo("no-reply@reeftechnology.com");
            assertions.assertThat(notificationServiceData.get("templateCode").toString()).isEqualTo("IAM_ORGANIZATION_USER_INVITE");
            assertions.assertThat(notificationServiceData.get("consumerId").toString()).isNotNull();
            assertions.assertThat(notificationServiceData.get("subject").toString()).isEqualTo("Welcome to REEF");
            assertions.assertAll();
        });

        Given("Invite user to new organization with Admin role", () -> {
            //Set user email
            UserVariablres userVariablres = new UserVariablres();
            userVariablres.setEmail(userData.email);
            userVariablres.setSourceId("1");
            userVariablres.setUserSourceIdentifier("Test");
            ArrayList<UserVariablres> userList = new ArrayList<>() {{
                add(userVariablres);
            }};
            //Obtain Admin id from DB
            roleData.roleId = SQLQueries.findRoleId(organizationData.id);
            //Invite user request
            generalData.response = InviteUsersHelprs
                    .inviteSpecificUsers(generalData.gqlBaseUri, generalData.token, userList, organizationData.id, roleData.roleId);
        });


    }
}
