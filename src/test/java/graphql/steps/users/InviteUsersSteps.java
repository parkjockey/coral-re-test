package graphql.steps.users;

import com.google.gson.Gson;
import common.helpers.db_helpers.JdbcHelpers;
import data_containers.GeneralData;
import data_containers.OrganizationData;
import data_containers.RoleData;
import data_containers.UserData;
import graphql.helpers.GraphQLHelpers;
import graphql.helpers.InviteUsersHelprs;
import graphql.responses.OrganizationResponse;
import graphql.responses.users.invite_users.InviteUserResponse;
import io.cucumber.java8.En;
import org.assertj.core.api.SoftAssertions;

import java.util.ArrayList;
import java.util.List;

public class InviteUsersSteps implements En {
    GeneralData generalData;
    RoleData roleData;
    UserData userData;
    OrganizationData organizationData;
    OrganizationResponse organizationResponse;
    private SoftAssertions softAssertions = new SoftAssertions();

    public InviteUsersSteps(GeneralData generalData, OrganizationData organizationData, OrganizationResponse organizationResponse, RoleData roleData, UserData userData) {

        this.generalData = generalData;
        this.roleData = roleData;
        this.userData = userData;
        this.organizationData = organizationData;
        this.organizationResponse = new OrganizationResponse(generalData);

        When("Invite {int} user to organization request is sent", (Integer numberOfInvitedUsers) -> {
            generalData.response = GraphQLHelpers.addRoleToOrganization();
            roleData.roleId = generalData.response.jsonPath().getInt("data.addOrganizationRole.id");
            generalData.response = InviteUsersHelprs.inviteUsers(numberOfInvitedUsers, true);
        });

        When("{int} users are re-invited", (Integer numberOfUsers) -> {
            generalData.response = GraphQLHelpers.addRoleToOrganization();
            roleData.roleId = generalData.response.jsonPath().getInt("data.addOrganizationRole.id");
            ArrayList<String> userEmails = InviteUsersHelprs.getInvitedUsersEmails(numberOfUsers, organizationData.id);
            ArrayList<String> usernameList = new ArrayList<>();

            for (int i = 0; i < numberOfUsers; i++) {
                usernameList.add(InviteUsersHelprs.getUsernameByEmail(organizationData.id, userEmails.get(i)));
            }
            generalData.response = InviteUsersHelprs.reinviteUsers(userEmails, usernameList, organizationData.id, roleData.roleId);
        });

        When("{int} user is invited to the new organization with a role assignment", (Integer numberOfUsers) -> {
            generalData.response = InviteUsersHelprs.inviteUsers(numberOfUsers, true);
        });

        When("Roles are assigned to a user", () ->{

            List<Integer> roleIds = new ArrayList<>();
            roleIds.add(roleData.roleId);

            generalData.response = InviteUsersHelprs.assignRoles(userData.id, organizationData.id, roleIds);
        });

        Then("User has the assigned roles", () -> {

            softAssertions.assertThat(organizationResponse.getListOfUsersRoles())
                    .as("List of users roles contains new assigned roles")
                    .contains(GraphQLHelpers.getOrganizationRoleIds());
            softAssertions.assertAll();
        });


        Then("Response contains {int} invited and assigned user", (Integer numberOfUsers) -> {
            Integer actualInvitedAndAssignedTo = generalData.response.jsonPath().get("data.inviteUsers.numberOfInvitedAndAssignedToRole");

            softAssertions.assertThat(actualInvitedAndAssignedTo)
                    .isEqualTo(numberOfUsers);
            softAssertions.assertAll();
        });

        Then("Response contains {int} invited users", (Integer numberOfInvitedUser) -> {
            Gson gson = new Gson();
            InviteUserResponse inviteUserResponse = gson.fromJson(generalData.response.asString(), InviteUserResponse.class);
            Integer actualInvited = inviteUserResponse.getData().getInviteUsers().getNumberOfInvited();
            softAssertions.assertThat(actualInvited).isEqualTo(numberOfInvitedUser);
            softAssertions.assertAll();
        });

        Then("Response contains {int} ignored users", (Integer numberOfInvitedUser) -> {
            Integer actualInvited = generalData.response.jsonPath().get("data.inviteUsers.numberOfIgnored");
            softAssertions.assertThat(actualInvited).isEqualTo(numberOfInvitedUser);
            softAssertions.assertAll();
        });

        Then("User has only the new assigned role", () -> {

            generalData.response = GraphQLHelpers.GetOrganizationUserRoles(organizationData.id, userData.id);
            softAssertions.assertThat(organizationResponse.getListOfUsersRoles())
                    .as("List of users roles contains the new assigned role")
                    .contains(roleData.roleName);
            softAssertions.assertThat(organizationResponse.getListOfUsersRoles())
                    .as("List of users roles doesn't contain old role")
                    .doesNotContain("Global REEF Administrator");
            softAssertions.assertThat(organizationResponse.getListOfUsersRoles().size())
                    .as("List size is 1")
                    .isEqualTo(1);
            softAssertions.assertAll();

        });

        When("OrganizationUserInviteData query is called", () -> {
         generalData.response = InviteUsersHelprs.organizationUserInviteData(userData.invitationCode);
        });

        Then("response contains email, but ssoUrl is null", () -> {

            try {
               generalData
                        .response.jsonPath().get("data.organizationUserInviteData.ssoUrl").toString();

            } catch (NullPointerException nullPointer){
                softAssertions.assertThat(nullPointer).isNull();
            }

        });

        Then("response contains email and ssoUrl is not null", () -> {
            softAssertions.assertThat(generalData
                    .response.jsonPath().get("data.organizationUserInviteData.email").toString()).isEqualTo(userData.email);
            softAssertions.assertThat(  generalData
                    .response.jsonPath().get("data.organizationUserInviteData.ssoUrl").toString()).isNotNull();
            softAssertions.assertAll();
        });

        Then("Response contains assignRoles true", () -> {
            softAssertions.assertThat(generalData
                    .response.jsonPath().get("data.assignRoles").toString()).isEqualTo("true");
            softAssertions.assertAll();
        });

        When("Global Admin and random role are assigned to a user", () ->{

            List<Integer> roleIds = new ArrayList<>();
            roleIds.add(roleData.roleId);
            roleIds.add(JdbcHelpers.getIntValueFromDb("select id from roles where roles.name = 'Global REEF Administrator';", "id"));

            generalData.response = InviteUsersHelprs.assignRoles(userData.id, organizationData.id, roleIds);
        });

    }

}
