package graphql.steps.users;

import com.google.gson.Gson;
import common.helpers.redis.RedisHelpers;
import common.helpers.rest.TokenProvider;
import data_containers.GeneralData;
import data_containers.UserData;
import graphql.helpers.users.AuthenticationHelpers;
import graphql.responses.AuthenticationResponse;
import graphql.responses.users.RefreshTokenResponse;
import io.cucumber.java8.En;
import org.assertj.core.api.SoftAssertions;

import java.util.Map;

public class LoginGQLSteps implements En {
    GeneralData generalData;
    Gson gson;
    SoftAssertions assertions;
    String accessCode;
    String refreshToken;
    UserData userData;

    public LoginGQLSteps(GeneralData generalData, UserData userData) {
        this.generalData = generalData;
        this.userData = userData;
        gson = new Gson();
        assertions = new SoftAssertions();

        When("Access code and redirectUrl are sent( again) via gql getTokens query", () -> {
            generalData.response = AuthenticationHelpers.getTokensQuery(accessCode, generalData.redirectUrl);
        });

        Then("User receives id, access and refresh tokens", () -> {
            assertions.assertThat(AuthenticationResponse.accessToken(generalData.response)).isNotNull();
            assertions.assertThat(AuthenticationResponse.idToken(generalData.response)).isNotNull();
            assertions.assertThat(AuthenticationResponse.refreshToken(generalData.response)).isNotNull();
            assertions.assertThat(AuthenticationResponse.tokenType(generalData.response)).isEqualTo("Bearer");
            assertions.assertThat(AuthenticationResponse.expiresIn(generalData.response)).isEqualTo(28800);
            assertions.assertAll();
        });

        When("No sso user credentials are sent via gql login query", () -> {
            accessCode = AuthenticationHelpers.getAccessCode(generalData.adNoSSOUsername, generalData.adPassword);
        });

        When("Sso user credentials are sent via gql login query", () -> {
            generalData.response = AuthenticationHelpers.loginQuery(generalData.adSSOUsername, generalData.adPassword);
        });

        Then("Access code is received", () -> {
            assertions.assertThat(accessCode).isNotNull();
            assertions.assertAll();
        });

        When("Access code and invalid redirectUrl are sent via gql getTokens query", () -> {
            generalData.response = AuthenticationHelpers.getTokensQuery(accessCode, "fdsfas");
        });

        Given("Refresh token is obtained", () -> {
            generalData.response = AuthenticationHelpers.getTokens(generalData.adNoSSOUsername, generalData.adPassword);
            refreshToken = AuthenticationResponse.refreshToken(generalData.response);
        });

        When("Refresh tokens query is called( again)", () -> {
            generalData.response = AuthenticationHelpers.refreshTokens(refreshToken);
        });

        Then("User receives id, access tokens but refresh token is null", () -> {
            assertions.assertThat(RefreshTokenResponse.accessToken(generalData.response)).isNotNull();
            assertions.assertThat(RefreshTokenResponse.idToken(generalData.response)).isNotNull();
            assertions.assertThat(RefreshTokenResponse.tokenType(generalData.response)).isEqualTo("Bearer");
            assertions.assertThat(RefreshTokenResponse.expiresIn(generalData.response)).isEqualTo(28800);
            assertions.assertAll();

            try {
                RefreshTokenResponse.refreshToken(generalData.response);

            } catch (NullPointerException nullPointer) {
                assertions.assertThat(nullPointer).isNull();
            }
        });

        When("Logout mutation is called", () -> {
            generalData.response = AuthenticationHelpers.logout(generalData.token);
        });

        Then("Response doesn't contain identityProviderLogoutUrl since it is not SSO user", () -> {

            try {
                generalData.response.jsonPath().get("data.logout.identityProviderLogoutUrl").toString();

            } catch (NullPointerException nullPointer) {
               nullPointer.toString().equals("null");
            }
        });

        Then("Token's Jti is stored in Redis blacklist", () -> {
            String userJti = TokenProvider.getJtiClaim(generalData.token);
            Map<String, String> redisMap = RedisHelpers.getFromRedis(RedisHelpers.prefixBlackList + userJti);
            assertions.assertThat(userJti).isEqualTo(redisMap.get("token"));
            assertions.assertAll();
        });
    }
}
