package graphql.steps.users;

import com.google.gson.Gson;
import data_containers.GeneralData;
import data_containers.OrganizationData;
import data_containers.RoleData;
import data_containers.UserData;
import graphql.helpers.users.UsersGqlHelpers;
import graphql.responses.users.organization_users_page.Content;
import graphql.responses.users.organization_users_page.OrganizationUsersPageResponse;
import graphql.responses.users.organization_users_page.Role;
import io.cucumber.java8.En;
import io.restassured.response.Response;
import org.assertj.core.api.SoftAssertions;

import java.util.ArrayList;

public class OrganizationUsersPageSteps implements En {

    GeneralData generalData;
    UserData userData;
    OrganizationData organizationData;
    RoleData roleData;
    Gson gson;
    SoftAssertions assertions;
    ArrayList<Integer> roleIds;

    public OrganizationUsersPageSteps(GeneralData generalData, UserData userData, OrganizationData organizationData, RoleData roleData) {
        this.generalData = generalData;
        this.userData = userData;
        this.organizationData = organizationData;
        this.roleData = roleData;
        gson = new Gson();
        assertions = new SoftAssertions();
        roleIds = new ArrayList<>();

        When("Organization Users Page query is called with email that contains {string}", (String email) -> {
            generalData.response = UsersGqlHelpers.filterOrganizationUsersPageByEmail(generalData.token, organizationData.id, email, 0);
        });

        Then("Response contains only {string} emails", (String email) -> {
            OrganizationUsersPageResponse response = gson.fromJson(generalData.response.asString(), OrganizationUsersPageResponse.class);
            Integer totalPageNumber = response.getData().getOrganizationUsersPage().getTotalPages();

            for (Integer i = 0; i < totalPageNumber; i++) {
                Response resp = UsersGqlHelpers.filterOrganizationUsersPageByEmail(generalData.token, organizationData.id, email, i);
                OrganizationUsersPageResponse responseObject = gson.fromJson(resp.asString(), OrganizationUsersPageResponse.class);
                ArrayList<Content> userList = (ArrayList<Content>) responseObject.getData().getOrganizationUsersPage().getContent();

                for (Content user : userList) {
                    assertions.assertThat(user.getEmail()).contains(email);
                    assertions.assertAll();
                }
            }
        });

        When("Organization Users Page query is called with name that contains {string}", (String name) -> {
            generalData.response = UsersGqlHelpers.filterOrganizationUsersPageByName(generalData.token, organizationData.id, name, 0);
        });

        Then("Response contains only {string} name", (String name) -> {
            OrganizationUsersPageResponse response = gson.fromJson(generalData.response.asString(), OrganizationUsersPageResponse.class);
            Integer totalPageNumber = response.getData().getOrganizationUsersPage().getTotalPages();

            for (Integer i = 0; i < totalPageNumber; i++) {
                Response resp = UsersGqlHelpers.filterOrganizationUsersPageByName(generalData.token, organizationData.id, name, i);
                OrganizationUsersPageResponse responseObject = gson.fromJson(resp.asString(), OrganizationUsersPageResponse.class);
                ArrayList<Content> userList = (ArrayList<Content>) responseObject.getData().getOrganizationUsersPage().getContent();

                for (Content user : userList) {
                    assertions.assertThat(user.getFullName()).contains(name);
                    assertions.assertThat(user.getSourceSystemName()).isNull();
                    assertions.assertAll();
                }
            }
        });

        When("Organization Users Page query is called with newly created role id and Global Reef Admin id", () -> {
            roleIds.add(roleData.roleId);
            roleIds.add(1);
            generalData.response = UsersGqlHelpers.filterOrganizationUsersPageByRoleIds(generalData.token, organizationData.id, roleIds, 0);
        });


        Then("response contains users with newly created role and Global Reef Admin users", () -> {
            OrganizationUsersPageResponse response = gson.fromJson(generalData.response.asString(), OrganizationUsersPageResponse.class);
            Integer totalPageNumber = response.getData().getOrganizationUsersPage().getTotalPages();

            for (Integer i = 0; i < totalPageNumber; i++) {
                Response resp = UsersGqlHelpers.filterOrganizationUsersPageByRoleIds(generalData.token, organizationData.id, roleIds, i);
                OrganizationUsersPageResponse responseObject = gson.fromJson(resp.asString(), OrganizationUsersPageResponse.class);
                ArrayList<Content> userList = (ArrayList<Content>) responseObject.getData().getOrganizationUsersPage().getContent();

                for (Content user : userList) {
                    for (Role r : user.getRoles()) {
                        if (r.getId().equals(roleData.roleId)) {
                            assertions.assertThat(r.getId()).isEqualTo(roleData.roleId);
                            assertions.assertAll();
                        }
                        if (r.getId().equals(1)) {
                            assertions.assertThat(r.getId()).isEqualTo(1);
                            assertions.assertAll();
                        }
                    }
                }
            }
        });


        When("Organization Users Page query is called with {string} status filter", (String status) -> {
            generalData.response = UsersGqlHelpers.filterOrganizationUsersPageByStatuses(generalData.token, organizationData.id, new ArrayList<>() {{
                add(status);
            }}, 0);
        });

        Then("First and last pages contain {string} status", (String status) -> {
            //Check 1st page
            OrganizationUsersPageResponse response = gson.fromJson(generalData.response.asString(), OrganizationUsersPageResponse.class);
            ArrayList<Content> userList = (ArrayList<Content>) response.getData().getOrganizationUsersPage().getContent();

            for (Content user : userList) {
                assertions.assertThat(user.getUserStatus()).contains(status);
                assertions.assertAll();
            }

            //Check last page
            Integer totalPageNumber = response.getData().getOrganizationUsersPage().getTotalPages();
            Response responseLastPage = UsersGqlHelpers.filterOrganizationUsersPageByStatuses(generalData.token, organizationData.id, new ArrayList<>() {{
                add(status);
            }}, totalPageNumber - 1);
            OrganizationUsersPageResponse responseLastPageGson = gson.fromJson(responseLastPage.asString(), OrganizationUsersPageResponse.class);
            ArrayList<Content> userListLastPage = (ArrayList<Content>) responseLastPageGson.getData().getOrganizationUsersPage().getContent();

            for (Content user : userListLastPage) {
                assertions.assertThat(user.getUserStatus()).contains(status);
                assertions.assertAll();
            }
        });

        When("Organization Users Page query is called with {string} order by statuses", (String orderBy) -> {
            generalData.response = UsersGqlHelpers.filterOrganizationUsersPageByOrderBy(generalData.token, organizationData.id, new ArrayList<>() {{
                add(orderBy);
            }}, 0);
        });

        Then("First user on the first page has status {string} and last user on the last page has status {string} status", (String status1, String status2) -> {
            //Check 1st user
            OrganizationUsersPageResponse response = gson.fromJson(generalData.response.asString(), OrganizationUsersPageResponse.class);
            ArrayList<Content> userList = (ArrayList<Content>) response.getData().getOrganizationUsersPage().getContent();
            assertions.assertThat(userList.get(0).getUserStatus()).contains(status1);
            assertions.assertAll();

            //Check last user
            Integer totalPageNumber = response.getData().getOrganizationUsersPage().getTotalPages();
            Response responseLastPage = UsersGqlHelpers.filterOrganizationUsersPageByOrderBy(generalData.token, organizationData.id, new ArrayList<>() {{
                add("STATUS_ASC");
            }}, totalPageNumber - 1);
            OrganizationUsersPageResponse responseLastPageGson = gson.fromJson(responseLastPage.asString(), OrganizationUsersPageResponse.class);
            ArrayList<Content> userListLastPage = (ArrayList<Content>) responseLastPageGson.getData().getOrganizationUsersPage().getContent();
            assertions.assertThat(userListLastPage.get(userListLastPage.size() - 1).getUserStatus()).contains(status2);
            assertions.assertAll();
        });

        When("Organization Users Page query is called with {int} pageSize filter", (Integer pageSize) -> {
            generalData.response = UsersGqlHelpers.filterOrganizationUsersPageSize(generalData.token, organizationData.id, pageSize, 0);
        });

        Then("Response contains {int} user size list", (Integer pageSize) -> {
            OrganizationUsersPageResponse response = gson.fromJson(generalData.response.asString(), OrganizationUsersPageResponse.class);
            ArrayList<Content> userList = (ArrayList<Content>) response.getData().getOrganizationUsersPage().getContent();
            assertions.assertThat(userList.size()).isEqualTo(pageSize);
            assertions.assertAll();
        });


        When("Organization Users Page query is called for desired email", () -> {
            generalData.response = UsersGqlHelpers.filterOrganizationUsersPageByEmail(generalData.token, organizationData.id, userData.email, 0);
        });

        Then("Response has similar invitationDate and lastModifiedDate", () -> {
            OrganizationUsersPageResponse response = gson.fromJson(generalData.response.asString(), OrganizationUsersPageResponse.class);
            Content user = response.getData().getOrganizationUsersPage().getContent().get(0);

            assertions.assertThat(user.getInvitedOnDate().substring(0, 19)).isEqualTo(user.getLastModifiedDate().substring(0, 19));
            assertions.assertAll();

        });

        Then("Response has similar acceptedOnDat and lastModifiedDate", () -> {
            OrganizationUsersPageResponse response = gson.fromJson(generalData.response.asString(), OrganizationUsersPageResponse.class);
            Content user = response.getData().getOrganizationUsersPage().getContent().get(0);

            assertions.assertThat(user.getAcceptedOnDate().substring(0, 19)).isEqualTo(user.getLastModifiedDate().substring(0, 19));
            assertions.assertAll();

        });

        Then("Response has different invitationDate, acceptedOnDat from lastModifiedDate", () -> {
            OrganizationUsersPageResponse response = gson.fromJson(generalData.response.asString(), OrganizationUsersPageResponse.class);
            Content user = response.getData().getOrganizationUsersPage().getContent().get(0);

            assertions.assertThat(user.getAcceptedOnDate().substring(0, 19)).isNotEqualTo(user.getLastModifiedDate().substring(0, 19));
            assertions.assertThat(user.getInvitedOnDate().substring(0, 19)).isNotEqualTo(user.getLastModifiedDate().substring(0, 19));
            assertions.assertAll();

        });

        Then("Response contains total number of {int} elements", (Integer numberOfElements) -> {
            OrganizationUsersPageResponse response = gson.fromJson(generalData.response.asString(), OrganizationUsersPageResponse.class);

            assertions.assertThat(response.getData().getOrganizationUsersPage().getTotalItems()).isEqualTo(numberOfElements);
            assertions.assertAll();

        });

    }
}
