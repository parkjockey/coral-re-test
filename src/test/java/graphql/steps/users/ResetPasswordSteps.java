package graphql.steps.users;

import common.helpers.db_helpers.SQLQueries;
import data_containers.GeneralData;
import data_containers.OrganizationData;
import data_containers.RoleData;
import data_containers.UserData;
import graphql.helpers.users.ResetPasswordHelpers;
import io.cucumber.java8.En;
import org.assertj.core.api.SoftAssertions;

public class ResetPasswordSteps implements En {
    GeneralData generalData;
    UserData userData;
    OrganizationData organizationData;
    RoleData roleData;
    SoftAssertions assertions;
    String passwordVerificationCode;
    String newPassword;

    public ResetPasswordSteps(GeneralData generalData, UserData userData, OrganizationData organizationData, RoleData roleData) {
        this.generalData = generalData;
        this.userData = userData;
        this.organizationData = organizationData;
        this.roleData = roleData;
        assertions = new SoftAssertions();
        newPassword = "Test12345!";

        When("Reset Password mutation is called", () -> {
            generalData.response = ResetPasswordHelpers.updatePassword(userData.email);
        });

        Then("Response contains resetPassword true", () -> {
            assertions.assertThat(generalData.response
                    .jsonPath().get("data.resetPassword").toString())
                    .as("ResetPassword is set to true").isEqualTo("true");
            assertions.assertAll();
        });

        Then("Reset password verification code is exists in DB", () -> {
            passwordVerificationCode = SQLQueries.getResetPasswordVerificationCode(userData.id);
            assertions.assertThat(passwordVerificationCode).isNotNull();
            assertions.assertAll();
        });

        When("Confirm Reset Password Mutation is called", () -> {
            generalData.response = ResetPasswordHelpers.confirmResetPassword(newPassword, passwordVerificationCode);
        });


        Then("Response contains confirmResetPassword true", () -> {
            assertions.assertThat(generalData.response
                    .jsonPath().get("data.confirmResetPassword").toString())
                    .as("ResetPassword is set to true").isEqualTo("true");
            assertions.assertAll();
        });
    }
}



