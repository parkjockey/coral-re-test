package graphql.steps.users;

import com.google.gson.Gson;
import data_containers.GeneralData;
import data_containers.OrganizationData;
import data_containers.RoleData;
import data_containers.UserData;
import graphql.helpers.users.AuthenticationHelpers;
import graphql.helpers.users.UpdatePasswordHelpers;
import io.cucumber.java8.En;
import org.assertj.core.api.SoftAssertions;

public class UpdatePasswordSteps implements En {
    GeneralData generalData;
    UserData userData;
    OrganizationData organizationData;
    RoleData roleData;
    SoftAssertions assertions;
    Gson gson;
    String newPassword;

    public UpdatePasswordSteps(GeneralData generalData, UserData userData, OrganizationData organizationData, RoleData roleData) {
        this.generalData = generalData;
        this.userData = userData;
        this.organizationData = organizationData;
        this.roleData = roleData;
        gson = new Gson();
        assertions = new SoftAssertions();
        newPassword = "Test12345!";

        When("User update his password( again| first time)", () -> {
            generalData.response = UpdatePasswordHelpers.updatePassword(userData.token, userData.password, newPassword);
        });

        Then("Response contains updatePassword true", () -> {
            assertions.assertThat(generalData.response
                    .jsonPath().get("data.updatePassword").toString())
                    .as("UpdateProfile is set to true").isEqualTo("true");
            assertions.assertAll();
        });

        Then("User can obtain token with new password", () -> {
            generalData.token = AuthenticationHelpers.getToken(userData.email, newPassword);
            assertions.assertThat(generalData.token).isNotNull();
            assertions.assertAll();
        });

        When("User tries to update his password with wrong current password", () -> {
            generalData.response = UpdatePasswordHelpers.updatePassword(userData.token, "fdsafdsa", newPassword);
        });

    }


}
