package graphql.steps.users;

import com.google.gson.Gson;
import common.helpers.aws_helpers.sqs.SQSHelpers;
import common.helpers.rest.HelperRest;
import common.helpers.rest.PictureReader;
import data_containers.GeneralData;
import data_containers.OrganizationData;
import data_containers.RoleData;
import data_containers.UserData;
import graphql.helpers.GraphQLHelpers;
import graphql.helpers.users.UpdateProfileHelpers;
import graphql.responses.organization.org_user_data.OrganizationUserData;
import graphql.responses.organization.org_user_data.OrganizationUserDataResponse;
import io.cucumber.java8.En;
import org.assertj.core.api.SoftAssertions;

import java.io.File;

public class UpdateProfileSteps implements En {
    GeneralData generalData;
    UserData userData;
    OrganizationData organizationData;
    RoleData roleData;
    SoftAssertions assertions;
    Gson gson;
    String picture;
    String oldEmail;

    public UpdateProfileSteps(GeneralData generalData, UserData userData, OrganizationData organizationData, RoleData roleData) {
        this.generalData = generalData;
        this.userData = userData;
        this.organizationData = organizationData;
        this.roleData = roleData;
        assertions = new SoftAssertions();
        gson = new Gson();

        When("User update his profile", () -> {
            userData.firstName = "NewFirstName" + HelperRest.getRandomNumber();
            userData.lastName = "NewLastName" + HelperRest.getRandomNumber();
            generalData.response = UpdateProfileHelpers.updateProfile(userData.token, userData.firstName, userData.lastName, null);
        });

        Then("Response contains updateProfile true", () -> {
            assertions.assertThat(generalData.response
                    .jsonPath().get("data.updateProfile").toString())
                    .as("UpdateProfile is set to true").isEqualTo("true");
            assertions.assertAll();
        });

        Then("User details are now updated", () -> {
            generalData.response = GraphQLHelpers.getOrganizationUserData(generalData.gqlBaseUri, userData.token, organizationData.id.toString());
            OrganizationUserDataResponse userDataResponse = gson.fromJson(generalData.response.asString(), OrganizationUserDataResponse.class);

            OrganizationUserData data = userDataResponse.getData().getOrganizationUserData();

            assertions.assertThat(data.getGivenName()).as("First name is correct").isEqualTo(userData.firstName);
            assertions.assertThat(data.getFamilyName()).as("Last name is correct").isEqualTo(userData.lastName);
            assertions.assertAll();
        });

        When("Consumer update his profile with new email", () -> {
            userData.firstName = "NewFirstName" + HelperRest.getRandomNumber();
            userData.lastName = "NewLastName" + HelperRest.getRandomNumber();
            userData.email = "reefuato+" + HelperRest.getRandomNumber() + "@reefplatform.com";
            generalData.response = UpdateProfileHelpers
                    .updateProfile(userData.token, userData.firstName, userData.lastName, userData.email);
        });

        When("User tries to update his email", () -> {
            userData.firstName = "NewFirstName" + HelperRest.getRandomNumber();
            userData.lastName = "NewLastName" + HelperRest.getRandomNumber();
            userData.email = "reefuato+" + HelperRest.getRandomNumber() + "@reefplatform.com";
            generalData.response = UpdateProfileHelpers
                    .updateProfile(userData.token, userData.firstName, userData.lastName, userData.email);
        });

        When("User update his profile picture", () -> {
            //Picture must be encoded to byte 64 format
            picture = PictureReader.encodeFileToBase64Binary(new File("src/test/resources/profile_pic/profilePicture.jpg"));
            generalData.response = UpdateProfileHelpers.updateProfilePicture(userData.token, picture, "jpg");
        });

        Then("Response contains updateProfilePicture is true", () -> {
            assertions.assertThat(generalData.response
                    .jsonPath().get("data.updateProfilePicture").toString()).isEqualTo("true");
            assertions.assertAll();
        });

        When("User update his profile picture with smaller resolution", () -> {
            //Picture must be encoded to byte 64 format
            String picture = PictureReader.encodeFileToBase64Binary(new File("src/test/resources/profile_pic/400x400.png"));
            generalData.response = UpdateProfileHelpers.updateProfilePicture(userData.token, picture, "jpg");
        });

        When("User update his profile picture with invalid extension", () -> {
            //Picture must be encoded to byte 64 format
            String picture = PictureReader.encodeFileToBase64Binary(new File("src/test/resources/profile_pic/profilePicture.jpg"));
            generalData.response = UpdateProfileHelpers.updateProfilePicture(userData.token, picture, "aaaaa");
        });

        When("ProfilePicture query is called", () -> {
            generalData.response = UpdateProfileHelpers.profilePicture(userData.token);
        });

        Then("Response contains content and content type", () -> {
            assertions.assertThat(generalData.response
                    .jsonPath().get("data.profilePicture.content").toString()).isEqualTo(picture);
            assertions.assertThat(generalData.response
                    .jsonPath().get("data.profilePicture.contentType").toString()).isEqualTo("image/jpeg");
            assertions.assertAll();
        });

        When("UpdateEmail mutation is called", () -> {
            oldEmail = userData.email;
            userData.email = "reefuato+" + HelperRest.getRandomNumber() + "@reefplatform.com";
            generalData.response = UpdateProfileHelpers.consumerUpdateEmail(userData.token, userData.email);
        });

        Then("Response contains updateEmail is true", () -> {
            assertions.assertThat(generalData.response
                    .jsonPath().get("data.updateEmail").toString()).isEqualTo("true");
            assertions.assertAll();
        });

        When("ConfirmConsumerUserEmailWithOtp mutation is called", () -> {
            generalData.response = UpdateProfileHelpers.confirmConsumerUserEmailWithOtp(userData.token, userData.otpCode, userData.email);
        });

        Then("Response contains confirmConsumerUserEmailWithOtp is true", () -> {
            assertions.assertThat(generalData.response
                    .jsonPath().get("data.confirmConsumerUserEmailWithOtp").toString()).isEqualTo("true");
            assertions.assertAll();
        });

        Then("Email is received on the old email address with subject: {string}", (String subject) -> {
//            await("Wait condition to be true")
//                    .atMost(30, TimeUnit.SECONDS)
//                    .pollDelay(2, TimeUnit.SECONDS)
//                    .pollInterval(2, TimeUnit.SECONDS)
//                    .until(() ->
//                            EmailHelpers.waitMailToBeObtained(oldEmail, subject).getSubject().equals(subject));
            SQSHelpers.getSpecificMessage(oldEmail, subject).contains(subject);
        });
    }
}
