package graphql.steps.users;

import com.google.gson.Gson;
import common.helpers.redis.RedisHelpers;
import common.helpers.rest.TokenProvider;
import data_containers.GeneralData;
import data_containers.OrganizationData;
import data_containers.RoleData;
import data_containers.UserData;
import graphql.helpers.GraphQLHelpers;
import graphql.helpers.kafka.KafkaHelpers;
import graphql.helpers.users.AuthenticationHelpers;
import graphql.helpers.users.UsersGqlHelpers;
import graphql.responses.ErrorResponses;
import graphql.responses.organization.org_user_data.OrganizationUserData;
import graphql.responses.organization.org_user_data.OrganizationUserDataResponse;
import io.cucumber.java8.En;
import io.restassured.response.Response;
import org.apache.avro.generic.GenericRecord;
import org.assertj.core.api.SoftAssertions;

import java.util.ArrayList;
import java.util.Map;

public class UpdateUsersStatusSteps implements En {
    GeneralData generalData;
    UserData userData;
    OrganizationData organizationData;
    RoleData roleData;
    SoftAssertions assertions;
    Gson gson;

    public UpdateUsersStatusSteps(GeneralData generalData, UserData userData, OrganizationData organizationData, RoleData roleData) {
        this.generalData = generalData;
        this.userData = userData;
        this.organizationData = organizationData;
        this.roleData = roleData;
        assertions = new SoftAssertions();
        gson = new Gson();

        When("Update User Status mutation is called with status {string}", (String status) -> {
          generalData.response = UsersGqlHelpers.updateUserStatus(generalData.token, organizationData.id, userData.id, status);
        });

        Then("Response contains updateStatus true", () -> {
            assertions.assertThat(generalData.response
                    .jsonPath().get("data.updateUserStatus").toString())
                    .as("ResetPassword is set to true").isEqualTo("true");
            assertions.assertAll();
        });

        Then("Suspended user can't login with received error message {string}", (String message) -> {
           Response response = AuthenticationHelpers.loginQuery(userData.email, userData.password);
            assertions.assertThat(ErrorResponses.getErrorMessageKey(response)).isEqualTo(message);
            assertions.assertAll();
        });

        When("Update User Status mutation is called with status {string} for first organization", (String status) -> {
            generalData.response = UsersGqlHelpers.updateUserStatus(generalData.token, organizationData.listOfOrganizations.get(0).id, userData.id, status);
        });

        Then("Organization user data for first organization has not provided", () -> {
            generalData.response = GraphQLHelpers
                    .getOrganizationUserData(generalData.gqlBaseUri, userData.token,
                            organizationData.listOfOrganizations.get(0).id.toString());

            assertions.assertThat(ErrorResponses.getErrorMessageKey(generalData.response)).isEqualTo("access.denied");
            assertions.assertAll();
        });

        Then("Organization user data for second organization has status active", () -> {
            generalData.response = GraphQLHelpers
                    .getOrganizationUserData(generalData.gqlBaseUri, userData.token,
                            organizationData.listOfOrganizations.get(1).id.toString());
            OrganizationUserDataResponse userDataResponse = gson.fromJson(generalData.response.asString(), OrganizationUserDataResponse.class);

            OrganizationUserData data = userDataResponse.getData().getOrganizationUserData();
            assertions.assertThat(data.getUserStatus()).as("Status is correct").isEqualTo("ACTIVE");
            assertions.assertAll();
        });

        Then("Kafka message not contains user's permissions", () -> {
            String username = TokenProvider.getUsernameClaim(userData.token);
            GenericRecord userDataKafka = KafkaHelpers.consumeUserDataTopic(username);

            assertions.assertThat(userDataKafka.get("organizationAuthorizationData").toString()).isEqualTo("[]");
            assertions.assertAll();
        });

        Then("Redis doesn't have information about suspended user", () -> {
            Map<String, String> redisMap = RedisHelpers.getFromRedis(RedisHelpers.prefixUserAuthData+userData.userName);

            assertions.assertThat(redisMap.toString()).isEqualTo("{}");
            assertions.assertAll();
        });

        Then("Redis doesn't contain information from suspended organization", () -> {
            Map<String, String> redisMap = RedisHelpers.getFromRedis(RedisHelpers.prefixUserAuthData+userData.userName);

            assertions.assertThat(redisMap.toString()).doesNotContain(organizationData.listOfOrganizations.get(0).id.toString());
            assertions.assertAll();
        });

        Then("Kafka message not contains user's permissions of the first org", () -> {
            String username = TokenProvider.getUsernameClaim(userData.token);

            ArrayList<GenericRecord> list = KafkaHelpers.pickSpecificMessageConsumeUserDataTopic(username, 3);

            assertions.assertThat(list.get(2).get("organizationAuthorizationData").toString())
                    .doesNotContain(organizationData.listOfOrganizations.get(0).id.toString());
            assertions.assertAll();
        });

    }
}
