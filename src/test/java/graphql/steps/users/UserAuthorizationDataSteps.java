package graphql.steps.users;

import com.google.gson.Gson;
import data_containers.GeneralData;
import data_containers.OrganizationData;
import data_containers.UserData;
import graphql.helpers.users.UserAuthorizationDataHelpers;
import graphql.responses.users.userAuthorizationData.ModulePermission;
import graphql.responses.users.userAuthorizationData.OrganizationAuthorizationData;
import graphql.responses.users.userAuthorizationData.UserAuthorizationDataResponse;
import io.cucumber.java8.En;
import org.assertj.core.api.SoftAssertions;

public class UserAuthorizationDataSteps implements En {
    GeneralData generalData;
    UserData userData;
    OrganizationData organizationData;
    SoftAssertions assertions;
    Gson gson;

    public UserAuthorizationDataSteps(GeneralData generalData, UserData userData, OrganizationData organizationData) {
        this.generalData = generalData;
        this.userData = userData;
        this.organizationData = organizationData;
        assertions = new SoftAssertions();
        gson = new Gson();

        When("UserAuthorizationData query is called", () -> {
            generalData.response = UserAuthorizationDataHelpers.userAuthorizationData(userData.token);
        });

        Then("Response contains globalReefAdmin true with all module permissions set to OWNER", () -> {
            UserAuthorizationDataResponse response =
                    gson.fromJson(generalData.response.asString(), UserAuthorizationDataResponse.class);
            OrganizationAuthorizationData orgData =
                    response.getData().getUserAuthorizationData().getOrganizationAuthorizationData().get(0);

            assertions.assertThat(response.getData().getUserAuthorizationData().getGlobalReefAdmin()).isTrue();
            assertions.assertThat(orgData.getOrganizationId()).isEqualTo(organizationData.id);
            for (ModulePermission permission : orgData.getModulePermissions()){
                assertions.assertThat(permission.getPermission()).isEqualTo("OWNER");
            }
        });
    }
}
