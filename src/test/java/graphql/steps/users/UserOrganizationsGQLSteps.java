package graphql.steps.users;

import com.google.gson.Gson;
import data_containers.GeneralData;
import data_containers.OrganizationData;
import data_containers.UserData;
import graphql.helpers.users.UserOrganizationsHelpers;
import graphql.responses.users.user_organizations.Content;
import graphql.responses.users.user_organizations.UserOrganizationsResponse;
import io.cucumber.java8.En;
import org.assertj.core.api.SoftAssertions;

public class UserOrganizationsGQLSteps implements En {
    GeneralData generalData;
    UserData userData;
    OrganizationData organizationData;
    Gson gson;
    SoftAssertions assertions;

    public UserOrganizationsGQLSteps(GeneralData generalData, UserData userData, OrganizationData organizationData) {
        this.generalData = generalData;
        this.userData = userData;
        this.organizationData = organizationData;
        gson = new Gson();
        assertions = new SoftAssertions();

        When("User Organizations query is called", () -> {
            generalData.response = UserOrganizationsHelpers
                    .userOrganizationsQueryWithParam(userData.token, "REEF", null, null);
        });


        Then("Response contains details about Reef Organization", () -> {
            UserOrganizationsResponse response = gson.fromJson(generalData.response.asString(), UserOrganizationsResponse.class);
            Content org = response.getData().getUserOrganizations().getContent().get(0);
            assertions.assertThat(response.getData().getUserOrganizations().getTotalItems()).isEqualTo(1);
            assertions.assertThat(response.getData().getUserOrganizations().getTotalPages()).isEqualTo(1);
            assertions.assertThat(org.getName()).isEqualTo("REEF");
            assertions.assertThat(org.getDescription()).isNotNull();
            assertions.assertThat(org.getId()).isEqualTo(organizationData.id.toString());
            assertions.assertThat(org.getStatus()).isEqualTo("ACTIVE");
            assertions.assertAll();

        });
    }
}
