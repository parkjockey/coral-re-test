package graphql.steps.users;

import common.helpers.db_helpers.SQLQueries;
import data_containers.GeneralData;
import data_containers.OrganizationData;
import data_containers.RoleData;
import data_containers.UserData;
import graphql.helpers.InviteUsersHelprs;
import graphql.helpers.users.UsersGqlHelpers;
import graphql.responses.users.FindOrgUserByEmailResponse;
import graphql.variables.invite_users.UserVariablres;
import io.cucumber.java8.En;
import org.assertj.core.api.SoftAssertions;

import java.util.ArrayList;

public class UsersSteps implements En {

    GeneralData generalData;
    UserData userData;
    OrganizationData organizationData;
    RoleData roleData;
    SoftAssertions assertions;

    public UsersSteps(GeneralData generalData, UserData userData, OrganizationData organizationData, RoleData roleData) {
        this.generalData = generalData;
        this.userData = userData;
        this.organizationData = organizationData;
        this.roleData = roleData;
        assertions = new SoftAssertions();

        When("UsersStatuses query is called", () -> {
            generalData.response = UsersGqlHelpers.userStatuses(generalData.token);
        });

        Then("Response contains list of statuses", () -> {
            assertions.assertThat(generalData.response.jsonPath().get("data.userStatuses.statuses[0]").toString()).isEqualTo("ACTIVE");
            assertions.assertThat(generalData.response.jsonPath().get("data.userStatuses.statuses[1]").toString()).isEqualTo("INVITED");
            assertions.assertThat(generalData.response.jsonPath().get("data.userStatuses.statuses[2]").toString()).isEqualTo("SUSPENDED");
            assertions.assertAll();
        });

        Given("Invite user to the first new organizations with Admin role", () -> {
            //Set user email
            UserVariablres userVariablres = new UserVariablres();
            userVariablres.setEmail(userData.email);
            ArrayList<UserVariablres> userList = new ArrayList<>() {{
                add(userVariablres);
            }};

            //Obtain Admin id from DB
            roleData.roleId = SQLQueries.findRoleId(organizationData.listOfOrganizations.get(0).id);
            //Invite user request
            generalData.response = InviteUsersHelprs
                    .inviteSpecificUsers(generalData.gqlBaseUri, generalData.token, userList, organizationData.listOfOrganizations.get(0).id, roleData.roleId);

        });

        Given("User id with invitation code is obtained for the first organization", () -> {
            generalData.response = InviteUsersHelprs.getUserByEmail(organizationData.listOfOrganizations.get(0).id, userData.email);
            userData.id = Integer.parseInt(FindOrgUserByEmailResponse.getUserId(generalData.response));
            userData.userName =  FindOrgUserByEmailResponse.getUserName(generalData.response);

            //Go to DB and find invitation code
            userData.invitationCode = SQLQueries.getUserInvitationCode(userData.userName);
        });


        Given("Invite user to the second new organizations with Admin role", () -> {
            //Set user email
            UserVariablres userVariablres = new UserVariablres();
            userVariablres.setEmail(userData.email);
            ArrayList<UserVariablres> userList = new ArrayList<>() {{
                add(userVariablres);
            }};

            //Obtain Admin id from DB
            roleData.roleId = SQLQueries.findRoleId(organizationData.listOfOrganizations.get(1).id);
            //Invite user request
            generalData.response = InviteUsersHelprs
                    .inviteSpecificUsers(generalData.gqlBaseUri, generalData.token, userList, organizationData.listOfOrganizations.get(1).id, roleData.roleId);

        });

        Given("User id with invitation code is obtained for the second organization", () -> {
            generalData.response = InviteUsersHelprs.getUserByEmail(organizationData.listOfOrganizations.get(1).id, userData.email);
            userData.id = Integer.parseInt(FindOrgUserByEmailResponse.getUserId(generalData.response));
            userData.userName =  FindOrgUserByEmailResponse.getUserName(generalData.response);

            //Go to DB and find invitation code
            userData.invitationCode = SQLQueries.getUserInvitationCode(userData.userName);
        });


    }
}
