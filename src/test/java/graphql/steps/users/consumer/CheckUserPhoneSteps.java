package graphql.steps.users.consumer;

import common.helpers.twilio.TwilioHelpers;
import data_containers.GeneralData;
import data_containers.OrganizationData;
import data_containers.RoleData;
import data_containers.UserData;
import graphql.helpers.users.consumers.ConsumerUserCheckPhoneNumberHelpers;
import io.cucumber.java8.En;
import org.assertj.core.api.SoftAssertions;

public class CheckUserPhoneSteps implements En {
    GeneralData generalData;
    UserData userData;
    OrganizationData organizationData;
    RoleData roleData;
    SoftAssertions assertions;

    public CheckUserPhoneSteps(GeneralData generalData, UserData userData, OrganizationData organizationData, RoleData roleData) {
        this.generalData = generalData;
        this.userData = userData;
        this.organizationData = organizationData;
        this.roleData = roleData;
        assertions = new SoftAssertions();

        When("ConsumerUserPhoneExists query is called", () -> {
           generalData.response = ConsumerUserCheckPhoneNumberHelpers.checkPhone(TwilioHelpers.phoneReceiver);
        });

        When("ConsumerUserPhoneExists query is called with not existing P#", () -> {

            generalData.response = ConsumerUserCheckPhoneNumberHelpers.checkPhone("+17028660065");
        });

        Then("Response contains consumerUserPhoneExists {string}", (String status) -> {
           assertions.assertThat(generalData.response
                   .jsonPath().get("data.consumerUserPhoneNumberExists").toString()).isEqualTo(status);
           assertions.assertAll();
        });
    }
}
