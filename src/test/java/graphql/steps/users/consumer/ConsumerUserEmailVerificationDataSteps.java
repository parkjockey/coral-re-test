package graphql.steps.users.consumer;

import com.google.gson.Gson;
import common.helpers.db_helpers.JdbcHelpers;
import common.helpers.db_helpers.SQLQueries;
import common.helpers.rest.TokenProvider;
import data_containers.GeneralData;
import data_containers.OrganizationData;
import data_containers.UserData;
import graphql.helpers.users.consumers.ConsumerEmailVerificationDataHelpers;
import graphql.responses.consumers.confirm_passwordless_consumer_response.ConfirmPasswordLessConsumerResponse;
import graphql.responses.consumers.confirm_passwordless_consumer_response.ConfirmPasswordlessConsumerUserSignUp;
import io.cucumber.java8.En;
import org.assertj.core.api.SoftAssertions;

public class ConsumerUserEmailVerificationDataSteps implements En {

    GeneralData generalData;
    OrganizationData organizationData;
    UserData userData;
    SoftAssertions assertions;
    Gson gson;
    String verificationCode;

    public ConsumerUserEmailVerificationDataSteps(GeneralData generalData, OrganizationData organizationData, UserData userData) {
        this.generalData = generalData;
        this.organizationData = organizationData;
        this.userData = userData;
        assertions = new SoftAssertions();
        gson = new Gson();

        Given("Consumer token and id are obtained", () -> {
            //Obtain token
            ConfirmPasswordLessConsumerResponse confirmResponse = gson.fromJson(generalData.response.asString(),
                    ConfirmPasswordLessConsumerResponse.class);
            ConfirmPasswordlessConsumerUserSignUp response = confirmResponse.getData().getConfirmPasswordlessConsumerUserSignUp();
            userData.token = response.getAuthenticationResponse().getAccessToken();

            //Obtain consumerId
            userData.userName = TokenProvider.getUsernameClaim(userData.token);
            userData.id = SQLQueries.findUserId(userData.userName);
            assertions.assertThat(userData.userName).isNotNull();
            assertions.assertThat(userData.id).isNotNull();
            assertions.assertAll();

        });

        Given("Verification code is obtained from DB", () -> {
            verificationCode = JdbcHelpers
                    .getStringValueFromDb("select email_verification_code from users where id=" + userData.id + ";", "email_verification_code");

            assertions.assertThat(verificationCode).isNotNull();
            assertions.assertAll();
        });

        When("ConsumerUserEmailVerificationData query is called", () -> {
            generalData.response = ConsumerEmailVerificationDataHelpers.consumerVerificationData(verificationCode);
        });

        Then("Response contains updated temporaryEmail", () -> {
            assertions.assertThat(generalData.response
                    .jsonPath().get("data.consumerUserEmailVerificationData.temporaryEmail").toString())
                    .isEqualTo(userData.email);
        });

    }
}
