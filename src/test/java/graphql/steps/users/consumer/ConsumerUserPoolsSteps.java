package graphql.steps.users.consumer;

import com.google.gson.Gson;
import data_containers.GeneralData;
import data_containers.OrganizationData;
import data_containers.RoleData;
import data_containers.UserData;
import graphql.helpers.users.consumers.ConsumerUsersHelpers;
import graphql.responses.consumers.consumer_user_pools.ConsumerUserPoolsResponse;
import io.cucumber.java8.En;
import org.assertj.core.api.SoftAssertions;

import java.util.ArrayList;

public class ConsumerUserPoolsSteps implements En {

    GeneralData generalData;
    UserData userData;
    OrganizationData organizationData;
    RoleData roleData;
    SoftAssertions assertions;
    Gson gson;

    public ConsumerUserPoolsSteps(GeneralData generalData, UserData userData, OrganizationData organizationData, RoleData roleData) {
        this.generalData = generalData;
        this.userData = userData;
        this.organizationData = organizationData;
        this.roleData = roleData;
        assertions = new SoftAssertions();
        gson = new Gson();

        When("Consumer User Pool query is called", () -> {
            generalData.response = ConsumerUsersHelpers.consumerUserPools(generalData.token);
        });

        Then("Response contains list of available regions", () -> {
            ConsumerUserPoolsResponse response = gson.fromJson(generalData.response.asString(), ConsumerUserPoolsResponse.class);
            ArrayList<graphql.responses.consumers.consumer_user_pools.UserPool> poolList = response.getData().getConsumerUserPools().getUserPools();

            assertions.assertThat(poolList.get(0).getKey()).isEqualTo("ca");
            assertions.assertThat(poolList.get(0).getName()).isEqualTo("Canada");
            assertions.assertThat(poolList.get(1).getKey()).isEqualTo("us");
            assertions.assertThat(poolList.get(1).getName()).isEqualTo("US");
            assertions.assertAll();
        });
    }
}
