package graphql.steps.users.consumer;

import com.google.gson.Gson;
import common.helpers.rest.HelperRest;
import data_containers.GeneralData;
import data_containers.OrganizationData;
import data_containers.RoleData;
import data_containers.UserData;
import graphql.helpers.users.PasswordLessHelpers;
import graphql.helpers.users.consumers.ConsumerUsersHelpers;
import graphql.helpers.users.consumers.ConsumerUsersPageHelpers;
import graphql.responses.ErrorResponses;
import graphql.responses.consumers.consumer_user_data.ConsumerUserData;
import graphql.responses.consumers.consumer_user_data.ConsumerUserDataResponse;
import graphql.responses.consumers.consumer_user_data_by_id.ConsumerUserDataById;
import graphql.responses.consumers.consumer_user_data_by_id.ConsumerUserDataByIdResponse;
import graphql.responses.consumers.consumer_users_page.ConsumerUserPageResponse;
import graphql.responses.consumers.consumer_users_page.Content;
import graphql.variables.consumers.ContactInformation;
import graphql.variables.consumers.UpdateConsumerVariables;
import graphql.variables.consumers.UserInformation;
import io.cucumber.java8.En;
import io.restassured.response.Response;
import org.assertj.core.api.SoftAssertions;

public class ConsumerUserSteps implements En {
    GeneralData generalData;
    UserData userData;
    OrganizationData organizationData;
    RoleData roleData;
    SoftAssertions assertions;
    Gson gson;

    public ConsumerUserSteps(GeneralData generalData, UserData userData, OrganizationData organizationData, RoleData roleData) {
        this.generalData = generalData;
        this.userData = userData;
        this.organizationData = organizationData;
        this.roleData = roleData;
        assertions = new SoftAssertions();
        gson = new Gson();

        When("ConsumerUserData query is called", () -> {
            generalData.response = ConsumerUsersHelpers.consumerUserData(userData.token);
        });

        Then("Response contains consumer user data", () -> {
            ConsumerUserDataResponse response = gson.fromJson(generalData.response.asString(), ConsumerUserDataResponse.class);
            ConsumerUserData consumerUserData = response.getData().getConsumerUserData();

            assertions.assertThat(consumerUserData.getConsumerId()).isNotNull();
            assertions.assertThat(consumerUserData.getUsername()).isNotNull();
            assertions.assertThat(consumerUserData.getEmail()).isEqualTo(userData.email);
            assertions.assertThat(consumerUserData.getUserStatus()).isEqualTo("ACTIVE");
            assertions.assertThat(consumerUserData.getSourceSystemName()).isNullOrEmpty();
            assertions.assertThat(consumerUserData.getSourceSystemUserIdentifier()).isNullOrEmpty();
            assertions.assertAll();

        });

        When("UpdateConsumerUser mutation is called with new first, last name phone and email", () -> {
            //Set new first, last name phone and email
            userData.firstName = "AutoFN"+ HelperRest.getRandomNumber();
            userData.lastName = "AutoLN"+ HelperRest.getRandomNumber();
            userData.email = HelperRest.getRandomEmail();
            userData.phone = "+1"+ HelperRest.getRandomNumber();

            ContactInformation contactInformation = new ContactInformation();
            contactInformation.setEmail(userData.email);
            contactInformation.setPhoneNumber(userData.phone);

            UserInformation userInformation = new UserInformation();
            userInformation.setFirstName(userData.firstName);
            userInformation.setLastName(userData.lastName);

            UpdateConsumerVariables consumerVariables = new UpdateConsumerVariables();
            consumerVariables.setUserId(userData.id);
            consumerVariables.setContactInformation(contactInformation);
            consumerVariables.setUserInformation(userInformation);

            //Send request
            generalData.response = ConsumerUsersHelpers.updateConsumerUser(generalData.token, consumerVariables);
        });

        Then("Consumer's first, last name phone and email are updated", () -> {
            //Get consumers data via get consumer by email
            generalData.response = ConsumerUsersPageHelpers.getConsumerByEmail(generalData.token, generalData.countryCode, userData.email);

            ConsumerUserPageResponse response = new Gson().fromJson(generalData.response.asString(), ConsumerUserPageResponse.class);
            Content consumer = response.getData().getConsumerUsersPage().getContent().get(0);
            //Check data
            assertions.assertThat(consumer.getEmail()).isEqualTo(userData.email);
            assertions.assertThat(consumer.getPhoneNumber()).isEqualTo(userData.phone);
            assertions.assertThat(consumer.getFullName()).isEqualTo(userData.firstName+" "+userData.lastName);
            assertions.assertThat(consumer.getGivenName()).isEqualTo(userData.firstName);
            assertions.assertThat(consumer.getFamilyName()).isEqualTo(userData.lastName);
            assertions.assertAll();
        });

        When("UpdateConsumerStatus mutation is called with status {string}", (String status) -> {
            generalData.response = ConsumerUsersHelpers.updateConsumerStatus(generalData.token, userData.id, status);
        });

        Then("Response contains updateConsumerStatus true", () -> {
            assertions.assertThat(generalData.response
                    .jsonPath().get("data.updateConsumerStatus").toString())
                    .as("ResetPassword is set to true").isEqualTo("true");
            assertions.assertAll();
        });

        Then("Suspended consumer can't login with received error message {string}", (String error) -> {
            Response response = PasswordLessHelpers.passwordLessConsumerLogin(userData.email);
            assertions.assertThat(ErrorResponses.getErrorMessageKey(response)).isEqualTo(error);
            assertions.assertAll();
        });

        Then("Reactivated consumer can obtain activation code again", () -> {
            generalData.response = PasswordLessHelpers.passwordLessConsumerLogin(userData.email);

            userData.session = generalData.response.jsonPath().get("data.passwordlessConsumerUserLogin.session").toString();
            assertions.assertThat(userData.session).isNotNull();
            assertions.assertAll();
        });

        When("ConsumerUserDataById query is called", () -> {
            generalData.response = ConsumerUsersHelpers.consumerUserDataById(generalData.token, userData.id);
        });

        Then("Response contains consumer user data by id", () -> {
            ConsumerUserDataByIdResponse response = gson.fromJson(generalData.response.asString(), ConsumerUserDataByIdResponse.class);
            ConsumerUserDataById consumerUserDataById = response.getData().getConsumerUserDataById();

            assertions.assertThat(consumerUserDataById.getConsumerId()).isNotNull();
            assertions.assertThat(consumerUserDataById.getUsername()).isNotNull();
            assertions.assertThat(consumerUserDataById.getEmail()).isEqualTo(userData.email);
            assertions.assertThat(consumerUserDataById.getUserStatus()).isEqualTo("ACTIVE");
            assertions.assertAll();

        });




    }
}
