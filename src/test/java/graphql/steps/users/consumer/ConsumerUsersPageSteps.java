package graphql.steps.users.consumer;

import com.google.gson.Gson;
import data_containers.GeneralData;
import data_containers.OrganizationData;
import data_containers.RoleData;
import data_containers.UserData;
import graphql.helpers.users.consumers.ConsumerUsersPageHelpers;
import graphql.responses.consumers.consumer_users_page.ConsumerUserPageResponse;
import graphql.responses.consumers.consumer_users_page.Content;
import io.cucumber.java8.En;
import org.assertj.core.api.SoftAssertions;

public class ConsumerUsersPageSteps implements En {

    GeneralData generalData;
    UserData userData;
    OrganizationData organizationData;
    RoleData roleData;
    SoftAssertions assertions;

    public ConsumerUsersPageSteps(GeneralData generalData, UserData userData, OrganizationData organizationData, RoleData roleData) {
        this.generalData = generalData;
        this.userData = userData;
        this.organizationData = organizationData;
        this.roleData = roleData;
        assertions = new SoftAssertions();

        When("Consumer Users Page Query is called with created email consumer", () -> {
           generalData.response = ConsumerUsersPageHelpers.getConsumerByEmail(generalData.token, generalData.countryCode, userData.email);
        });

        Then("Response contains email and other consumer information", () -> {
            ConsumerUserPageResponse response = new Gson().fromJson(generalData.response.asString(), ConsumerUserPageResponse.class);
            Content consumer = response.getData().getConsumerUsersPage().getContent().get(0);

            assertions.assertThat(consumer.getEmail()).isEqualTo(userData.email);
            assertions.assertThat(consumer.getConsumerId()).isNotNull();
            assertions.assertThat(consumer.getId()).isNotNull();
            assertions.assertThat(consumer.getUsername()).isNotNull();
            assertions.assertThat(consumer.getUserStatus()).isEqualTo("ACTIVE");
            assertions.assertAll();
        });

        When("Consumer Users Page Query is called with created phone# consumer", () -> {
            generalData.response = ConsumerUsersPageHelpers.getConsumerByPhone(generalData.token, generalData.countryCode, userData.phone);
        });

        Then("Response contains phone# and other consumer information", () -> {
            ConsumerUserPageResponse response = new Gson().fromJson(generalData.response.asString(), ConsumerUserPageResponse.class);
            Content consumer = response.getData().getConsumerUsersPage().getContent().get(0);

            assertions.assertThat(consumer.getPhoneNumber()).isEqualTo(userData.phone);
            assertions.assertThat(consumer.getConsumerId()).isNotNull();
            assertions.assertThat(consumer.getId()).isNotNull();
            assertions.assertThat(consumer.getUsername()).isNotNull();
            assertions.assertThat(consumer.getUserStatus()).isEqualTo("ACTIVE");
            assertions.assertAll();
        });

    }
}
