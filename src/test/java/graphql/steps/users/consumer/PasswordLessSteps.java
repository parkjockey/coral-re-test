package graphql.steps.users.consumer;

import com.google.gson.Gson;
import common.helpers.aws_helpers.sqs.SQSHelpers;
import common.helpers.mail_helpers.EmailHelpers;
import common.helpers.rest.HelperRest;
import common.helpers.rest.TokenProvider;
import common.helpers.twilio.TwilioHelpers;
import data_containers.GeneralData;
import data_containers.UserData;
import graphql.helpers.kafka.ConsumerUserDataKafkaResponse;
import graphql.helpers.kafka.KafkaHelpers;
import graphql.helpers.users.PasswordLessHelpers;
import graphql.responses.consumers.PasswordLessConsumerUserSignUpResponse;
import graphql.responses.consumers.confirm_passless_login_consumer_response.ConfirmPasswordLessConsumerLoginResponse;
import graphql.responses.consumers.confirm_passless_login_consumer_response.ConfirmPasswordlessConsumerUserLogin;
import graphql.responses.consumers.confirm_passwordless_consumer_response.ConfirmPasswordLessConsumerResponse;
import graphql.responses.consumers.confirm_passwordless_consumer_response.ConfirmPasswordlessConsumerUserSignUp;
import io.cucumber.java8.En;
import org.assertj.core.api.SoftAssertions;

public class PasswordLessSteps implements En {
    UserData userData;
    GeneralData generalData;
    SoftAssertions assertions;
    String oldOtp;
    String oldSession;
    Gson gson;

    public PasswordLessSteps(UserData userData, GeneralData generalData) {
        this.userData = userData;
        this.generalData = generalData;
        assertions = new SoftAssertions();
        userData.email = HelperRest.getRandomUatoMail();
        gson = new Gson();

        When("PasswordLessConsumerUserSignUp mutation is called with not existing email", () -> {
            generalData.response = PasswordLessHelpers.passwordLessConsumerUserSignUp(userData.email);
        });

        Then("Response contains session", () -> {
            userData.session = PasswordLessConsumerUserSignUpResponse.getSession(generalData.response);

            assertions.assertThat(userData.session).isNotNull();
            assertions.assertAll();
        });

        Then("Email is received with OTP code", () -> {
            String subject = "REEF Sign Up";
//            final Message[] mail = {null};
//            await("Wait condition to be true")
//                    .atMost(20, TimeUnit.SECONDS)
//                    .pollDelay(2, TimeUnit.SECONDS)
//                    .pollInterval(2, TimeUnit.SECONDS)
//                    .until(() -> {
//                        mail[0] = EmailHelpers.waitMailToBeObtained(userData.email, subject);
//                        return !mail[0].equals(null);
//                    });
//            userData.otpCode = EmailHelpers.getOTP(mail[0].getContent().toString());
            userData.otpCode = SQSHelpers.getSpecificMessage(userData.email, subject);
            assertions.assertThat(userData.otpCode).isNotNull();
            assertions.assertAll();
        });


        Given("First OTP code and session are obtained for not existing email", () -> {
            generalData.response = PasswordLessHelpers.passwordLessConsumerUserSignUp(userData.email);
         //   Message mail = EmailHelpers.waitMailToBeObtained(userData.email, );
            oldOtp = SQSHelpers.getSpecificMessage(userData.email, "REEF Sign Up");
            oldSession = PasswordLessConsumerUserSignUpResponse.getSession(generalData.response);
        });

        When("PasswordLessConsumerUserSignUp mutation is called again for the same email", () -> {
            generalData.response = PasswordLessHelpers.passwordLessConsumerUserSignUp(userData.email);
            String subject = "REEF Sign Up";
//            final Message[] mail = {null};
//            await("Wait condition to be true")
//                    .atMost(20, TimeUnit.SECONDS)
//                    .pollDelay(2, TimeUnit.SECONDS)
//                    .pollInterval(2, TimeUnit.SECONDS)
//                    .until(() -> {
//                        mail[0] = EmailHelpers.waitMailToBeObtained(userData.email, subject);
//                        return !mail[0].equals(null);
//                    });
//            userData.otpCode = EmailHelpers.getOTP(mail[0].getContent().toString());
            userData.otpCode = SQSHelpers.getSpecificMessage(userData.email, subject);
            userData.session = PasswordLessConsumerUserSignUpResponse.getSession(generalData.response);
        });

        Then("New OTP code is obtained", () -> {
            assertions.assertThat(oldOtp).isNotEqualTo(userData.otpCode);
            assertions.assertThat(oldSession).isNotEqualTo(userData.session);
            assertions.assertAll();
        });

        Given("OTP code and session are obtained for not existing email", () -> {
            generalData.response = PasswordLessHelpers.passwordLessConsumerUserSignUp(userData.email);
            String subject = "REEF Sign Up";
//            final Message[] mail = {null};
//            await("Wait condition to be true")
//                    .atMost(20, TimeUnit.SECONDS)
//                    .pollDelay(2, TimeUnit.SECONDS)
//                    .pollInterval(2, TimeUnit.SECONDS)
//                    .until(() -> {
//                        mail[0] = EmailHelpers.waitMailToBeObtained(userData.email, subject);
//                        return !mail[0].equals(null);
//                    });
//
//            userData.otpCode = EmailHelpers.getOTP(mail[0].getContent().toString());
            userData.otpCode = EmailHelpers.getOTP(SQSHelpers.getSpecificMessage(userData.email, subject));
            userData.session = PasswordLessConsumerUserSignUpResponse.getSession(generalData.response);
        });

        When("ConfirmPasswordlessConsumerUserSignUp mutation is called( again)", () -> {
            generalData.response = PasswordLessHelpers
                    .confirmPasswordLessUserSignUp(userData.email, userData.otpCode, userData.session);
        });

        Then("Response contains successfulAuthentication true and all tokens", () -> {
            ConfirmPasswordLessConsumerResponse confirmResponse = gson.fromJson(generalData.response.asString(),
                    ConfirmPasswordLessConsumerResponse.class);
            ConfirmPasswordlessConsumerUserSignUp response = confirmResponse.getData().getConfirmPasswordlessConsumerUserSignUp();
            userData.token = response.getAuthenticationResponse().getAccessToken();
            assertions.assertThat(response.getSuccessfulAuthentication()).isTrue();
            assertions.assertThat(response.getSession()).isNull();
            assertions.assertThat( userData.token).isNotNull();
            assertions.assertThat(response.getAuthenticationResponse().getIdToken()).isNotNull();
            assertions.assertThat(response.getAuthenticationResponse().getRefreshToken()).isNotNull();
            assertions.assertThat(response.getAuthenticationResponse().getExpiresIn()).isEqualTo(3600);
            assertions.assertAll();

        });

        When("ConfirmPasswordlessConsumerUserSignUp mutation is called with old otp and session", () -> {
            generalData.response = PasswordLessHelpers
                    .confirmPasswordLessUserSignUp(userData.email, oldOtp, oldSession);
        });

        Then("Consumer can't be confirmed", () -> {
            // Write code here that turns the phrase above into concrete actions
            throw new cucumber.api.PendingException();
        });

        When("PasswordLessConsumerUserLogin query is called", () -> {
            generalData.response = PasswordLessHelpers.passwordLessConsumerLogin(userData.email);
        });

        Then("Login Email is received with OTP code", () -> {
            String subject = "REEF Login";
//            final Message[] mail = {null};
//            await("Wait condition to be true")
//                    .atMost(20, TimeUnit.SECONDS)
//                    .pollDelay(2, TimeUnit.SECONDS)
//                    .pollInterval(2, TimeUnit.SECONDS)
//                    .until(() -> {
//                        mail[0] = EmailHelpers.waitMailToBeObtained(userData.email, subject);
//                        return !mail[0].equals(null);
//                    });
//            userData.otpCode = EmailHelpers.getOTP(mail[0].getContent().toString());
            userData.otpCode = EmailHelpers.getOTP(SQSHelpers.getSpecificMessage(userData.email, subject));

            assertions.assertThat(userData.otpCode).isNotNull();
            assertions.assertAll();
        });

        Then("Response has session code", () -> {
            userData.session = generalData.response.jsonPath().get("data.passwordlessConsumerUserLogin.session").toString();
            assertions.assertThat(userData.session).isNotNull();
            assertions.assertAll();
        });

        When("ConfirmPasswordLessConsumerUserLogin mutation is called", () -> {
            generalData.response = PasswordLessHelpers
                    .confirmPasswordLessConsumerUserLogin(userData.email, userData.otpCode, userData.session);
        });

        Then("Confirm PasswordLess Login Response contains successfulAuthentication true and all tokens", () -> {
            ConfirmPasswordLessConsumerLoginResponse confirmResponse = gson.fromJson(generalData.response.asString(),
                    ConfirmPasswordLessConsumerLoginResponse.class);
            ConfirmPasswordlessConsumerUserLogin response = confirmResponse.getData().getConfirmPasswordlessConsumerUserLogin();
            userData.token = response.getAuthenticationResponse().getAccessToken();
            assertions.assertThat(response.getSuccessfulAuthentication()).isTrue();
            assertions.assertThat(response.getSession()).isNull();
            assertions.assertThat( userData.token).isNotNull();
            assertions.assertThat(response.getAuthenticationResponse().getIdToken()).isNotNull();
            assertions.assertThat(response.getAuthenticationResponse().getRefreshToken()).isNotNull();
            assertions.assertThat(response.getAuthenticationResponse().getExpiresIn()).isEqualTo(3600);
            assertions.assertAll();
        });

        When("ConfirmOrganizationUserInvite mutation is called with enterprize admin user credentials", () -> {
            generalData.response = PasswordLessHelpers.passwordLessConsumerLogin(generalData.adNoSSOUsername);
        });


        When("PasswordLessConsumerUserSignUp mutation is called with not existing P#", () -> {
            generalData.response = PasswordLessHelpers.passwordLessConsumerUserSignUpPhone(TwilioHelpers.phoneReceiver);
        });

        Then("SMS is received with OTP code", () -> {
           userData.otpCode = TwilioHelpers.getOtpSignUp();
            assertions.assertThat(userData.otpCode).isNotNull();
            assertions.assertAll();
        });


        Given("OTP code and session are obtained for not existing phone number", () -> {
            generalData.response = PasswordLessHelpers.passwordLessConsumerUserSignUpPhone(TwilioHelpers.phoneReceiver);

            userData.otpCode = TwilioHelpers.getOtpSignUp();
            userData.session = PasswordLessConsumerUserSignUpResponse.getSession(generalData.response);
        });

        When("ConfirmPasswordlessConsumerUserSignUp mutation is called for P#", () -> {
            generalData.response = PasswordLessHelpers
                    .confirmPasswordLessUserSignUpPhone(TwilioHelpers.phoneReceiver, userData.otpCode, userData.session);
        });

        When("PasswordLessConsumerUserLogin query is called with phone number", () -> {
            generalData.response = PasswordLessHelpers.passwordLessConsumerLoginPhone(TwilioHelpers.phoneReceiver);
        });

        Then("Login SMS is received with OTP code", () -> {
            userData.otpCode = TwilioHelpers.getOtpLogin();
            assertions.assertThat(userData.otpCode).isNotNull();
            assertions.assertAll();
        });

        When("ConfirmPasswordLessConsumerUserLogin mutation is called for Phone number", () -> {
            generalData.response = PasswordLessHelpers
                    .confirmPasswordLessConsumerUserLoginPhone(TwilioHelpers.phoneReceiver, userData.otpCode, userData.session);
        });

        Then("Kafka message is obtained for newly created consumer user", () -> {
            String userName = TokenProvider.getUsernameClaim(userData.token);
            ConsumerUserDataKafkaResponse message = KafkaHelpers.consumeConsumerDataTopic(userName);

            assertions.assertThat(message.getId()).isNotNull();
            assertions.assertThat(message.getUsername()).isEqualTo(userName);
            assertions.assertThat(message.getStatus()).isEqualTo("ACTIVE");
            assertions.assertAll();
        });
    }
}
