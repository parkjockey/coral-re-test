package graphql.steps.users.consumer;

import com.google.gson.Gson;
import common.helpers.twilio.TwilioHelpers;
import data_containers.GeneralData;
import data_containers.OrganizationData;
import data_containers.RoleData;
import data_containers.UserData;
import graphql.helpers.users.consumers.ConsumerUpdatePhoneHelpers;
import graphql.helpers.users.consumers.ConsumerUsersHelpers;
import graphql.responses.consumers.consumer_user_data.ConsumerUserData;
import graphql.responses.consumers.consumer_user_data.ConsumerUserDataResponse;
import io.cucumber.java8.En;
import org.assertj.core.api.SoftAssertions;

public class UpdatePhoneNumberSteps implements En {
    GeneralData generalData;
    UserData userData;
    OrganizationData organizationData;
    RoleData roleData;
    SoftAssertions assertions;
    Gson gson;


    public UpdatePhoneNumberSteps(GeneralData generalData, UserData userData, OrganizationData organizationData, RoleData roleData) {
        this.generalData = generalData;
        this.userData = userData;
        this.organizationData = organizationData;
        this.roleData = roleData;
        assertions = new SoftAssertions();
        gson = new Gson();

        When("Update Phone Number gql is called for consumer user", () -> {
                generalData.response = ConsumerUpdatePhoneHelpers.updatePhoneNumber(userData.token, TwilioHelpers.phoneReceiver);
        });

        Then("Response contains Update Phone Number true", () -> {
            assertions.assertThat(generalData.response.jsonPath().get("data.updatePhoneNumber").toString()).isEqualTo("true");
            assertions.assertAll();
        });

        Then("Sms is received with OTP for phone update", () -> {
            userData.otpCode = TwilioHelpers.getPhoneUpdateOtp();
            assertions.assertThat( userData.otpCode).isNotNull();
            assertions.assertAll();
        });

        When("ConfirmConsumerUserPhoneNumberWithOtp mutation is called", () -> {
            generalData.response = ConsumerUpdatePhoneHelpers.confirmConsumerUserPhoneNumberWithOtp(userData.token, userData.otpCode, TwilioHelpers.phoneReceiver);
        });

        Then("Response contains Confirm Consumer User Phone Number with OTP true", () -> {
            assertions.assertThat(generalData.response.jsonPath().get("data.confirmConsumerUserPhoneNumberWithOtp").toString()).isEqualTo("true");
            assertions.assertAll();
        });

        Then("Consumer user now has temporary phone number", () -> {
            generalData.response = ConsumerUsersHelpers.consumerUserData(userData.token);
            ConsumerUserDataResponse response = gson.fromJson(generalData.response.asString(), ConsumerUserDataResponse.class);
            ConsumerUserData consumerUserData = response.getData().getConsumerUserData();

            assertions.assertThat(consumerUserData.getTemporaryPhoneNumber()).isEqualTo(TwilioHelpers.phoneReceiver);
            assertions.assertThat(consumerUserData.getPhoneNumber()).isEqualTo(userData.phone);
            assertions.assertAll();
        });

        Then("Consumer user now has new phone", () -> {
            generalData.response = ConsumerUsersHelpers.consumerUserData(userData.token);
            ConsumerUserDataResponse response = gson.fromJson(generalData.response.asString(), ConsumerUserDataResponse.class);
            ConsumerUserData consumerUserData = response.getData().getConsumerUserData();

            assertions.assertThat(consumerUserData.getPhoneNumber()).isEqualTo(TwilioHelpers.phoneReceiver);
            assertions.assertThat(consumerUserData.getTemporaryPhoneNumber()).isNull();
            assertions.assertAll();
        });

    }
}

