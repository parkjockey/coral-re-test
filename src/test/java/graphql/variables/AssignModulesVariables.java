package graphql.variables;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import lombok.Setter;

import java.util.List;

@Setter
public class AssignModulesVariables {
    @SerializedName("modulesList")
    @Expose
    public List<Integer> modulesList = null;
    @SerializedName("organizationId")
    @Expose
    private Integer organizationId;
}
