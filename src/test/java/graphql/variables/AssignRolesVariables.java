package graphql.variables;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import lombok.Setter;

import java.util.List;
@Setter
public class AssignRolesVariables {
    @SerializedName("userId")
    @Expose
    private Object userId;
    @SerializedName("organizationId")
    @Expose
    private Integer organizationId;
    @SerializedName("rolesList")
    @Expose
    public List<Integer> rolesList = null;
}
