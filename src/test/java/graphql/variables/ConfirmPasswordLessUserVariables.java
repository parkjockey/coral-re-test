package graphql.variables;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import lombok.Setter;

@Setter
public class ConfirmPasswordLessUserVariables {
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("code")
    @Expose
    private String code;
    @SerializedName("session")
    @Expose
    private String session;
    @SerializedName("phoneNumber")
    @Expose
    private String phoneNumber;
}
