package graphql.variables;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import lombok.Setter;

@Setter
public class FindOrganizationRoleByIdVariables {
    @SerializedName("organizationId")
    @Expose
    private Integer organizationId;
    @SerializedName("roleId")
    @Expose
    private Integer roleId;
}
