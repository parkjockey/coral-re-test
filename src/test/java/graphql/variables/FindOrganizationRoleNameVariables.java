package graphql.variables;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import lombok.Setter;

@Setter
public class FindOrganizationRoleNameVariables {
    @SerializedName("organizationId")
    @Expose
    private Integer organizationId;
    @SerializedName("name")
    @Expose
    private String name;
}
