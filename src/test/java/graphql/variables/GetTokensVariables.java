package graphql.variables;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import lombok.Setter;

@Setter
public class GetTokensVariables {
    @SerializedName("accessCode")
    @Expose
    private String accessCode;
    @SerializedName("redirectUrl")
    @Expose
    private String redirectUrl;
}
