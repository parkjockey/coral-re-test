package graphql.variables;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import lombok.Setter;

@Setter
public class RoleModulesVariables {
    @SerializedName("organizationId")
    @Expose
    private String organizationId;
    @SerializedName("roleId")
    @Expose
    private String roleId;
}
