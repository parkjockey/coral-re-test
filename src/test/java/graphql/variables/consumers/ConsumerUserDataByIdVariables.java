package graphql.variables.consumers;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import lombok.Setter;

@Setter
public class ConsumerUserDataByIdVariables {
    @SerializedName("id")
    @Expose
    private Integer id;
}
