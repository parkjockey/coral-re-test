package graphql.variables.consumers;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import lombok.Setter;

@Setter
public class UpdateConsumerStatusVariables {
    @SerializedName("userId")
    @Expose
    private Integer userId;
    @SerializedName("userStatus")
    @Expose
    private String userStatus;
}
