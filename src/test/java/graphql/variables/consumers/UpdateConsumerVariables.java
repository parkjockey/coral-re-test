package graphql.variables.consumers;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import lombok.Setter;

@Setter
public class UpdateConsumerVariables {
    @SerializedName("userId")
    @Expose
    private Integer userId;
    @SerializedName("userInformation")
    @Expose
    private UserInformation userInformation;
    @SerializedName("contactInformation")
    @Expose
    private ContactInformation contactInformation;
}
