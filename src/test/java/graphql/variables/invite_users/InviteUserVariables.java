package graphql.variables.invite_users;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;

@Getter
@Setter
public class InviteUserVariables {
    @SerializedName("usersList")
    @Expose
    public ArrayList<UserVariablres> usersList;
    @SerializedName("roleId")
    @Expose
    private Integer roleId;
    @SerializedName("organizationId")
    @Expose
    private Integer organizationId;
}
