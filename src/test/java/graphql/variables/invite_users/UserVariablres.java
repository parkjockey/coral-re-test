package graphql.variables.invite_users;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UserVariablres {
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("username")
    @Expose
    private String username;
    @SerializedName("sourceId")
    @Expose
    private String sourceId;
    @SerializedName("userSourceIdentifier")
    @Expose
    private String userSourceIdentifier;
}
