package graphql.variables.update_org_role_modules;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;

@Getter
@Setter
public class SubModules {
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("permissionLevel")
    @Expose
    private String permissionLevel;
    @SerializedName("subModules")
    @Expose
    public ArrayList<SubSubModules> subModules =new ArrayList<>();
}
