package graphql.variables.update_org_role_modules;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SubSubModules {
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("permissionLevel")
    @Expose
    private String permissionLevel;
}
