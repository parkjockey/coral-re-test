package graphql.variables.update_org_role_modules;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;

@Getter
@Setter
public class UpdateOrganizationRoleModulesVariables {
    @SerializedName("roleId")
    @Expose
    private Integer roleId;
    @SerializedName("organizationId")
    @Expose
    private Integer organizationId;
    @SerializedName("modules")
    @Expose
    public ArrayList<Module> modules;
}
