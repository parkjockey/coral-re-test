package graphql.variables.users;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import lombok.Data;

import java.util.ArrayList;

@Data
public class OrganizationUsersPageVariables {
    @SerializedName("organizationId")
    @Expose
    private Integer organizationId;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("roleIds")
    @Expose
    public ArrayList<Integer> roleIds = null;
    @SerializedName("statuses")
    @Expose
    public ArrayList<String> statuses = null;
    @SerializedName("dateFrom")
    @Expose
    private String dateFrom;
    @SerializedName("dateTo")
    @Expose
    private String dateTo;
    @SerializedName("orderBy")
    @Expose
    public ArrayList<String> orderBy = null;
    @SerializedName("pageNumber")
    @Expose
    private Integer pageNumber;
    @SerializedName("pageSize")
    @Expose
    private Integer pageSize;
}
