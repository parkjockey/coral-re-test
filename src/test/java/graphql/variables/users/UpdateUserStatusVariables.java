package graphql.variables.users;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import lombok.Setter;

@Setter
public class UpdateUserStatusVariables {
    @SerializedName("organizationId")
    @Expose
    private Integer organizationId;
    @SerializedName("userId")
    @Expose
    private Integer userId;
    @SerializedName("userStatus")
    @Expose
    private String userStatus;
}
