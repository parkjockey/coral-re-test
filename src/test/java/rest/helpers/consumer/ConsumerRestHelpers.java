package rest.helpers.consumer;

import com.google.gson.Gson;
import common.helpers.rest.HelperRest;
import data_containers.GeneralData;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import rest.requests.consumers.ConsumerSignUpBody;
import rest.requests.consumers.ConsumerUserByEmailAddress;
import rest.requests.consumers.ConsumerUserById;

import static org.hamcrest.Matchers.nullValue;

public class ConsumerRestHelpers {
    static GeneralData generalData;
    static Gson gson = new Gson();
    private static Logger logger = LoggerFactory.getLogger(ConsumerRestHelpers.class);

    public ConsumerRestHelpers(GeneralData generalData) {
        this.generalData = generalData;
    }

    public static Response consumerSignUpWithOtp(String email, String phone, String sourceId, String userSourceIdentifier) {
        ConsumerSignUpBody body = new ConsumerSignUpBody();
        if (!email.equals("")) body.setEmail(email);
        if (!phone.equals("")) body.setPhoneNumber(phone);

        body.setSourceId(sourceId);
        body.setUserSourceIdentifier(userSourceIdentifier);
        body.setCountryCode(generalData.countryCode);

        RequestSpecBuilder builder = new RequestSpecBuilder();
        builder.setBaseUri(generalData.restBaseUri);
        builder.setBasePath("/api/v2/consumers/sign-up");
        builder.setContentType(ContentType.JSON);
        builder.setBody(gson.toJson(body));
        RequestSpecification rSpec = builder.build();

        Response response = HelperRest.sendPut(rSpec);
        if (response.toString().equals(nullValue()) || response.toString().contains("error")) {
            logger.error("ConsumerSignUp OTP not obtained " + response.toString());
        }
        return response;

    }

    public static Response consumerLoginWithOtp(String email, String phone) {
        ConsumerSignUpBody body = new ConsumerSignUpBody();
        if (!email.equals("")) body.setEmail(email);
        if (!phone.equals("")) body.setPhoneNumber(phone);

        RequestSpecBuilder builder = new RequestSpecBuilder();
        builder.setBaseUri(generalData.restBaseUri);
        builder.setBasePath("/api/v2/consumers/login");
        builder.setContentType(ContentType.JSON);
        builder.setBody(gson.toJson(body));
        RequestSpecification rSpec = builder.build();

        Response response = HelperRest.sendPut(rSpec);
        if (response.toString().equals(nullValue()) || response.toString().contains("error")) {
            logger.error("Login OTP not obtained " + response.toString());
        }
        return response;

    }

    public static Response getConsumerUserById(String token, Integer id) {
        RequestSpecBuilder builder = new RequestSpecBuilder();
        ConsumerUserById body = new ConsumerUserById();
        body.setId(id);
        builder.setBaseUri(generalData.restBaseUri);
        builder.setBasePath("/api/v1/internal/consumers/" + id);
        builder.addHeader("Authorization", token);
        builder.addQueryParam("id", id);
        builder.setContentType(ContentType.JSON);
        builder.setBody(body);
        RequestSpecification rSpec = builder.build();

        Response response = HelperRest.sendGetRequest(rSpec);
        if (response.toString().equals(nullValue()) || response.toString().contains("error")) {
            logger.error("GetUsersByUserId not obtained " + response.toString());
        }
        return response;

    }

    public static Response getConsumerUserByEmailAddress(String token, String email) {
        RequestSpecBuilder builder = new RequestSpecBuilder();
        ConsumerUserByEmailAddress body = new ConsumerUserByEmailAddress();
        body.setEmail(email);
        builder.setBaseUri(generalData.restBaseUri);
        builder.setBasePath("/api/v1/internal/consumers/email/" + email);
        builder.addHeader("Authorization", token);
        builder.addQueryParam("email", email);
        builder.setContentType(ContentType.JSON);
        builder.setBody(body);
        RequestSpecification rSpec = builder.build();

        Response response = HelperRest.sendGetRequest(rSpec);
        if (response.toString().equals(nullValue()) || response.toString().contains("error")) {
            logger.error("GetUsersByUserId not obtained " + response.toString());
        }
        return response;

    }

}
