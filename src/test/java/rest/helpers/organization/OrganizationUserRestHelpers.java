package rest.helpers.organization;

import com.google.gson.Gson;
import common.helpers.rest.HelperRest;
import data_containers.GeneralData;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import rest.helpers.user.UserRestHelpers;
import rest.requests.organization.*;
import rest.requests.organization.update_user.UpdateOrganizationUserAccount;

import static org.hamcrest.Matchers.nullValue;

public class OrganizationUserRestHelpers {
    static GeneralData generalData;
    static Gson gson = new Gson();
    private static Logger logger = LoggerFactory.getLogger(UserRestHelpers.class);

    public OrganizationUserRestHelpers(GeneralData generalData) {
        this.generalData = generalData;
    }

    public static Response getOrganizationUsers(String token, Integer organizationId) {

        RequestSpecBuilder builder = new RequestSpecBuilder();
        OrganizationUsers body = new OrganizationUsers();
        body.setOrganizationId(organizationId);
        builder.setBaseUri(generalData.restBaseUri);
        builder.setBasePath("/api/v1/organizations/" + organizationId + "/users");
        builder.addHeader("Authorization", token);
        builder.addQueryParam("organizationId", organizationId);
        builder.setContentType(ContentType.JSON);
        builder.setBody(body);
        RequestSpecification rSpec = builder.build();

        Response response = HelperRest.sendGetRequest(rSpec);
        if (response.toString().equals(nullValue()) || response.toString().contains("error")) {
            logger.error("Get organization users not obtained " + response.toString());
        }
        return response;

    }

    public static Response getOrganizationUserByEmail(String token, Integer organizationId, String email) {

        RequestSpecBuilder builder = new RequestSpecBuilder();
        OrganizationUserByEmail body = new OrganizationUserByEmail();
        body.setOrganizationId(organizationId);
        body.setEmail(email);
        builder.setBaseUri(generalData.restBaseUri);
        builder.setBasePath("/api/v1/organizations/" + organizationId + "/users/emails/" + email);
        builder.addHeader("Authorization", token);
        builder.addQueryParam("organizationId", organizationId);
        builder.addQueryParam("email", email);
        builder.setContentType(ContentType.JSON);
        builder.setBody(body);
        RequestSpecification rSpec = builder.build();

        Response response = HelperRest.sendGetRequest(rSpec);
        if (response.toString().equals(nullValue()) || response.toString().contains("error")) {
            logger.error("Get organization users not obtained " + response.toString());
        }
        return response;

    }

    public static Response getOrganizationUsersByIds(String token, Integer organizationId, Integer id, Integer pageNumber, Integer  pageSize ) {

        RequestSpecBuilder builder = new RequestSpecBuilder();
        OrganizationUserByIds body = new OrganizationUserByIds();
        body.setOrganizationId(organizationId);
        body.setId(id);
        builder.setBaseUri(generalData.restBaseUri);
        builder.setBasePath("/api/v1/internal/organizations/" + organizationId + "/users");
        builder.addHeader("Authorization", token);
        builder.addQueryParam("organizationId", organizationId);
        builder.addQueryParam("userIds", id);
        builder.addQueryParam("pageNumber", pageNumber==null? 0 : pageNumber);
        builder.addQueryParam("pageSize", pageSize==null? 10 : pageSize);
        builder.setContentType(ContentType.JSON);
        builder.setBody(body);
        RequestSpecification rSpec = builder.build();

        Response response = HelperRest.sendGetRequest(rSpec);
        if (response.toString().equals(nullValue()) || response.toString().contains("error")) {
            logger.error("Get organization users not obtained " + response.toString());
        }
        return response;

    }

    public static Response getOrganizationUserData(String token, Integer organizationId) {

        RequestSpecBuilder builder = new RequestSpecBuilder();
        OrganizationUserData body = new OrganizationUserData();
        body.setOrganizationId(organizationId);
        builder.setBaseUri(generalData.restBaseUri);
        builder.setBasePath("/api/v1/organizations/" + organizationId + "/users/me");
        builder.addHeader("Authorization", token);
        builder.addQueryParam("organizationId", organizationId);
        builder.setContentType(ContentType.JSON);
        builder.setBody(body);
        RequestSpecification rSpec = builder.build();

        Response response = HelperRest.sendGetRequest(rSpec);
        if (response.toString().equals(nullValue()) || response.toString().contains("error")) {
            logger.error("Get organization users not obtained " + response.toString());
        }
        return response;

    }

    public static Response deleteOrganizationUserAccount(String token, String email,  Integer organizationId, Integer userCountLimit) {

        RequestSpecBuilder builder = new RequestSpecBuilder();
        DeleteOrganizationUserAccount body = new DeleteOrganizationUserAccount();
        body.setOrganizationId(organizationId);
        body.setEmail(email);
        body.setUserCountLimit(userCountLimit);
        builder.setBaseUri(generalData.restBaseUri);
        builder.setBasePath("/api/v1/organizations/" + organizationId + "/users/" + email + "/" + userCountLimit);
        builder.addHeader("Authorization", token);
        builder.addQueryParam("organizationId", organizationId);
        builder.addQueryParam("email", email);
        builder.addQueryParam("userCountLimit", userCountLimit);
        builder.setContentType(ContentType.JSON);
        builder.setBody(body);
        RequestSpecification rSpec = builder.build();

        Response response = HelperRest.sendDeleteRequest(rSpec);
        if (response.toString().equals(nullValue()) || response.toString().contains("error")) {
            logger.error("Delete organization users not obtained " + response.toString());
        }
        return response;

    }

    public static Response updateOrganizationUserAccount(String token, Integer organizationId, Integer id, UpdateOrganizationUserAccount userAccount) {

        RequestSpecBuilder builder = new RequestSpecBuilder();
        UpdateOrganizationUserAccount body = new UpdateOrganizationUserAccount();
        body.setOrganizationId(organizationId);
        body.setId(id);
        body.setUserInformation(userAccount.getUserInformation());
        body.setContactInformation(userAccount.getContactInformation());
        builder.setBaseUri(generalData.restBaseUri);
        builder.setBasePath("/api/v1/organizations/" + organizationId + "/users/" + id);
        builder.addHeader("Authorization", token);
        builder.setContentType(ContentType.JSON);
        builder.setBody(body);

        RequestSpecification rSpec = builder.build();

        Response response = HelperRest.sendPut(rSpec);
        if (response.toString().equals(nullValue()) || response.toString().contains("error")) {
            logger.error("Update organization users not obtained " + response.toString());
        }
        return response;

    }


    public static Response getOrganizationUserByPhoneNumber(String token, Integer organizationId, String phoneNumber) {

        RequestSpecBuilder builder = new RequestSpecBuilder();
        GetOrganizationUserByPhoneNumber body = new GetOrganizationUserByPhoneNumber();
        body.setOrganizationId(organizationId);
        body.setPhoneNumber(phoneNumber);
        builder.setBaseUri(generalData.restBaseUri);
        builder.setBasePath("/api/v1/organizations/" + organizationId + "/users/phones/" + phoneNumber);
        builder.addHeader("Authorization", token);
        builder.addQueryParam("organizationId", organizationId);
        builder.addQueryParam("phoneNumber", phoneNumber);
        builder.setContentType(ContentType.JSON);
        builder.setBody(body);
        RequestSpecification rSpec = builder.build();

        Response response = HelperRest.sendGetRequest(rSpec);
        if (response.toString().equals(nullValue()) || response.toString().contains("error")) {
            logger.error("Get organization users not obtained " + response.toString());
        }
        return response;

    }




}
