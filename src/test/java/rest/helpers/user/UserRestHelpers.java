package rest.helpers.user;

import com.google.gson.Gson;
import common.helpers.rest.HelperRest;
import data_containers.GeneralData;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;

import static org.hamcrest.Matchers.nullValue;

public class UserRestHelpers {
    static GeneralData generalData;
    static Gson gson = new Gson();
    private static Logger logger = LoggerFactory.getLogger(UserRestHelpers.class);

    public UserRestHelpers(GeneralData generalData) {
        this.generalData = generalData;
    }

    public static Response getUsersByIds(String token, ArrayList<Integer>userIds, Integer pageNumber, Integer  pageSize) {

        RequestSpecBuilder builder = new RequestSpecBuilder();
        builder.setBaseUri(generalData.restBaseUri);
        builder.setBasePath("/api/v1/internal/users");
        builder.addHeader("Authorization", token);
        builder.addQueryParam("userIds", userIds);
        builder.addQueryParam("pageNumber", pageNumber==null? 0 : pageNumber);
        builder.addQueryParam("pageSize", pageSize==null? 10 : pageSize);
        builder.setContentType(ContentType.JSON);
        RequestSpecification rSpec = builder.build();

        Response response = HelperRest.sendGetRequest(rSpec);
        if (response.toString().equals(nullValue()) || response.toString().contains("error")) {
            logger.error("GetUsersByIds not obtained " + response.toString());
        }
        return response;

    }
}
