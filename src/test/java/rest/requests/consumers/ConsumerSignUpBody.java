package rest.requests.consumers;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import lombok.Setter;

@Setter
public class ConsumerSignUpBody {
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("phoneNumber")
    @Expose
    private String phoneNumber;
    @SerializedName("countryCode")
    @Expose
    private String countryCode;
    @SerializedName("sourceId")
    @Expose
    private String sourceId;
    @SerializedName("userSourceIdentifier")
    @Expose
    private String userSourceIdentifier;
}
