package rest.requests.consumers;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ConsumerUserByEmailAddress {
    @SerializedName("email")
    @Expose
    private String email;
}
