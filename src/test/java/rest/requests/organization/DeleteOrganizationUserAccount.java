package rest.requests.organization;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DeleteOrganizationUserAccount {
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("organizationId")
    @Expose
    private Integer organizationId;
    @SerializedName("userCountLimit")
    @Expose
    private Integer userCountLimit;
}
