package rest.requests.organization;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class OrganizationUserByEmail {
    @SerializedName("organizationId")
    @Expose
    private Integer organizationId;
    @SerializedName("email")
    @Expose
    private String email;
}
