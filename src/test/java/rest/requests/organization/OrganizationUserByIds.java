package rest.requests.organization;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;

@Getter
@Setter
public class OrganizationUserByIds {
    @SerializedName("organizationId")
    @Expose
    private Integer organizationId;
    @SerializedName("userIds")
    @Expose
    private Integer id;

}
