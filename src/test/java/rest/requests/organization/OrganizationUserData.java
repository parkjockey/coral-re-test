package rest.requests.organization;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class OrganizationUserData {
    @SerializedName("organizationId")
    @Expose
    private Integer organizationId;
}
