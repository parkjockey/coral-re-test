package rest.requests.organization.update_user;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
public class UpdateOrganizationUserAccount {
   @SerializedName("organizationId")
   @Expose
   private Integer organizationId;
   @SerializedName("id")
   @Expose
   private Integer id;
   @SerializedName("userInformation")
   @Expose
   private UserInformation userInformation;
   @SerializedName("contactInformation")
   @Expose
   private ContactInformation contactInformation;

}
