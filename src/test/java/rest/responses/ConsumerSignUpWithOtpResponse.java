package rest.responses;

import io.restassured.response.Response;

public class ConsumerSignUpWithOtpResponse {

   public static String getOtp (Response response){
        return response.jsonPath().get("otp").toString();
    }

    public static String getSession (Response response){
        return response.jsonPath().get("session").toString();
    }
}
