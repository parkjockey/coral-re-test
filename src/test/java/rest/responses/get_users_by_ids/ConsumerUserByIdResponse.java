package rest.responses.get_users_by_ids;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ConsumerUserByIdResponse {
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("consumerId")
    @Expose
    private Integer consumerId;
    @SerializedName("username")
    @Expose
    private String username;
    @SerializedName("isSsoUser")
    @Expose
    private Object isSsoUser;
    @SerializedName("givenName")
    @Expose
    private Object givenName;
    @SerializedName("familyName")
    @Expose
    private Object familyName;
    @SerializedName("fullName")
    private Object fullName;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("phoneNumber")
    @Expose
    private Object phoneNumber;
    @SerializedName("userStatus")
    @Expose
    private String userStatus;
    @SerializedName("temporaryEmail")
    @Expose
    private Object temporaryEmail;
    @SerializedName("temporaryPhoneNumber")
    @Expose
    private Object temporaryPhoneNumber;
    @SerializedName("lastModifiedDate")
    @Expose
    private String lastModifiedDate;
}
