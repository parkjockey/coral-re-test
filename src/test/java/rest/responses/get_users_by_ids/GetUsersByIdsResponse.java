package rest.responses.get_users_by_ids;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import lombok.Getter;

import java.util.List;

@Getter
public class GetUsersByIdsResponse {
    @SerializedName("content")
    @Expose
    private List<Content> content = null;
    @SerializedName("totalPages")
    @Expose
    private Integer totalPages;
    @SerializedName("totalItems")
    @Expose
    private Integer totalItems;
}
