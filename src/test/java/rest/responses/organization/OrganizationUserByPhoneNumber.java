package rest.responses.organization;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;

@Getter
@Setter
public class OrganizationUserByPhoneNumber {
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("username")
    @Expose
    private String username;
    @SerializedName("isSsoUser")
    private Boolean isSsoUser;
    @SerializedName("givenName")
    @Expose
    private String givenName;
    @SerializedName("familyName")
    @Expose
    private String familyName;
    @SerializedName("fullName")
    @Expose
    private String fullName;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("phoneNumber")
    @Expose
    private String phoneNumber;
    @SerializedName("profilePictureUrl")
    @Expose
    private String profilePictureUrl;
    @SerializedName("userStatus")
    @Expose
    private String userStatus;
    @SerializedName("invitedOnDate")
    @Expose
    private String invitedOnDate;
    @SerializedName("acceptedOnDate")
    @Expose
    private String acceptedOnDate;
    @SerializedName("temporaryEmail")
    @Expose
    private String temporaryEmail;
    @SerializedName("lastModifiedDate")
    @Expose
    private String lastModifiedDate;
    @SerializedName("sourceSystemName")
    @Expose
    private String sourceSystemName;
    @SerializedName("sourceSystemUserIdentifier")
    @Expose
    private String sourceSystemUserIdentifier;
    @SerializedName("roles")
    @Expose
    private ArrayList<Roles> roles;




}
