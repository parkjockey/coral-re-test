package rest.responses.organization;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class content {
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("username")
    @Expose
    private String username;
    @SerializedName("isSsoUser")
    @Expose
    private Object isSsoUser;
    @SerializedName("givenName")
    @Expose
    private Object givenName;
    @SerializedName("familyName")
    @Expose
    private Object familyName;
    @SerializedName("fullName")
    private Object fullName;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("phoneNumber")
    @Expose
    private Object phoneNumber;
    @SerializedName("profilePictureUrl")
    @Expose
    private String profilePictureUrl;
    @SerializedName("userStatus")
    @Expose
    private String userStatus;
    @SerializedName("invitedOnDate")
    @Expose
    private String invitedOnDate;
    @SerializedName("acceptedOnDate")
    @Expose
    private String acceptedOnDate;
    @SerializedName("temporaryEmail")
    @Expose
    private Object temporaryEmail;
    @SerializedName("lastModifiedDate")
    @Expose
    private String lastModifiedDate;
    @SerializedName("sourceSystemName")
    @Expose
    private String sourceSystemName;
    @SerializedName("sourceSystemUserIdentifier")
    @Expose
    private String sourceSystemUserIdentifier;
}
