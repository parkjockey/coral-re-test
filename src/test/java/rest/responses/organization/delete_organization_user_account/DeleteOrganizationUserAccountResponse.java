package rest.responses.organization.delete_organization_user_account;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class DeleteOrganizationUserAccountResponse {
    @SerializedName("deletedAccountEmailList")
    @Expose
    private List<deletedAccountEmailList> deletedAccountEmailList = null;
}
