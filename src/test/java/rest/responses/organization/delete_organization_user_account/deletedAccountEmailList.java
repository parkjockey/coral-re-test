package rest.responses.organization.delete_organization_user_account;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class deletedAccountEmailList {
    @SerializedName("email")
    @Expose
    private String email;
}
