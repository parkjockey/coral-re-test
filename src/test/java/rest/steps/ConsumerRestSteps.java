package rest.steps;

import com.google.gson.Gson;
import common.helpers.db_helpers.SQLQueries;
import common.helpers.rest.HelperRest;
import common.helpers.rest.TokenProvider;
import common.helpers.twilio.TwilioHelpers;
import data_containers.GeneralData;
import data_containers.OrganizationData;
import data_containers.RoleData;
import data_containers.UserData;
import graphql.helpers.users.PasswordLessHelpers;
import graphql.responses.consumers.confirm_passwordless_consumer_response.ConfirmPasswordLessConsumerResponse;
import graphql.responses.consumers.confirm_passwordless_consumer_response.ConfirmPasswordlessConsumerUserSignUp;
import graphql.responses.consumers.passwordless_consumer_login_mediator.PasswordlessConsumerLoginResponse;
import graphql.responses.consumers.passwordless_consumer_login_mediator.confirmPasswordlessConsumerUserLogin;
import graphql.responses.consumers.passwordless_consumer_login_mediator.confirmPasswordlessConsumerUserLoginResponse;
import io.cucumber.java8.En;
import org.assertj.core.api.SoftAssertions;
import rest.helpers.consumer.ConsumerRestHelpers;
import rest.responses.ConsumerSignUpWithOtpResponse;
import rest.responses.get_users_by_ids.ConsumerUserByIdResponse;

public class ConsumerRestSteps implements En {
    GeneralData generalData;
    UserData userData;
    OrganizationData organizationData;
    RoleData roleData;
    SoftAssertions assertions;
    Gson gson;


    public ConsumerRestSteps(GeneralData generalData, UserData userData, OrganizationData organizationData, RoleData roleData) {
        this.generalData = generalData;
        this.userData = userData;
        this.organizationData = organizationData;
        this.roleData = roleData;
        assertions = new SoftAssertions();
        gson = new Gson();

        When("ConsumerSingUp ep is called with email value", () -> {
            String randomMail = "restMail"+ HelperRest.getRandomNumber()+"@"+HelperRest.getRandomNumber()+"mail.com";
            generalData.response = ConsumerRestHelpers.consumerSignUpWithOtp(randomMail, "", "1", "Test");
        });

        When("ConsumerSingUp ep is called with phone value", () -> {
            String randomPhone = "+381"+ HelperRest.getRandomNumber();
            generalData.response = ConsumerRestHelpers.consumerSignUpWithOtp("", randomPhone, "1", "Test");
        });

        Then("Response contains session and otp", () -> {
          assertions.assertThat(ConsumerSignUpWithOtpResponse.getSession(generalData.response)).isNotNull();
          assertions.assertThat(ConsumerSignUpWithOtpResponse.getOtp(generalData.response)).isNotNull();
          assertions.assertAll();
        });

        When("Login EP is called with email", () -> {
             generalData.response = ConsumerRestHelpers.consumerLoginWithOtp(userData.email, "");
        });

        When("Login EP is called with phone", () -> {
            generalData.response = ConsumerRestHelpers.consumerLoginWithOtp("", TwilioHelpers.phoneReceiver);
        });

        When("ConsumerSingUp ep is called with both email and phone values", () -> {
            String randomMail = "restMail"+ HelperRest.getRandomNumber()+"@"+HelperRest.getRandomNumber()+"mail.com";
            String randomPhone = "+381"+ HelperRest.getRandomNumber();
            generalData.response = ConsumerRestHelpers.consumerSignUpWithOtp(randomMail, randomPhone, "1", "Test");
        });

        Given("Consumer with phone is created", () -> {
            //generate phone
           userData.phone = "+1"+ HelperRest.getRandomNumber();

            //sign up consumer
            generalData.response = ConsumerRestHelpers.consumerSignUpWithOtp("" ,  userData.phone, "1", "Test");
            userData.otpCode = ConsumerSignUpWithOtpResponse.getOtp(generalData.response);
            userData.session = ConsumerSignUpWithOtpResponse.getSession(generalData.response);

            assertions.assertThat(userData.session).isNotNull();
            assertions.assertThat(userData.otpCode).isNotNull();
            assertions.assertAll();

            //consumer sign up confirmation
            generalData.response = PasswordLessHelpers
                    .confirmPasswordLessUserSignUpPhone(userData.phone, userData.otpCode, userData.session);

            //obtain token and username
            ConfirmPasswordLessConsumerResponse confirmResponse = gson.fromJson(generalData.response.asString(),
                    ConfirmPasswordLessConsumerResponse.class);
            ConfirmPasswordlessConsumerUserSignUp response = confirmResponse.getData().getConfirmPasswordlessConsumerUserSignUp();
            userData.token = response.getAuthenticationResponse().getAccessToken();
            userData.userName = TokenProvider.getUsernameClaim(userData.token);
            userData.id = SQLQueries.findUserId(userData.userName);

            assertions.assertThat(userData.token).isNotNull();
            assertions.assertThat(userData.userName).isNotNull();
            assertions.assertAll();

        });


        Given("Consumer with email is created", () -> {
            //generate phone
            String randomMail = "restMail"+ HelperRest.getRandomNumber()+"@"+HelperRest.getRandomNumber()+"mail.com";

            //sign up consumer
            generalData.response = ConsumerRestHelpers.consumerSignUpWithOtp(userData.email ,  "", "1", "Test");
            userData.otpCode = ConsumerSignUpWithOtpResponse.getOtp(generalData.response);
            userData.session = ConsumerSignUpWithOtpResponse.getSession(generalData.response);

            assertions.assertThat(userData.session).isNotNull();
            assertions.assertThat(userData.otpCode).isNotNull();
            assertions.assertAll();

            //consumer sign up confirmation
            generalData.response = PasswordLessHelpers
                    .confirmPasswordLessUserSignUp(userData.email, userData.otpCode, userData.session);

            //obtain token and username
            ConfirmPasswordLessConsumerResponse confirmResponse = gson.fromJson(generalData.response.asString(),
                    ConfirmPasswordLessConsumerResponse.class);
            ConfirmPasswordlessConsumerUserSignUp response = confirmResponse.getData().getConfirmPasswordlessConsumerUserSignUp();
            userData.token = response.getAuthenticationResponse().getAccessToken();
            userData.userName = TokenProvider.getUsernameClaim(userData.token);
            userData.id = SQLQueries.findUserId(userData.userName);

            assertions.assertThat(userData.token).isNotNull();
            assertions.assertThat(userData.userName).isNotNull();
            assertions.assertAll();

        });


        Given("Consumer with {string} phone is created", (String countryCode) -> {
            generalData.countryCode = countryCode;
            //generate phone
            userData.phone = "+1"+ HelperRest.getRandomNumber();

            //sign up consumer
            generalData.response = ConsumerRestHelpers.consumerSignUpWithOtp("" ,  userData.phone, "1", "Test");
            userData.otpCode = ConsumerSignUpWithOtpResponse.getOtp(generalData.response);
            userData.session = ConsumerSignUpWithOtpResponse.getSession(generalData.response);

            assertions.assertThat(userData.session).isNotNull();
            assertions.assertThat(userData.otpCode).isNotNull();
            assertions.assertAll();

            //consumer sign up confirmation
            generalData.response = PasswordLessHelpers
                    .confirmPasswordLessUserSignUpPhone(userData.phone, userData.otpCode, userData.session);

            //obtain token and username
            ConfirmPasswordLessConsumerResponse confirmResponse = gson.fromJson(generalData.response.asString(),
                    ConfirmPasswordLessConsumerResponse.class);
            ConfirmPasswordlessConsumerUserSignUp response = confirmResponse.getData().getConfirmPasswordlessConsumerUserSignUp();
            userData.token = response.getAuthenticationResponse().getAccessToken();
            userData.userName = TokenProvider.getUsernameClaim(userData.token);
            userData.id = SQLQueries.findUserId(userData.userName);

            assertions.assertThat(userData.token).isNotNull();
            assertions.assertThat(userData.userName).isNotNull();
            assertions.assertAll();

        });

        Given("Consumer with {string} email is created", (String countryCode) -> {
            generalData.countryCode = countryCode;
            //generate phone
            String randomMail = "restMail"+ HelperRest.getRandomNumber()+"@"+HelperRest.getRandomNumber()+"mail.com";

            //sign up consumer
            generalData.response = ConsumerRestHelpers.consumerSignUpWithOtp(userData.email ,  "", "1", "Test");
            userData.otpCode = ConsumerSignUpWithOtpResponse.getOtp(generalData.response);
            userData.session = ConsumerSignUpWithOtpResponse.getSession(generalData.response);

            assertions.assertThat(userData.session).isNotNull();
            assertions.assertThat(userData.otpCode).isNotNull();
            assertions.assertAll();

            //consumer sign up confirmation
            generalData.response = PasswordLessHelpers
                    .confirmPasswordLessUserSignUp(userData.email, userData.otpCode, userData.session);

            //obtain token and username
            ConfirmPasswordLessConsumerResponse confirmResponse = gson.fromJson(generalData.response.asString(),
                    ConfirmPasswordLessConsumerResponse.class);
            ConfirmPasswordlessConsumerUserSignUp response = confirmResponse.getData().getConfirmPasswordlessConsumerUserSignUp();
            userData.token = response.getAuthenticationResponse().getAccessToken();
            userData.userName = TokenProvider.getUsernameClaim(userData.token);
            userData.id = SQLQueries.findUserId(userData.userName);

            assertions.assertThat(userData.token).isNotNull();
            assertions.assertThat(userData.userName).isNotNull();
            assertions.assertAll();

        });

        When("I send GetConsumerUserByEmailAddress request with created email", () -> {
            generalData.response = ConsumerRestHelpers.getConsumerUserByEmailAddress(generalData.token,UserData.email);
        });

        Then("Response contains email and other consumer information details", () -> {
            ConsumerUserByIdResponse consumerUserByIdResponse = gson.fromJson(generalData.response.asString(),ConsumerUserByIdResponse.class);
            assertions.assertThat(consumerUserByIdResponse.getId()).isNotNull();
            assertions.assertThat(consumerUserByIdResponse.getConsumerId()).isNotNull();
            assertions.assertThat(consumerUserByIdResponse.getEmail()).isEqualTo(userData.email);
            assertions.assertThat(consumerUserByIdResponse.getUserStatus()).isEqualTo("ACTIVE");
            assertions.assertThat(consumerUserByIdResponse.getUsername()).isNotNull();
            assertions.assertAll();
        });

        When("I send GetConsumerUserByEmailAddress request with {string} email", (String email) -> {
            generalData.response = ConsumerRestHelpers.getConsumerUserByEmailAddress(generalData.token, email);
        });

        When("I send a request to getConsumerUserDataByUserId with non-existing id", () -> {
            generalData.response = ConsumerRestHelpers.getConsumerUserById(generalData.token,1);
        });

        When("I send GetConsumerUserByEmailAddress request with email that is {string}", (String email) -> {
            generalData.response = ConsumerRestHelpers.getConsumerUserByEmailAddress(generalData.token, "reefuato+1@reefplatform.com");
        });

        And("Consumer is created", () -> {

            userData.phone = "500"+ HelperRest.getRandomNumber();

            generalData.response = PasswordLessHelpers.passwordlessConsumerLoginMediator(userData.email, userData.phone);
            userData.session = PasswordlessConsumerLoginResponse.getSession(generalData.response);
            userData.code = PasswordlessConsumerLoginResponse.getCode(generalData.response);

            assertions.assertThat(userData.session).isNotNull();
            assertions.assertThat(userData.code).isNotNull();
            assertions.assertAll();

            generalData.response = PasswordLessHelpers.confirmPasswordlessConsumerLoginMediator(userData.email, userData.phone, userData.code);

            confirmPasswordlessConsumerUserLoginResponse confirmLogin = gson.fromJson(generalData.response.asString(),
                    confirmPasswordlessConsumerUserLoginResponse.class);
            confirmPasswordlessConsumerUserLogin response = confirmLogin.getData().getConfirmPasswordlessConsumerUserLogin();
            userData.token = response.getAuthenticationResponse().getAccessToken();

            assertions.assertThat(userData.token).isNotNull();

        });


    }
}
