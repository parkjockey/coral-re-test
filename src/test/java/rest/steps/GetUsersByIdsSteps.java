package rest.steps;

import com.google.gson.Gson;
import common.helpers.db_helpers.SQLQueries;
import common.helpers.rest.TokenProvider;
import data_containers.GeneralData;
import data_containers.OrganizationData;
import data_containers.RoleData;
import data_containers.UserData;
import io.cucumber.java8.En;
import org.assertj.core.api.SoftAssertions;
import rest.helpers.consumer.ConsumerRestHelpers;
import rest.helpers.user.UserRestHelpers;
import rest.responses.get_users_by_ids.ConsumerUserByIdResponse;
import rest.responses.get_users_by_ids.Content;
import rest.responses.get_users_by_ids.GetUsersByIdsResponse;

import java.util.ArrayList;

public class GetUsersByIdsSteps implements En {

    GeneralData generalData;
    UserData userData;
    OrganizationData organizationData;
    RoleData roleData;
    SoftAssertions assertions;
    Gson gson;
    ArrayList<Integer> usersIds;



    public GetUsersByIdsSteps(GeneralData generalData, UserData userData, OrganizationData organizationData, RoleData roleData) {
        this.generalData = generalData;
        this.userData = userData;
        this.organizationData = organizationData;
        this.roleData = roleData;
        assertions = new SoftAssertions();
        gson = new Gson();
        usersIds = new ArrayList<>();


        Given("Users Ids obtained", () -> {

          usersIds.add(SQLQueries.findUserId(TokenProvider.getUsernameClaim(generalData.token)));
        });

        When("GetUsersByIds request is sent", () -> {
                generalData.response = UserRestHelpers.getUsersByIds(generalData.token, usersIds, null, null);
        });

        Then("Response contains users data", () -> {
            GetUsersByIdsResponse response = gson.fromJson(generalData.response.asString(), GetUsersByIdsResponse.class);
            Content user = response.getContent().get(0);
            assertions.assertThat(user.getEmail()).isEqualTo(generalData.adNoSSOUsername);
            assertions.assertThat(user.getFamilyName()).isNotNull();
            assertions.assertThat(user.getId()).isNotNull();
            assertions.assertThat(user.getUsername()).isEqualTo(TokenProvider.getUsernameClaim(generalData.token));
            assertions.assertThat(user.getGivenName()).isNotNull();
            assertions.assertThat(user.getFullName()).isEqualTo(user.getGivenName()+" "+user.getFamilyName());
            assertions.assertThat(user.getPhoneNumber()).isNull();
            assertions.assertAll();
        });

        When("I send a request to getConsumerUserDataByUserId", () -> {
            generalData.response = ConsumerRestHelpers.getConsumerUserById(generalData.token,UserData.id);
        });

        And("Response contains all the relevant data", () -> {
            ConsumerUserByIdResponse consumerUserByIdResponse = gson.fromJson(generalData.response.asString(),ConsumerUserByIdResponse.class);
            assertions.assertThat(consumerUserByIdResponse.getId()).isNotNull();
            assertions.assertThat(consumerUserByIdResponse.getConsumerId()).isNotNull();
            assertions.assertThat(consumerUserByIdResponse.getEmail()).isEqualTo(userData.email);
            assertions.assertThat(consumerUserByIdResponse.getUserStatus()).isEqualTo("ACTIVE");
            assertions.assertThat(consumerUserByIdResponse.getUsername()).isNotNull();
            assertions.assertAll();
        });




    }
}
