package rest.steps;

import com.google.gson.Gson;
import common.helpers.rest.HelperRest;
import data_containers.GeneralData;
import data_containers.OrganizationData;
import data_containers.RoleData;
import data_containers.UserData;
import io.cucumber.java8.En;
import org.assertj.core.api.SoftAssertions;
import rest.helpers.organization.OrganizationUserRestHelpers;
import rest.requests.organization.update_user.ContactInformation;
import rest.requests.organization.update_user.UpdateOrganizationUserAccount;
import rest.requests.organization.update_user.UserInformation;
import rest.responses.organization.OrganizationUserByEmailResponse;
import rest.responses.organization.OrganizationUserByPhoneNumber;
import rest.responses.organization.OrganizationUsersResponse;
import rest.responses.organization.content;

import java.util.ArrayList;

public class OrganizationUsersSteps implements En {
    GeneralData generalData;
    OrganizationData organizationData;
    UserData userData;
    RoleData roleData;
    SoftAssertions assertions;
    Gson gson;
    ArrayList<Integer> userIds;


    public OrganizationUsersSteps(GeneralData generalData, OrganizationData organizationData, UserData userData, RoleData roleData) {
        this.generalData = generalData;
        this.organizationData = organizationData;
        this.userData = userData;
        this.roleData = roleData;
        assertions = new SoftAssertions();
        gson = new Gson();
        userIds = new ArrayList<>();


        When("Get Organization Users request is sent", () -> {
            generalData.response = OrganizationUserRestHelpers.getOrganizationUsers(generalData.token, 1);
        });

        And("The Response contains all relevant data", () -> {
            OrganizationUsersResponse organizationUsersResponse = gson.fromJson(generalData.response.asString(), OrganizationUsersResponse.class);
            content user = organizationUsersResponse.getContent().get(0);
            assertions.assertThat(user.getId()).isNotNull();
            assertions.assertThat(user.getUsername()).isNotNull();
            assertions.assertThat(user.getGivenName()).isNotNull();
            assertions.assertThat(user.getEmail()).isNotNull();
            assertions.assertThat(user.getFamilyName()).isNotNull();
            assertions.assertThat(user.getFullName()).isEqualTo(user.getGivenName()+" "+user.getFamilyName());
            assertions.assertThat(user.getEmail()).isNotNull();
            assertions.assertThat(user.getInvitedOnDate()).isNotNull();
            assertions.assertThat(user.getAcceptedOnDate()).isNotNull();
            assertions.assertThat(user.getTemporaryEmail()).isEqualTo(null);
            assertions.assertThat(user.getLastModifiedDate()).isNotNull();
            assertions.assertAll();
        });

        And("Get an user by email request is sent", () -> {
            generalData.response = OrganizationUserRestHelpers.getOrganizationUserByEmail(generalData.token, organizationData.id, userData.email);
        });

        And("Response contains user's data", () -> {
            OrganizationUserByEmailResponse organizationUserByEmailResponse = gson.fromJson(generalData.response.asString(),OrganizationUserByEmailResponse.class);
            assertions.assertThat(organizationUserByEmailResponse.getId()).isNotNull();
            assertions.assertThat(organizationUserByEmailResponse.getUsername()).isNotNull();
            assertions.assertThat(organizationUserByEmailResponse.getEmail()).isNotNull();
            assertions.assertThat(organizationUserByEmailResponse.getUserStatus()).isEqualTo("INVITED");
            assertions.assertThat(organizationUserByEmailResponse.getInvitedOnDate()).isNotNull();
            assertions.assertThat(organizationUserByEmailResponse.getTemporaryEmail()).isEqualTo(null);
            assertions.assertThat(organizationUserByEmailResponse.getLastModifiedDate()).isNotNull();
            assertions.assertThat(organizationUserByEmailResponse.getSourceSystemName()).isNotNull();
            assertions.assertThat(organizationUserByEmailResponse.getSourceSystemUserIdentifier()).isNotNull();
            assertions.assertThat(organizationUserByEmailResponse.getRoles()).isNotNull();
            assertions.assertAll();
        });

        And("Get user by ids request is sent", () -> {
            generalData.response = OrganizationUserRestHelpers.getOrganizationUsersByIds(generalData.token, organizationData.id,
                    userData.id, null, null);
        });

        When("GetUserDataForCurrentUser request is called with obtained token", () -> {
            generalData.response = OrganizationUserRestHelpers.getOrganizationUserData(userData.token, organizationData.id);
        });

        And("Response contains user's data and role for user which token is used in request", () -> {
            OrganizationUserByEmailResponse organizationUserByEmailResponse = gson.fromJson(generalData.response.asString(),OrganizationUserByEmailResponse.class);
            assertions.assertThat(organizationUserByEmailResponse.getId()).isNotNull();
            assertions.assertThat(organizationUserByEmailResponse.getUsername()).isNotNull();
            assertions.assertThat(organizationUserByEmailResponse.getIsSsoUser()).isNotNull();
            assertions.assertThat(organizationUserByEmailResponse.getGivenName()).isNotNull();
            assertions.assertThat(organizationUserByEmailResponse.getFamilyName()).isNotNull();
            assertions.assertThat(organizationUserByEmailResponse.getFullName()).isEqualTo(organizationUserByEmailResponse.getGivenName()+" "+organizationUserByEmailResponse.getFamilyName());
            assertions.assertThat(organizationUserByEmailResponse.getEmail()).isNotNull();
            assertions.assertThat(organizationUserByEmailResponse.getPhoneNumber()).isNull();
            assertions.assertThat(organizationUserByEmailResponse.getProfilePictureUrl()).isEqualTo(null);
            assertions.assertThat(organizationUserByEmailResponse.getUserStatus()).isEqualTo("ACTIVE");
            assertions.assertThat(organizationUserByEmailResponse.getInvitedOnDate()).isNotNull();
            assertions.assertThat(organizationUserByEmailResponse.getAcceptedOnDate()).isNotNull();
            assertions.assertThat(organizationUserByEmailResponse.getTemporaryEmail()).isEqualTo(null);
            assertions.assertThat(organizationUserByEmailResponse.getLastModifiedDate()).isNotNull();
            assertions.assertThat(organizationUserByEmailResponse.getSourceSystemUserIdentifier()).isNotNull();
            assertions.assertThat(organizationUserByEmailResponse.getRoles()).isNotNull();
            assertions.assertThat(organizationUserByEmailResponse.getRoles()).isNotNull();
            assertions.assertAll();
        });

        When("DeleteOrganizationUser request is called", () -> {
            generalData.response = OrganizationUserRestHelpers.deleteOrganizationUserAccount(generalData.token, userData.email, organizationData.id, 60);
        });

        And("Response contains list of deleted users", () -> {
            assertions.assertThat(generalData.response.jsonPath().getList("deletedAccountEmailList")).isNotNull();
        });

        When("User update his profile information", () -> {
            userData.firstName = "UpdatedFirstName" + HelperRest.getRandomNumber();
            userData.lastName = "UpdatedLastName" + HelperRest.getRandomNumber();
            userData.phone = "+1500"+ HelperRest.getRandomNumber();
            userData.email = HelperRest.getRandomEmail();

            ContactInformation contactInformation = new ContactInformation();
            contactInformation.setEmail(userData.email);
            contactInformation.setPhoneNumber(userData.phone);

            UserInformation userInformation = new UserInformation();
            userInformation.setFirstName(userData.firstName);
            userInformation.setLastName(userData.lastName);

            UpdateOrganizationUserAccount userAccount = new UpdateOrganizationUserAccount();
            userAccount.setOrganizationId(organizationData.id);
            userAccount.setId(userData.id);
            userAccount.setContactInformation(contactInformation);
            userAccount.setUserInformation(userInformation);

            // send request
            generalData.response = OrganizationUserRestHelpers.updateOrganizationUserAccount(userData.token, organizationData.id, userData.id, userAccount);
        });

        And("Get user by phone request is sent", () -> {
            generalData.response = OrganizationUserRestHelpers.getOrganizationUserByPhoneNumber(userData.token, organizationData.id, userData.phone);
        });

        And("Response contains updated user's data", () -> {
            OrganizationUserByPhoneNumber organizationUserByPhoneNumber = gson.fromJson(generalData.response.asString(),OrganizationUserByPhoneNumber.class);
            assertions.assertThat(organizationUserByPhoneNumber.getEmail()).isEqualTo(userData.email);
            assertions.assertThat(organizationUserByPhoneNumber.getPhoneNumber()).isEqualTo(userData.phone);
            assertions.assertThat(organizationUserByPhoneNumber.getGivenName()).isEqualTo(userData.firstName);
            assertions.assertThat(organizationUserByPhoneNumber.getFamilyName()).isEqualTo(userData.lastName);
            assertions.assertAll();
        });



    }



}