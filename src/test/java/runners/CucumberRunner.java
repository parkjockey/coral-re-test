package runners;


import common.helpers.aws_helpers.cognito.CognitoHelpers;
import common.helpers.aws_helpers.sqs.SQSHelpers;
import common.helpers.cukedoctor.GenerateDoc;
import common.helpers.db_helpers.JdbcHelpers;
import common.helpers.db_helpers.Neo4JHelpers;
import common.helpers.db_helpers.SQLQueries;
import common.helpers.environment.Env;
import common.helpers.environment.YamlReader;
import common.helpers.graph_api.MSGraphAPI;
import common.helpers.redis.RedisHelpers;
import common.helpers.testrail.TestRailHelpers;
import data_containers.GeneralData;
import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import io.qameta.allure.restassured.AllureRestAssured;
import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.text.ParseException;

@RunWith(Cucumber.class)

@CucumberOptions(
        features = {"src/test/resources"},
        glue = {
                "common.steps",
                "ui",
                "rest",
                "graphql.steps"
        },
        tags = {
              // "@gql"
              "@ui", "@wip"
                // "@rest"
//                "@regression"
        },
        plugin = {"io.qameta.allure.cucumber4jvm.AllureCucumber4Jvm", "json:target/cucumber.json"}

)
public class CucumberRunner {
    public static final Boolean TEST_RAIL_FLAG = false; // true if I want to add new test run to TestRail
    static GeneralData generalData;

    @BeforeClass
    public static void beforeTestSuiteActions() throws ParseException, InterruptedException, IOException {
        Logger logger = LoggerFactory.getLogger(CucumberRunner.class);
        RestAssured.requestSpecification = new RequestSpecBuilder().build().noFilters().filter(new AllureRestAssured());
        //Create Test Rail run

        if (TEST_RAIL_FLAG) {
            TestRailHelpers.createTestRailInstance();
            TestRailHelpers.setMilestoneId("IAM-Fugu", "TestMilestone");
            TestRailHelpers.setProjectSuiteId("IAM-Fugu", "Automation");
            TestRailHelpers.createRun();
        }
        //Read ENV property values
        generalData.envName = System.getProperty("env");
        if (generalData.envName == null || generalData.envName.isEmpty()) generalData.envName = "qa2Container";

        generalData.driverType = System.getProperty("driver");
        if (generalData.driverType == null || generalData.driverType.isEmpty())
            generalData.driverType = "chrome";//"chromeGrid";

        Env testEnvironment = YamlReader.getEnvironmentFromYaml(generalData.envName);
        generalData.reefWebApp = testEnvironment.reefWebApp;
        generalData.gqlBaseUri = testEnvironment.gqlBaseUri;
        GeneralData.hmtGqlUri = testEnvironment.hmtGqlUri;
        generalData.loginWebApp = testEnvironment.loginWebApp;
        generalData.loginRedirectUrl = testEnvironment.loginRedirectUrl;
        GeneralData.restBaseUri = testEnvironment.restBaseUri;
        Neo4JHelpers.setNeo4JCredentials(testEnvironment.neo4JUrl, testEnvironment.neo4JUser, testEnvironment.neo4JPass);
        JdbcHelpers.dbUrl = testEnvironment.dbUrl;
        JdbcHelpers.dbUser = testEnvironment.dbUser;
        JdbcHelpers.dbPass = testEnvironment.dbPass;
        MSGraphAPI.tenantId = testEnvironment.azureTenantId;
        MSGraphAPI.clientId = testEnvironment.azureClientId;
        MSGraphAPI.clientSecret = testEnvironment.azureSecret;
        MSGraphAPI.azureUserDomain = testEnvironment.azureUserDomain;
        RedisHelpers.redisHost = testEnvironment.redis;
        CognitoHelpers.userPoolId = testEnvironment.userPoolId;
        GeneralData.mediatorGql = testEnvironment.mediatorGql;
        //  generalData.optimizelyBoolean = OptimizelyHelpers.enableFeature(testEnvironment.optimizelyKey, "organization_modules", "");
        //Insert Test module
        for (String s : SQLQueries.deleteTestModule) {
            JdbcHelpers.executeUpdate(s);
        }
        for (String s : SQLQueries.insertTestModule) {
            JdbcHelpers.executeUpdate(s);
        }
        SQLQueries.setReefOrgName();

    }

    @AfterClass
    public static void afterTestSuiteActions() {
        //Delete Test module
        for (String s : SQLQueries.deleteTestModule) {
            JdbcHelpers.executeUpdate(s);
        }
        //Delete all SQS emails
        SQSHelpers.deleteAllMessages();
        GenerateDoc.generate();
    }


}
