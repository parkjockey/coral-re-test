package ui.page_object;

import common.helpers.ui.PageObjectBase;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class CreatedAccountPopUp extends PageObjectBase {
    public static By successUserCreation = By.xpath("//h1[contains(text(), 'Account created successfully!')]");
    public static By informationMessage = By.xpath("//p[contains(text(), 'Please keep in mind that some of the users are already part of our organization with same or different role.')]");
    public static By continueToHomePageButton = By.id("invitation-page-confirm-invitation-continue-to-home-screen-button");

    public static WebElement getSuccessUserCreationTitle(){
        return retryFindElement(successUserCreation);
    }
    public static WebElement getInformationMessage(){
     return waitToBeClickable(driver.findElement(informationMessage));
    }
    public static WebElement getContinueToHomePage(){
        return waitToBeClickable(driver.findElement(continueToHomePageButton));
    }

}
