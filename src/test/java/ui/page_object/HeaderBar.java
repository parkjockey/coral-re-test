package ui.page_object;

import common.helpers.ui.PageObjectBase;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class HeaderBar extends PageObjectBase {

    public static By avatar = By.xpath("//button[@data-qa-name='profile-avatar']");
    public static By avatarDropdown = By.xpath("//div [@class = 'dropdown header-navigation-avatar__dropdown']");
    public static By userAccount = By.xpath("//li [contains(text(), 'Account')]");
    public static By logOut = By.xpath("//li[contains(text(), 'Logout')]");
    public static By switchOrganization = By.xpath("//li [contains(text(), 'Switch Organization')]");
    public static By reefOrganizationLogoHeader = By.xpath("//img[@data-testid='dti-organization-logo-placeholder'and@alt = 'Organization logo']");

    public static WebElement getAvatar() {return retryFindElement(avatar);}
    public static WebElement getAccount() {return waitToBeClickable(driver.findElement(userAccount));}
    public static WebElement getLogOut() {return waitToBeClickable(driver.findElement(logOut));}
    public static WebElement getSwitchOrganization() {return retryFindElement(switchOrganization);}
    public static WebElement getHeaderOrgName(String orgName) {
        return retryFindElement(By.xpath("//h5[contains(text(), '" + orgName +"')]"));
    }
    public static WebElement getReefOrganizationLogo() {return retryFindElement(reefOrganizationLogoHeader);}



}
