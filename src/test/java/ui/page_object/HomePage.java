package ui.page_object;

import common.helpers.ui.PageObjectBase;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class HomePage extends PageObjectBase {
    public static By homeHeader = By.xpath("//div[@class='flex-layout page-header']//div[contains(text(), 'Home')]");
    public static By userProfileBtn = By.xpath("//div[@class='flex-layout header-navigation-avatar']");
    public static By alertToast = By.xpath("//div [@role = 'alert']");
    public static By users = By.xpath("//h2[contains(text(), 'Users')]");
    public static By roles = By.xpath("//h2[contains(text(), 'Roles')]");
    public static By organizations = By.xpath("//h2[contains(text(), 'Organizations')]");
    public static By rolesViewButton = By.xpath("//button[@data-qa-name='roles-module-view']");

    public static WebElement getHomeHeader() {return retryFindElement(homeHeader);}
    public static WebElement getUserProfileBtn() {return retryFindElement(userProfileBtn);}
    public static WebElement getAlertToast() {return retryFindElement(alertToast);}
    public static WebElement getUsers() {return retryFindElement(users);}
    public static WebElement getRoles() {return retryFindElement(roles);}
    public static WebElement getOrganizations() {return retryFindElement(organizations);}
    public static WebElement getRolesViewButton() {return retryFindElement(rolesViewButton);}
}
