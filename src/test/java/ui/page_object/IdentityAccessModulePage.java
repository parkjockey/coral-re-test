package ui.page_object;

import common.helpers.ui.PageObjectBase;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.security.PublicKey;

public class IdentityAccessModulePage extends PageObjectBase {
    public static By viewUsersPage = By.id("home-page-home-tiles-organization-users-page");
    public static By viewConsumersPage = By.xpath("//p[.='Consumers']"); //By.id("home-page-home-tiles-consumers-page");

    public static WebElement getViewUsersPage() {return retryFindElement(viewUsersPage);}
    public static WebElement getViewConsumersPage() {return retryFindElement(viewConsumersPage);}

}
