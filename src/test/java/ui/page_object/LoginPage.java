package ui.page_object;

import common.helpers.ui.PageObjectBase;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class LoginPage extends PageObjectBase {

  public static By microsoftButton = By.xpath("//button[contains(text(), 'Microsoft')]");
  public static By userEmailField = By.xpath("//input[@placeholder='Email']");
  public static By continueLoginButton = By.id("login-page-continue-button");
  public static By userNoSSOPasswordField = By.xpath("//input[@placeholder='Password']");
  public static By logInToYourAccountNoSSOUserButton = By.id("login-page-login-button");
  public static By passwordErrorMessage = By.xpath("//div[contains(text(), 'Wrong password')]");
  public static By cantLogInToYourAccount = By.xpath("//span[contains(text(), 't log in to your account?')]");
  public static By sendRecoveryLinkButton = By.id("recovery-page-send-recovery-link-button");
  public static By recoveryLinkSentSuccessfullMessage = By.xpath("//h1[contains(text(), 'We sent you a recovery link!')]");
  public static By newPasswordInput = By.xpath("//input[@placeholder='New Password']");
  public static By createANewPasswordButton = By.id("recovery-password-link-page-submit-password");
  public static By resetPasswordToastMessage = By.xpath("//span[contains(text(), 'Password successfuly set')]");
  public static By accountIsSuspendedMessage = By.xpath("//div[contains(text(), 'Account suspended in all organizations')]");
  public static By accountIsSuspendedInEnterpriseOrgMessage = By.xpath("//div[contains(text(), 'Account suspended in enterprise organizations')]");



 public static WebElement getMicrosoftButton(){
      return  waitToBeClickable(driver.findElement(microsoftButton));
    }

    public static WebElement getUserEmailField() {return retryFindElement(userEmailField);}
    public static WebElement getContinueLoginButton() {return waitToBeClickable(driver.findElement(continueLoginButton));}
    public static WebElement getUserNoSSOPasswordField() {return retryFindElement(userNoSSOPasswordField);}
    public static WebElement getLogInToYourAccountNoSSOUserButton() {return waitToBeClickable(driver.findElement(logInToYourAccountNoSSOUserButton));}
    public static WebElement getPasswordErrorMessage() {return retryFindElement(passwordErrorMessage);}
    public static WebElement getCantLogInToYourAccount() {return retryFindElement(cantLogInToYourAccount);}
    public static WebElement getSendRecoveryLinkButton() {return waitToBeClickable(driver.findElement(sendRecoveryLinkButton));}
    public static WebElement getRecoveryLinkSentSuccessfullMessage() {return retryFindElement(recoveryLinkSentSuccessfullMessage);}
    public static WebElement getNewPasswordInput() {return retryFindElement(newPasswordInput);}
    public static WebElement getCreateANewPasswordButton() {return waitToBeClickable(driver.findElement(createANewPasswordButton));}
    public static WebElement getResetPasswordToastMessage() {return retryFindElement(resetPasswordToastMessage);}
    public static WebElement getAccountIsSuspendedMessage() {return retryFindElement(accountIsSuspendedMessage);}
    public static WebElement getAccountIsSuspendedInEnterpriseOrgMessage() {return retryFindElement(accountIsSuspendedInEnterpriseOrgMessage);}

    public static void userLogin(String email){
     getUserEmailField().clear();
     getUserEmailField().sendKeys(email);
     getContinueLoginButton().click();

    }
}
