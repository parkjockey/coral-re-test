package ui.page_object;

import common.helpers.ui.PageObjectBase;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;

public class MicrosoftLoginPage extends PageObjectBase {

    public static By emailInputField = By.id("i0116");
    public static By confirmButton = By.xpath("//input[@type='submit']");
    public static By passwordInputField = By.id("i0118");
    public static By cancelLinkButton = By.id("CancelLinkButton");
    public static By userNameError = By.id("usernameError");
    public static By passwordError = By.id("passwordError");

    public static WebElement getEmailInputField() {
        return waitToBeClickable(driver.findElement(emailInputField));
    }

    public static WebElement getConfirmButton() {
        return retryFindElement(confirmButton);
    }

    public static WebElement getPasswordInputField() {
        return retryFindElement(passwordInputField);
    }

    public static WebElement getUserNameError() {
        return retryFindElement(userNameError);
    }

    public static WebElement getPasswordError() {
        return retryFindElement(passwordError);
    }

    public static WebElement getCancelLinkButton() {
        return retryFindElement(cancelLinkButton);
    }

    public static void loginMicrosoftAccount(String email, String password) {
        waitForLoad(2);
        getEmailInputField().clear();
        getEmailInputField().sendKeys(email);
        getConfirmButton().click();
        getPasswordInputField().clear();
        getPasswordInputField().sendKeys(password);
        getConfirmButton().click();
        getConfirmButton().click();
        try {
            waitForLoad(2);
            getCancelLinkButton().click();
            getConfirmButton().click();
        }
        catch (NoSuchElementException e) {
            //
        }


    }


}
