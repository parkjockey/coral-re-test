package ui.page_object;

import common.helpers.ui.PageObjectBase;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class SideBar extends PageObjectBase {

    public static By sideBar = By.xpath("//aside[@class='sidebar']");
    public static By sideBarIdentityAccess = By.xpath("//p[contains(text(), 'Identity Access')]");
    public static By homePage = By.xpath("//div[contains(text(), 'Home')]");
    public static By roles = By.xpath("//p[contains(text(), 'Roles')]");
    public static By organizations = By.xpath("//div[contains(text(), 'Organizations')]");
    public static By manageOrganizations = By.xpath("//div[contains(text(), 'Manage organization')]");
    public static By users = By.xpath("//aside[@class='sidebar']//div[contains(text(), 'Users')]");


    public static WebElement getSideBar() {
        return retryFindElement(sideBar);
    }

    public static WebElement getManageOrganization() {
        return retryFindElement(manageOrganizations);
    }

    public static WebElement getRoles() {
        return retryFindElement(roles);
    }

    public static WebElement getOrganizations() {
        return retryFindElement(organizations);
    }

    public static WebElement getUsers() {return retryFindElement(users);}

    public static WebElement getSideBarIdentityAccess() {return retryFindElement(sideBarIdentityAccess);}


    //navigates to the real estate module
    public static By getSideBarRealEstate = By.xpath("//p[contains(text(), 'Real Estate')]");
    public static WebElement getSideBarRealEstate() {return retryFindElement(getSideBarRealEstate);}
}
