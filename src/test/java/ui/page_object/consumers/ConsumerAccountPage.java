package ui.page_object.consumers;

import common.helpers.ui.PageObjectBase;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class ConsumerAccountPage extends PageObjectBase {
    public static By firstNameInput = By.xpath("//input[@placeholder = 'First Name']");
    public static By lastNameInput = By.xpath("//input[@placeholder = 'Last Name']");
    public static By saveNameChangesButton = By.xpath("//button[@data-testid= 'user-information-save-button']");
    public static By userInformationSuccessfullUpdatedToastMessage = By.xpath("//span[contains(text(), 'User information successfully updated!')]");
    public static By divModal = By.xpath("//div[@data-testid = 'modal']");
    public static By profile = By.xpath("//div[@class='profile-tab-content__heading']");
    public static By emailInput = By.xpath("//input[@placeholder='Email / Username']");
    public static By phoneNumberInput = By.xpath("//input[@placeholder='Phone number']");
    public static By paymentMethods = By.xpath("//p[.='Payment']");
    public static By paymentMethodsSubModule = By.xpath("//div[.='Payment']");
    public static By vehicles = By.xpath("//p[.='Vehicles']");
    public static By vehiclesSubModule = By.xpath("//span[.='TESTPLATE']");
    public static By addVehicleButton = By.xpath("//span[.='Add Vehicle']");
    public static By licensePlateInput = By.xpath("//input[@placeholder='License Plate']");
    public static By vehicleDropdown = By.xpath("//span[@class='icon dropdown-icon dropdown-icon--has-value']");
    public static By firstState = By.xpath("//div[.='ALABAMA']");
    public static By saveVehicleButton = By.xpath("//span[.='ADD VEHICLE']");
    public static By vehicleAddedSuccessfulToastMessage = By.xpath("//span[contains(text(), 'New vehicle added!')]");
    public static By vehicleThreeDotsMenu = By.xpath("//span[@data-qa-name='vehicle-more-options']");
    public static By removeVehicle = By.xpath("//li[.='Remove Vehicle']");
    public static By vehicleRemovedSuccessfulMessage = By.xpath("//span[contains(text(), 'The selected vehicle has been removed.')]");
    public static By purchases = By.xpath("//p[.='Purchases']");
    public static By purchasesSubModule = By.xpath("//h4[.='User transactions not available.']");//By.xpath("//h4[.='No Purchases Found']");
    public static By invalidEmailMessage = By.xpath("//div[.='Email invalid format']");
    public static By invalidPhoneNumberMessage = By.xpath("//div[.='Phone number invalid format']");
    public static By invalidLicensePlateMessage = By.xpath("//div[.='This license already exists for this user']");


    public static WebElement getFirstNameInput() {return retryFindElement(firstNameInput);}
    public static WebElement getLastNameInput() {return retryFindElement(lastNameInput);}
    public static WebElement getSaveNameChangesButton() {return retryFindElement(saveNameChangesButton);}
    public static WebElement getUserInformationSuccessfullUpdatedToastMessage() {return retryFindElement(userInformationSuccessfullUpdatedToastMessage);}
    public static WebElement getDivModal() {return retryFindElement(divModal);}
    public static WebElement getProfile(){return retryFindElement(profile);}
    public static WebElement getEmailInput(){return retryFindElement(emailInput);}
    public static WebElement getPhoneNumberInput(){return retryFindElement(phoneNumberInput);}
    public static WebElement getPaymentMethods(){return retryFindElement(paymentMethods);}
    public static WebElement getPaymentMethodsSubModule(){return retryFindElement(paymentMethodsSubModule);}
    public static WebElement getVehicles(){return retryFindElement(vehicles);}
    public static WebElement getVehiclesSubModule(){return retryFindElement(vehiclesSubModule);}
    public static WebElement getAddVehicleButton(){return retryFindElement(addVehicleButton);}
    public static WebElement getLicensePlateInput(){return retryFindElement(licensePlateInput);}
    public static WebElement getVehicleDropdown(){return retryFindElement(vehicleDropdown);}
    public static WebElement getFirstState(){return retryFindElement(firstState);}
    public static WebElement getSaveVehicleButton(){return retryFindElement(saveVehicleButton);}
    public static WebElement getVehicleAddedSuccessfulToastMessage(){return retryFindElement(vehicleAddedSuccessfulToastMessage);}
    public static WebElement getVehicleThreeDotsMenu(){return retryFindElement(vehicleThreeDotsMenu);}
    public static WebElement getRemoveVehicle(){return retryFindElement(removeVehicle);}
    public static WebElement getVehicleRemovedSuccessfulMessage(){return retryFindElement(vehicleRemovedSuccessfulMessage);}
    public static WebElement getPurchases(){return retryFindElement(purchases);}
    public static WebElement getPurchasesSubModule(){return retryFindElement(purchasesSubModule);}
    public static WebElement getInvalidEmailMessage(){return retryFindElement(invalidEmailMessage);}
    public static WebElement getInvalidPhoneNumberMessage(){return retryFindElement(invalidPhoneNumberMessage);}
    public static WebElement getInvalidLicensePlateMessage(){return retryFindElement(invalidLicensePlateMessage);}
}
