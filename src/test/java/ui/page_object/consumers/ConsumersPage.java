package ui.page_object.consumers;

import common.helpers.ui.PageObjectBase;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class ConsumersPage extends PageObjectBase {
    public static By searchViaDrowpdown = By.id("consumers-page-search-type-dropdown");
    public static By searchViaMobilePhoneOption = By.xpath("//li[contains(text(), 'Mobile phone')]");
    public static By searchViaEmailOption = By.xpath("//li[contains(text(), 'Email')]");
    public static By regionDropdown = By.id("consumers-page-region-dropdown");
    public static By regionUSOption = By.xpath("//li[contains(text(), 'US')]");
    public static By regionCanadaOption = By.xpath("//li[contains(text(), 'Canada')]");
    public static By searchConsumer = By.xpath("//input[@name = 'search-consumers']");
    public static By threeDotMenu = By.xpath("//*[@data-qa-name = 'consumer-more-options']");
    public static By editConsumer = By.xpath("//li[contains(text(), 'Manage user')]");
    public static By faqPage = By.xpath("//a[@href='/identity-access/consumers/faq']");
    public static By consumersFaqPage = By.xpath("//h1[.='Consumers - FAQ']");
    public static By firstQuestion = By.xpath("//p[.='1. How do I update a license plate for an active order?']");
    public static By secondQuestion = By.xpath("//p[.='2. What is the current set of pilot locations?']");
    public static By thirdQuestion = By.xpath("//p[.='3. How can I find additional information about a transaction purchase?']");
    public static By forthQuestion = By.xpath("//p[.='4. How do I submit feedback about the new system?']");
    public static By fifthQuestion = By.xpath("//p[.='5. Is this module going to be updated and include new features and information?']");



    public static WebElement getSearchViaDropdown(){return retryFindElement(searchViaDrowpdown);}
    public static WebElement getSearchViaMobilePhone() {return retryFindElement(searchViaMobilePhoneOption);}
    public static WebElement getSearchViaEmail() {return retryFindElement(searchViaEmailOption);}
    public static WebElement getRegionDropdown() {return retryFindElement(regionDropdown);}
    public static WebElement getRegionUS() {return retryFindElement(regionUSOption);}
    public static WebElement getRegionCanada() {return retryFindElement(regionCanadaOption);}
    public static WebElement getSearchConsumer() {return retryFindElement(searchConsumer);}
    public static WebElement getConsumerPhoneNumber(String phoneNumber) {return driver.findElement(By.xpath("//tbody//tr//td[3]//*[contains(text(), '" + phoneNumber + "')]"));}
    public static WebElement getConsumerStatus(String consumerStatus) {return driver.findElement(By.xpath("//tbody//tr//td[4]//*[contains(text(), '" + consumerStatus + "')]"));}
    public static WebElement getConsumerEmail(String consumerEmail) {return driver.findElement(By.xpath("//tbody//tr//td[2]//*[contains(text(), '" + consumerEmail + "')]"));}
    public static WebElement getThreeDotMenu() {return retryFindElement(threeDotMenu);}
    public static WebElement getEditConsumer() {return retryFindElement(editConsumer);}
    public static WebElement getConsumerFullName (String consumerFirstName, String consumerLastName) {return driver.findElement(By.xpath("//tbody//tr//td[1]//*[contains(text(), '" + consumerFirstName + " " + consumerLastName + "')]"));}
    public static WebElement getFaqPage(){return retryFindElement(faqPage);}
    public static WebElement getConsumersFaqPage(){return retryFindElement(consumersFaqPage);}
    public static WebElement getFirstQuestion(){return retryFindElement(firstQuestion);}
    public static WebElement getSecondQuestion(){return retryFindElement(secondQuestion);}
    public static WebElement getThirdQuestion(){return retryFindElement(thirdQuestion);}
    public static WebElement getForthQuestion(){return retryFindElement(forthQuestion);}
    public static WebElement getFifthQuestion(){return retryFindElement(fifthQuestion);}
}
