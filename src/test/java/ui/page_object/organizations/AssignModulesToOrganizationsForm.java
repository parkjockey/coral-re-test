package ui.page_object.organizations;

import common.helpers.ui.PageObjectBase;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class AssignModulesToOrganizationsForm extends PageObjectBase {
    public static By assignModulesToOrgButton = By.id("organizations-page-assign-modules-organization-modal-assign-module-button");

    public static WebElement getAssignModulesToOrgButton() {return driver.findElement(assignModulesToOrgButton);}
    public static WebElement getAssignSpecificModuleToOrganization(String moduleName) {
        return driver.findElement(By.xpath("//div[@class='flex-item assign-modules-organization-modal-content__module_row']//div[text()[contains(.,'"+ moduleName +"')]]"));


    }
}
