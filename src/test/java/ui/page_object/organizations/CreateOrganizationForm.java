package ui.page_object.organizations;

import common.helpers.ui.PageObjectBase;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class CreateOrganizationForm extends PageObjectBase {
    public static By createNewOrganizationTitle = By.xpath("//h2[contains(text(), 'Create New Organization')]");
    public static By organizationName = By.xpath("//input[@placeholder='Enter organization name*']");
    public static By description = By.xpath("//input[@placeholder='Add some description here']");
    public static By orgNameAvailable = By.xpath("//p[contains(text(), 'Organization name available')]");
    public static By createButton = By.id("organizations-page-create-update-modal-create-edit-button");
    public static By orgNameCharLengthValidationMessage = By.xpath("//p[contains(text(), 'Organization name must be under 50 characters')]");
    public static By orgDescCharLengthValidationMessage = By.xpath("//p[contains(text(), 'Description must be under 200 characters')]");
    public static By orgNameIsAlreadyTaken = By.xpath("//p[contains(text(), 'This organization name is already taken')]");
    public static By createOrgSpecCharValidationMessage = By.xpath("//p[contains(text(), 'A-Z, 1-9, space character. Space-only name not allowed')]");

    public static WebElement getCreateNewOrganizationTitle() {
        return retryFindElement(createNewOrganizationTitle);
    }

    public static WebElement getOrganizationName() {
        return retryFindElement(organizationName);
    }

    public static WebElement getDescription() {
        return retryFindElement(description);
    }

    public static WebElement getOrgNameAvailable() {
        return retryFindElement(orgNameAvailable);
    }

    public static WebElement getCreateButton() {
        return driver.findElement(createButton);
    }

    public static WebElement getOrgNameCharLengthValidationMessage() {return driver.findElement(orgNameCharLengthValidationMessage);}

    public static WebElement getOrgDescCharLengthValidationMessage() {return driver.findElement(orgDescCharLengthValidationMessage);}

    public static WebElement getOrgNameIsAlreadyTaken() {return driver.findElement(orgNameIsAlreadyTaken);}

    public static WebElement getCreateOrgSpecCharValidationMessage() {return driver.findElement(createOrgSpecCharValidationMessage);}

}
