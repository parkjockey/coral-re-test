package ui.page_object.organizations;

import common.helpers.ui.PageObjectBase;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class EditOrganizationForm extends PageObjectBase {
    public static By editOrgNameField = By.xpath("//input [@placeholder = 'Enter organization name*']");
    public static By editOrgDescriptionField = By.xpath("//input [@placeholder = 'Add some description here']");
    public static By orgEditButton = By.id("organizations-page-create-update-modal-create-edit-button");
    public static By orgNameAlreadyTaken = By.xpath("//p[contains (text(), 'This organization name is already taken')]");



    public static WebElement getEditOrgNameField() {return retryFindElement(editOrgNameField);}
    public static WebElement getEditOrgDescriptionField() {return retryFindElement(editOrgDescriptionField);}
    public static WebElement getOrgEditButton() {return driver.findElement(orgEditButton);}
    public static WebElement getOrgNameAlreadyTaken() {return retryFindElement(orgNameAlreadyTaken);}
    public static WebElement getSpecificOrgDescciption(String orgDescription) {
        return driver.findElement(By.xpath("//input[@value = '"+ orgDescription +"']"));
    }
}
