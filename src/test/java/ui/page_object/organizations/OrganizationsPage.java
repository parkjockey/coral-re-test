package ui.page_object.organizations;

import common.helpers.ui.PageObjectBase;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.util.List;

public class OrganizationsPage extends PageObjectBase {

    public static By organizationsTitle = By.xpath("//h1[contains(text(), 'Organizations')]");
    public static By createOrganization = By.id("organizations-page-create-new-button");
    public static By tableNameHeader = By.xpath("//th[contains(text(), 'Name')]");
    public static By orgAssignModulHeader = By.xpath("//span[contains(text(), 'Assigned Modules')]");
    public static By orgListDiv = By.xpath("//div[@class='page-content is-scrollable-y page-content--padding']");
    public static By orgPageLoader = By.xpath("//p[@class='paragraph paragraph--2 paragraph-- paragraph--left']");
    public static By orgTableBody = By.xpath("//tbody");
    public static By successTostify = By.xpath("//span[contains(text(), 'Organization sucessfully created')]");
    public static By orgPaginationToNextPage = By.xpath("//span[@ data-qa-name='paginationNextPageArrow']");
    public static By orgRowsPerPageDropDown = By.xpath("//span[@ data-qa-name='paginationDropDownArrow']");
    public static By org40RowsPerPage = By.xpath("//li[contains(text(), '40')]");
    public static By orgSearchField = By.xpath("//input[@placeholder='Search organizations']");
    public static By orgOrganizationNameLabel = By.xpath("//span[@class='text text-color--primary text--bold']");
    public static By orgNotFoundMessage = By.xpath("//h4[contains(text(), 'No organization found!')]");
    public static By orgMoreOptions = By.xpath("//span[@data-qa-name='organizations-more-options']");
    public static By renameOrg = By.xpath("//li[contains(text(), 'Rename Org')]");
    public static By manageOrg = By.xpath("//span[@data-qa-name='organizations-manage-options']");
    public static By manageModulesOrg = By.xpath("//li[contains(text(), 'Modules')]");
    public static By expandAssignedModulesToOrg = By.xpath("//span[@class= 'icon expanding-row-icon icon--clickable']");
    public static By orgLogoInOrganizationList = By.xpath("//div[@data-testid='flex-layout']//img[@alt = 'Organization logo']");






    public static WebElement getOrganizationsTitle(){
        return retryFindElement(organizationsTitle);
    }

    public static WebElement getCreateOrgButton(){
        return retryFindElement(createOrganization);
    }

    public static WebElement getOrgNameColumnHeader(){
        return waitToBeClickable(driver.findElement(tableNameHeader));
    }

    public static WebElement getOrgAssigModulHeader(){
        return waitToBeClickable(driver.findElement(orgAssignModulHeader));
    }

    public static WebElement getOrgListDiv() {return retryFindElement(orgListDiv);}

    public static WebElement getOrgPageLoader() {return retryFindElement(orgPageLoader);}

    public static WebElement getOrgTableBody() {return retryFindElement(orgTableBody);}

    public static WebElement getSuccessTostify() {return retryFindElement(successTostify);}

    public static WebElement getOrgPaginationToNextPage() {return retryFindElement(orgPaginationToNextPage);}

    public static WebElement getOrgRowsPerPageDropDown() {return retryFindElement(orgRowsPerPageDropDown);}

    public static WebElement getOrg40RowsPerPage() {return retryFindElement(org40RowsPerPage);}

    public static WebElement getOrgSearchField() {return retryFindElement(orgSearchField);}

    public static List<WebElement> getOrgOrganizationNameLabel() {return driver.findElements(orgOrganizationNameLabel);}

    public static WebElement getOrgNotFoundMessage() {return retryFindElement(orgNotFoundMessage);}

    public static WebElement getOrgMoreOptions() {return retryFindElement(orgMoreOptions);}

    public static WebElement getRenameOrg() {return retryFindElement(renameOrg);}

    public static WebElement getManageOrg() {return retryFindElement(manageOrg);}

    public static WebElement getManageModulesOrg() {return retryFindElement(manageModulesOrg);}

    public static WebElement getExpandAssignedModulesToOrg() {return retryFindElement(expandAssignedModulesToOrg);}

    public static WebElement getOrgLogoInOrganizationsList() {return retryFindElement(orgLogoInOrganizationList);}

    public static WebElement getAssignedModulesToOrganizations(String moduleName) {
        return driver.findElement(By.xpath("//div[@class = 'flex-layout expanding-row__container expanding-row__container--show']//div[contains(text(), '"+ moduleName +"')]"));
    }

}
