package ui.page_object.organizations;

import common.helpers.ui.PageObjectBase;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class SwitchOrganizationForm extends PageObjectBase {
    public static By switchOrgSearchField = By.xpath("//div[@data-qa-name='switch-organizations-search']//input");
    public static By switchOrgAccessOrganizationButton = By.xpath("//span[contains(text(), 'Access Organization')]");
    public static By switchOrganization = By.xpath("//button[@data-qa-name='switch-organization-button']");

    public static WebElement getSwitchOrgSearchField() {return retryFindElement(switchOrgSearchField);}
    public static WebElement getSwitchOrgAccessOrganizationButton() {return waitToBeClickable(driver.findElement(switchOrgAccessOrganizationButton));}
    public static WebElement getSwitchOrgOrganization() {
        return waitToBeClickable(driver.findElement(switchOrganization));
    }
}
