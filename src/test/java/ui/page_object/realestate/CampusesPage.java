package ui.page_object.realestate;

import common.helpers.ui.PageObjectBase;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.util.List;

public class CampusesPage extends PageObjectBase {

    //campus page fields
    public static By addNewCampusButton = By.xpath("//button[@data-testid=\"add-new-campus-button\"]");
    public static WebElement getAddNewCampusButton() { return retryFindElement(addNewCampusButton);}

    public static By campusPageTitle = By.xpath("//h4[contains(text(), 'Campuses')]");
    public static WebElement getCampusPageTitle(){return retryFindElement(campusPageTitle);}

    public static By searchCampusField = By.id("standard-basic");
    public static WebElement getCampusSearchField(){return retryFindElement(searchCampusField);}

    public static By campusCards = By.xpath("//div[@data-testid='campus-card']");
    public static List<WebElement> getCampusCards() {return driver.findElements(campusCards);}

    public static By campusPerPagePaginationField = By.xpath("//div[@aria-haspopup='listbox']");
    public static WebElement getCampusPerPagePaginationField() { return retryFindElement(campusPerPagePaginationField);}

    public static By campusPerPagePaginationList = By.xpath("//li[@role='option']");
    public static List<WebElement>  getCampusPerPagePaginationList() { return driver.findElements(campusPerPagePaginationList);}


    //create new campus modal fields
    public static By createNewCampusModal = By.xpath("//div[@role='dialog']");
    public static By createNewCampusTitleField = By.name("");
    public static By campusNameField = By.name("campusName");
        /*public static By campusTypeField = By.xpath("//div[@data-testid=\"dti-campus-type-input\"]/div"); */
    public static By campusTypeField = By.xpath("//div[@class='real-estate-MuiInputBase-root real-estate-MuiInput-root real-estate-MuiInput-underline real-estate-MuiInputBase-formControl real-estate-MuiInput-formControl']//div[@role='button']");
    public static By campusDescriptionField = By.xpath("//textarea[@name=\"description\"]");
    public static By selectLandLord = By.xpath("//*[@id=\"menu-\"]/div[3]/ul/li");
    public static By createCampusButton = By.xpath("//*[text()='CREATE CAMPUS']");
    public static By cancelCreateCampusButton = By.xpath("//*[text()='CREATE CAMPUS']");

    public static WebElement getCreateNewCampusModal() {return retryFindElement(createNewCampusModal);}
    public static WebElement getCreateNewCampusTitleField() {return retryFindElement(createNewCampusTitleField);}
    public static WebElement getCampusNameField() {return retryFindElement(campusNameField);}
    public static WebElement getCampusTypeField() {return retryFindElement(campusTypeField);}
    public static WebElement getCampusDescriptionField() {return retryFindElement(campusDescriptionField);}
    public static WebElement getLandLord(){return retryFindElement(selectLandLord);}
    public static WebElement getCreateCampusButton(){return retryFindElement(createCampusButton);}
    public static WebElement getCancelCreateCampusButton(){return retryFindElement(cancelCreateCampusButton);}

//    public static By campusCard =By.xpath("//div[@class='real-estate-MuiPaper-root real-estate-MuiCard-root real-estate-real-estate-prod15 real-estate-MuiPaper-elevation1 real-estate-MuiPaper-rounded']");
//    public static WebElement getCampusCard() { return retryFindElement(campusCard);}

    public static void addNewCampus(String campusName, String campusDescription) throws InterruptedException{
        getCampusNameField().sendKeys(campusName);
        PageObjectBase.waitToBeClickable(getCampusTypeField());
        getCampusTypeField().click();
        getLandLord().click();
        getCampusDescriptionField().sendKeys(campusDescription);
        getCreateCampusButton().click();
    }
}
