package ui.page_object.realestate;


import common.helpers.ui.BaseDriver;
import common.helpers.ui.PageObjectBase;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class RealEstateModulePage extends PageObjectBase {

    public static By findLocationsPage = By.xpath("//*[text()='Find Locations']");
    public static By pendingApprovalPage = By.xpath("//*[text()='Pending Approval']");
    public static By campusesPage = By.xpath("//*[text()='Campuses']");
    public static WebElement getFindLocationsPage() {return retryFindElement(findLocationsPage);}
    public static WebElement getPendingApprovalPage() {return retryFindElement(pendingApprovalPage);}
    public static WebElement getCampusesPage1() {return retryFindElement(campusesPage);}

    public static WebElement navigateToRealEstatePage(String pageName) {
        return driver.findElement(By.xpath("//*[text()='"+ pageName +"']"));
    }

    public static WebElement    getCampusesPage() {return waitToBeClickable(driver.findElement(campusesPage));}

}