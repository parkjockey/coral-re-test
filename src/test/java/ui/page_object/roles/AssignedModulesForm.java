package ui.page_object.roles;

import common.helpers.ui.PageObjectBase;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class AssignedModulesForm extends PageObjectBase {
    public static By cancelButton = By.xpath("//button[contains(text(), 'Cancel')]");
    public static By assignButton = By.id("roles-page-manage-modules-modal-assign-button");



    public static WebElement getCancel() {
        return retryFindElement(cancelButton);
    }
    public static WebElement getAssignButton() {
        return retryFindElement(assignButton);
    }

    public static WebElement getAssignModulesHeading(String roleName) {
        return driver.findElement(By.xpath("//h2[contains(text(), 'Assigned modules to " + roleName + "')]"));
    }

    public static WebElement chooseModulePermission(String moduleName, String permissionLevel) {
        return driver.findElement(
                By.xpath("//span[contains(text(), '" + moduleName + "')]/../..//input[@value='"+permissionLevel+"']"));
    }

    public static WebElement getModuleArrow(String moduleName) {
        return retryFindElement(
                By.xpath("//*[contains(text(), '" + moduleName + "')]/../../../..//span[@class='icon icon--clickable']"));
    }

    public static WebElement getModuleNumber(String moduleName, int moduleNum) {
        return retryFindElement(
                By.xpath("//*[contains(text(), '" + moduleName + "')]/../.." +
                        "//div[contains(text(), '"+moduleNum+" Modules')]"));

    }



}
