package ui.page_object.roles;

import common.helpers.ui.PageObjectBase;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class CreateRoleForm extends PageObjectBase {
    public static By createNewRoleTitle = By.xpath("//h2[contains(text(), 'Create New Role')]");
    public static By roleName = By.xpath("//input[@placeholder='Role name']");
    public static By description = By.xpath("//input[@placeholder='Description']");
    public static By roleNameAvailable = By.xpath("//div[contains(text(), 'Role name available')]");
    public static By createButton = By.id("roles-page-create-update-role-modal-create-edit-button");

    public static WebElement getCreateRoleTitle() {
        return retryFindElement(createNewRoleTitle);
    }

    public static WebElement getRoleName() {
        return retryFindElement(roleName);
    }

    public static WebElement getDescription() {
        return retryFindElement(description);
    }

    public static WebElement getRoleNameAvailable() {
        return retryFindElement(roleNameAvailable);
    }

    public static WebElement getCreateButton() {
        return retryFindElement(createButton);
    }


}
