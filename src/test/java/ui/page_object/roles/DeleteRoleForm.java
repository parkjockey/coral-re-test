package ui.page_object.roles;

import common.helpers.ui.PageObjectBase;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class DeleteRoleForm extends PageObjectBase {

    public static By removeButton = By.id("roles-page-delete-role-modal-delete-button");
    public static By yesPleasButton = By.xpath("//button[contains(text(), 'Yes, please')]");
    public static By informationUserMessage = By.xpath("//p[contains(text(), 'Before deleting, you must manage users. Do you want to proceed?')]");
    public static By deleteRoleWithUsers = By.xpath("//p[contains(text(), 'Roles with assigned users cannot be deleted')]");

    public static WebElement getConfirmationHeading(String removeButton) {
        return retryFindElement(By.xpath("//p[contains(text(), 'Do you still want to remove  "+removeButton+"  role?')]"));
    }

    public static WebElement getUserModulesInfo(int moduleNumber) {
        return retryFindElement(By.xpath("//h1[contains(text(), 'This role has "+moduleNumber+" assigned modules')]"));
    }

    public static WebElement getRemoveButton() {
        return retryFindElement(removeButton);
    }

    public static WebElement getRoleHasUserHeading(int numberOfUsers) {
        return retryFindElement(By.xpath("//h1[contains(text(), 'This role has "+numberOfUsers+" user')]"));
    }


    public static WebElement getYesPleaseButton() {
        return driver.findElement(yesPleasButton);
    }

    public static WebElement getInfoUserMessage() {
        return retryFindElement(informationUserMessage);
    }

    public static WebElement getDeleteRoleWithUsers() {return retryFindElement(deleteRoleWithUsers);}
}
