package ui.page_object.roles;

import common.helpers.ui.PageObjectBase;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class EditRoleForm extends PageObjectBase {
    public static By editRoleTitle = By.xpath("//h2[contains(text(), 'Edit Role')]");
    public static By roleName = By.xpath("//input[@placeholder='Role name']");
    public static By description = By.xpath("//input[@placeholder='Description']");
    public static By roleNameAvailable = By.xpath("//p[contains(text(), 'Role name available')]");
    public static By editButton = By.id("roles-page-create-update-role-modal-create-edit-button");


    public static WebElement getEditRoleTitle() {
        return retryFindElement(editRoleTitle);
    }

    public static WebElement getRoleName() {
        return retryFindElement(roleName);
    }

    public static WebElement getDescription() {
        return retryFindElement(description);
    }

    public static WebElement getRoleNameAvailable() {
        return retryFindElement(roleNameAvailable);
    }

    public static WebElement getEditButton() {
        return retryFindElement(editButton);
    }
}
