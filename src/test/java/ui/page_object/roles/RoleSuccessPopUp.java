package ui.page_object.roles;

import common.helpers.ui.PageObjectBase;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class RoleSuccessPopUp extends PageObjectBase {
    public static By closeSuccessPop = By.id("roles-page-create-role-success-modal-close-button");
    public static By addModulesButton = By.id("roles-page-create-role-success-modal-add-modules-button");


    public static WebElement getClosePopButton() {
        return retryFindElement(closeSuccessPop);
    }
    public static WebElement getAddModulesButton() {
        return retryFindElement(addModulesButton);
    }
}
