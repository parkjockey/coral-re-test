package ui.page_object.roles;

import common.helpers.ui.PageObjectBase;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;

public class RolesPage extends PageObjectBase {

    public static By rolesTitle = By.xpath("//h1[contains(text(), 'Roles')]");
    public static By searchRoles = By.xpath("//input[@placeholder='Search roles']");
    public static By createRole = By.id("roles-page-create-new-role-button");
    public static By editButton = By.xpath("//li[contains(text(), 'Edit Role')]");
    public static By deleteButton = By.xpath("//li[contains(text(), 'Delete Role')]");
    public static By successHeading = By.xpath("//h1[contains(text(), 'Success !')]");
    public static By addModulesButton = By.xpath("//button[contains(text(), 'Add Modules')]");
    public static By roleScrollableDiv = By.xpath("//div[@class='page-content roles-page-content page-content--scrollable page-content--padding']");
    public static By rolePageLoader = By.xpath("//p[@class='paragraph paragraph--2 paragraph-- paragraph--left']");
    public static By roleCard = By.xpath("//div[@class='flex-layout role-card']");
    public static By addModule = By.xpath("//div[contains(text(), 'Add Modules')]");
    public static By adminRolesIsCreated = By.xpath("//div[@class='flex-layout role-card']//div[contains(text(), 'Admin')]");
    public static By penEditModule = By.xpath("//div[@class = 'flex-layout role-card__row-action']");
    public static By roleUsers = By.xpath("//div[@class='item-holder role-card__row-item-number--users']");


    public static WebElement getRolesTitle() {
        return retryFindElement(rolesTitle);
    }

    public static WebElement getSearchRoles() {
        return retryFindElement(searchRoles);
    }

    public static WebElement getCreateRoleButton() {
        return retryFindElement(createRole);
    }

    public static WebElement getSpecificRole(String roleName) {
        return driver.findElement(By.xpath("//div[contains(text(), '" + roleName + "')]"));
    }

    public static boolean roleIsNotPresent(String roleName) {
        try {
            driver.findElement(By.xpath("//div[contains(text(), '" + roleName + "')]"));
            return false;

        } catch (NoSuchElementException e) {
            System.out.println("Element absent");
            return true;
        }

    }

    public static WebElement getRoleHoriz(String roleName) {
        return retryFindElement(By.xpath("//div[contains(text(), '" + roleName + "')]/..//span"));
    }

    public static WebElement getEditRoleButton() {
        return retryFindElement(editButton);
    }

    public static WebElement getSuccessHeading() {
        return retryFindElement(successHeading);
    }

    public static WebElement getAddModulesButton() {
        return retryFindElement(addModulesButton);
    }

    public static WebElement getDeleteRoleButton() {
        return retryFindElement(deleteButton);
    }

    public static WebElement getEditRoleModules(String roleName) {
        return retryFindElement(
                By.xpath(
                        "//div[contains(text(), '" + roleName + "')]" +
                                "/../..//span[@class='icon icon--clickable']"));
    }


    public static WebElement getNumberOfAssignedModules(Integer number) {
        return retryFindElement(
                By.xpath(
                        "//div[contains(text(), '"+number+" Modules/Submodules assigned')]"));
    }

    public static WebElement getRoleScrollableDiv() {return retryFindElement(roleScrollableDiv);}

    public static WebElement getRolePageLoader() {return retryFindElement(rolePageLoader);}

    public static WebElement getRoleCard() {return retryFindElement(roleCard);}

    public static WebElement getAddModule() {return retryFindElement(addModule);}

    public static WebElement getAdminRolesIsCreated() {return retryFindElement(adminRolesIsCreated);}

    public static WebElement getPenEditModule() {return retryFindElement(penEditModule);}

    public static WebElement getRoleUsers() {return retryFindElement(roleUsers);}
}
