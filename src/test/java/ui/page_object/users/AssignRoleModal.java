package ui.page_object.users;

import common.helpers.ui.PageObjectBase;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.util.List;

public class AssignRoleModal extends PageObjectBase {
    public static By modalTitle = By.xpath("//h2[contains(text(), 'Assign role(s) to user')]");
    public static By assignRoleCheckBox = By.xpath("//input[@type = 'checkbox']");
    public static By assignRoleButton = By.id("organization-users-page-assign-roles-organization-users-modal-assign-role-button");
    public static WebElement getModalTitle() {
        return retryFindElement(modalTitle);
    }

    public static List<WebElement> getAssignRoleCheckBox() {return driver.findElements(assignRoleCheckBox);}

    public static WebElement getAssignRoleButton() {
        return retryFindElement(assignRoleButton);
    }
}
