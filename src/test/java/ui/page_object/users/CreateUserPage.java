package ui.page_object.users;

import common.helpers.ui.PageObjectBase;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class CreateUserPage extends PageObjectBase {
    public static By createUserPageTitle = By.xpath("//div[contains(text(), 'Please create your account')]");
    public static By firstName = By.xpath("//input[@placeholder='First Name']");
    public static By lastName = By.xpath("//input[@placeholder='Last Name']");
    public static By password = By.xpath("//input[@placeholder='Password']");
    public static By passwordMessage = By.xpath("//div[contains(text(), 'Password must be at least 8 characters, have special character, number, upper and lower character')]");
    public static By captchaCheckbox = By.xpath("/html/body/div[2]/div[3]/div[1]/div/div/span/div[1]");
    public static By createAccountButton = By.id("invitation-page-create-account-button");

    public static WebElement getCreateUserPageTitle() {return retryFindElement(createUserPageTitle);}
    public static WebElement getEmailFiled(String email){
        return driver.findElement(By.xpath("//input[@value='"+email+"']"));
    }
    public static WebElement getFirstName(){
        return waitToBeClickable(driver.findElement(firstName));
    }
    public static WebElement getLastName(){
        return waitToBeClickable(driver.findElement(lastName));
    }
    public static WebElement getPassword(){
        return waitToBeClickable(driver.findElement(password));
    }
    public static WebElement getPasswordMessage(){
        return waitToBeClickable(driver.findElement(passwordMessage));
    }

    public static WebElement getCaptchaCheckbox(){
        return waitToBeClickable(driver.findElement(captchaCheckbox));
    }

    public static WebElement getCreateAccountButton(){
        return (driver.findElement(createAccountButton));
    }


}
