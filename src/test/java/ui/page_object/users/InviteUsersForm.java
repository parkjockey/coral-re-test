package ui.page_object.users;

import common.helpers.ui.PageObjectBase;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.util.List;

public class InviteUsersForm extends PageObjectBase {
    public static String blackColor = "#5A606C";
    public static String greenColor = "#3AD29F";

    public static By usrEmailForm = By.xpath("//input[@placeholder='Enter one or multiple emails to invite']");
    public static By usrNameWhenUserIsAlreadyActive = By.xpath("//div[contains(text(), 'Reef Tester')]");
    public static By usrSelectRoleList = By.xpath("//input[@name='user-role']");
    public static By usrAutoRole = By.xpath("//div[@class='select-options']//div[contains(text(), 'Global REEF Administrator')]");
    public static By usrInviteUserBtn = By.id("organization-users-page-invite-organization-users-modal-invite-users-button");
    public static By usrCheckYourEmailEntriesMessage = By.xpath("//div[contains(text(), 'Check your email entries')]");
    public static By passwordPolicyConditions = By.xpath("//div[@data-test-id = 'password-policy-item']");
    public static By least8CharactersPasswordPolicy = By.xpath("//*[contains(text(), '8 characters' )]");
    public static By lowerUpperCasePasswordPolicy = By.xpath("//*[contains(text(), 'contains both lower (a-z) and upper case letters (A-Z)' )]");
    public static By leastOneNumberPasswordPolicy = By.xpath("//*[contains(text(), 'contains at lesat one number (0-9)' )]");
    public static By oneSpecialCharacterPasswordPolicy = By.xpath("//*[contains(text(), 'contains at least one special character' )]");
    public static By doesnNotContainNameEmailPasswordPolicy = By.xpath("//*[contains(text(), 'does not contain your name or email' )]");


    public static WebElement getUsrEmailForm() {return retryFindElement(usrEmailForm);}
    public static WebElement getUsrNameWhenUserIsAlreadyActive() {return retryFindElement(usrNameWhenUserIsAlreadyActive);}
    public static WebElement getUsrInviteUser(String userEmail) {return driver.findElement(By.xpath("//div[contains(text(), '" + userEmail + "')]"));}
    public static WebElement getUsrSelectRoleList() {return retryFindElement(usrSelectRoleList);}
    public static WebElement getUsrAutoRole() {return retryFindElement(usrAutoRole);}
    public static WebElement getUsrInviteUserBtn() {return driver.findElement(usrInviteUserBtn);}
    public static WebElement getUsrCheckYourEmailEntriesMessage() {return retryFindElement(usrCheckYourEmailEntriesMessage);}
    public static List<WebElement> getPasswordPolicyConditions() {return driver.findElements(passwordPolicyConditions);}
    public static WebElement getLeast8ChartersPasswordPolicy() {return getPasswordPolicyConditions().get(0);}
    public static WebElement getLowerUpperCasePasswordPolicy() {return retryFindElement(lowerUpperCasePasswordPolicy);}
    public static WebElement getLeastOneNumberPasswordPolicy() {return retryFindElement(leastOneNumberPasswordPolicy);}
    public static WebElement getOneSpecialCharacterPasswordPolicy() {return retryFindElement(oneSpecialCharacterPasswordPolicy);}
    public static WebElement getDoesNotContainNameEmailPasswordPolicy() {return retryFindElement(doesnNotContainNameEmailPasswordPolicy);}
}
