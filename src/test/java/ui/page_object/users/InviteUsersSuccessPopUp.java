package ui.page_object.users;

import common.helpers.ui.PageObjectBase;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class InviteUsersSuccessPopUp extends PageObjectBase {
    public static By usrUsersInvitedMessage = By.xpath("//div[@class='confirm-invite-organization-users-paragraph']");
    public static By invitedUsersNumber = By.xpath("//span[@class='confirm-invite-organization-users-paragraph--users']");
    public static By usrGoToUsersListButton = By.xpath("//button[contains (text(), 'GO TO USERS LIST')]");

    public static WebElement getUsrUsersInvitedMessage() {return retryFindElement(usrUsersInvitedMessage);}
    public static WebElement getUsrGoToUsersListButton() {return retryFindElement(usrGoToUsersListButton);}
    public static WebElement getInvitedUsersNumbers(){return retryFindElement(invitedUsersNumber);}

}
