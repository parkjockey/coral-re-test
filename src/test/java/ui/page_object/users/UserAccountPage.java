package ui.page_object.users;

import common.helpers.ui.PageObjectBase;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.util.List;

public class UserAccountPage extends PageObjectBase {

    public static By firstName = By.xpath("//input [@name = 'firstName']");
    public static By lastName = By.xpath("//input [@name = 'lastName']");
    public static By email = By.xpath("//input [@name = 'email']");

    public static By updateButton = By.xpath("//span[contains(text(), 'Save Changes')]");
    public static By cancelButton = By.xpath("//button [contains(text(), 'Cancel')]");

    public static By securitySection = By.xpath("//span[contains(text(), 'Security')]");
    public static By changePasswordSSOUser = By.xpath("//*[contains(text(), 'Change password')]");
    public static By currentPasswordInput = By.xpath("//input [@name = 'currentPassword']");
    public static By newPasswordInput = By.xpath("//input [@name = 'newPassword']");
    public static By newPasswordConfirmInput = By.xpath("//input [@name = 'confirmPassword']");
    public static By updatePasswordButtonNoSSO = By.xpath("//span[contains(text(), 'Update Password')]");
    public static By uploadProfileImageCircle = By.id("user-profile-modal-profile-tab-upload-picture-avatar");
    public static By uploadProfileImageButton = By.id("upload-profile-picture-upload-button");
    public static By uploadProfileImageSaveButton = By.id("upload-profile-picture-save-button");
    public static By uploadedProfilePicture = By.xpath("//img[contains(@src,'data:image/jpeg;base64,iVBORw0KGgoAAAANSUhEUgAABB0AAAQdCAYAAAAS')]");


    public static WebElement getFirstName() {return driver.findElement(firstName);}
    public static WebElement getLastName() {return driver.findElement(lastName);}
    public static WebElement getEmail() {return retryFindElement(email);}
    public static WebElement getUpdateButton() {return waitToBeClickable(driver.findElement(updateButton));}
    public static WebElement getCancelButton() {return waitToBeClickable(driver.findElement(cancelButton));}
    public static WebElement getSecuritySection() {return waitToBeClickable(driver.findElement(securitySection));}
    public static WebElement getCurrentPasswordInput() {return retryFindElement(currentPasswordInput);}
    public static WebElement getNewPasswordInput() {return retryFindElement(newPasswordInput);}
    public static WebElement getChangePasswordSSOUser() {return retryFindElement(changePasswordSSOUser);}
    public static WebElement getNewPasswordConfirmInput() {return retryFindElement(newPasswordConfirmInput);}
    public static WebElement getUpdatePasswordButtonNoSSO() {return retryFindElement(updatePasswordButtonNoSSO);}
    public static WebElement getUploadProfileImageCircle() {return driver.findElement(uploadProfileImageCircle);}
    public static WebElement getUploadProfileImageButton() {return retryFindElement(uploadProfileImageButton);}
    public static WebElement getUploadProfileImageSaveButton() {return waitToBeClickable(driver.findElement(uploadProfileImageSaveButton));}
    public static WebElement getUploadedProfilePicture() {return retryFindElement(uploadedProfilePicture);}

    public static List<WebElement> getUploadedProfileImage(String base64) {return driver.findElements(By.xpath("//img[contains(@src,'data:image/png;base64,"+base64+"')]"));}

    public static WebElement getAvatarProfilePicture(String base64) {return driver.findElement(By.xpath("//button[@data-qa-name = 'profile-avatar']//img[contains(@src,'data:image/png;base64,"+base64+"')]"));}
    public static WebElement getCircleProfilePicture(String base64) {return driver.findElement(By.xpath("//button[@data-qa-name = 'profile-avatar']//img[contains(@src,'data:image/png;base64,"+base64+"')]"));}


    public static void updateProfile() {

        getUpdateButton().click();

    }
    public static void cancelUpdateProfile() {

        getCancelButton().click();

    }
}
