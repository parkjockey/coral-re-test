package ui.page_object.users;

import common.helpers.ui.PageObjectBase;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.util.List;

public class UsersPage extends PageObjectBase {
    public static By usrUserName = By.xpath("//span[@class = 'text user-info--name text-color--primary text--bold']");
    public static By usrRowsPerPage = By.xpath("//span[@ data-qa-name='paginationDropDownArrow']");
    public static By usr10RowsPerPage = By.xpath("//li[contains(text(), '10')]");
    public static By usrHeader = By.xpath("//h1[contains(text(), 'Users')]");
    public static By usrInviteUsersBtn = By.id("organization-users-invite-users-button");
    public static By usrPaginationToNextPage = By.xpath("//span[@ data-qa-name='paginationNextPageArrow']");
    public static By usrSearchUser = By.xpath("//input [@placeholder = 'Search users']");
    public static By usrUserEmailOnUserLabel = By.xpath("//span[@class='text user-info--email text-color--primary']");
    public static By usrNoUsersFoundMessage = By.xpath("//h4[contains(text(), 'No users found!')]");
    public static By usrAssignRolesToUsersModal = By.xpath("//li[contains(text(), 'Assign Role(s)')]");
    public static By tostyfySuccessRoleUpdate = By.xpath("//span[contains(text(), 'Roles successfully assigned')]");
    public static By usrTableHeaderName = By.xpath("//th//span[contains(text(), 'Name')]");
    public static By usrTableHeaderEmail = By.xpath("//th//span[contains(text(), 'Email')]");
    public static By usrTableHeaderRoles = By.xpath("//th//span[contains(text(), 'Role(s)')]");
    public static By usrTableHeaderStatus = By.xpath("//th//span[contains(text(), 'Status')]");
    public static By usrTableHeaderUpdated = By.xpath("//th//span[contains(text(), 'Updated')]");
    public static By usrTableHeaderDataAccess = By.xpath("//th//span[contains(text(), 'Data Access')]");
    public static By usrUserLabel = By.xpath("//table/tbody/tr");
    public static By usrResendInvitation = By.xpath("//li[contains(text(), 'Resend Invitaiton')]");
    public static By usrInvitationResendToastify = By.xpath("//span[contains(text(), 'Invitation resent')]");
    public static By usrMoreUser = By.xpath("//span[@data-qa-name = 'organization-users-row-option-column']");
    public static By usrMoreUserFirst = By.xpath("(//span[@data-qa-name = 'organization-users-row-option-column'])[1]");
    public static By usrSearchViaField = By.id("organization-users-page-search-type-dropdown");
    public static By usrSearchViaEmail = By.id("organization-users-page-search-type-dropdown-option-1");
    public static By usrSearchViaName = By.id("organization-users-page-search-type-dropdown-option-0");
    public static By usrStatusesFilter = By.xpath("//input[@data-qa-name = 'organization-users-status-filter-input-field']");
    public static By usrSelectStatus = By.xpath("//li[@data-option-index = '0']//div[@data-qa-name = 'organization-users-status-filter-option']");
    public static By usrRolesFilterInput = By.xpath("//input[@data-qa-name = 'organization-users-roles-filter-input-field']");
    public static By usrSelectRolesFilter = By.xpath("//li[@data-option-index = '0']//div[@data-qa-name = 'organization-users-roles-filter-option']");
    public static By usrDateRangeFilter = By.xpath("//*[@data-qa-name='users-datepicker']");
    public static By usrPreviousNextMonthDateRangeFilter = By.xpath("//button[@class = 'MuiButtonBase-root MuiIconButton-root MuiPickersCalendarHeader-iconButton']");
    public static By firstDayInMonthDateRangeFilter = By.xpath("//*[@data-qa-name = 'datepicker-day-1']");
    public static By twentyEighthDayInMonthDateRangeFilter = By.xpath("//*[@data-qa-name = 'datepicker-day-28']");
    public static By usrSuspendUserOption = By.xpath("//li[contains(text(), 'Suspend User')]");
    public static By suspendUserButton = By.id("organization-users-page-confirm-activate-suspend-user-modal-action-button");
    public static By failedToUpdateUserStatusToastMessage = By.xpath("//span[contains(text(), 'Failed to update user status!')]");
    public static By checkedCheckBox = By.cssSelector("input:checked[type='checkbox']");
    public static By roleScrollableDiv1 = By.xpath("//div[@class='modal__inner modal__inner--fixed-height']");


    public static WebElement getUsrUserName() {return retryFindElement(usrUserName);}
    public static WebElement getUsrRowsPerPage() {return retryFindElement(usrRowsPerPage);}
    public static WebElement getUsr10RowsPerPage() {return retryFindElement(usr10RowsPerPage);}
    public static WebElement getUsrHeader() {return retryFindElement(usrHeader);}
    public static WebElement getInviteUsersBtn() {return retryFindElement(usrInviteUsersBtn);}
    public static WebElement getUsrPaginationToNextPage() {return retryFindElement(usrPaginationToNextPage);}
    public static WebElement getUsrSearchUser() {return retryFindElement(usrSearchUser);}
    public static List<WebElement> getUsrUserEmailOnUserLabel() {return driver.findElements(usrUserEmailOnUserLabel);}
    public static WebElement getUsrNoUsersFoundMessage() {return driver.findElement(usrNoUsersFoundMessage);}
    public static WebElement getMoreUser() {
        return retryFindElement(usrMoreUser);
    }
    public static WebElement getMoreUserFirst() {
        return retryFindElement(usrMoreUserFirst);
    }
    public static WebElement getUsrAssignRolesToUsersModal() {return retryFindElement(usrAssignRolesToUsersModal);}
    public static WebElement getTostRoleAssigned() {return retryFindElement(tostyfySuccessRoleUpdate);}
    public static WebElement getUsrTableHeaderName() {return retryFindElement(usrTableHeaderName);}
    public static WebElement getUsrTableHeaderEmail() {return retryFindElement(usrTableHeaderEmail);}
    public static WebElement getUsrTableHeaderRoles() {return retryFindElement(usrTableHeaderRoles);}
    public static WebElement getUsrTableHeaderStatus() {return retryFindElement(usrTableHeaderStatus);}
    public static WebElement getUsrTableHeaderUpdated() {return retryFindElement(usrTableHeaderUpdated);}
    public static WebElement getUsrTableHeaderDataAccess() {return retryFindElement(usrTableHeaderDataAccess);}
    public static List<WebElement> getUsrUserLabel() {return driver.findElements(usrUserLabel);}
    public static WebElement getUsrResendInvitation() {return retryFindElement(usrResendInvitation);}
    public static WebElement getUsrInvitationResendToastify() {return retryFindElement(usrInvitationResendToastify);}
    public static WebElement getUsrSearchViaField() {return retryFindElement(usrSearchViaField);}
    public static WebElement getUsrSearchViaEmail() {return retryFindElement(usrSearchViaEmail);}
    public static WebElement getUsrSearchViaName() {return retryFindElement(usrSearchViaName);}
    public static WebElement getUsrStatusesFilter() {return retryFindElement(usrStatusesFilter);}
    public static WebElement getUsrSelectStatus() {return retryFindElement(usrSelectStatus);}
    public static By userStatusList(String status) { return By.xpath("//*[contains(text(), '" + status + "')]"); }
    public static WebElement getUsrRolesFilterInput() {return retryFindElement(usrRolesFilterInput);}
    public static WebElement getUsrSelectRolesFilter() {return retryFindElement(usrSelectRolesFilter);}
    public static WebElement getUsrSuspendUserOption() {return retryFindElement(usrSuspendUserOption);}
    public static WebElement getSuspendUserButton() {return retryFindElement(suspendUserButton);}
    public static WebElement getFailedToUpdateUserStatusToastMessage() {return retryFindElement(failedToUpdateUserStatusToastMessage);}
    public static List<WebElement> getPreviousNextMonthDateRangeFilter() {return driver.findElements(usrPreviousNextMonthDateRangeFilter);}
    public static WebElement getPreviousMonthDateRangeFilter() {return getPreviousNextMonthDateRangeFilter().get(0);}
    public static WebElement getUsrDateRangeFilter() {return retryFindElement(usrDateRangeFilter);}
    public static WebElement getFirstDayInMonthDateRangeFilter() {return retryFindElement(firstDayInMonthDateRangeFilter);}
    public static WebElement getTwentyEighthDayInMonthDateRangeFilter() {return retryFindElement(twentyEighthDayInMonthDateRangeFilter);}
    public static List<WebElement> getRolesList(String roleName) {return driver.findElements(By.xpath("//div[contains(text(), '" + roleName + "')]"));}
    public static List<WebElement> getUpdatedMonths(String previousMonth) {
        return driver.findElements(By.xpath("//tbody//tr//span[contains(text(), '" + previousMonth + "')]"));}
    public static WebElement getCurrentDayInDateRangeFilter(String currentDay) {return driver.findElement(By.xpath("//*[@data-qa-name = 'datepicker-day-" + currentDay + "']"));}
    public static WebElement getCheckedCheckBoxes(){return driver.findElement(checkedCheckBox);}
    public static WebElement getRoleScrollableDiv1() {return retryFindElement(roleScrollableDiv1);}
}
