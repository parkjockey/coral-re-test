package ui.steps;

import common.helpers.db_helpers.SQLQueries;
import common.helpers.graph_api.MSGraphAPI;
import common.helpers.rest.HelperRest;
import common.helpers.ui.PageObjectBase;
import common.helpers.ui.PageUrls;
import data_containers.GeneralData;
import data_containers.UserData;
import io.cucumber.java.After;
import io.cucumber.java.sl.In;
import io.cucumber.java8.En;
import org.assertj.core.api.SoftAssertions;
import ui.page_object.HeaderBar;
import ui.page_object.HomePage;
import ui.page_object.LoginPage;
import ui.page_object.MicrosoftLoginPage;
import ui.page_object.users.CreateUserPage;
import ui.page_object.users.InviteUsersForm;

import java.util.HashMap;
import java.util.concurrent.TimeUnit;

import static common.helpers.ui.BaseDriver.driver;
import static org.awaitility.Awaitility.await;


public class AuthenticationSteps implements En {
    GeneralData generalData;
    UserData userData;
    String notExistingUser;
    SoftAssertions softAssertions = new SoftAssertions();
    String disabledUser;


    public AuthenticationSteps(GeneralData generalData, UserData userData) {
        this.generalData = generalData;
        this.userData = userData;

        Given("NoSSO admin user is logged in", () -> {
            LoginPage.userLogin(generalData.adNoSSOUsername);
            LoginPage.getUserNoSSOPasswordField().sendKeys(generalData.adPassword);
            LoginPage.getLogInToYourAccountNoSSOUserButton().click();
            if (generalData.envName.contains("Container")) {
                await("Wait condition to be true")
                        .atMost(10, TimeUnit.SECONDS)
                        .pollDelay(1, TimeUnit.SECONDS)
                        .pollInterval(1, TimeUnit.SECONDS)
                        .until(() ->
                        PageObjectBase.driver.getCurrentUrl().contains(generalData.reefWebApp));
            } else {
                await("Wait condition to be true")
                        .atMost(10, TimeUnit.SECONDS)
                        .pollDelay(1, TimeUnit.SECONDS)
                        .pollInterval(1, TimeUnit.SECONDS)
                        .until(() ->
                        PageObjectBase.driver.getCurrentUrl().contains(generalData.reefWebApp + PageUrls.Pages.HOME.path));
            }


        });

        Given("an administrator has already created an AD account for me", () -> {
            softAssertions.assertThat(generalData.adSSOUsername).as("User is created").isNotNull();
            softAssertions.assertAll();
        });

        Given("my account doesn't exist in Active Directory", () -> {
            notExistingUser = "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa";
        });

        When("I try to log in with my AD credentials", () -> {
            LoginPage.userLogin(generalData.adSSOUsername);
            MicrosoftLoginPage.loginMicrosoftAccount(generalData.adSSOUsername, generalData.adPassword);

        });

        When("I try to insert Not existing username", () -> {
            LoginPage.userLogin(generalData.adSSOUsername);
            PageObjectBase.waitForLoad(2);
            MicrosoftLoginPage.getEmailInputField().sendKeys(notExistingUser);
            MicrosoftLoginPage.getConfirmButton().click();
        });

        Then("SSO User is logged in", () -> {
            if (generalData.envName.contains("Container")) {
                await("Wait condition to be true")
                        .atMost(10, TimeUnit.SECONDS).until(() ->
                        PageObjectBase.driver.getCurrentUrl().contains(generalData.reefWebApp));
            } else {
                await("Wait condition to be true")
                        .atMost(10, TimeUnit.SECONDS).until(() ->
                        PageObjectBase.driver.getCurrentUrl().contains(generalData.reefWebApp + PageUrls.Pages.HOME.path));
            }
        });

        Then("the system will display a Microsoft invalid username error message", () -> {
            softAssertions.assertThat(MicrosoftLoginPage.getUserNameError().isDisplayed())
                    .as("Invalid username error is displayed").isTrue();
            softAssertions.assertThat(MicrosoftLoginPage.getUserNameError().getText())
                    .isEqualTo("We couldn't find an account with that username.");
            softAssertions.assertAll();


        });

        Given("I log in with my AD", () -> {
            LoginPage.userLogin(generalData.adSSOUsername);
        });

        Given("the system redirects me to Microsoft AD page", () -> {
            MicrosoftLoginPage.getEmailInputField().sendKeys(generalData.adSSOUsername);
            MicrosoftLoginPage.getConfirmButton().click();
        });

        When("I insert wrong AD password", () -> {
            MicrosoftLoginPage.getPasswordInputField().sendKeys("fdasfadsfa");
            MicrosoftLoginPage.getConfirmButton().click();
        });

        Then("the system will display a Microsoft invalid password error", () -> {
            softAssertions.assertThat(MicrosoftLoginPage.getPasswordError().isDisplayed())
                    .as("Invalid username error is displayed").isTrue();
            softAssertions.assertThat(MicrosoftLoginPage.getPasswordError().getText())
                    .isEqualTo("Your account or password is incorrect. If you don't remember your password, reset it now.");
            softAssertions.assertAll();
        });

        Given("disabled user in Active Directory", () -> {
            HashMap<String, String> adUser = MSGraphAPI.createADUser(MSGraphAPI.getADToken(), false);
            disabledUser = adUser.get("mail");
            //We must wait to Microsoft process all created data
            Thread.sleep(5000);
            System.out.println(disabledUser);
        });

        Given("I insert disabled AD account", () -> {
            LoginPage.userLogin(generalData.adSSOUsername);
            MicrosoftLoginPage.getEmailInputField().sendKeys(disabledUser);
            MicrosoftLoginPage.getConfirmButton().click();
        });

        When("I insert AD password", () -> {
            MicrosoftLoginPage.getPasswordInputField().sendKeys(generalData.adPassword);
            MicrosoftLoginPage.getConfirmButton().click();
        });

        Then("the system will display a Microsoft account locked error", () -> {
            softAssertions.assertThat(MicrosoftLoginPage.getPasswordError().isDisplayed())
                    .as("Invalid username error is displayed").isTrue();
            softAssertions.assertThat(MicrosoftLoginPage.getPasswordError().getText())
                    .isEqualTo("Your account has been locked. Contact your support person to unlock it, then try again.");
            softAssertions.assertAll();
        });

        Given("an administrator has already created an no SSO account for me", () -> {
            softAssertions.assertThat(generalData.adNoSSOUsername).as("User is created").isNotNull();
            softAssertions.assertAll();
        });

        When("I try to log in with my No SSO credentials", () -> {
            LoginPage.userLogin(generalData.adNoSSOUsername);
            LoginPage.getUserNoSSOPasswordField().sendKeys(generalData.adPassword);
            LoginPage.getLogInToYourAccountNoSSOUserButton().click();
        });

        Then("the system will take me to the home page", () -> {
            PageObjectBase.waitForLoad(3);
            if (generalData.envName.contains("Container")) {
                await("Wait condition to be true")
                        .atMost(5, TimeUnit.SECONDS).until(() ->
                        PageObjectBase.driver.getCurrentUrl().contains(generalData.reefWebApp));
            } else {
                softAssertions.assertThat(HomePage.getRoles().isDisplayed()).isTrue();
                softAssertions.assertThat(HomePage.getUsers().isDisplayed()).isTrue();
                softAssertions.assertThat(HomePage.getOrganizations().isDisplayed()).isTrue();
                softAssertions.assertThat(PageObjectBase.driver.getCurrentUrl())
                        .as("Home page url is true").isEqualTo(generalData.reefWebApp + PageUrls.Pages.HOME.path);
                softAssertions.assertAll();
            }

        });


        Then("the system will display a invalid password error", () -> {
            PageObjectBase.waitForLoad(1);
            softAssertions.assertThat(LoginPage.getPasswordErrorMessage().isDisplayed())
                    .as("Invalid password error is displayed").isTrue();
            softAssertions.assertThat(LoginPage.getPasswordErrorMessage().getText())
                    .isEqualTo("Wrong password");
            softAssertions.assertAll();
        });

        Given("I log in with my no SSO email", () -> {
            LoginPage.userLogin(generalData.adNoSSOUsername);
        });

        When("I insert wrong password", () -> {
            LoginPage.getUserNoSSOPasswordField().sendKeys("Guest321!");
            LoginPage.getLogInToYourAccountNoSSOUserButton().click();
        });

        Given("user go to option for recovery password and send recovery link to his email", () -> {
            LoginPage.getCantLogInToYourAccount().click();
            PageObjectBase.waitForLoad(1);
            LoginPage.getUserEmailField().sendKeys(userData.email);
            LoginPage.getSendRecoveryLinkButton().click();
            softAssertions.assertThat(LoginPage.getRecoveryLinkSentSuccessfullMessage().isDisplayed()).as("Link sent successfully message is displayed").isTrue();
            softAssertions.assertAll();
        });

        Given("User goes to Reset Password link", () -> {
            String  passwordVerificationCode = SQLQueries.getResetPasswordVerificationCode(userData.id);
            driver.get(generalData.loginWebApp+"/recovery/password/"+passwordVerificationCode);
            PageObjectBase.waitForLoad(5);
            softAssertions.assertThat(PageObjectBase.rgbaToHexColor(InviteUsersForm.getLeast8ChartersPasswordPolicy()))
                    .as("Color is black").isEqualTo(InviteUsersForm.blackColor);
            softAssertions.assertThat(PageObjectBase.rgbaToHexColor(InviteUsersForm.getLowerUpperCasePasswordPolicy()))
                    .as("Color is black").isEqualTo(InviteUsersForm.blackColor);
            softAssertions.assertThat(PageObjectBase.rgbaToHexColor(InviteUsersForm.getLeastOneNumberPasswordPolicy()))
                    .as("Color is black").isEqualTo(InviteUsersForm.blackColor);
            softAssertions.assertThat(PageObjectBase.rgbaToHexColor(InviteUsersForm.getOneSpecialCharacterPasswordPolicy()))
                    .as("Color is black").isEqualTo(InviteUsersForm.blackColor);
            softAssertions.assertThat(PageObjectBase.rgbaToHexColor(InviteUsersForm.getDoesNotContainNameEmailPasswordPolicy()))
                    .as("Color is black").isEqualTo(InviteUsersForm.blackColor);
            softAssertions.assertAll();
        });

        Given("User set new password on recovery password page", () -> {
            userData.updatedPassword = "NewPassword1234!";
            LoginPage.getNewPasswordInput().sendKeys(userData.updatedPassword);
            softAssertions.assertThat(PageObjectBase.rgbaToHexColor(InviteUsersForm.getLeast8ChartersPasswordPolicy()))
                    .as("Color is green").isEqualTo(InviteUsersForm.greenColor);
            softAssertions.assertThat(PageObjectBase.rgbaToHexColor(InviteUsersForm.getLowerUpperCasePasswordPolicy()))
                    .as("Color is green").isEqualTo(InviteUsersForm.greenColor);
            softAssertions.assertThat(PageObjectBase.rgbaToHexColor(InviteUsersForm.getLeastOneNumberPasswordPolicy()))
                    .as("Color is green").isEqualTo(InviteUsersForm.greenColor);
            softAssertions.assertThat(PageObjectBase.rgbaToHexColor(InviteUsersForm.getOneSpecialCharacterPasswordPolicy()))
                    .as("Color is green").isEqualTo(InviteUsersForm.greenColor);
            softAssertions.assertThat(PageObjectBase.rgbaToHexColor(InviteUsersForm.getDoesNotContainNameEmailPasswordPolicy()))
                    .as("Color is green").isEqualTo(InviteUsersForm.greenColor);
            softAssertions.assertAll();
        });

        When("User confirm recovery password", () -> {
            LoginPage.getCreateANewPasswordButton().click();
            softAssertions.assertThat(LoginPage.getResetPasswordToastMessage().isDisplayed()).as("Reset Password successfully toast message is displayed").isTrue();
            Thread.sleep(10000);
            PageObjectBase.waitForLoad(2);
            softAssertions.assertThat(HeaderBar.getAvatar().isDisplayed())
                    .as("Avatar is displayed").isTrue();
            softAssertions.assertAll();
        });


        String greenColor = "#3AD29F";

        Given("User enter first and last name on the confirmation form", () -> {
            userData.firstName = "AutoName"+ HelperRest.getRandomNumber();
            userData.lastName = "AutoLastName"+HelperRest.getRandomNumber();
            CreateUserPage.getFirstName().sendKeys(userData.firstName);
            CreateUserPage.getLastName().sendKeys(userData.lastName);
            softAssertions.assertThat(PageObjectBase.rgbaToHexColor(InviteUsersForm.getLeast8ChartersPasswordPolicy()))
                    .as("Color is black").isEqualTo(InviteUsersForm.blackColor);
            softAssertions.assertThat(PageObjectBase.rgbaToHexColor(InviteUsersForm.getLowerUpperCasePasswordPolicy()))
                    .as("Color is black").isEqualTo(InviteUsersForm.blackColor);
            softAssertions.assertThat(PageObjectBase.rgbaToHexColor(InviteUsersForm.getLeastOneNumberPasswordPolicy()))
                    .as("Color is black").isEqualTo(InviteUsersForm.blackColor);
            softAssertions.assertThat(PageObjectBase.rgbaToHexColor(InviteUsersForm.getOneSpecialCharacterPasswordPolicy()))
                    .as("Color is black").isEqualTo(InviteUsersForm.blackColor);
            softAssertions.assertThat(PageObjectBase.rgbaToHexColor(InviteUsersForm.getDoesNotContainNameEmailPasswordPolicy()))
                    .as("Color is black").isEqualTo(InviteUsersForm.blackColor);
            softAssertions.assertAll();
        });

        When("User enters a password that meets all the conditions", () -> {
            CreateUserPage.getPassword().sendKeys(userData.password);
            PageObjectBase.waitForLoad(2);
        });

        Then("User should see that all the conditions in the list have turned green", () -> {
            softAssertions.assertThat(PageObjectBase.rgbaToHexColor(InviteUsersForm.getLeast8ChartersPasswordPolicy()))
                    .as("Color is green").isEqualTo(InviteUsersForm.greenColor);
            softAssertions.assertThat(PageObjectBase.rgbaToHexColor(InviteUsersForm.getLowerUpperCasePasswordPolicy()))
                    .as("Color is green").isEqualTo(InviteUsersForm.greenColor);
            softAssertions.assertThat(PageObjectBase.rgbaToHexColor(InviteUsersForm.getLeastOneNumberPasswordPolicy()))
                    .as("Color is green").isEqualTo(InviteUsersForm.greenColor);
            softAssertions.assertThat(PageObjectBase.rgbaToHexColor(InviteUsersForm.getOneSpecialCharacterPasswordPolicy()))
                    .as("Color is green").isEqualTo(InviteUsersForm.greenColor);
            softAssertions.assertThat(PageObjectBase.rgbaToHexColor(InviteUsersForm.getDoesNotContainNameEmailPasswordPolicy()))
                    .as("Color is green").isEqualTo(InviteUsersForm.greenColor);
            softAssertions.assertAll();
        });

    }


    @After("@disabledUser")
    public void deleteDisabledUser() {
        if (disabledUser != null) MSGraphAPI.deleteADUser(MSGraphAPI.getADToken(), disabledUser);
    }
}
