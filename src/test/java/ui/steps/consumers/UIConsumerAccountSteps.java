package ui.steps.consumers;

import common.helpers.rest.HelperRest;
import common.helpers.ui.PageObjectBase;
import data_containers.GeneralData;
import data_containers.UserData;
import graphql.helpers.users.consumers.ConsumerUserVehiclesHelpers;
import io.cucumber.java8.En;
import org.assertj.core.api.SoftAssertions;
import org.openqa.selenium.Keys;
import ui.page_object.consumers.ConsumerAccountPage;
import ui.page_object.consumers.ConsumersPage;

public class UIConsumerAccountSteps implements En {
    GeneralData generalData;
    UserData userData;
    SoftAssertions softAssertions;

    public UIConsumerAccountSteps(GeneralData generalData, UserData userData) {
        this.generalData = generalData;
        this.userData = userData;
        softAssertions = new SoftAssertions();

        Given("user open consumer profile pop-up", () -> {
            PageObjectBase.waitForLoad(3);
            Thread.sleep(15000);
            ConsumersPage.getThreeDotMenu().click();
            ConsumersPage.getEditConsumer().click();
        });

        When("user change consumer first and last name", () -> {
            userData.updatedFirstName = "UpdatedName"+ HelperRest.getRandomNumber();
            userData.updatedLastName = "UpdatedLastName"+HelperRest.getRandomNumber();
            PageObjectBase.waitForLoad(3);
            ConsumerAccountPage.getFirstNameInput().clear();
            ConsumerAccountPage.getFirstNameInput().sendKeys(userData.updatedFirstName);
            ConsumerAccountPage.getLastNameInput().clear();
            ConsumerAccountPage.getLastNameInput().sendKeys(userData.updatedLastName);
            ConsumerAccountPage.getSaveNameChangesButton().click();
        });

        Then("user should see successfull toast message and on consumers page changed first and last name", () -> {
            softAssertions.assertThat(ConsumerAccountPage.getUserInformationSuccessfullUpdatedToastMessage().isDisplayed())
                    .as("User information successfully changed toast message is displayed").isTrue();
            ConsumerAccountPage.getDivModal().click();
            PageObjectBase.waitForLoad(1);
            softAssertions.assertThat(ConsumersPage.getConsumerFullName(userData.updatedFirstName, userData.updatedLastName).isDisplayed())
                    .as("Updated consumer first and last name are visible").isTrue();
            softAssertions.assertAll();
        });

        Then("user should see consumer's profile", () -> {
            PageObjectBase.waitForLoad(5);
            softAssertions.assertThat(ConsumerAccountPage.getProfile().isDisplayed());

        });
        When("user updates email and phone number", () -> {
            userData.updatedEmail = "UpdatedEmail" + HelperRest.getRandomUatoMail();
            userData.updatedPhoneNumber = "1456" + HelperRest.getRandomNumber();
            PageObjectBase.waitForLoad(6);
            ConsumerAccountPage.getEmailInput().sendKeys(Keys.COMMAND, "a", Keys.DELETE);
            ConsumerAccountPage.getEmailInput().sendKeys(userData.updatedEmail);
            ConsumerAccountPage.getPhoneNumberInput().sendKeys(Keys.COMMAND, "a", Keys.DELETE);
            ConsumerAccountPage.getPhoneNumberInput().sendKeys(userData.updatedPhoneNumber);
            ConsumerAccountPage.getSaveNameChangesButton().click();
        });

        Then("user should see successful toast message and updated email and phone number", () -> {
            softAssertions.assertThat(ConsumerAccountPage.getUserInformationSuccessfullUpdatedToastMessage().isDisplayed())
                    .as("User information successfully changed toast message is displayed").isTrue();
        });

        When("user goes to Payment methods submodule", () -> {
            PageObjectBase.waitForLoad(3);
            ConsumerAccountPage.getPaymentMethods().click();
            Thread.sleep(2000);
        });

        Then("user should see the list of all active payment methods", () -> {
            softAssertions.assertThat(ConsumerAccountPage.getPaymentMethodsSubModule().isDisplayed());
        });

        And("mutation CreateVehicleForUser is sent", () -> {
            generalData.response = ConsumerUserVehiclesHelpers.createVehicleForUser();
        });

        When("user goes to Vehicles submodule", () -> {
            PageObjectBase.waitForLoad(5);
            ConsumerAccountPage.getVehicles().click();
            Thread.sleep(2000);
        });

        Then("user should see the list of all consumer's vehicles", () -> {
            softAssertions.assertThat(ConsumerAccountPage.getVehiclesSubModule().isDisplayed());
        });

        And("user clicks add vehicle button", () -> {
            PageObjectBase.waitForLoad(3);
            ConsumerAccountPage.getAddVehicleButton().click();
        });

        And("fills out required license plate and province fields and clicks add vehicle", () -> {
            PageObjectBase.waitForLoad(3);
            ConsumerAccountPage.getLicensePlateInput().sendKeys("TEST");
            ConsumerAccountPage.getVehicleDropdown().click();
            ConsumerAccountPage.getFirstState().click();
            ConsumerAccountPage.getSaveVehicleButton().click();
        });
        Then("user should see successful toast message", () -> {
            softAssertions.assertThat(ConsumerAccountPage.getVehicleAddedSuccessfulToastMessage().isDisplayed())
                    .as("New vehicle added!").isTrue();
        });

        When("user deletes an existing vehicle", () -> {
            PageObjectBase.waitForLoad(3);
            ConsumerAccountPage.getVehicleThreeDotsMenu().click();
            ConsumerAccountPage.getRemoveVehicle().click();
        });

        Then("successful toast message is displayed", () -> {
            softAssertions.assertThat(ConsumerAccountPage.getVehicleRemovedSuccessfulMessage().isDisplayed())
                    .as("The selected vehicle has been removed.").isTrue();
        });

        When("user goes to Purchases submodule", () -> {
            PageObjectBase.waitForLoad(5);
            ConsumerAccountPage.getPurchases().click();
        });

        Then("user should see Purchases submodule", () -> {
            softAssertions.assertThat(ConsumerAccountPage.getPurchasesSubModule().isDisplayed());
        });

        When("user updates email and phone with invalid inputs", () -> {
            userData.invalidEmail = "aysa#";
            userData.invalidPhoneNumber = "5028228574";
            PageObjectBase.waitForLoad(5);
            ConsumerAccountPage.getEmailInput().sendKeys(Keys.COMMAND, "a", Keys.DELETE);
            ConsumerAccountPage.getEmailInput().sendKeys(userData.invalidEmail);
            ConsumerAccountPage.getPhoneNumberInput().sendKeys(Keys.COMMAND, "a", Keys.DELETE);
            ConsumerAccountPage.getPhoneNumberInput().sendKeys(userData.invalidPhoneNumber);
        });

        Then("user should see error messages", () -> {
            softAssertions.assertThat(ConsumerAccountPage.getInvalidEmailMessage().isDisplayed());
            softAssertions.assertThat(ConsumerAccountPage.getInvalidPhoneNumberMessage().isDisplayed());
            softAssertions.assertAll();
        });

        And("fills out required license plate and province fields", () -> {
            PageObjectBase.waitForLoad(3);
            ConsumerAccountPage.getLicensePlateInput().sendKeys("TEST");
        });

        Then("user should see error message that license plate is already exists", () -> {
            softAssertions.assertThat(ConsumerAccountPage.getInvalidLicensePlateMessage().isDisplayed());
        });

    }
}
