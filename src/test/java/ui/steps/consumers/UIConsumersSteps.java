package ui.steps.consumers;

import common.helpers.rest.HelperRest;
import common.helpers.ui.PageObjectBase;
import data_containers.GeneralData;
import data_containers.UserData;
import io.cucumber.java8.En;
import org.openqa.selenium.Keys;
import ui.page_object.IdentityAccessModulePage;
import ui.page_object.SideBar;
import ui.page_object.consumers.ConsumerAccountPage;
import ui.page_object.consumers.ConsumersPage;
import org.assertj.core.api.SoftAssertions;

import java.util.Set;

public class UIConsumersSteps implements En {
    GeneralData generalData;
    UserData userData;
    SoftAssertions softAssertions;

    public UIConsumersSteps(GeneralData generalData, UserData userData){
        this.generalData = generalData;
        this.userData = userData;
        softAssertions = new SoftAssertions();


        Given("user go to consumers page", () -> {
            PageObjectBase.waitForLoad(3);
            Thread.sleep(5000);
            SideBar.getSideBarIdentityAccess().click();
            IdentityAccessModulePage.getViewConsumersPage().click();
        });

        Given("user select option for Canada region", () -> {
            PageObjectBase.waitForLoad(3);
            ConsumersPage.getRegionDropdown().click();
            ConsumersPage.getRegionCanada().click();
        });

        When("user enter consumer mobile phone in search field", () -> {
            String phoneWithoutAreaCode = (userData.phone).substring(2);
            PageObjectBase.waitForLoad(3);
            ConsumersPage.getSearchConsumer().clear();
            ConsumersPage.getSearchConsumer().sendKeys(phoneWithoutAreaCode + Keys.ENTER);
        });

        Then("User should see that consumer with this mobile phone exists in {string} status", (String consumerStatus) -> {
            softAssertions.assertThat(ConsumersPage.getConsumerPhoneNumber(userData.phone).isDisplayed())
                    .as("Consumer phone number is displayed").isTrue();
            softAssertions.assertThat(ConsumersPage.getConsumerStatus(consumerStatus).isDisplayed())
                    .as("Consumer status is active").isTrue();
            softAssertions.assertAll();
        });

        Given("user select search via email option", () -> {
            PageObjectBase.waitForLoad(3);
            Thread.sleep(5000);
            ConsumersPage.getSearchViaDropdown().click();
            ConsumersPage.getSearchViaEmail().click();
        });

        When("user enter consumer email in search field", () -> {
            PageObjectBase.waitForLoad(3);
            ConsumersPage.getSearchConsumer().clear();
            ConsumersPage.getSearchConsumer().sendKeys(userData.email + Keys.ENTER);
        });

        Then("User should see that consumer with this email exists in {string} status", (String consumerStatus) -> {
            softAssertions.assertThat(ConsumersPage.getConsumerEmail(userData.email).isDisplayed())
                    .as("Consumer phone number is displayed").isTrue();
            softAssertions.assertThat(ConsumersPage.getConsumerStatus(consumerStatus).isDisplayed())
                    .as("Consumer status is active").isTrue();
            softAssertions.assertAll();
        });

        When("user enters consumer mobile phone in search field", () -> {
            PageObjectBase.waitForLoad(3);
            ConsumersPage.getSearchConsumer().clear();
            ConsumersPage.getSearchConsumer().sendKeys(userData.phone + Keys.ENTER);
        });

        Then("User should see that consumer with this mobile phone exists", () -> {
            PageObjectBase.waitForLoad(5);
            softAssertions.assertThat(ConsumersPage.getConsumerPhoneNumber(userData.phone).isDisplayed())
                    .as("Consumer phone number is displayed").isTrue();
        });

        Then("User should see that consumer with this email exists", () -> {
            PageObjectBase.waitForLoad(5);
            softAssertions.assertThat(ConsumersPage.getConsumerEmail(userData.email).isDisplayed())
                    .as("Consumer email is displayed").isTrue();
        });

        When("user clicks on FAQ link", () -> {
            String currentWindowHandle = PageObjectBase.driver.getWindowHandle();
            System.out.println("currentWindowHandle = " + currentWindowHandle);
            ConsumersPage.getFaqPage().click();

            Set<String> windowHandles = PageObjectBase.driver.getWindowHandles();
            for (String each: windowHandles) {
                PageObjectBase.driver.switchTo().window(each);
                if(PageObjectBase.driver.getTitle().equals("Consumers - FAQ")){
                    softAssertions.assertThat(PageObjectBase.driver.getTitle().equals("Consumers - FAQ"));
                    break;
                }
            }
        });

        Then("user should be able to see FAQ questions", () -> {
            PageObjectBase.waitForLoad(5);
            softAssertions.assertThat(ConsumersPage.getConsumersFaqPage().isDisplayed());
            softAssertions.assertThat(ConsumersPage.getFirstQuestion().isDisplayed());
            softAssertions.assertThat(ConsumersPage.getSecondQuestion().isDisplayed());
            softAssertions.assertThat(ConsumersPage.getThirdQuestion().isDisplayed());
            softAssertions.assertThat(ConsumersPage.getForthQuestion().isDisplayed());
            softAssertions.assertThat(ConsumersPage.getFifthQuestion().isDisplayed());
            softAssertions.assertAll();
        });


    }

}
