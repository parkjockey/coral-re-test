package ui.steps.organizations;

import common.helpers.db_helpers.JdbcHelpers;
import common.helpers.rest.HelperRest;
import common.helpers.ui.PageObjectBase;
import common.helpers.ui.PageUrls;
import data_containers.GeneralData;
import data_containers.OrganizationData;
import graphql.helpers.GraphQLHelpers;
import io.cucumber.java8.En;
import org.assertj.core.api.SoftAssertions;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import ui.page_object.HeaderBar;
import ui.page_object.HomePage;
import ui.page_object.SideBar;
import ui.page_object.organizations.*;
import ui.page_object.roles.RolesPage;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static common.helpers.ui.BaseDriver.driver;
import static org.awaitility.Awaitility.await;

public class OrganizationSteps implements En {
    GeneralData generalData;
    OrganizationData organizationData;
    String notExistingOrganization;
    String existingOrganization1;
    String existingOrganization2;
    private SoftAssertions softAssertions;

    public OrganizationSteps(GeneralData generalData, OrganizationData organizationData) {
        this.generalData = generalData;
        this.organizationData = organizationData;
        softAssertions = new SoftAssertions();
        organizationData.name = "OrgAuto" + HelperRest.getRandomNumber();
        organizationData.description = "Description" + HelperRest.getRandomNumber();

        Given("user can reach Organizations page", () -> {
            PageObjectBase.waitForLoad(2);
            PageObjectBase.driver.get(generalData.reefWebApp + PageUrls.Pages.ORGANIZATIONS.path);
            await("Wait condition to be true")
                    .atMost(10, TimeUnit.SECONDS).until(() ->
                    PageObjectBase.driver.getCurrentUrl().equals(generalData.reefWebApp+ PageUrls.Pages.ORGANIZATIONS.path));
            PageObjectBase.waitForLoad(20);
            softAssertions.assertThat(OrganizationsPage.getOrganizationsTitle().isDisplayed())
                    .as("Organization title is displayed").isTrue();
            softAssertions.assertThat(OrganizationsPage.getCreateOrgButton().isDisplayed())
                    .as("Create organization button is displayed").isTrue();
            softAssertions.assertAll();
        });

        Given("user can open Organization Creation form", () -> {
            PageObjectBase.waitForLoad(2);
            OrganizationsPage.getCreateOrgButton().click();

            softAssertions.assertThat(CreateOrganizationForm.getCreateNewOrganizationTitle().isDisplayed())
                    .as("Create organization title is displayed").isTrue();
            //softAssertions.assertThat(CreateOrganizationForm.getCreateButton().isEnabled()).isFalse();
            softAssertions.assertAll();
        });

        When("user insert all details on Organization Creation form and click on Create button", () -> {
            CreateOrganizationForm.getOrganizationName().sendKeys(organizationData.name);
            CreateOrganizationForm.getDescription().click();
            softAssertions.assertThat(CreateOrganizationForm.getOrgNameAvailable().isDisplayed()).isTrue();
            softAssertions.assertAll();
            CreateOrganizationForm.getDescription().sendKeys(organizationData.description);
            PageObjectBase.waitForLoad(5);
            PageObjectBase.performClick(CreateOrganizationForm.getCreateButton());
            PageObjectBase.waitForLoad(1);

        });

        Then("new organization can be found", () -> {
            String queryFindOrg = "select * from organization where name = '" + organizationData.name + "';";
            String orgNameDb = JdbcHelpers.getStringValueFromDb(queryFindOrg, "name");

            softAssertions.assertThat(OrganizationsPage.getOrganizationsTitle().isDisplayed())
                    .as("Organization title is displayed").isTrue();
            softAssertions.assertThat(orgNameDb).as("Organization name is stored in DB").isEqualTo(organizationData.name);
            softAssertions.assertAll();
        });

        When("user insert 51 character long organization name", () -> {
            CreateOrganizationForm.getOrganizationName().sendKeys(HelperRest.getAlphaNumericString(51));

        });


        Then("user should see a message that he cannot enter over 50 characters for org name", () -> {
            PageObjectBase.waitForLoad(3);
            softAssertions.assertThat(CreateOrganizationForm.getOrgNameCharLengthValidationMessage().isDisplayed()).isTrue();
            softAssertions.assertAll();
        });

        When("user insert 201 character long organization description", () -> {
            CreateOrganizationForm.getDescription().sendKeys(HelperRest.getAlphaNumericString(201));

        });

        Then("user should see a message that he cannot enter over 200 characters for org description", () -> {
            PageObjectBase.waitForLoad(3);
            softAssertions.assertThat(CreateOrganizationForm.getOrgDescCharLengthValidationMessage().isDisplayed()).isTrue();
            softAssertions.assertAll();
        });

        Given("organization already exist", () -> {
            PageObjectBase.waitForLoad(2);
            PageObjectBase.driver.get(generalData.reefWebApp + PageUrls.Pages.ORGANIZATIONS.path);
            await("Wait condition to be true")
                    .atMost(5, TimeUnit.SECONDS).until(() ->
                    PageObjectBase.driver.getCurrentUrl().equals(generalData.reefWebApp+ PageUrls.Pages.ORGANIZATIONS.path));

            PageObjectBase.waitForLoad(5);
            PageObjectBase.performClick( OrganizationsPage.getCreateOrgButton());
            PageObjectBase.waitForLoad(2);
            CreateOrganizationForm.getOrganizationName().sendKeys(organizationData.name);
            CreateOrganizationForm.getDescription().click();
            softAssertions.assertThat(CreateOrganizationForm.getOrgNameAvailable().isDisplayed()).isTrue();
            softAssertions.assertAll();
            CreateOrganizationForm.getDescription().sendKeys(organizationData.description);
            CreateOrganizationForm.getCreateButton().click();
            PageObjectBase.waitForLoad(1);
            PageObjectBase.performClick(OrganizationsPage.getSuccessTostify());
        });

        When("user try to create organization with a name that already exists", () -> {
            CreateOrganizationForm.getOrganizationName().sendKeys(organizationData.name);
        });

        Then("the user should see a message that the name already exists", () -> {
            PageObjectBase.waitForLoad(3);
            softAssertions.assertThat(CreateOrganizationForm.getOrgNameIsAlreadyTaken().isDisplayed()).isTrue();
            softAssertions.assertAll();
        });

        When("user insert special character long organization name", () -> {
            CreateOrganizationForm.getOrganizationName().sendKeys("A%$#!A;-");
        });

        Then("user should see a message that special character not allowed organization name", () -> {
            PageObjectBase.waitForLoad(3);
            softAssertions.assertThat(CreateOrganizationForm.getCreateOrgSpecCharValidationMessage().isDisplayed()).isTrue();
            softAssertions.assertAll();
        });

        When("user scroll down on organizations page", () -> {

            PageObjectBase.waitForLoad(3);
            PageObjectBase.performScrollDown(OrganizationsPage.getOrgListDiv(), 1000, 4);

        });

        Then("user should see loader", () -> {
            softAssertions.assertThat(OrganizationsPage.getOrgPageLoader().isDisplayed()).isTrue();
            softAssertions.assertAll();
        });

        Then("user should see that organizations exist on the page", () -> {
            PageObjectBase.waitForLoad(3);

            softAssertions.assertThat(OrganizationsPage.getOrgTableBody().isDisplayed())
                    .as("Organizations table body is displayed").isTrue();
            softAssertions.assertThat(OrganizationsPage.getOrganizationsTitle().isDisplayed())
                    .as("Organization title is displayed").isTrue();
            softAssertions.assertThat(OrganizationsPage.getOrgAssigModulHeader().isDisplayed())
                    .as("Table assign modul header displayed").isTrue();
            softAssertions.assertThat(OrganizationsPage.getCreateOrgButton().isDisplayed())
                    .as("Create organization button is displayed").isTrue();
            softAssertions.assertAll();
        });

        When("user go to next organization page", () -> {
            OrganizationsPage.getOrgPaginationToNextPage().click();
            PageObjectBase.waitToLoaderDisappear();

        });

        When("the user selects forty rows per page on organizations page", () -> {
            OrganizationsPage.getOrgRowsPerPageDropDown().click();
            OrganizationsPage.getOrg40RowsPerPage().click();

        });

        Then("user should see on page forty organizations", () -> {
            PageObjectBase.waitForLoad(3);
            List<WebElement> listOfOrganizations = driver.findElements(By.xpath("//table/tbody/tr"));
            int lst = listOfOrganizations.size();
            PageObjectBase.waitForLoad(3);
            softAssertions.assertThat(lst == 40);
            softAssertions.assertAll();
        });

        When("enter organization name in search field on organizations page", () -> {
            OrganizationsPage.getOrgSearchField().sendKeys(organizationData.name);

        });

        Then("user should see that this organization exists in search results", () -> {
            PageObjectBase.waitForLoad(3);
            List<WebElement> listOfOrganizationsName = OrganizationsPage.getOrgOrganizationNameLabel();
            int OrganizationName = listOfOrganizationsName.size();
            PageObjectBase.waitForLoad(3);
            softAssertions.assertThat(OrganizationName == 1);
            softAssertions.assertThat(listOfOrganizationsName.get(0).getText())
                    .as("Organization name is correct").isEqualTo(organizationData.name);
            softAssertions.assertAll();
        });

        Given("organization doesn't exist in the Reef system", () -> {
            notExistingOrganization = "notExistingOrganization AutoTest";
        });

        When("user enter not existing organization name in the search field on the organizations page", () -> {
            OrganizationsPage.getOrgSearchField().sendKeys(notExistingOrganization);

        });

        Then("the user should see no organization found message", () -> {
            PageObjectBase.waitToLoaderDisappear();
            softAssertions.assertThat(OrganizationsPage.getOrgNotFoundMessage().isDisplayed())
                    .as("Organization not found message is displayed").isTrue();
            softAssertions.assertThat(OrganizationsPage.getCreateOrgButton().isDisplayed())
                    .as("Create button is displayed").isTrue();
            softAssertions.assertThat(OrganizationsPage.getOrganizationsTitle().isDisplayed())
                    .as("Organization title is displayed").isTrue();
            softAssertions.assertAll();
        });

        Given("organization is already exists", () -> {
            ArrayList<String> orgNames = GraphQLHelpers.createOrganizations(1);
            existingOrganization1 = orgNames.get(0);

        });

        Given("user can edit created organization", () -> {
            PageObjectBase.waitForLoad(2);
            OrganizationsPage.getOrgSearchField().sendKeys(existingOrganization1 + Keys.ENTER);
            PageObjectBase.waitForLoad(3);
            OrganizationsPage.getOrgMoreOptions().click();
            OrganizationsPage.getRenameOrg().click();
        });

        When("user update created organization", () -> {
            EditOrganizationForm.getEditOrgNameField().clear();
            EditOrganizationForm.getEditOrgNameField().sendKeys(organizationData.name);
            PageObjectBase.waitForLoad(2);
            EditOrganizationForm.getOrgEditButton().click();
        });

        Then("updated organization can be found via Search field on Organizations page", () -> {
            PageObjectBase.waitForLoad(5);
            OrganizationsPage.getOrgSearchField().sendKeys(Keys.chord(Keys.CONTROL,"a", Keys.DELETE));
            OrganizationsPage.getOrgSearchField().sendKeys(organizationData.name + Keys.ENTER);
            List<WebElement> listOfOrganizationsName = OrganizationsPage.getOrgOrganizationNameLabel();
            int OrganizationName = listOfOrganizationsName.size();
            PageObjectBase.waitForLoad(3);
            softAssertions.assertThat(OrganizationName == 1);
            softAssertions.assertThat(listOfOrganizationsName.get(0).getText())
                    .as("Organization name is correct").isEqualTo(organizationData.name);
            softAssertions.assertAll();

        });

        Given("two organizations already exists", () -> {
            ArrayList<String> orgNames = GraphQLHelpers.createOrganizations(2);
            existingOrganization1 = orgNames.get(0);
            existingOrganization2 = orgNames.get(1);
        });

        When("user tries to update the name of the first organization with the same name as the second organization", () -> {
            EditOrganizationForm.getEditOrgNameField().clear();
            EditOrganizationForm.getEditOrgNameField().sendKeys(existingOrganization2);
        });

        Then("user should see message that this organization name already used", () -> {
            PageObjectBase.waitForLoad(2);
            softAssertions.assertThat(EditOrganizationForm.getOrgNameAlreadyTaken().isDisplayed())
                    .as("Organization name already taken message is displayed").isTrue();
            softAssertions.assertThat(EditOrganizationForm.getOrgEditButton().isEnabled())
                    .as("Organization edit button is disabled").isFalse();
            softAssertions.assertAll();
        });

        When("user update organization description and save changes", () -> {
            EditOrganizationForm.getEditOrgDescriptionField().clear();
            EditOrganizationForm.getEditOrgDescriptionField().sendKeys(organizationData.description);
            EditOrganizationForm.getOrgEditButton().click();
        });

        Then("user should see new description on this organization", () -> {
            PageObjectBase.waitForLoad(2);
            OrganizationsPage.getOrgMoreOptions().click();
            OrganizationsPage.getRenameOrg().click();

            softAssertions.assertThat(EditOrganizationForm.getSpecificOrgDescciption(organizationData.description).isDisplayed())
                    .as("Organization description is updated").isTrue();
            softAssertions.assertAll();
        });

        Given("user can assign modules to organization", () -> {
            PageObjectBase.waitForLoad(2);
            OrganizationsPage.getOrgSearchField().sendKeys(existingOrganization1 + Keys.ENTER);
            PageObjectBase.waitForLoad(3);
            OrganizationsPage.getManageOrg().click();
            OrganizationsPage.getManageModulesOrg().click();
        });

        When("user assign {string} modules to organization", (String moduleName) -> {
            PageObjectBase.waitForLoad(2);
            AssignModulesToOrganizationsForm.getAssignSpecificModuleToOrganization(moduleName).click();
        });

        When("user click on Assign Modules button and open expand modules", () -> {
            PageObjectBase.waitForLoad(2);
            AssignModulesToOrganizationsForm.getAssignModulesToOrgButton().click();
            PageObjectBase.waitForLoad(2);
            OrganizationsPage.getExpandAssignedModulesToOrg().click();
        });

        Then("user should in expanding label that {string} modules is assigned on organization", (String moduleName) -> {
            softAssertions.assertThat(OrganizationsPage.getAssignedModulesToOrganizations(moduleName).isDisplayed())
                    .as("Modules successfully assigned ").isTrue();
            softAssertions.assertAll();
        });

        Given("organization with assigned modules already exist", () -> {
            PageObjectBase.waitForLoad(2);
            PageObjectBase.driver.get(generalData.reefWebApp + PageUrls.Pages.ORGANIZATIONS.path);
            await("Wait condition to be true")
                    .atMost(5, TimeUnit.SECONDS).until(() ->
                    PageObjectBase.driver.getCurrentUrl().equals(generalData.reefWebApp+ PageUrls.Pages.ORGANIZATIONS.path));

            PageObjectBase.waitForLoad(5);
            PageObjectBase.performClick( OrganizationsPage.getCreateOrgButton());
            PageObjectBase.waitForLoad(2);
            CreateOrganizationForm.getOrganizationName().sendKeys(organizationData.name);
            CreateOrganizationForm.getDescription().click();
            softAssertions.assertThat(CreateOrganizationForm.getOrgNameAvailable().isDisplayed()).isTrue();
            softAssertions.assertAll();
            CreateOrganizationForm.getDescription().sendKeys(organizationData.description);
            CreateOrganizationForm.getCreateButton().click();
            PageObjectBase.waitForLoad(1);
            PageObjectBase.performClick(OrganizationsPage.getSuccessTostify());
            PageObjectBase.waitForLoad(2);
            OrganizationsPage.getOrgSearchField().sendKeys(organizationData.name + Keys.ENTER);
            PageObjectBase.waitForLoad(3);
            OrganizationsPage.getManageOrg().click();
            OrganizationsPage.getManageModulesOrg().click();
            PageObjectBase.waitForLoad(2);
            AssignModulesToOrganizationsForm.getAssignSpecificModuleToOrganization("Identity Access").click();
            PageObjectBase.waitForLoad(2);
            AssignModulesToOrganizationsForm.getAssignModulesToOrgButton().click();
        });

        When("user switch the organization", () -> {
            PageObjectBase.waitForLoad(2);
            HeaderBar.getAvatar().click();
            PageObjectBase.waitToToastifyDisappear();
            HeaderBar.getSwitchOrganization().click();
            SwitchOrganizationForm.getSwitchOrgSearchField().sendKeys(organizationData.name + Keys.ENTER);
           PageObjectBase.waitForLoad(2);
           PageObjectBase.performClick(SwitchOrganizationForm.getSwitchOrgOrganization());
            SwitchOrganizationForm.getSwitchOrgAccessOrganizationButton().click();
            softAssertions.assertAll();
            PageObjectBase.waitForLoad(3);
        });

        Then("user should see organization name in header bar and modules assigned on home page", () -> {
            softAssertions.assertThat(HeaderBar.getHeaderOrgName(organizationData.name).isDisplayed())
                    .as("Organization name is displayed in header").isTrue();
            PageObjectBase.driver.get(generalData.reefWebApp + PageUrls.Pages.HOME.path);
            softAssertions.assertThat(HomePage.getUsers().isDisplayed())
                    .as("Users page is displayed").isTrue();
            softAssertions.assertThat(HomePage.getRoles().isDisplayed())
                    .as("Roles page is displayed").isTrue();
            softAssertions.assertAll();
        });

        Then("user should see on roles page admin role is created", () -> {
            PageObjectBase.waitForLoad(3);
            SideBar.getSideBarIdentityAccess().click();
            PageObjectBase.waitForLoad(2);
            HomePage.getRolesViewButton().click();
            softAssertions.assertThat(RolesPage.getAdminRolesIsCreated().isDisplayed())
                    .as("Admin role is displayed").isTrue();
            softAssertions.assertAll();
        });

        When("user find the {string} organization using the search field", (String orgName) -> {
            PageObjectBase.waitForLoad(2);
            OrganizationsPage.getOrgSearchField().sendKeys(orgName + Keys.ENTER);
        });

        Then("user should see Reef Enterprise logo", () -> {
            PageObjectBase.waitForLoad(1);
            softAssertions.assertThat(OrganizationsPage.getOrgLogoInOrganizationsList().isDisplayed())
                    .as("Organization logo is displayed").isTrue();
            softAssertions.assertAll();
        });

        Then("user should see the Reef Enterprise logo in the header bar", () -> {
            PageObjectBase.waitForLoad(1);
            softAssertions.assertThat(HeaderBar.getReefOrganizationLogo().isDisplayed())
                    .as("Organization header logo is displayed").isTrue();
            softAssertions.assertAll();
        });

    }
}
