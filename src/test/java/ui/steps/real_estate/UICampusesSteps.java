package ui.steps.real_estate;


import common.helpers.rest.HelperRest;
import common.helpers.ui.PageObjectBase;
import data_containers.GeneralData;
import data_containers.UserData;
import io.cucumber.java8.En;
import org.assertj.core.api.SoftAssertions;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import ui.page_object.LoginPage;
import ui.page_object.SideBar;
import ui.page_object.realestate.CampusesPage;
import ui.page_object.realestate.RealEstateModulePage;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.assertj.core.api.SoftAssertions;
import ui.page_object.users.InviteUsersForm;

import static common.helpers.ui.BaseDriver.driver;


public class UICampusesSteps implements En {

    GeneralData generalData;
    SoftAssertions softAssertions = new SoftAssertions();
    private String campusName;
    String campusDescription;

    public UICampusesSteps(GeneralData generalData) {
        this.generalData = generalData;

        Given("Real Estate admin user is logged in", () -> {
            LoginPage.userLogin(generalData.adNoSSOUsername);
            LoginPage.getUserNoSSOPasswordField().sendKeys(generalData.adPassword);
            LoginPage.getLogInToYourAccountNoSSOUserButton().click();
            PageObjectBase.waitToLoaderDisappear();

            WebDriverWait wait = new WebDriverWait(driver, 6);
            wait.until(ExpectedConditions.elementToBeClickable(SideBar.getSideBarRealEstate));
            SideBar.getSideBarRealEstate().click();
        });

        When("user navigates to the Campuses Page", () -> {
            RealEstateModulePage.getCampusesPage().click();
        });

        Then("user views the Campus cards", () -> {
            List<WebElement> listOfCampuses = CampusesPage.getCampusCards();
            softAssertions.assertThat(listOfCampuses.size() == 10);
            System.out.println("listOfCampuses = " + listOfCampuses.toString());
        });

        Then("user should view the Campus page elements on page", () -> {
            softAssertions.assertThat(CampusesPage.getCampusPageTitle().isDisplayed()).as("Campuses").isTrue();
            softAssertions.assertThat(CampusesPage.getAddNewCampusButton().isEnabled());
            softAssertions.assertThat(CampusesPage.getCampusSearchField().isEnabled());
            softAssertions.assertAll();
        });

        When("user clicks on the ADD NEW CAMPUS button", () -> {
            PageObjectBase.waitToLoaderDisappear();
            CampusesPage.getAddNewCampusButton().click();
        });

        Then("user should be able to view create campus modal", () -> {
            softAssertions.assertThat(CampusesPage.getCreateNewCampusModal().isDisplayed());
            softAssertions.assertAll();
        });

        Then("user be able to create a new campus", () -> {
            PageObjectBase.waitToLoaderDisappear();
            CampusesPage.getAddNewCampusButton().click();
            int generateNumber = HelperRest.getRandomNumber();
            campusName = "Northern Campus - " + generateNumber;
            campusDescription = "the campus located in ID - " + generateNumber;
            CampusesPage.addNewCampus(campusName, campusDescription);
            System.out.println("Test - new campus is created: " + campusName);
        });

        When("user selects the pagination of 25 Campuses rows per page", () -> {
            //checks if the page is 10 campuses default
            PageObjectBase.performScrollDown(CampusesPage.getCampusPerPagePaginationField(), 1000, 1);
            softAssertions.assertThat(CampusesPage.getCampusPerPagePaginationField().getText().equals(10));
            softAssertions.assertAll();

            CampusesPage.getCampusPerPagePaginationField().click();
            CampusesPage.getCampusPerPagePaginationList().get(1).click(); //index 1 == 25 campuses
        });

        Then("user views 25 Campus Cards rows per page", () -> {
            List<WebElement> listOfCampuses = CampusesPage.getCampusCards();
            softAssertions.assertThat(listOfCampuses.size() == 25);
            softAssertions.assertThat(CampusesPage.getCampusPerPagePaginationField().getText().equals("25"));
            softAssertions.assertAll();
        });

        When("user selects the pagination of 50 Campuses rows per page", () -> {
            PageObjectBase.performScrollDown(CampusesPage.getCampusPerPagePaginationField(), 1000, 1);
            softAssertions.assertThat(CampusesPage.getCampusPerPagePaginationField().getText().equals("10"));
            softAssertions.assertAll();

            CampusesPage.getCampusPerPagePaginationField().click();
            CampusesPage.getCampusPerPagePaginationList().get(2).click(); //index 2 == 50 campuses
        });

        Then("user views 50 Campus Cards rows per page", () -> {
            PageObjectBase.waitToLoaderDisappear();
            PageObjectBase.performScrollDown(CampusesPage.getCampusPerPagePaginationField(), 1000, 1);
            List<WebElement> listOfCampuses = CampusesPage.getCampusCards();
            softAssertions.assertThat(listOfCampuses.size() == 50);
            softAssertions.assertThat(CampusesPage.getCampusPerPagePaginationField().getText().equals("10"));
            softAssertions.assertAll();
        });

        When("user selects the pagination of 100 Campuses rows per page", () -> {
            PageObjectBase.performScrollDown(CampusesPage.getCampusPerPagePaginationField(), 1000, 1);
            softAssertions.assertThat(CampusesPage.getCampusPerPagePaginationField().getText().equals("10"));
            softAssertions.assertAll();
            CampusesPage.getCampusPerPagePaginationField().click();
            CampusesPage.getCampusPerPagePaginationList().get(3).click(); //index 3 == 100 campuses
        });

        Then("user views 100 Campus Cards rows per page", () -> {
            PageObjectBase.waitToLoaderDisappear();
            PageObjectBase.performScrollDown(CampusesPage.getCampusPerPagePaginationField(), 1000, 2);
            List<WebElement> listOfCampuses = CampusesPage.getCampusCards();
            softAssertions.assertThat(CampusesPage.getCampusPerPagePaginationField().getText().equals("100"));
            softAssertions.assertThat(listOfCampuses.size() == 50);
            softAssertions.assertAll();
        });

        When("user searches the already Created campus", () -> {
            if (campusName == null) campusName = "Northern Campus"; // configure the campusName to createCampus method above
            PageObjectBase.waitForLoad(2000);
            CampusesPage.getCampusSearchField().sendKeys(campusName);
            PageObjectBase.waitToLoaderDisappear();
            Thread.sleep(3000);
        });

        Then("user should be able to view the campus details are available", () -> {
            PageObjectBase.waitForLoad(2000);
            List<WebElement> campusCardsList = CampusesPage.getCampusCards();
            softAssertions.assertThat(campusCardsList.size()>0);

            for (WebElement each:campusCardsList) {
                System.out.println(each.getText());
                softAssertions.assertThat(each.getText().contains(campusName));
                System.out.println("-----");
            }

            softAssertions.assertAll();
        });

    }
}
