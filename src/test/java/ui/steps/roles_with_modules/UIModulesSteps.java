package ui.steps.roles_with_modules;

import common.helpers.ui.PageObjectBase;
import data_containers.GeneralData;
import data_containers.RoleData;
import io.cucumber.java8.En;
import org.assertj.core.api.SoftAssertions;
import org.openqa.selenium.Keys;
import ui.page_object.roles.AssignedModulesForm;
import ui.page_object.roles.RolesPage;

public class UIModulesSteps implements En {
    GeneralData generalData;
    RoleData roleData;
    SoftAssertions softAssertions;

    public UIModulesSteps(GeneralData generalData, RoleData roleData) {
        this.generalData = generalData;
        this.roleData = roleData;
        softAssertions = new SoftAssertions();

        Given("user can find created role", () -> {
            PageObjectBase.waitForLoad(3);
            RolesPage.getSearchRoles().clear();
            RolesPage.getSearchRoles().sendKeys(roleData.roleName + Keys.ENTER);
            softAssertions.assertThat(RolesPage.getSpecificRole(roleData.roleName).isDisplayed())
                    .as("Searched role is displayed").isTrue();
            softAssertions.assertAll();
        });

        Given("user can open role's mange module form", () -> {
            RolesPage.getAddModule().click();
            softAssertions.assertThat(AssignedModulesForm.getAssignModulesHeading(roleData.roleName).isDisplayed())
                    .as("Assigned Module form is displayed").isTrue();
            softAssertions.assertAll();
        });

        When("I select Module {string} with level on that module: {string}", (String moduleName, String permissionLevel) -> {
            PageObjectBase.performScrollDown(AssignedModulesForm.getModuleArrow("Test"), 1000, 3);
            AssignedModulesForm.chooseModulePermission(moduleName, permissionLevel).click();
            softAssertions.assertThat(AssignedModulesForm.chooseModulePermission(moduleName, permissionLevel).isSelected())
                    .as("Assigned permission level is selected").isTrue();
            softAssertions.assertAll();
        });

        Then("all child and grandchild modules are checked also with the same permission level {string}", (String permLevel) -> {
            PageObjectBase.performScrollDown(AssignedModulesForm.getModuleArrow("Test"), 1000, 3);
            PageObjectBase.performClick(AssignedModulesForm.getModuleArrow("Test"));
            PageObjectBase.performScrollDown(AssignedModulesForm.chooseModulePermission("Test child module", permLevel), 1000, 3);
            softAssertions.assertThat(AssignedModulesForm.chooseModulePermission("Test child module", permLevel).isSelected())
                    .as("Permission level inherited from parent").isTrue();
            softAssertions.assertThat(AssignedModulesForm.chooseModulePermission("Test grandchild module", permLevel).isSelected())
                    .as("Permission level inherited from parent").isTrue();
            softAssertions.assertAll();
        });

        Then("all child elements can't check {string} permission level", (String permLevel) -> {
            softAssertions.assertThat(!AssignedModulesForm.chooseModulePermission("Test child module", permLevel).isEnabled())
                    .as("Permission level can't be greater then parent").isTrue();
            softAssertions.assertThat(!AssignedModulesForm.chooseModulePermission("Test grandchild module", permLevel).isEnabled())
                    .as("Permission level can't be greater then parent").isTrue();
            softAssertions.assertAll();
        });

        Then("number of assigned modules is {int}", (Integer numberOfAssignedModules) -> {
            AssignedModulesForm.getAssignButton().click();
            PageObjectBase.waitForLoad(3);

            softAssertions.assertThat(RolesPage.getSpecificRole(roleData.roleName).isDisplayed())
                    .as("Searched role is displayed").isTrue();

            softAssertions.assertThat(RolesPage.getNumberOfAssignedModules(numberOfAssignedModules).isDisplayed())
                    .as("Number of assigned modules is true").isTrue();
            softAssertions.assertAll();
        });

        Given("user selected (Module|SubModule) {string} with level on that module: {string}", (String moduleName, String permissionLevel) -> {
            AssignedModulesForm.chooseModulePermission(moduleName, permissionLevel).click();
            softAssertions.assertThat(AssignedModulesForm.chooseModulePermission(moduleName, permissionLevel).isSelected())
                    .as("Assigned permission level is selected").isTrue();
            softAssertions.assertAll();
            PageObjectBase.performScrollDown(AssignedModulesForm.getModuleArrow(moduleName), 1000, 3);
            PageObjectBase.performClick( AssignedModulesForm.getModuleArrow(moduleName));
        });

        When("user select SubModule {string} with level {string}", (String submodule, String permissionLevel) -> {
            PageObjectBase.waitForLoad(1);
            AssignedModulesForm.chooseModulePermission(submodule, permissionLevel).click();
            softAssertions.assertThat(AssignedModulesForm.chooseModulePermission(submodule, permissionLevel).isSelected())
                    .as("Assigned permission level is selected").isTrue();
            softAssertions.assertAll();
        });

        Then("parent Module {string} still has {string} level", (String module, String permLevel) -> {
            softAssertions.assertThat(AssignedModulesForm.chooseModulePermission(module, permLevel).isSelected())
                    .as("Assigned permission level is selected").isTrue();
            softAssertions.assertAll();
        });

        Then("child Module {string} is changed to {string}", (String child, String permLevel) -> {
            softAssertions.assertThat(AssignedModulesForm.chooseModulePermission(child, permLevel).isSelected())
                    .as("Assigned permission level is selected").isTrue();
            softAssertions.assertAll();
        });


        Then("in Assign Module Form for {string} module number of total modules is {int} and assigned is {int}",
                (String moduleName, Integer totalSubmodules, Integer assignedModules) -> {
                    RolesPage.getPenEditModule().click();

                    softAssertions.assertThat(AssignedModulesForm.getAssignModulesHeading(roleData.roleName).isDisplayed())
                            .as("Assigned Module form is displayed").isTrue();

                    PageObjectBase.performScrollDown(AssignedModulesForm.chooseModulePermission("Test", "VIEWER"), 1000, 3);
                    String modulesNumberAndSubmodules =
                            AssignedModulesForm.getModuleNumber(moduleName, totalSubmodules).getText().trim();
                    softAssertions.assertThat(modulesNumberAndSubmodules)
                            .as("Number of submodules and assigned modules is correct")
                            .isEqualTo(totalSubmodules + " Modules /\n" + assignedModules + " Assigned");
                    softAssertions.assertAll();
                });
    }
}
