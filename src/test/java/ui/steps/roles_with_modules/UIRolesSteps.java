package ui.steps.roles_with_modules;

import common.helpers.rest.HelperRest;
import common.helpers.ui.PageObjectBase;
import common.helpers.ui.PageUrls;
import data_containers.GeneralData;
import data_containers.RoleData;
import data_containers.UserData;
import io.cucumber.java8.En;
import org.assertj.core.api.SoftAssertions;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import ui.page_object.LoginPage;
import ui.page_object.MicrosoftLoginPage;
import ui.page_object.roles.*;
import ui.page_object.users.UsersPage;

import java.util.List;
import java.util.concurrent.TimeUnit;

import static org.awaitility.Awaitility.await;

public class UIRolesSteps implements En {
    private GeneralData generalData;
    private RoleData roleData;
    private UserData userData;
    private SoftAssertions softAssertions;
    private String newRole;


    public UIRolesSteps(GeneralData generalData, RoleData roleData, UserData userData) {
        this.generalData = generalData;
        this.roleData = roleData;
        this.userData = userData;
        softAssertions = new SoftAssertions();
        roleData.roleName = "AutoRole" + HelperRest.getRandomNumber();
        roleData.roleDescription = "Description" + HelperRest.getRandomNumber();
        newRole = "UpdatedRole" + HelperRest.getRandomNumber();

        Given("user is logged in", () -> {
            LoginPage.getUserEmailField().clear();
            LoginPage.getUserEmailField().sendKeys(userData.email);
            LoginPage.getContinueLoginButton().click();
            LoginPage.getUserNoSSOPasswordField().sendKeys(userData.password);
            LoginPage.getLogInToYourAccountNoSSOUserButton().click();
        });

        Given("admin user is logged in", () -> {

            LoginPage.getUserEmailField().clear();
            LoginPage.getUserEmailField().sendKeys(generalData.adSSOUsername);
            LoginPage.getContinueLoginButton().click();
              MicrosoftLoginPage.loginMicrosoftAccount(generalData.adSSOUsername, generalData.adPassword);


        });

        Given("user can reach Roles page", () -> {
            PageObjectBase.waitForLoad(2);
            PageObjectBase.driver.get(generalData.reefWebApp + PageUrls.Pages.ROLES.path);
            await("Wait condition to be true")
                    .atMost(10, TimeUnit.SECONDS).until(() ->
                    PageObjectBase.driver.getCurrentUrl().equals(generalData.reefWebApp+ PageUrls.Pages.ROLES.path));
            PageObjectBase.waitForLoad(10);
            Thread.sleep(10);
            softAssertions.assertThat(RolesPage.getRolesTitle().isDisplayed())
                    .as("Roles title is displayed").isTrue();
            softAssertions.assertThat(RolesPage.getSearchRoles().isDisplayed())
                    .as("Search roles is displayed").isTrue();
            softAssertions.assertThat(RolesPage.getCreateRoleButton().isDisplayed())
                    .as("Create role button is displayed").isTrue();
            softAssertions.assertAll();
        });

        Given("user can open Role Creation form", () -> {
            RolesPage.getCreateRoleButton().click();

            softAssertions.assertThat(CreateRoleForm.getCreateRoleTitle().isDisplayed())
                    .as("Create role title is displayed").isTrue();
            softAssertions.assertAll();
        });

        When("user insert all details on Role Creation form and click on Create button", () -> {
            CreateRoleForm.getRoleName().sendKeys(roleData.roleName);
            CreateRoleForm.getDescription().click();
            softAssertions.assertThat(CreateRoleForm.getRoleNameAvailable().isDisplayed()).isTrue();
            softAssertions.assertAll();
            CreateRoleForm.getDescription().sendKeys(roleData.roleDescription);

            PageObjectBase.performClick(CreateRoleForm.getCreateButton());
            RoleSuccessPopUp.getClosePopButton().click();
            softAssertions.assertThat(RolesPage.getRolesTitle().isDisplayed())
                    .as("Roles Page is displayed").isTrue();
            softAssertions.assertAll();
        });


        Then("new role can be found via Search field on Roles page", () -> {
            PageObjectBase.waitForLoad(3);
            RolesPage.getSearchRoles().clear();
            RolesPage.getSearchRoles().sendKeys(roleData.roleName + Keys.ENTER);
            softAssertions.assertThat(RolesPage.getSpecificRole(roleData.roleName).isDisplayed())
                    .as("Searched role is displayed").isTrue();
            softAssertions.assertAll();
        });

        Given("the role is created with assigned module", () -> {
            RolesPage.getCreateRoleButton().click();
            CreateRoleForm.getRoleName().sendKeys(roleData.roleName);
            CreateRoleForm.getDescription().click();
            softAssertions.assertThat(CreateRoleForm.getRoleNameAvailable().isDisplayed()).isTrue();
            softAssertions.assertAll();
            CreateRoleForm.getDescription().sendKeys(roleData.roleDescription);
            PageObjectBase.performClick(CreateRoleForm.getCreateButton());
            PageObjectBase.waitForLoad(3);
            RoleSuccessPopUp.getAddModulesButton().click();
            PageObjectBase.waitForLoad(3);
            AssignedModulesForm.chooseModulePermission("Financial Reporting", "OWNER").click();
            AssignedModulesForm.getAssignButton().click();
            PageObjectBase.waitForLoad(3);
            RolesPage.getSearchRoles().clear();
            RolesPage.getSearchRoles().sendKeys(roleData.roleName + Keys.ENTER);
        });

        Given("user can edit created role", () -> {
            PageObjectBase.waitForLoad(3);
            PageObjectBase.performClick(RolesPage.getRoleHoriz(roleData.roleName));
            RolesPage.getEditRoleButton().click();

            softAssertions.assertThat(EditRoleForm.getEditRoleTitle().isDisplayed()).isTrue();
            softAssertions.assertAll();

        });

        When("user update created role", () -> {
            PageObjectBase.waitForLoad(1);
            EditRoleForm.getRoleName().clear();
            EditRoleForm.getRoleName().sendKeys(newRole);
            EditRoleForm.getDescription().clear();
            softAssertions.assertThat(CreateRoleForm.getRoleNameAvailable().isDisplayed()).isTrue();
            softAssertions.assertAll();
            EditRoleForm.getDescription().clear();

            PageObjectBase.performClick(EditRoleForm.getEditButton());
            softAssertions.assertThat(RolesPage.getRolesTitle().isDisplayed())
                    .as("Roles Page is displayed").isTrue();
            softAssertions.assertAll();
        });

        Then("updated role can be found via Search field on Roles page", () -> {
            PageObjectBase.waitForLoad(3);
            RolesPage.getSearchRoles().sendKeys(Keys.chord(Keys.CONTROL,"a", Keys.DELETE));;
            RolesPage.getSearchRoles().sendKeys(newRole + Keys.ENTER);
            softAssertions.assertThat(RolesPage.getSpecificRole(newRole).isDisplayed())
                    .as("Searched role is displayed").isTrue();
            softAssertions.assertAll();
        });

        Given("user click on Delete role button", () -> {
            PageObjectBase.waitForLoad(5);
            PageObjectBase.performClick(RolesPage.getRoleHoriz(roleData.roleName));
            RolesPage.getDeleteRoleButton().click();
        });

        When("user click on Remove button in the popup", () -> {
            PageObjectBase.waitForLoad(2);
            softAssertions.assertThat(DeleteRoleForm.getConfirmationHeading(roleData.roleName).isDisplayed())
                    .as("Heading is displayed").isTrue();
            softAssertions.assertThat(DeleteRoleForm.getUserModulesInfo( 15).isDisplayed())
                    .as("Information about assigned users and modules").isTrue();
            softAssertions.assertAll();

            DeleteRoleForm.getRemoveButton().click();
        });

        Then("role can't be found via Search field on Roles page", () -> {
            PageObjectBase.waitForLoad(3);
            RolesPage.getSearchRoles().clear();
            RolesPage.getSearchRoles().sendKeys( Keys.ENTER);
           // RolesPage.getSearchRoles().sendKeys(roleData.roleName + Keys.ENTER);
            softAssertions.assertThat(RolesPage.roleIsNotPresent(roleData.roleName))
                    .as("Searched role isn't displayed").isTrue();
            softAssertions.assertAll();
        });

        Given("user found role with assigned user", () -> {
           roleData.roleName = "Global REEF Administrator";
            RolesPage.getSearchRoles().clear();
            RolesPage.getSearchRoles().sendKeys(roleData.roleName + Keys.ENTER);
            softAssertions.assertThat(RolesPage.getSpecificRole(roleData.roleName).isDisplayed())
                    .as("Searched role is displayed").isTrue();
            softAssertions.assertAll();
        });

        When("user clicked on Delete role button", () -> {
            PageObjectBase.waitForLoad(5);
            PageObjectBase.performClick(RolesPage.getRoleHoriz(roleData.roleName));
            PageObjectBase.performClick(RolesPage.getDeleteRoleButton());

        });

     /*   Then("popup is shown with info Message that role can't be deleted right now", () -> {
            PageObjectBase.waitForLoad(2);
            softAssertions.assertThat(DeleteRoleForm.getRoleHasUserHeading(1).isDisplayed())
                    .as("Heading is displayed").isTrue();
            softAssertions.assertThat(DeleteRoleForm.getInfoUserMessage().isDisplayed())
                    .as("Information about assigned users").isTrue();
            softAssertions.assertThat(DeleteRoleForm.getYesPleaseButton().isEnabled())
                    .as("Information about assigned users").isFalse();
            softAssertions.assertAll();
        });

      */

        Then("popup is shown with info Message that role can't be deleted right now", () -> {
            PageObjectBase.waitForLoad(2);
            softAssertions.assertThat(DeleteRoleForm.getDeleteRoleWithUsers().isDisplayed())
                    .as("Can't delete role with users message").isTrue();
            softAssertions.assertAll();
        });

        When("user try to create role with same name", () -> {
            PageObjectBase.waitForLoad(5);
            RolesPage.getCreateRoleButton().click();
            CreateRoleForm.getRoleName().sendKeys(roleData.roleName);
            CreateRoleForm.getDescription().sendKeys(roleData.roleDescription);
            PageObjectBase.performClick(CreateRoleForm.getCreateButton());


        });

        Then("the user should see success pop-up message", () -> {
            PageObjectBase.waitForLoad(5);
            softAssertions.assertThat(RolesPage.getSuccessHeading().isDisplayed()).isTrue();
            softAssertions.assertAll();
        });

        When("user scroll down on roles page", () -> {

            PageObjectBase.waitForLoad(3);
            PageObjectBase.performScrollDown(RolesPage.getRoleScrollableDiv(), 1000, 4);
        });

        Then("user should see roles loader", () -> {
            softAssertions.assertThat(RolesPage.getRolePageLoader().isDisplayed()).isTrue();
            softAssertions.assertAll();
        });

        Then("after loader user should see other roles", () -> {
            PageObjectBase.waitForLoad(3);
            softAssertions.assertThat(RolesPage.getRolesTitle().isDisplayed())
                    .as("Roles title is displayed").isTrue();
            softAssertions.assertThat(RolesPage.getSearchRoles().isDisplayed())
                    .as("Search roles is displayed").isTrue();
            softAssertions.assertThat(RolesPage.getCreateRoleButton().isDisplayed())
                    .as("Create role button is displayed").isTrue();
            softAssertions.assertThat(RolesPage.getRoleCard().isDisplayed())
                    .as("Roles card is displayed").isTrue();
            softAssertions.assertAll();
        });

        When("user clicked on role users label", () -> {
            softAssertions.assertThat(RolesPage.getRoleUsers().isDisplayed())
                    .as("Role users exist on role").isTrue();
            PageObjectBase.waitForLoad(2);
            PageObjectBase.performClick(RolesPage.getRoleUsers());
        });

        Then("user should be redirected on users page and see only users with {string} role", (String roleName) -> {
            PageObjectBase.waitForLoad(3);
            softAssertions.assertThat(UsersPage.getUsrHeader().isDisplayed())
                    .as("User header is displayed").isTrue();
            softAssertions.assertThat(UsersPage.getInviteUsersBtn().isDisplayed())
                    .as("User invite button is displayed").isTrue();
            softAssertions.assertThat(UsersPage.getUsrTableHeaderName().isDisplayed())
                    .as("Name column in table header is displayed").isTrue();
            List<WebElement> listOfRolesUsers = UsersPage.getRolesList(roleName);
            int userList = listOfRolesUsers.size();
            PageObjectBase.waitForLoad(1);
            softAssertions.assertThat(userList).isEqualTo(20);
            softAssertions.assertAll();
        });

    }
}
