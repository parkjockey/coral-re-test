package ui.steps.users;


import common.helpers.rest.HelperRest;
import common.helpers.ui.PageObjectBase;
import common.helpers.ui.PageUrls;
import data_containers.GeneralData;
import data_containers.OrganizationData;
import data_containers.RoleData;
import data_containers.UserData;
import io.cucumber.java8.En;
import org.assertj.core.api.SoftAssertions;
import org.openqa.selenium.*;
import ui.page_object.*;
import ui.page_object.roles.RolesPage;
import ui.page_object.users.*;


import java.awt.event.KeyEvent;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static common.helpers.ui.BaseDriver.driver;
import static org.awaitility.Awaitility.await;

public class UIUsersSteps implements En {
    GeneralData generalData;
    UserData userData;
    RoleData roleData;
    String notExistingEmail;
    private SoftAssertions softAssertions;
    OrganizationData organizationData;

    public UIUsersSteps(GeneralData generalData, UserData userData, RoleData roleData, OrganizationData organizationData) {
        this.generalData = generalData;
        this.userData = userData;
        this.roleData = roleData;
        softAssertions = new SoftAssertions();
        this.organizationData = organizationData;



        Given("user can reach users page", () -> {
            PageObjectBase.waitForLoad(3);
            PageObjectBase.driver.get(generalData.reefWebApp + PageUrls.Pages.USERS.path);
            PageObjectBase.waitForLoad(3);
            await("Wait condition to be true")
                    .atMost(10, TimeUnit.SECONDS).until(() ->
                    PageObjectBase.driver.getCurrentUrl().equals(generalData.reefWebApp+ PageUrls.Pages.USERS.path));
            PageObjectBase.waitToLoaderDisappear();

        });

        Given("user go to users page", () -> {
            PageObjectBase.waitForLoad(3);
            SideBar.getSideBarIdentityAccess().click();
            IdentityAccessModulePage.getViewUsersPage().click();
        });

        When("the user selects ten rows per page", () -> {
            UsersPage.getUsrRowsPerPage().click();
            UsersPage.getUsr10RowsPerPage().click();
        });

        Then("user should see on page ten users", () -> {
            PageObjectBase.waitForLoad(3);
            List<WebElement> listOfUsers = driver.findElements(By.xpath("//table/tbody/tr"));
            int lst = listOfUsers.size();
            PageObjectBase.waitForLoad(3);
            softAssertions.assertThat(lst == 10);
            softAssertions.assertAll();

        });

        When("user add an existing user to the organization and press enter", () -> {

            PageObjectBase.waitForLoad(3);
            UsersPage.getInviteUsersBtn().click();
            InviteUsersForm.getUsrEmailForm().sendKeys("reefuato@gmail.com");
            InviteUsersForm.getUsrEmailForm().sendKeys(Keys.ENTER);
        });

        When("user go to third page", () -> {
            UsersPage.getUsrPaginationToNextPage().click();
            PageObjectBase.waitToLoaderDisappear();
            UsersPage.getUsrPaginationToNextPage().click();
        });

        Then("user should see that users exist on the page", () -> {
            softAssertions.assertThat(UsersPage.getUsrUserName().isDisplayed())
                    .as("users is displayed").isTrue();
            softAssertions.assertThat(UsersPage.getInviteUsersBtn().isDisplayed())
                    .as("Invite btn is displayed").isTrue();
            softAssertions.assertThat(UsersPage.getUsrHeader().isDisplayed())
                    .as("User header is displayed").isTrue();
            softAssertions.assertAll();
        });

        Then("user should see the user first and last name", () -> {
            softAssertions.assertThat(InviteUsersForm.getUsrNameWhenUserIsAlreadyActive().isDisplayed())
                    .as("First And Last Name exist").isTrue();
            softAssertions.assertAll();
        });

        When("user add a user who does not exist in the Reef system to the organization and press enter", () -> {
            userData.email = HelperRest.getRandomEmail();
            PageObjectBase.waitForLoad(3);
            UsersPage.getInviteUsersBtn().click();
            InviteUsersForm.getUsrEmailForm().sendKeys(userData.email);
            InviteUsersForm.getUsrEmailForm().sendKeys(Keys.ENTER);
        });


        Then("user should see the user email", () -> {
            softAssertions.assertThat(InviteUsersForm.getUsrInviteUser(userData.email).isDisplayed())
                    .as("Email for non existing user is displayed").isTrue();
            softAssertions.assertAll();

        });

        When("user add a email that is not in a valid format", () -> {

            PageObjectBase.waitForLoad(3);
            UsersPage.getInviteUsersBtn().click();
            InviteUsersForm.getUsrEmailForm().sendKeys("em,ail.@.com");
            InviteUsersForm.getUsrEmailForm().sendKeys(Keys.ENTER);
        });

        Then("user should see a warning message", () -> {
            softAssertions.assertThat(InviteUsersForm.getUsrCheckYourEmailEntriesMessage().isDisplayed())
                    .as("Warning message for non valid email format is displayed").isTrue();
            softAssertions.assertAll();

        });

        Given("user go to invite users pop-up entered email in email invitation field", () -> {
            userData.email = "reefuato+"+HelperRest.getRandomNumber()+"@reefplatform.com";
            PageObjectBase.waitForLoad(3);
            UsersPage.getInviteUsersBtn().click();
            InviteUsersForm.getUsrEmailForm().sendKeys(userData.email);
            InviteUsersForm.getUsrEmailForm().sendKeys(Keys.ENTER);
        });

        Given("and the user has selected a role", () -> {
            PageObjectBase.waitForLoad(2);
            InviteUsersForm.getUsrSelectRoleList().sendKeys("Global REEF Administrator");
            PageObjectBase.waitForLoad(3);

            PageObjectBase.performClick(InviteUsersForm.getUsrAutoRole());
        });

        When("user click on invite user button", () -> {
            PageObjectBase.waitForLoad(3);
            InviteUsersForm.getUsrInviteUserBtn().click();
        });

        Then("user should see success pop with appropriate messages", () -> {
             softAssertions.assertThat(InviteUsersSuccessPopUp.getUsrUsersInvitedMessage().getText().contains("User was invited to join with the Global REEF Administrator role.")).isTrue();
            softAssertions.assertThat(InviteUsersSuccessPopUp.getInvitedUsersNumbers().getText().contains("1")).isTrue();
            PageObjectBase.waitForLoad(2);
            softAssertions.assertThat(UsersPage.getUsrHeader().isDisplayed())
                    .as("user header is displayed").isTrue();
            softAssertions.assertAll();

        });

        Given("User goes to Create your account page", () -> {
            driver.get(generalData.loginWebApp+"/invitation/"+userData.invitationCode);

            PageObjectBase.waitForLoad(2);
            softAssertions.assertThat(CreateUserPage.getCreateUserPageTitle().isDisplayed()).as("Page title is displayed").isTrue();
            softAssertions.assertThat(CreateUserPage.getCreateAccountButton().isEnabled()).as("Page title is displayed").isFalse();
            softAssertions.assertAll();
        });

        When("Not SSO user fill all necessary data", () -> {
           userData.firstName = "AutoName"+HelperRest.getRandomNumber();
           userData.lastName = "AutoLastName"+HelperRest.getRandomNumber();
           CreateUserPage.getFirstName().sendKeys(userData.firstName);
           CreateUserPage.getLastName().sendKeys(userData.lastName);
           CreateUserPage.getPassword().sendKeys(userData.password);
           PageObjectBase.waitForLoad(2);
           PageObjectBase.performClick(CreateUserPage.getCreateAccountButton());
        });

        Then("CreateAccountPopUp is shown and user is redirected to HomePage", () -> {
            softAssertions.assertThat(CreatedAccountPopUp.getSuccessUserCreationTitle().isDisplayed()).isTrue();
            softAssertions.assertThat(CreatedAccountPopUp.getInformationMessage().isDisplayed()).isTrue();
            softAssertions.assertAll();
            PageObjectBase.performClick(CreatedAccountPopUp.getContinueToHomePage());

            await("Wait condition to be true")
                    .atMost(10, TimeUnit.SECONDS)
                    .pollDelay(2, TimeUnit.SECONDS)
                    .until(() ->
                    driver.getCurrentUrl().contains(generalData.reefWebApp));
        });

        When("SSO user open activation link and login with his credentials", () -> {
            driver.get(generalData.loginWebApp+"/invitation/"+userData.invitationCode);
            LoginPage.getUserEmailField().clear();
            LoginPage.getUserEmailField().sendKeys(userData.email);
            LoginPage.getContinueLoginButton().click();
            MicrosoftLoginPage.loginMicrosoftAccount(userData.email, userData.password);
        });

        Then("Home page is opened", () -> {
            PageObjectBase.waitForLoad(3);
            softAssertions.assertThat(driver.getCurrentUrl()).isEqualTo(generalData.reefWebApp + "/");
            softAssertions.assertAll();
        });


        When("User goes to user account page", () -> {
            PageObjectBase.waitForLoad(3);
            Thread.sleep(3000);
            PageObjectBase.performClick(HeaderBar.getAvatar());
            HeaderBar.getAccount().click();
        });

        Then("User details are correct", () -> {
            softAssertions.assertThat(UserAccountPage.getFirstName().getAttribute("value"))
                    .as("First name is correct").isEqualTo(userData.firstName);
            softAssertions.assertThat(UserAccountPage.getLastName().getAttribute("value"))
                    .as("Last name is correct").isEqualTo(userData.lastName);
            softAssertions.assertAll();
        });

        When("user update first and last name", () -> {
            userData.updatedFirstName = "UpdatedName"+HelperRest.getRandomNumber();
            userData.updatedLastName = "UpdatedLastName"+HelperRest.getRandomNumber();
            PageObjectBase.performClear(UserAccountPage.getFirstName());
            UserAccountPage.getFirstName().sendKeys(userData.updatedFirstName);
            PageObjectBase.performClear(UserAccountPage.getLastName());
            UserAccountPage.getLastName().sendKeys(userData.updatedLastName);
            UserAccountPage.updateProfile();
        });

        Then("the user should see the updated first name and last name when again goes to the user account tab", () -> {
            driver.navigate().refresh();
            PageObjectBase.waitForLoad(3);
            HeaderBar.getAvatar().click();
            HeaderBar.getAccount().click();
            softAssertions.assertThat(UserAccountPage.getFirstName().getAttribute("value"))
                    .as("Updated first name is correct").isEqualTo(userData.firstName+userData.updatedFirstName);
            softAssertions.assertThat(UserAccountPage.getLastName().getAttribute("value"))
                    .as("Updated last name is correct").isEqualTo(userData.lastName+userData.updatedLastName);
            softAssertions.assertAll();
        });

        Given("user go to security section", () -> {
            PageObjectBase.waitForLoad(2);
            UserAccountPage.getSecuritySection().click();
            softAssertions.assertThat(PageObjectBase.rgbaToHexColor(InviteUsersForm.getLeast8ChartersPasswordPolicy()))
                    .as("Color is black").isEqualTo(InviteUsersForm.blackColor);
            softAssertions.assertThat(PageObjectBase.rgbaToHexColor(InviteUsersForm.getLowerUpperCasePasswordPolicy()))
                    .as("Color is black").isEqualTo(InviteUsersForm.blackColor);
            softAssertions.assertThat(PageObjectBase.rgbaToHexColor(InviteUsersForm.getLeastOneNumberPasswordPolicy()))
                    .as("Color is black").isEqualTo(InviteUsersForm.blackColor);
            softAssertions.assertThat(PageObjectBase.rgbaToHexColor(InviteUsersForm.getOneSpecialCharacterPasswordPolicy()))
                    .as("Color is black").isEqualTo(InviteUsersForm.blackColor);
            softAssertions.assertThat(PageObjectBase.rgbaToHexColor(InviteUsersForm.getDoesNotContainNameEmailPasswordPolicy()))
                    .as("Color is black").isEqualTo(InviteUsersForm.blackColor);
            softAssertions.assertThat(UserAccountPage.getCurrentPasswordInput().isDisplayed()).as("Current password input is displayed").isTrue();
            softAssertions.assertThat(UserAccountPage.getNewPasswordInput().isDisplayed()).as("New password input is displayed").isTrue();
            softAssertions.assertAll();
        });

        When("user update password", () -> {
            userData.updatedPassword = "Test6789!";
            UserAccountPage.getCurrentPasswordInput().clear();
            UserAccountPage.getCurrentPasswordInput().sendKeys(userData.password);
            UserAccountPage.getNewPasswordInput().clear();
            UserAccountPage.getNewPasswordInput().sendKeys(userData.updatedPassword);
            UserAccountPage.getUpdatePasswordButtonNoSSO().click();
        });

        Then("the user should be able to log in with the new password", () -> {
            driver.navigate().refresh();
            PageObjectBase.waitForLoad(1);
            HeaderBar.getAvatar().click();
            HeaderBar.getLogOut().click();
            PageObjectBase.waitForLoad(2);
            LoginPage.getUserEmailField().clear();
            LoginPage.getUserEmailField().sendKeys(userData.email);
            LoginPage.getContinueLoginButton().click();
            LoginPage.getUserNoSSOPasswordField().sendKeys(userData.updatedPassword);
            LoginPage.getLogInToYourAccountNoSSOUserButton().click();
            Thread.sleep(10000);
            softAssertions.assertThat(HeaderBar.getAvatar().isDisplayed())
                    .as("Avatar is displayed");
            softAssertions.assertAll();
        });

        When("enter user email in search field on users page", () -> {
          //  UsersPage.getUsrSearchUser().clear();
            PageObjectBase.waitForLoad(3);
            UsersPage.getUsrSearchViaField().click();
            UsersPage.getUsrSearchViaEmail().click();
            UsersPage.getUsrSearchUser().sendKeys(generalData.adSSOUsername);
            PageObjectBase.waitToLoaderDisappear();
        });

        Then("user should see that this user exists in search results", () -> {

            List<WebElement> listOfEmails = UsersPage.getUsrUserEmailOnUserLabel();
            int OneEmail = listOfEmails.size();
            PageObjectBase.waitForLoad(3);
            softAssertions.assertThat(OneEmail == 1);
            softAssertions.assertThat(listOfEmails.get(0).getText())
                    .as("Email is correct").isEqualTo(GeneralData.adSSOUsername);
            softAssertions.assertAll();
        });

        Given("user email doesn't exist in the Reef system", () -> {
            notExistingEmail = "aaaaaaaaaaaaaaaaaa@123aaaaaaaaaaaa";
        });

        When("user enter not existing email in the search field on the users page", () -> {
           // UsersPage.getUsrSearchUser().clear();
            PageObjectBase.waitForLoad(5);
            UsersPage.getUsrSearchViaField().click();
            UsersPage.getUsrSearchViaEmail().click();
            UsersPage.getUsrSearchUser().sendKeys(notExistingEmail);
            PageObjectBase.waitToLoaderDisappear();
        });

        Then("the user should see no user found message", () -> {
            softAssertions.assertThat(UsersPage.getUsrNoUsersFoundMessage().getText())
                    .as("No user found message is displayed").isEqualTo("No users found!");
            softAssertions.assertAll();
        });

        Then("user should see that the invite button is disabled", () -> {
            softAssertions.assertThat(InviteUsersForm.getUsrInviteUserBtn().isEnabled())
                    .as("Invite button is disabled").isFalse();
            softAssertions.assertAll();
        });

        Given("Assign roles to user modal is opened for newly created user", () -> {
            UsersPage.getUsrSearchUser().clear();
            Thread.sleep(2000);
            UsersPage.getUsrSearchViaField().click();
            UsersPage.getUsrSearchViaEmail().click();
            UsersPage.getUsrSearchUser().sendKeys(userData.email);
            PageObjectBase.waitToLoaderDisappear();
            UsersPage.getMoreUser().click();
            UsersPage.getUsrAssignRolesToUsersModal().click();
            PageObjectBase.waitToBeTrue(AssignRoleModal.getModalTitle().isDisplayed());

        });

        When("First role in modal is checked and assign role button is clicked", () -> {
            AssignRoleModal.getAssignRoleCheckBox().get(0).click();
            AssignRoleModal.getAssignRoleButton().click();
            });

        Then("Success tostyfy message is displayed", () -> {
            PageObjectBase.waitToBeTrue(UsersPage.getTostRoleAssigned().isDisplayed());
        });

        Then("User is redirected to HomePage", () -> {

            await("Wait condition to be true")
                    .atMost(10, TimeUnit.SECONDS)
                    .pollDelay(2, TimeUnit.SECONDS)
                    .until(() ->
                            driver.getCurrentUrl().contains(generalData.reefWebApp));
        });

        Given("User go to security tab in account settings", () -> {
            HeaderBar.getAvatar().click();
            HeaderBar.getAccount().click();
            UserAccountPage.getSecuritySection().click();
        });

        When("user click reset password button", () -> {
            PageObjectBase.waitForLoad(1);
            PageObjectBase.performClick(UserAccountPage.getChangePasswordSSOUser());
        });

        Then("user is redirected to the new tab - reef reset password", () -> {
            PageObjectBase.waitForLoad(3);
            String parendWindowHandle=driver.getWindowHandle();
            for(String childTab:driver.getWindowHandles())
            {driver.switchTo().window(childTab);}

            await("Wait condition to be true")
                    .atMost(10, TimeUnit.SECONDS)
                    .pollDelay(2, TimeUnit.SECONDS)
                    .until(() ->
                            driver.getCurrentUrl().contains("https://reset.reefplatform.com/showLogin.cc"));
        });

        When("B2B user login", () -> {
            LoginPage.getUserEmailField().clear();
            LoginPage.getUserEmailField().sendKeys(userData.email);
            LoginPage.getContinueLoginButton().click();
            LoginPage.getUserNoSSOPasswordField().sendKeys(userData.password);
            LoginPage.getLogInToYourAccountNoSSOUserButton().click();
        });

        Then("user should see organization name in header bar", () -> {
            PageObjectBase.waitForLoad(2);
            softAssertions.assertThat(HeaderBar.getHeaderOrgName(organizationData.name).isDisplayed())
                    .as("Organization name is true").isTrue();
            softAssertions.assertAll();
        });

        Then("user should see all elements on page", () -> {

            softAssertions.assertThat(UsersPage.getUsrHeader().isDisplayed())
                    .as("User header is displayed").isTrue();
            softAssertions.assertThat(UsersPage.getUsrSearchUser().isDisplayed())
                    .as("Search field is displayed").isTrue();
            softAssertions.assertThat(UsersPage.getInviteUsersBtn().isDisplayed())
                    .as("User invite button is displayed").isTrue();
            softAssertions.assertThat(UsersPage.getUsrTableHeaderName().isDisplayed())
                    .as("Name column in table header is displayed").isTrue();
            softAssertions.assertThat(UsersPage.getUsrTableHeaderEmail().isDisplayed())
                    .as("Email column in table header is displayed").isTrue();
            softAssertions.assertThat(UsersPage.getUsrTableHeaderRoles().isDisplayed())
                    .as("Roles in table header is displayed").isTrue();
            softAssertions.assertThat(UsersPage.getUsrTableHeaderStatus().isDisplayed())
                    .as("Status column in table header is displayed").isTrue();
            softAssertions.assertThat(UsersPage.getUsrTableHeaderUpdated().isDisplayed())
                    .as("Updated column in table header is displayed").isTrue();
            softAssertions.assertThat(UsersPage.getUsrTableHeaderDataAccess().isDisplayed())
                    .as("Data Access column in table header is displayed").isTrue();
            softAssertions.assertThat(UsersPage.getUsrPaginationToNextPage().isDisplayed())
                    .as("Button for next page is displayed").isTrue();
            softAssertions.assertThat(UsersPage.getUsrRowsPerPage().isDisplayed())
                    .as("Rows per page is displayed").isTrue();
            softAssertions.assertAll();
            PageObjectBase.waitForLoad(1);
            List<WebElement> listOfUsers = UsersPage.getUsrUserLabel();
            int lst = listOfUsers.size();
            PageObjectBase.waitForLoad(1);
            softAssertions.assertThat(lst).isEqualTo(20);
            softAssertions.assertAll();
        });

        Given("user can reach invited user", () -> {
            PageObjectBase.waitForLoad(1);
            UsersPage.getUsrSearchUser().clear();
            UsersPage.getUsrSearchViaField().click();
            UsersPage.getUsrSearchViaEmail().click();
            UsersPage.getUsrSearchUser().sendKeys(userData.email);
        });

        When("User resend invitation", () -> {
            PageObjectBase.waitForLoad(1);
            PageObjectBase.performClick(UsersPage.getMoreUser());
            UsersPage.getUsrResendInvitation().click();

        });

        Then("invitation pop-up message is displayed", () -> {
            softAssertions.assertThat(UsersPage.getUsrInvitationResendToastify().isDisplayed())
                    .as("Resend invitation toastify is displayed").isTrue();
            softAssertions.assertAll();
        });

        When("user select active status filter", () -> {
            PageObjectBase.waitForLoad(3);
            UsersPage.getUsrStatusesFilter().sendKeys("Active");
            PageObjectBase.performClick(UsersPage.getUsrSelectStatus());
            UsersPage.getUsrSearchUser().click();
            PageObjectBase.waitToLoaderDisappear();
        });

        Then("user should see on users page only active users", () -> {
            PageObjectBase.waitForLoad(3);
            softAssertions.assertThat(PageObjectBase.isElementPresent(UsersPage.userStatusList("Active"))).isTrue();
            softAssertions.assertThat(PageObjectBase.isElementPresent(UsersPage.userStatusList("Invited"))).isFalse();
            softAssertions.assertThat(PageObjectBase.isElementPresent(UsersPage.userStatusList("Suspended"))).isFalse();
            softAssertions.assertAll();
        });

        When("user select invited status filter", () -> {
            PageObjectBase.waitForLoad(3);
            UsersPage.getUsrStatusesFilter().sendKeys("Invited");
            PageObjectBase.performClick(UsersPage.getUsrSelectStatus());
            UsersPage.getUsrSearchUser().click();
            PageObjectBase.waitToLoaderDisappear();
        });

        Then("user should see on users page only invited users", () -> {
            PageObjectBase.waitForLoad(3);
            softAssertions.assertThat(PageObjectBase.isElementPresent(UsersPage.userStatusList("Invited"))).isTrue();
            softAssertions.assertThat(PageObjectBase.isElementPresent(UsersPage.userStatusList("Active"))).isFalse();
            softAssertions.assertThat(PageObjectBase.isElementPresent(UsersPage.userStatusList("Suspended"))).isFalse();
            softAssertions.assertAll();
        });

        When("user select suspended status filter", () -> {
            PageObjectBase.waitForLoad(3);
            UsersPage.getUsrStatusesFilter().sendKeys("Suspended");
            PageObjectBase.performClick(UsersPage.getUsrSelectStatus());
            UsersPage.getUsrSearchUser().click();
            PageObjectBase.waitToLoaderDisappear();
        });

        Then("user should see on users page only suspended users", () -> {
            PageObjectBase.waitForLoad(3);
            softAssertions.assertThat(PageObjectBase.isElementPresent(UsersPage.userStatusList("Suspended"))).isTrue();
            softAssertions.assertThat(PageObjectBase.isElementPresent(UsersPage.userStatusList("Active"))).isFalse();
            softAssertions.assertThat(PageObjectBase.isElementPresent(UsersPage.userStatusList("Invited"))).isFalse();
            softAssertions.assertAll();
        });

        When("user select {string} role from roles filter", (String roleName) -> {
            PageObjectBase.waitForLoad(3);
            UsersPage.getUsrRolesFilterInput().sendKeys(roleName);
            Thread.sleep(2000);
            PageObjectBase.performClick(UsersPage.getUsrSelectRolesFilter());
            UsersPage.getUsrSearchUser().click();
            PageObjectBase.waitToLoaderDisappear();
        });

        When("user select new created role role from roles filter", () -> {
            PageObjectBase.waitForLoad(3);
            UsersPage.getUsrRolesFilterInput().sendKeys(roleData.roleName);
            PageObjectBase.performClick(UsersPage.getUsrSelectRolesFilter());
            UsersPage.getUsrSearchUser().click();
            PageObjectBase.waitToLoaderDisappear();
        });

        Then("user should see on users page only users with {string} role", (String roleName) -> {
            PageObjectBase.waitForLoad(3);
            List<WebElement> listOfRolesUsers = UsersPage.getRolesList(roleName);
            int lst = listOfRolesUsers.size();
            PageObjectBase.waitForLoad(1);
            softAssertions.assertThat(lst).isEqualTo(20);
            softAssertions.assertAll();
        });

        Then("user should see on user only user with this new created role", () -> {
            PageObjectBase.waitForLoad(3);
            softAssertions.assertThat(UsersPage.getRolesList(roleData.roleName)).size().isEqualTo(1);
            softAssertions.assertThat(UsersPage.getUsrUserEmailOnUserLabel()).size().isEqualTo(1);
            softAssertions.assertAll();
        });


        Given("User goes to upload profile picture", () -> {
            PageObjectBase.waitForLoad(1);
            PageObjectBase.performMouseHover(UserAccountPage.getUploadProfileImageCircle());
            PageObjectBase.waitForLoad(1);
            UserAccountPage.getUploadProfileImageCircle().click();
            PageObjectBase.waitForLoad(1);
            UserAccountPage.getUploadProfileImageButton().click();
        });

        When("user upload profile picture", () -> {
            PageObjectBase.waitForLoad(1);
          //  PageObjectBase.uploadFileOSSide("C:\\ReefAutomation\\profilePicture.jpg");
           // PageObjectBase.uploadFileOSSide("Users/Desktop/profilePic.jpg");
            JavascriptExecutor js = (JavascriptExecutor) driver;
            js.executeScript("arguments[0].setAttribute('style', arguments[1])", driver.findElement(By.xpath("//input[@type='file']")), "0");
            js.executeScript("arguments[0].setAttribute('class', arguments[1])", driver.findElement(By.xpath("//input[@type='file']/../../div[2]")), "a");
            driver.findElement(By.xpath("//input[@type='file']")).sendKeys("/Users/aysa.dzhalaeva/Desktop/profilePicture.jpg");

            PageObjectBase.waitForLoad(1);
            UserAccountPage.getUploadProfileImageSaveButton().click();
        });

        Then("User should see uploaded profile picture", () -> {
            PageObjectBase.waitForLoad(3);
//            driver.navigate().refresh();
            softAssertions.assertThat(UserAccountPage.getUploadedProfileImage("iVBORw0KGgoAAAANSUhEUgAABB0AAAQdCAYAAAAS").get(0).isDisplayed())
                    .as("Profile picture is visible").isTrue();
            softAssertions.assertThat(UserAccountPage.getAvatarProfilePicture("iVBORw0KGgoAAAANSUhEUgAABB0AAAQdCAYAAAAS").isDisplayed())
                    .as("Profile picture is visible").isTrue();
            softAssertions.assertThat(UserAccountPage.getCircleProfilePicture("iVBORw0KGgoAAAANSUhEUgAABB0AAAQdCAYAAAAS").isDisplayed())
                    .as("Profile picture is visible").isTrue();
            softAssertions.assertAll();

        });

        When("Admin search user by Name on users page", () -> {
            PageObjectBase.waitForLoad(5);
            UsersPage.getUsrSearchUser().clear();
            UsersPage.getUsrSearchViaField().click();
            UsersPage.getUsrSearchViaName().click();
            UsersPage.getUsrSearchUser().sendKeys(userData.firstName + ' ' + userData.lastName);
            PageObjectBase.waitToLoaderDisappear();
        });

        Then("user should see that this user exists in search results on users page", () -> {

            List<WebElement> listOfEmails = UsersPage.getUsrUserEmailOnUserLabel();
            int OneEmail = listOfEmails.size();
            PageObjectBase.waitForLoad(3);
            softAssertions.assertThat(OneEmail == 1);
            softAssertions.assertThat(listOfEmails.get(0).getText())
                    .as("Email is correct").isEqualTo(userData.email.toLowerCase());
            softAssertions.assertAll();
        });


        Given("user suspend organization user", () -> {
            PageObjectBase.waitForLoad(1);
            UsersPage.getMoreUser().click();
            PageObjectBase.performClick(UsersPage.getUsrSuspendUserOption());
            UsersPage.getSuspendUserButton().click();

        });

        Given("user is logged out", () -> {
            PageObjectBase.waitToToastifyDisappear();
            PageObjectBase.waitForLoad(2);
            PageObjectBase.performClick(HeaderBar.getAvatar());
            HeaderBar.getLogOut().click();
        });

        When("B2B user try to login", () -> {
            LoginPage.getUserEmailField().clear();
            LoginPage.getUserEmailField().sendKeys(userData.email);
            LoginPage.getContinueLoginButton().click();
        });

        Then("user should see a message that account is suspended", () -> {
            softAssertions.assertThat(LoginPage.getAccountIsSuspendedMessage().isDisplayed())
                    .as("Account is suspended message is displayed").isTrue();
            softAssertions.assertAll();
        });

        Then("user should see toast message Failed to update users status", () -> {
            softAssertions.assertThat(UsersPage.getFailedToUpdateUserStatusToastMessage().isDisplayed())
                    .as("Resend invitation toastify is displayed").isTrue();
            softAssertions.assertAll();
        });

        Then("user should see a message that Enterprise account is suspended", () -> {
            softAssertions.assertThat(LoginPage.getAccountIsSuspendedInEnterpriseOrgMessage().isDisplayed())
                    .as("Enterprise account is suspended message is displayed").isTrue();
            softAssertions.assertAll();
        });

        When("user select previous month in date range filter", () -> {
            PageObjectBase.waitForLoad(1);
            PageObjectBase.waitToLoaderDisappear();
            UsersPage.getUsrDateRangeFilter().click();
            UsersPage.getPreviousMonthDateRangeFilter().click();
            UsersPage.getFirstDayInMonthDateRangeFilter().click();
            UsersPage.getTwentyEighthDayInMonthDateRangeFilter().click();
            PageObjectBase.waitForLoad(3);
        });

        String previousMonth = PageObjectBase.previousMonthMMM();

        Then("user should only see users who were updated last month", () -> {
            PageObjectBase.waitToLoaderDisappear();
            softAssertions.assertThat(UsersPage.getUpdatedMonths(previousMonth)).size().isEqualTo(20);
            softAssertions.assertAll();
        });
//Step for commented "Combined filters on Users page" scenario from UIUsers.feature
        When("user selects combined filters on users page", () -> {
            PageObjectBase.waitForLoad(1);
            PageObjectBase.waitToLoaderDisappear();
            UsersPage.getUsrRolesFilterInput().sendKeys("Global REEF Administrator");
            PageObjectBase.performClick(UsersPage.getUsrSelectRolesFilter());
            UsersPage.getUsrSearchUser().click();
            PageObjectBase.waitForLoad(2);
            UsersPage.getUsrStatusesFilter().sendKeys("Active");
            PageObjectBase.performClick(UsersPage.getUsrSelectStatus());
            UsersPage.getUsrSearchUser().click();
            PageObjectBase.waitToLoaderDisappear();
            UsersPage.getUsrDateRangeFilter().click();
            UsersPage.getPreviousMonthDateRangeFilter().click();
            UsersPage.getFirstDayInMonthDateRangeFilter().click();
            UsersPage.getTwentyEighthDayInMonthDateRangeFilter().click();
            PageObjectBase.waitForLoad(10);
        });
//Step for commented "Combined filters on Users page" scenario from UIUsers.feature
        Then("user should only see users who match the filter criteria", () -> {
            PageObjectBase.waitForLoad(3);
            softAssertions.assertThat(PageObjectBase.isElementPresent(UsersPage.userStatusList("Active"))).isTrue();
            softAssertions.assertThat(PageObjectBase.isElementPresent(UsersPage.userStatusList("Invited"))).isFalse();
            softAssertions.assertThat(PageObjectBase.isElementPresent(UsersPage.userStatusList("Suspended"))).isFalse();
            softAssertions.assertThat(UsersPage.getRolesList("Global REEF Administrator")).size().isEqualTo(20);
            softAssertions.assertThat(UsersPage.getUpdatedMonths(previousMonth)).size().isEqualTo(20);
            softAssertions.assertAll();

        });

        String currentDay = PageObjectBase.getCurrentDay();

        When("user selects a combined filter that correspond to this user", () -> {
            PageObjectBase.waitForLoad(1);
            PageObjectBase.waitToLoaderDisappear();
            UsersPage.getUsrRolesFilterInput().sendKeys(roleData.roleName);
            PageObjectBase.performClick(UsersPage.getUsrSelectRolesFilter());
            UsersPage.getUsrSearchUser().click();
            PageObjectBase.waitForLoad(2);
            UsersPage.getUsrStatusesFilter().sendKeys("Active");
            PageObjectBase.performClick(UsersPage.getUsrSelectStatus());
            UsersPage.getUsrSearchUser().click();
            PageObjectBase.waitToLoaderDisappear();
            UsersPage.getUsrDateRangeFilter().click();
            PageObjectBase.performDoubleClick(UsersPage.getCurrentDayInDateRangeFilter(currentDay));
        });

        String currentMonth = PageObjectBase.currentMonthMMM();

        Then("user should only see user who match the filtered criteria", () -> {
            PageObjectBase.waitForLoad(3);
            softAssertions.assertThat(PageObjectBase.isElementPresent(UsersPage.userStatusList("Active"))).isTrue();
            softAssertions.assertThat(PageObjectBase.isElementPresent(UsersPage.userStatusList("Invited"))).isFalse();
            softAssertions.assertThat(PageObjectBase.isElementPresent(UsersPage.userStatusList("Suspended"))).isFalse();
            softAssertions.assertThat(UsersPage.getRolesList(roleData.roleName)).size().isEqualTo(1);
            softAssertions.assertThat(UsersPage.getUpdatedMonths(currentMonth)).size().isEqualTo(1);
            softAssertions.assertThat(UsersPage.getUsrUserEmailOnUserLabel()).size().isEqualTo(1);
            softAssertions.assertAll();

        });

        When("user selects {string}, {string} and {string} roles from roles filter", (String roleName1, String roleName2, String roleName3) -> {
            PageObjectBase.waitForLoad(4);
            UsersPage.getUsrRolesFilterInput().sendKeys(roleName1);
            PageObjectBase.performClick(UsersPage.getUsrSelectRolesFilter());
            UsersPage.getUsrRolesFilterInput().sendKeys(roleName2);
            PageObjectBase.performClick(UsersPage.getUsrSelectRolesFilter());
            UsersPage.getUsrRolesFilterInput().sendKeys(roleName3);
            PageObjectBase.performClick(UsersPage.getUsrSelectRolesFilter());
            UsersPage.getUsrSearchUser().click();
            PageObjectBase.waitToLoaderDisappear();
        });

        When("user should see on users page users with selected roles", () -> {
            PageObjectBase.waitForLoad(3);
            softAssertions.assertThat(UsersPage.getRolesList("Location Owner")).size().isEqualTo(6);
        });

        When("Assign roles to user modal is opened for first user", () -> {
            UsersPage.getMoreUserFirst().click();
            UsersPage.getUsrAssignRolesToUsersModal().click();
            PageObjectBase.waitToBeTrue(AssignRoleModal.getModalTitle().isDisplayed());
        });

        When("one role is unchecked and assign role button is clicked", () -> {
            PageObjectBase.waitForLoad(3);
            PageObjectBase.performScrollDown(UsersPage.getRoleScrollableDiv1(), 1000, 6);
            softAssertions.assertThat(RolesPage.getRolePageLoader().isDisplayed()).isTrue();
            softAssertions.assertAll();
            PageObjectBase.waitForLoad(2);
            PageObjectBase.performClick(UsersPage.getCheckedCheckBoxes());
            AssignRoleModal.getAssignRoleButton().click();
        });

        And("user scroll down the roles page", () -> {
            PageObjectBase.waitForLoad(3);
            PageObjectBase.performScrollDown(UsersPage.getRoleScrollableDiv1(), 1000, 6);
            softAssertions.assertThat(RolesPage.getRolePageLoader().isDisplayed()).isTrue();
            softAssertions.assertAll();
        });

        Then("user should see roles that are assigned to the first user", () -> {
            PageObjectBase.waitForLoad(2);
            softAssertions.assertThat(UsersPage.getCheckedCheckBoxes().isSelected()).isTrue();
            softAssertions.assertAll();
        });


    }
}
