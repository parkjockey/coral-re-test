@hmt @hmtGql @gql @hierarchyRootLevelsGql
Feature: GQL Hierarchy Root Levels feature - will cover test cases regarding Hierarchy level gql query

  Background:
    Given admin accessToken is obtained
    And Reef organization id is obtained

  Scenario: Hierarchy Root Levels query - happy path - gql
    And Test with id 37957 added to Testrail run
    When HierarchyRootLevels query is sent
    Then Response status code is 200
    Then Response contains all root levels hierarchies

