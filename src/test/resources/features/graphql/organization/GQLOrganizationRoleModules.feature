@gqlRoleModules @gql @gqlOrganizationRole
Feature: Organization roles modules graphQl

  This feature will test Organization roles modules queries and mutations

  Background:
    Given admin accessToken is obtained

  Scenario: Find organization role modules Reef organization role - gql
    And New role is added to Reef Enterprise
    And Test with id 13785 added to Testrail run
    When OrganizationRoleModules query is called
    Then Response status code is 200
    And Response contains list of modules and submodules and their names and id are not null

  Scenario: Update Organization Role Modules for Reef organization - gql
    And New role is added to Reef Enterprise
    And Test with id 13817 added to Testrail run
    And All module ids are obtained and ids for Test submodule and sub sub module
    When UpdateOrganizationRoleModules mutation is called tu update all modules and test submodule and test sub sub module to "OWNER" level
    Then Response status code is 200
    And Response contains boolean true

  Scenario: Assign Modules to Organization - happy path - gql
    And Test with id 17746 added to Testrail run
    And New organization is created
    And All level one module ids are obtained
    When AssignModules mutation is called
    Then Response status code is 200
    And AssignModules Response contains boolean true

  Scenario: Assign Modules to Organization - tenant org can't have Organization module - gql
    And Test with id 22938 added to Testrail run
    And New organization is created
    And Organizations module id is obtained
    When AssignModules mutation is called
    Then Response status code is 200
    And Response contains error messageKey "bad.request"

  Scenario: Organization Modules - happy path - gql
    And Test with id 18037 added to Testrail run
    And New organization is created
    And All level one module ids are obtained
    And AssignModules mutation is called
    When Organization modules query is called
    Then Response status code is 200
    And Organization module response contains modules list


