@gqlOrganizationRoles @gql @gqlOrganizationRole
Feature: Organization roles EP graphQl

  This feature will test Organization roles queries and mutations

  Background:
    Given admin accessToken is obtained

  Scenario: Add role to Reef organization - gql
    And Test with id 11917 added to Testrail run
    When New role is added to Reef Enterprise
    Then Response status code is 200
    And Response contains the correct role details

  Scenario: Update Reef organization role - gql
    And New role is added to Reef Enterprise
    And Test with id 13741 added to Testrail run
    When New role is updated
    Then Response status code is 200
    And Response contains updated role

  Scenario: Delete Reef organization role - gql
    And New role is added to Reef Enterprise
    And Test with id 13742 added to Testrail run
    When New role is deleted
    Then Response status code is 200
    And Response contains deleteOrganizationRole true

  Scenario: Get Reef organization Roles Page - gql
    And Test with id 13749 added to Testrail run
    And Reef organization id is obtained
    When Get organization roles request is sent for Reef organization
    Then Response status code is 200
    And Response list of roles with all desired fields

  Scenario: Find Organization Role By Id - happy path - gql
    And Test with id 14797 added to Testrail run
    And New role is added to Reef Enterprise
    And Users module is assigned to the role
    And Reef organization id is obtained
    And Invite Not SSO user to Reef organization with newly created role
    And Get user by email request is sent
    And Invitation code is obtained
    And ConfirmOrganizationUserInvite mutation is called
    When FindOrganizationRoleById query is called
    Then Response status code is 200
    And response contains all role detail with correct number of assigned modules and users
    And DeleteOrganizationUser request is called
    And Response contains list of deleted users

  Scenario: Find Organization Role Name - happy path - gql
    And Test with id 14808 added to Testrail run
    And New role is added to Reef Enterprise
    When FindOrganizationRoleName query is called
    Then Response status code is 200
    And Response contains role name
