@gqlOrganizationUserData @organizationGql @organization @gql @regression
Feature: GQL Organization User Data
  Will check obtaining user details for Not SSO users

  Background:
    Given admin accessToken is obtained

  Scenario: OrganizationUserData Query - obtain Not SSO user data reef organization
    And Test with id 12945 added to Testrail run
    And Invite Not SSO user to Reef organization as Global Reef Admin
    And Get user by email request is sent
    And Invitation code is obtained
    And ConfirmOrganizationUserInvite mutation is called
    And Activated user obtains token
    When OrganizationUserData query is called with obtained token
    Then Response status code is 200
    And Response contains user's data and desired role for user which token is used in request
    And admin accessToken is obtained
    And Get user by email request is sent
    And DeleteOrganizationUser request is called
    And Response status code is 200
    And Response contains list of deleted users

  Scenario: OrganizationUserData Query - Global Reef Admin can obtain user data from other organization
    And Test with id 17551 added to Testrail run
    And Invite Not SSO user to Reef organization as Global Reef Admin
    And Get user by email request is sent
    And Invitation code is obtained
    And ConfirmOrganizationUserInvite mutation is called
    And Activated user obtains token
    And New organization is created
    When OrganizationUserData query is called with obtained token
    Then Response status code is 200
    And Response contains user's data and desired role for user which token is used in request
    And admin accessToken is obtained
    And Get user by email request is sent
    And DeleteOrganizationUser request is called
    And Response status code is 200
    And Response contains list of deleted users


  #Note: scenario with sso user can't be automated since we can't currently obtain accessToken for SSO user

  Scenario: OrganizationUserData Query - user tries to obtain data from organization that isn't his
    And Test with id 12946 added to Testrail run
    And New role is added to Reef Enterprise
    And Invite Not SSO user to Reef organization with random role
    And Get user by email request is sent
    And Invitation code is obtained
    And ConfirmOrganizationUserInvite mutation is called
    And Activated user obtains token
    And New organization is created
    When OrganizationUserData query is called by Reef user for newly created organization
    Then Response contains error messageKey "bad.request"
    And DeleteOrganizationUser request is called
    And Response status code is 200
    And Response contains list of deleted users

  Scenario: OrganizationUserData Query - obtain Not SSO user data random organization
    And Test with id 12945 added to Testrail run
    And Invite Not SSO user to random organization with random role
    And Get user by email request is sent
    And Invitation code is obtained
    And ConfirmOrganizationUserInvite mutation is called
    And Activated user obtains token
    When OrganizationUserData query is called with obtained token
    Then Response status code is 200
    And Response contains user's data and desired role for user which token is used in request
    And DeleteOrganizationUser request is called
    And Response status code is 200
    And Response contains list of deleted users

  @redis
  Scenario: OrganizationUserData Query - check stored value in Redis
    And Test with id 17687 added to Testrail run
    And New role is added to Reef Enterprise
    And All module ids are obtained and ids for Test submodule and sub sub module
    And UpdateOrganizationRoleModules mutation is called tu update all modules and test submodule and test sub sub module to "OWNER" level
    And Invite Not SSO user to Reef organization with random role
    And Get user by email request is sent
    And Invitation code is obtained
    And ConfirmOrganizationUserInvite mutation is called
    And Activated user obtains token
    When OrganizationUserData query is called with obtained token
    Then Response contains user's data and desired role for user which token is used in request
    And User data is stored in redis without permission level "VIEWER" or "NO_ACCESS"
    And DeleteOrganizationUser request is called
    And Response status code is 200
    And Response contains list of deleted users

  @redis
  Scenario: OrganizationUserData Query - Redis update - Role permission is changed from OWNER to VIEWER
    And Test with id 17693 added to Testrail run
    And New role is added to Reef Enterprise
    And All module ids are obtained and ids for Test submodule and sub sub module
    And UpdateOrganizationRoleModules mutation is called tu update all modules and test submodule and test sub sub module to "OWNER" level
    And Invite Not SSO user to Reef organization with random role
    And Get user by email request is sent
    And Invitation code is obtained
    And ConfirmOrganizationUserInvite mutation is called
    And Activated user obtains token
    And OrganizationUserData query is called with obtained token
    And UpdateOrganizationRoleModules mutation is called tu update all modules and test submodule and test sub sub module to "VIEWER" level
    When OrganizationUserData query is called with obtained token again
    Then Response contains user's data and desired role for user which token is used in request
    And User data is stored in redis without permission level "OWNER" or "NO_ACCESS"
    And DeleteOrganizationUser request is called
    And Response status code is 200
    And Response contains list of deleted users

  @redis
  Scenario: OrganizationUserData Query - Redis update - User has 2 roles one with OWNER and one with VIEWER level
    And Test with id 17694 added to Testrail run
    And Invite Not SSO user to Reef organization as Global Reef Admin
    And Get user by email request is sent
    And Invitation code is obtained
    And ConfirmOrganizationUserInvite mutation is called
    And Activated user obtains token
    And OrganizationUserData query is called with obtained token
    And New role is added to Reef Enterprise
    And All module ids are obtained and ids for Test submodule and sub sub module
    And UpdateOrganizationRoleModules mutation is called tu update all modules and test submodule and test sub sub module to "VIEWER" level
    And Global Admin and random role are assigned to a user
    When OrganizationUserData query is called with obtained token again
    Then Response contains userName
    And User data is stored in redis without permission level "VIEWER" or "NO_ACCESS"
    And DeleteOrganizationUser request is called
    And Response status code is 200
    And Response contains list of deleted users

  @redis
  Scenario: OrganizationUserData Query - Redis Update - Organization module is changed
    And Test with id 18613 added to Testrail run
    And Random organization is created
    And "Identity Access" module is assigned to organization
    And New role is added to the new organization
    And "Identity Access" module with "Roles" and "Users" submodules is assigned to role with permission level "OWNER"
    And Invite user to new organization
    And Get user by email request is sent
    And Invitation code is obtained
    And ConfirmOrganizationUserInvite mutation is called
    And Activated user obtains token
    And OrganizationUserData query is called with obtained token
    And "Financial Reporting" module is assigned to organization
    And "Financial Reporting" module is assigned to role with permission level "OWNER"
    When OrganizationUserData query is called with obtained token again
    Then Response contains userName
    And User data is stored in redis without permission level "VIEWER" or "NO_ACCESS" and without "Identity Access" module
    And DeleteOrganizationUser request is called
    And Response status code is 200
    And Response contains list of deleted users






