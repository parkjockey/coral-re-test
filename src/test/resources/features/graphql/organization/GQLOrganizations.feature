@gqlOrganization @gql @organization @regression
Feature: Organizations EP graphQl

  This feature will test Organization queries and mutations

  Background:
    Given admin accessToken is obtained

  Scenario: Get Organization by id - gql
    And Test with id 11289 added to Testrail run
    And Reef organization id is obtained
    When Get Organization by id GraphQl request with Reef id is sent
    Then Find Organization response name is "REEF"
    And Find Organization response description is "REEF Enterprise organization"

  @kafka @hmt
  Scenario: Add Organization - gql
    And Test with id 11290 added to Testrail run
    When Add Organization GraphQl request is sent
    Then GraphQl Response status code is 200
    And Add Organization response contains the same data as the request
    And Organization is stored in HMT db via kafka

  Scenario: Get all Organizations - gql
    And Test with id 11912 added to Testrail run
    And Add Organization GraphQl request is sent
    When Get all Organizations GraphQl request is sent
    Then Get all Organizations Response contains new Organization

  Scenario: Get Organization Users - gql
    And Test with id 11913 added to Testrail run
    When Get Organization Users GQL request is sent
    Then The Response contains what is expected

  Scenario: Get Reef Organization Users - get specific not sso user - gql
    And Invite Not SSO user to Reef organization as Global Reef Admin
    And Get user by email request is sent
    And Invitation code is obtained
    And ConfirmOrganizationUserInvite mutation is called
    And Test with id 13946 added to Testrail run
    When Get Organization Users GQL request is sent with newly created user
    And The Response contains userData
    And DeleteOrganizationUser request is called
    Then Response status code is 200
    And Response contains list of deleted users

  Scenario: Get Organization Users - gql pagination test
    And Test with id 11914 added to Testrail run
    When Get Organization Users GQL request is sent
    Then The response is paginated correctly

  Scenario: Get organization user by email - gql
    And Test with id 11916 added to Testrail run
    And New organization is created
    Then Invite 1 user to organization request is sent
    And Response status code is 200
    And Response contains 1 invited users
    And Get user by email request is sent
    And Response contains a username
    And DeleteOrganizationUser request is called
    And Response status code is 200
    And Response contains list of deleted users

  Scenario: Add role to organization - gql
    And Test with id 11917 added to Testrail run
    And New organization is created
    And "Identity Access" module is assigned to organization
    And New role is added to the new organization
    And "Identity Access" module is assigned to role with permission level "OWNER"
    And Get organization roles request is sent
    Then Response status code is 200
    And Response contains the new role

  Scenario: Find organization by name - gql
    And Test with id 13929 added to Testrail run
    And Add Organization GraphQl request is sent
    When FindOrganizationByName query is called
    Then Response status code is 200
    And Response contains organization name

  Scenario: Update Organization - gql
    And Test with id 11290 added to Testrail run
    And Add Organization GraphQl request is sent
    When Update Organization GraphQl request is sent
    Then GraphQl Response status code is 200
    And Response contains updateOrganization "true"

  Scenario: Update Organization - not existing organization- gql
    And Test with id 17573 added to Testrail run
    When Update Organization GraphQl request is sent with not existing organization
    Then GraphQl Response status code is 200
    And Response contains error messageKey "bad.request"

  Scenario: FindAllOrganizations - find specific organization - happy path - gql
    And Test with id 26762 added to Testrail run
    And Random organization is created
    And "Identity Access" module is assigned to organization
    When FindAllOrganizations query is called for that organization
    Then GraphQl Response status code is 200
    And Response contains all organization information and assigned parent modules




