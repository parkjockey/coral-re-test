@gqlModulesPages @gql @gqlOrganizationRole
Feature: Modules Pages graphQl

  This feature will test Find all modules graphQl

  Background:
    Given admin accessToken is obtained

  Scenario: Module Pages - happy path - gql
    And Test with id 15197 added to Testrail run
    When Module Pages query is called
    Then Response status code is 200
    And Response contains modules list
