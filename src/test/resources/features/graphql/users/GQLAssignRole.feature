@gqlAssignRoles @gql @regression
Feature: Assign Roles Mutation - test cases regarding assigning roles to user

  Background:
    Given admin accessToken is obtained

  Scenario: Assign role to Reef enterprise user - gql
    And Test with id 11920 added to Testrail run
    And Invite Not SSO user to Reef organization as Global Reef Admin
    And Get user by email request is sent
    And Invitation code is obtained
    And ConfirmOrganizationUserInvite mutation is called
    And New role is added to Reef Enterprise
    When Roles are assigned to a user
    Then Response status code is 200
    And Response contains assignRoles true
    And User has only the new assigned role
    And DeleteOrganizationUser request is called
    And Response status code is 200
    And Response contains list of deleted users