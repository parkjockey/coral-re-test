#
#For auth tests disable @BeforeClass in CucumberRunner class
#
@graphQlAuth
Feature: GetOrganizationByIdUnauthorized

  This feature will test GraphQl EP authorization

  Scenario: Unauthorized Get Organization by id - GraphQl
    When An unauthorized get Organization by id GraphQl request is sent
    And Test with id 11287 added to Testrail run
    Then GraphQl response message is "[403: Forbidden]"



#    To Do: Add auth tests for other EP's