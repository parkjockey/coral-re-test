@checkEmail
Feature: Check Consumer Email feature wil check functionality of consumerUserEmailExists query
  Background:
    Given admin accessToken is obtained

  Scenario: Consumer User Email Exists - already created user - gql
    And Test with id 13931 added to Testrail run
    And Invite Not SSO user to Reef organization as Global Reef Admin
    And Get user by email request is sent
    And Invitation code is obtained
    And ConfirmOrganizationUserInvite mutation is called
    When ConsumerUserEmailExists query is called
    Then Response status code is 200
    Then Response contains consumerUserEmailExists "true"

  Scenario: Consumer User Email Exists - not existing user - gql
    And Test with id 13933 added to Testrail run
    When ConsumerUserEmailExists query is called with not existing user
    Then Response status code is 200
    Then Response contains consumerUserEmailExists "false"
