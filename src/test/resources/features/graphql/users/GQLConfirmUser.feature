@confirmUserGQL @confirmUser @gql @regression
Feature: GQL Confirm User Invite
  Will check user confirmation for SSO and NO SSO users

  Background:
    Given admin accessToken is obtained

  Scenario: ConfirmOrganizationUserInvite Mutation - no sso reef user
    And Test with id 12431 added to Testrail run
    And Invite Not SSO user to Reef organization as Global Reef Admin
    And Get user by email request is sent
    And Invitation code is obtained
    When ConfirmOrganizationUserInvite mutation is called
    Then Response status code is 200
    And Response contains confirmOrganizationUserInvite with value true
    And DeleteOrganizationUser request is called
    And Response status code is 200
    And Response contains list of deleted users

  Scenario: ConfirmOrganizationUserInvite Mutation - confirm already activated user
    And Test with id 12432 added to Testrail run
    And Invite Not SSO user to Reef organization as Global Reef Admin
    And Get user by email request is sent
    And Invitation code is obtained
    And ConfirmOrganizationUserInvite mutation is called
    When ConfirmOrganizationUserInvite mutation is called
    Then Response contains error messageKey "organization.user.status.not.invite"
    And DeleteOrganizationUser request is called
    And Response status code is 200
    And Response contains list of deleted users

  Scenario: ConfirmOrganizationUserInvite Mutation - sso user
    And Test with id 12613 added to Testrail run
    And SSO user is created
    And Invite SSO user to Reef organization as Global Reef Admin
    And Get user by email request is sent
    And Invitation code is obtained
    When ConfirmOrganizationSsoUserInvite mutation is called
    Then Response status code is 200
    And Response contains confirmOrganizationSsoUserInvite with value true
    And DeleteOrganizationUser request is called
    And Response status code is 200
    And Response contains list of deleted users

  Scenario: ConfirmOrganizationUserInvite Mutation - confirm already activated sso user
    And Test with id 12616 added to Testrail run
    And SSO user is created
    And Invite SSO user to Reef organization as Global Reef Admin
    And Get user by email request is sent
    And Invitation code is obtained
    And ConfirmOrganizationSsoUserInvite mutation is called
    When ConfirmOrganizationSsoUserInvite mutation is called
    Then Response contains error messageKey "organization.user.status.not.invite"
    And DeleteOrganizationUser request is called
    And Response status code is 200
    And Response contains list of deleted users

  Scenario: ConfirmOrganizationUserInvite Mutation - no sso random organization user
    And Test with id 12431 added to Testrail run
    And Invite Not SSO user to random organization with random role
    And Get user by email request is sent
    And Invitation code is obtained
    When ConfirmOrganizationUserInvite mutation is called
    Then Response status code is 200
    And Response contains confirmOrganizationUserInvite with value true
    And DeleteOrganizationUser request is called
    And Response status code is 200
    And Response contains list of deleted users

   #@kafka
  Scenario: Confirm User - Kafka - get user data
    And Test with id 25006 added to Testrail run
    And Invite Not SSO user to random organization with random role
    And Get user by email request is sent
    And Invitation code is obtained
    And ConfirmOrganizationUserInvite mutation is called
    When Activated user obtains token
    Then Kafka message is obtained for newly created user
    And DeleteOrganizationUser request is called
    And Response status code is 200
    And Response contains list of deleted users