@gqlInviteUsers @gql
Feature: Invite users to organization

  This feature will test inviting users to organization

  Background:
    Given admin accessToken is obtained
    When New organization is created

  Scenario: Invite a single user to an existing organization - gql
    And Test with id 11918 added to Testrail run
    And Invite 1 user to organization request is sent
    Then Response status code is 200
    And Response contains 1 invited users

  @inviteUser
  Scenario: Re-invite bulk users - gql
    And Test with id 11919 added to Testrail run
    And 5 users are re-invited
    Then Response status code is 200
    And Response contains 5 invited users

  Scenario: ConfirmOrganizationUserInvite Mutation - no sso reef user
    And Test with id 12431 added to Testrail run
    And Invite Not SSO user to Reef organization as Global Reef Admin
    And Get user by email request is sent
    And Invitation code is obtained

#    Scenario: Assign multiple roles to a user - gql
#      And Invite 1 user to organization request is sent
#      And 2 new roles are added to the new organization
#      And Roles are assigned to a user
#      Then Response status code is 200
#      And User has the assigned roles

  Scenario: Organization User Invite Data query - no sso reef user - gql
    And Test with id 14816 added to Testrail run
    And Invite Not SSO user to Reef organization as Global Reef Admin
    And Get user by email request is sent
    And Invitation code is obtained
    When OrganizationUserInviteData query is called
    Then Response status code is 200
    And response contains email, but ssoUrl is null

  Scenario: Organization User Invite Data query - sso reef user - gql
    And Test with id 14817 added to Testrail run
    And SSO user is created
    And Invite SSO user to Reef organization as Global Reef Admin
    And Get user by email request is sent
    And Invitation code is obtained
    When OrganizationUserInviteData query is called
    Then Response status code is 200
    And response contains email and ssoUrl is not null

  @kafka
  Scenario: Confirm User - Kafka - notification service
    And Test with id 25117 added to Testrail run
    And SSO user is created
    When Invite SSO user to Reef organization as Global Reef Admin
    Then Kafka message is obtained for notification topic