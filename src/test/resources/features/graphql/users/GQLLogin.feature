@gqlUser @gql @gqlLogin
Feature: Login EP graphQl

  This feature will test Login queries and mutations

  Scenario: Login query obtain access code - no sso - gql
    And Test with id 14831 added to Testrail run
    When No sso user credentials are sent via gql login query
    Then Access code is received

  Scenario: Login query obtain access code - sso can't obtain code- gql
    And Test with id 14832 added to Testrail run
    When Sso user credentials are sent via gql login query
    Then Response contains error messageKey "bad.credentials"

  Scenario: Get tokens query - no sso - gql
    And Test with id 12388 added to Testrail run
    And No sso user credentials are sent via gql login query
    When Access code and redirectUrl are sent via gql getTokens query
    Then Response status code is 200
    Then User receives id, access and refresh tokens

  Scenario: Get tokens query - invalid redirectUrl - gql
    And Test with id 14913 added to Testrail run
    And No sso user credentials are sent via gql login query
    When Access code and invalid redirectUrl are sent via gql getTokens query
    Then Response status code is 200
    Then Response contains error messageKey "bad.request"

  Scenario: Get tokens query - already used access code - gql
    And Test with id 14932 added to Testrail run
    And No sso user credentials are sent via gql login query
    And Access code and redirectUrl are sent via gql getTokens query
    When Access code and redirectUrl are sent again via gql getTokens query
    Then Response status code is 200
    Then Response contains error messageKey "bad.request"

  Scenario: Get refresh tokens - no sso - gql
    Given Refresh token is obtained
    And Test with id 14933 added to Testrail run
    When Refresh tokens query is called
    Then Response status code is 200
    Then User receives id, access tokens but refresh token is null

  Scenario: Get refresh tokens - the same refresh token can be used multiple times - gql
    Given Refresh token is obtained
    And Test with id 14946 added to Testrail run
    And Refresh tokens query is called
    When Refresh tokens query is called again
    Then Response status code is 200
    Then User receives id, access tokens but refresh token is null

  @redis
  Scenario: Logout - happy path - with Redis check
    Given admin accessToken is obtained
    And Test with id 17708 added to Testrail run
    When Logout mutation is called
    Then Response status code is 200
    And Response doesn't contain identityProviderLogoutUrl since it is not SSO user
    And Token's Jti is stored in Redis blacklist

 @failing1 # @redis
  Scenario: Logout - use token after logout
    Given admin accessToken is obtained
    And Test with id 17737 added to Testrail run
    And Logout mutation is called
    When Module Pages query is called
    Then Response status code is 401



#    Scenario: login requirements test - sso - gql
#      When login requirements request is sent for an sso user
#      Then Response status code is 200
#      And Response contains sso
