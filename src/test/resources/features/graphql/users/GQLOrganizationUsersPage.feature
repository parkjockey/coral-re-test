@gql @regression @gqlOrganizatoinUsersPage
Feature: GQL Organization Users Page
  Will check filters on the Organization Users Page

  Background:
    Given admin accessToken is obtained
    And Reef organization id is obtained

  Scenario: Organization Users Page query - email filter - happy path - gql
    And Test with id 30935 added to Testrail run
    When Organization Users Page query is called with email that contains "reefuato"
    Then Response status code is 200
    And Response contains only "reefuato" emails

  Scenario: Organization Users Page query - name filter - happy path - gql
    And Test with id 30973 added to Testrail run
    When Organization Users Page query is called with name that contains "Reefko Testerkovic"
    Then Response status code is 200
    And Response contains only "Reefko Testerkovic" name

  Scenario: Organization Users Page query - roleIds filter - happy path - gql
    And Test with id 30974 added to Testrail run
    And New role is added to Reef Enterprise
    And Invite Not SSO user to Reef organization with random role
    When Organization Users Page query is called with newly created role id and Global Reef Admin id
    Then Response status code is 200
    And response contains users with newly created role and Global Reef Admin users
    And DeleteOrganizationUser request is called
    And Response status code is 200
    And Response contains list of deleted users

  Scenario: Organization Users Page query - statuses filter - happy path - gql
    And Test with id 30977 added to Testrail run
    When Organization Users Page query is called with "INVITED" status filter
    Then Response status code is 200
    And First and last pages contain "INVITED" status

  Scenario: Organization Users Page query - order by statuses filter - happy path - gql
    And Test with id 30978 added to Testrail run
    When Organization Users Page query is called with "STATUS_ASC" order by statuses
    Then Response status code is 200
    And First user on the first page has status "ACTIVE" and last user on the last page has status "SUSPENDED" status

  Scenario: Organization Users Page query - order by statuses filter - happy path - gql
    And Test with id 30979 added to Testrail run
    When Organization Users Page query is called with 10 pageSize filter
    Then Response status code is 200
    And Response contains 10 user size list

  Scenario: Organization Users Page query - LastModifiedDate for invited user - happy path - gql
    And Invite Not SSO user to Reef organization as Global Reef Admin
    And Test with id 34330 added to Testrail run
    When Organization Users Page query is called for desired email
    Then Response status code is 200
    And Response has similar invitationDate and lastModifiedDate
    And DeleteOrganizationUser request is called
    And Response status code is 200
    And Response contains list of deleted users

  Scenario: Organization Users Page query - LastModifiedDate for confirmed user - happy path - gql
    And Test with id 34342 added to Testrail run
    And Invite Not SSO user to Reef organization as Global Reef Admin
    And Get user by email request is sent
    And Invitation code is obtained
    And ConfirmOrganizationUserInvite mutation is called
    When Organization Users Page query is called for desired email
    Then Response status code is 200
    And Response has similar acceptedOnDat and lastModifiedDate
    And DeleteOrganizationUser request is called
    And Response status code is 200
    And Response contains list of deleted users

  Scenario: Organization Users Page query - LastModifiedDate for suspended user - happy path - gql
    And Test with id 34343 added to Testrail run
    And Reef organization id is obtained
    And Invite Not SSO user to random organization with random role
    And Get user by email request is sent
    And Invitation code is obtained
    And ConfirmOrganizationUserInvite mutation is called
    And Activated user obtains token
    And Update User Status mutation is called with status "SUSPENDED"
    When Organization Users Page query is called for desired email
    Then Response status code is 200
    And Response has different invitationDate, acceptedOnDat from lastModifiedDate
    And DeleteOrganizationUser request is called
    And Response status code is 200
    And Response contains list of deleted users

  Scenario: Organization Users Page query - LastModifiedDate for activated user - happy path - gql
    And Test with id 34344 added to Testrail run
    And Reef organization id is obtained
    And Invite Not SSO user to random organization with random role
    And Get user by email request is sent
    And Invitation code is obtained
    And ConfirmOrganizationUserInvite mutation is called
    And Update User Status mutation is called with status "SUSPENDED"
    And Update User Status mutation is called with status "ACTIVE"
    When Organization Users Page query is called for desired email
    Then Response status code is 200
    And Response has different invitationDate, acceptedOnDat from lastModifiedDate
    And DeleteOrganizationUser request is called
    And Response status code is 200
    And Response contains list of deleted users

  Scenario: Organization Users Page query - LastModifiedDate for new role added - happy path - gql
    And Test with id 34345 added to Testrail run
    And Invite Not SSO user to Reef organization as Global Reef Admin
    And Get user by email request is sent
    And Invitation code is obtained
    And ConfirmOrganizationUserInvite mutation is called
    And Activated user obtains token
    And OrganizationUserData query is called with obtained token
    And New role is added to Reef Enterprise
    And All module ids are obtained and ids for Test submodule and sub sub module
    And UpdateOrganizationRoleModules mutation is called tu update all modules and test submodule and test sub sub module to "VIEWER" level
    And Global Admin and random role are assigned to a user
    When Organization Users Page query is called for desired email
    Then Response status code is 200
    And Response has different invitationDate, acceptedOnDat from lastModifiedDate
    And DeleteOrganizationUser request is called
    And Response status code is 200
    And Response contains list of deleted users

  Scenario: Organization Users Page query - not existing email filter - gql
    And Test with id 39579 added to Testrail run
    When Organization Users Page query is called with email that contains "notExisting"
    Then Response status code is 200
    And Response contains total number of 0 elements
