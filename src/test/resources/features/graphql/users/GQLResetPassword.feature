@resetPasswordGql @resetPassword @gql @regression
Feature: GQL Reset Password feature
  Will cover tests regarding reset password flow when user forgot his password

  Background:
    Given admin accessToken is obtained
@failing1
  Scenario: Reset Password Mutation - B2B user - happy path - gql
    And Test with id 31198 added to Testrail run
    And Random organization is created
    And "Identity Access" module is assigned to organization
    And Invite user to new organization with Admin role
    And Get user by email request is sent
    And Invitation code is obtained
    And ConfirmOrganizationUserInvite mutation is called
    When Reset Password mutation is called
    Then Response status code is 200
    And Response contains resetPassword true
    And Reset password verification code is exists in DB
    And Email is received with subject: "Reset password"
    And DeleteOrganizationUser request is called
    And Response status code is 200
    And Response contains list of deleted users
@failing1
  Scenario: Confirm Reset Password Mutation - B2B user - happy path - gql
    And Test with id 31199 added to Testrail run
    And Random organization is created
    And "Identity Access" module is assigned to organization
    And Invite user to new organization with Admin role
    And Get user by email request is sent
    And Invitation code is obtained
    And ConfirmOrganizationUserInvite mutation is called
    And Reset Password mutation is called
    And Reset password verification code is exists in DB
    When Confirm Reset Password Mutation is called
    Then Response status code is 200
    And Response contains confirmResetPassword true
    And User can obtain token with new password
    And Email is received with subject: "Password updated"
    And DeleteOrganizationUser request is called
    And Response status code is 200
    And Response contains list of deleted users

  Scenario: Reset Password Mutation - sso user can't reset pass via app - gql
    And Test with id 33031 added to Testrail run
    And SSO user is created
    And Invite SSO user to Reef organization as Global Reef Admin
    And Get user by email request is sent
    And Invitation code is obtained
    And ConfirmOrganizationSsoUserInvite mutation is called
    When Reset Password mutation is called
    Then Response status code is 200
    And Response contains error messageKey "not.allowed.reset.password.for.sso.user"
    And DeleteOrganizationUser request is called
    And Response status code is 200
    And Response contains list of deleted users

