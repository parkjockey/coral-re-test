@gqlUpdatePassword @userGQL @user @gql @regression
Feature: GQL Update Password
  Will check updating user password

  Background:
    Given admin accessToken is obtained
@failing1
  Scenario: UpdatePassword mutation - happy path reef organization - gql
    And Test with id 12874 added to Testrail run
    And Invite Not SSO user to Reef organization as Global Reef Admin
    And Get user by email request is sent
    And Invitation code is obtained
    And ConfirmOrganizationUserInvite mutation is called
    And Activated user obtains token
    When User update his password
    Then Response status code is 200
    And Response contains updatePassword true
    And Email is received with subject: "Password updated"
    And User can obtain token with new password
    And DeleteOrganizationUser request is called
    And Response status code is 200
    And Response contains list of deleted users

  Scenario: UpdatePassword mutation - update password with current password
    And Test with id 12890 added to Testrail run
    And Invite Not SSO user to Reef organization as Global Reef Admin
    And Get user by email request is sent
    And Invitation code is obtained
    And ConfirmOrganizationUserInvite mutation is called
    And Activated user obtains token
    And User update his password first time
    When User update his password again
    Then Response status code is 200
    Then Response contains error messageKey "bad.credentials"
    And DeleteOrganizationUser request is called
    And Response status code is 200
    And Response contains list of deleted users

  Scenario: UpdatePassword mutation - update password with wrong current password
    And Test with id 12891 added to Testrail run
    And Invite Not SSO user to Reef organization as Global Reef Admin
    And Get user by email request is sent
    And Invitation code is obtained
    And ConfirmOrganizationUserInvite mutation is called
    And Activated user obtains token
    When User tries to update his password with wrong current password
    Then Response status code is 200
    Then Response contains error messageKey "bad.credentials"
    And DeleteOrganizationUser request is called
    And Response status code is 200
    And Response contains list of deleted users
@failing1
  Scenario: UpdatePassword mutation - happy path random organization - gql
    And Test with id 12951 added to Testrail run
    And Invite Not SSO user to random organization with random role
    And Get user by email request is sent
    And Invitation code is obtained
    And ConfirmOrganizationUserInvite mutation is called
    And Activated user obtains token
    When User update his password
    Then Response status code is 200
    And Response contains updatePassword true
    And User can obtain token with new password
    And Email is received with subject: "Password updated"
    And admin accessToken is obtained
    And DeleteOrganizationUser request is called
    And Response status code is 200
    And Response contains list of deleted users