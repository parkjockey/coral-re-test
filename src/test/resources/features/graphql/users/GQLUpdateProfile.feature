@gqlUpdateProfile @userGQL @user @gql @regression
Feature: GQL Update Profile
  Will check updating user profile data

  Background:
    Given admin accessToken is obtained

  Scenario: UpdateProfile mutation - happy path reef organization
    And Test with id 12843 added to Testrail run
    And Invite Not SSO user to Reef organization as Global Reef Admin
    And Get user by email request is sent
    And Invitation code is obtained
    And ConfirmOrganizationUserInvite mutation is called
    And Activated user obtains token
    When User update his profile
    Then Response status code is 200
    And Response contains updateProfile true
    And User details are now updated
    And DeleteOrganizationUser request is called
    And Response status code is 200
    And Response contains list of deleted users


  Scenario: UpdateProfile mutation - happy path random organization
    And Test with id 12843 added to Testrail run
    And Invite Not SSO user to random organization with random role
    And Get user by email request is sent
    And Invitation code is obtained
    And ConfirmOrganizationUserInvite mutation is called
    And Activated user obtains token
    When User update his profile
    Then Response status code is 200
    And Response contains updateProfile true
    And User details are now updated
    And DeleteOrganizationUser request is called
    And Response status code is 200
    And Response contains list of deleted users
#FOR NOW FEATURE IS DISABLED
#  Scenario: UpdateProfile mutation - consumer user - can update email VIA VERIFICATION LINK
#    Given REEF Consumers organization id is obtained
#    And OTP code and session are obtained for not existing email
#    And ConfirmPasswordlessConsumerUserSignUp mutation is called
#    And Consumer token and id are obtained
#    When Consumer update his profile with new email
#    Then Response status code is 200
#    And Response contains updateProfile true
#    And Email is received with subject: "Verify email address"

  Scenario: UpdateProfile mutation - Enterprise user can't update email
    And Test with id 14258 added to Testrail run
    And Invite Not SSO user to Reef organization as Global Reef Admin
    And Get user by email request is sent
    And Invitation code is obtained
    And ConfirmOrganizationUserInvite mutation is called
    And Activated user obtains token
    When User tries to update his email
    Then Response status code is 200
    Then Response contains error messageKey "bad.request"


  Scenario: UpdateProfilePicture mutation - happy path reef organization
    And Test with id 19959 added to Testrail run
    And Invite Not SSO user to Reef organization as Global Reef Admin
    And Get user by email request is sent
    And Invitation code is obtained
    And ConfirmOrganizationUserInvite mutation is called
    And Activated user obtains token
    When User update his profile picture
    Then Response status code is 200
    And Response contains updateProfilePicture is true
    And DeleteOrganizationUser request is called
    And Response status code is 200
    And Response contains list of deleted users

  Scenario: UpdateProfilePicture mutation - minimal picture resolution is 640x640
    And Test with id 19959 added to Testrail run
    And Invite Not SSO user to Reef organization as Global Reef Admin
    And Get user by email request is sent
    And Invitation code is obtained
    And ConfirmOrganizationUserInvite mutation is called
    And Activated user obtains token
    When User update his profile picture with smaller resolution
    Then Response status code is 200
    And Response contains error messageKey "bad.request"
    And DeleteOrganizationUser request is called
    And Response status code is 200
    And Response contains list of deleted users


  Scenario: UpdateProfilePicture mutation - invalid picture extension
    And Test with id 20563 added to Testrail run
    And Invite Not SSO user to Reef organization as Global Reef Admin
    And Get user by email request is sent
    And Invitation code is obtained
    And ConfirmOrganizationUserInvite mutation is called
    And Activated user obtains token
    When User update his profile picture with invalid extension
    Then Response status code is 200
    And Response contains error messageKey "bad.request"
    And DeleteOrganizationUser request is called
    And Response status code is 200
    And Response contains list of deleted users

  Scenario: ProfilePicture query - happy path reef organization
    And Test with id 20575 added to Testrail run
    And Invite Not SSO user to Reef organization as Global Reef Admin
    And Get user by email request is sent
    And Invitation code is obtained
    And ConfirmOrganizationUserInvite mutation is called
    And Activated user obtains token
    And User update his profile picture
    When ProfilePicture query is called
    Then Response status code is 200
    And DeleteOrganizationUser request is called
    And Response status code is 200
    And Response contains list of deleted users

 @failing1 @passwordLess   # more passing than failing
  Scenario: UpdateEmail mutation with OTP - gql
    Given REEF Consumers organization id is obtained
    And OTP code and session are obtained for not existing email
    And ConfirmPasswordlessConsumerUserSignUp mutation is called
    And Consumer token and id are obtained
    And Test with id 22338 added to Testrail run
    When UpdateEmail mutation is called
    Then Response status code is 200
    And Response contains updateEmail is true
    And Email is received with subject: "Verify email address", body contains otp code

 @failing1 @passwordLess
  Scenario: ConfirmConsumerUserEmailWithOtp mutation with OTP - gql
    Given REEF Consumers organization id is obtained
    And OTP code and session are obtained for not existing email
    And ConfirmPasswordlessConsumerUserSignUp mutation is called
    And Consumer token and id are obtained
    And UpdateEmail mutation is called
    And Email is received with subject: "Verify email address", body contains otp code
    And Test with id 22339 added to Testrail run
    When ConfirmConsumerUserEmailWithOtp mutation is called
    Then Response status code is 200
    And Response contains confirmConsumerUserEmailWithOtp is true
    And Email is received on the old email address with subject: "Email address changed"




