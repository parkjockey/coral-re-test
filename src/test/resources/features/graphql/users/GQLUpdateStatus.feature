@gqlSuspendUser @gql @regression @suspendUser
Feature: GQL Update User Status feature
  Will cover test cases regarding suspending and activating B2E/B2B users

  Background:
    Given admin accessToken is obtained

  @failing1 @kafka @redis
  Scenario: Updated User Status - suspend B2E user with random role - happy path - gql
    And Test with id 31831 added to Testrail run
    And Reef organization id is obtained
    And Invite Not SSO user to random organization with random role
    And Get user by email request is sent
    And Invitation code is obtained
    And ConfirmOrganizationUserInvite mutation is called
    And Activated user obtains token
    When Update User Status mutation is called with status "SUSPENDED"
    Then Response status code is 200
    And Response contains updateStatus true
    And Suspended user can't login with received error message "user.suspended.in.all.organizations"
    And Kafka message not contains user's permissions
    And Redis doesn't have information about suspended user


  Scenario: Updated User Status - Activate suspended B2E user with random role - happy path - gql
    And Test with id 31832 added to Testrail run
    And Reef organization id is obtained
    And Invite Not SSO user to random organization with random role
    And Get user by email request is sent
    And Invitation code is obtained
    And ConfirmOrganizationUserInvite mutation is called
    And Update User Status mutation is called with status "SUSPENDED"
    When Update User Status mutation is called with status "ACTIVE"
    Then Response status code is 200
    And Response contains updateStatus true
    And Activated user obtains token
    And DeleteOrganizationUser request is called
    And Response status code is 200
    And Response contains list of deleted users

  Scenario: Updated User Status - B2e Global Reef admin can't be suspended - gql
    And Test with id 31833 added to Testrail run
    And Invite Not SSO user to Reef organization as Global Reef Admin
    And Get user by email request is sent
    And Invitation code is obtained
    And ConfirmOrganizationUserInvite mutation is called
    And Activated user obtains token
    When Update User Status mutation is called with status "SUSPENDED"
    Then Response contains error messageKey "bad.request"
    And DeleteOrganizationUser request is called
    And Response status code is 200
    And Response contains list of deleted users

  Scenario: Updated User Status - suspend B2B user with admin role - happy path - gql
    And Test with id 31834 added to Testrail run
    And Random organization is created
    And "Identity Access" module is assigned to organization
    And Invite user to new organization with Admin role
    And Get user by email request is sent
    And Invitation code is obtained
    And ConfirmOrganizationUserInvite mutation is called
    When Update User Status mutation is called with status "SUSPENDED"
    Then Response status code is 200
    And Response contains updateStatus true
    And Suspended user can't login with received error message "user.suspended.in.all.organizations"
    And DeleteOrganizationUser request is called
    And Response status code is 200
    And Response contains list of deleted users

  Scenario: Updated User Status - Activate suspended B2B user with admin role - happy path - gql
    And Test with id 31834 added to Testrail run
    And Random organization is created
    And "Identity Access" module is assigned to organization
    And Invite user to new organization with Admin role
    And Get user by email request is sent
    And Invitation code is obtained
    And ConfirmOrganizationUserInvite mutation is called
    And Update User Status mutation is called with status "SUSPENDED"
    When Update User Status mutation is called with status "ACTIVE"
    Then Response status code is 200
    And Response contains updateStatus true
    And Activated user obtains token
    And DeleteOrganizationUser request is called
    And Response status code is 200
    And Response contains list of deleted users

  @kafka @redis @CORE-1719
  Scenario: Updated User Status - Suspend B2B user from one of 2 organizations - happy path - gql
    And Test with id 31925 added to Testrail run
    And 2 random organizations are created
    And "Identity Access" module is assigned to 2 organizations
    And Invite user to the first new organizations with Admin role
    And User id with invitation code is obtained for the first organization
    And ConfirmOrganizationUserInvite mutation is called
    And Invite user to the second new organizations with Admin role
    And User id with invitation code is obtained for the second organization
    And ConfirmOrganizationUserInvite mutation is called
    When Update User Status mutation is called with status "SUSPENDED" for first organization
    And Activated user obtains token
    Then Organization user data for first organization has not provided
    But Organization user data for second organization has status active
    And Redis doesn't contain information from suspended organization
    And Kafka message not contains user's permissions of the first org


