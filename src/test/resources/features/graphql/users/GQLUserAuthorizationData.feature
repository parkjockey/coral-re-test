@gqlUserAuthorizationData @userGQL @user @gql @regression
Feature: GQL User Authorization Data
  Will check if query contains necessary modules permissions for specific user

  Background:
    Given admin accessToken is obtained

  Scenario: User Authorization Data query - happy path - reef organization admin
    And Test with id 14788 added to Testrail run
    And Invite Not SSO user to Reef organization as Global Reef Admin
    And Get user by email request is sent
    And Invitation code is obtained
    And ConfirmOrganizationUserInvite mutation is called
    And Activated user obtains token
    When UserAuthorizationData query is called
    Then Response status code is 200
    And Response contains globalReefAdmin true with all module permissions set to OWNER
    And DeleteOrganizationUser request is called
    And Response status code is 200
    And Response contains list of deleted users