@userOrganizationsGql @userGql @user @regression @gql
Feature: User Organizations GQL
  Will test User Organizations query

  Background:
    Given admin accessToken is obtained

    Scenario: User Organizations - happy path - gql
      And Test with id 14956 added to Testrail run
      And Invite Not SSO user to Reef organization as Global Reef Admin
      And Get user by email request is sent
      And Invitation code is obtained
      And ConfirmOrganizationUserInvite mutation is called
      And Activated user obtains token
      And Reef organization id is obtained
      When User Organizations query is called
      Then Response status code is 200
      And Response contains details about Reef Organization
      And DeleteOrganizationUser request is called
      And Response status code is 200
      And Response contains list of deleted users