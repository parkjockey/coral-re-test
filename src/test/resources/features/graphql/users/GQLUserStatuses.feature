@gql @regression @userStatuses
Feature: GQL Users Statuses

  Background:
    Given admin accessToken is obtained

  Scenario: Get Users Statuses - happy path - gql
    And Test with id 29044 added to Testrail run
    When UsersStatuses query is called
    Then Response status code is 200
    And Response contains list of statuses