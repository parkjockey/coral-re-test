@gql @gqlConsumerUserPools @regression
Feature: GQL Consumer User Pools feature will cover tests regarding fetching data for Cognito user pools

  Scenario: Consumer User Pools Query - happy path - gql
    Given admin accessToken is obtained
    And Test with id 31379 added to Testrail run
    When Consumer User Pool query is called
    Then Response status code is 200
    And Response contains list of available regions

