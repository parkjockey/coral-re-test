#@regression @gql @consumerUserPhoneExists
Feature: GQL Check Consumer Phone feature wil check functionality of consumerUserEmailExists query

 @failing1 @twilio
  Scenario: Consumer User Phone Number Exists - happy path - gql
    Given OTP code and session are obtained for not existing phone number
    And Test with id 22284 added to Testrail run
    And ConfirmPasswordlessConsumerUserSignUp mutation is called for P#
    And Response contains successfulAuthentication true and all tokens
    And Test with id 13931 added to Testrail run
    When ConsumerUserPhoneExists query is called
    Then Response status code is 200
    And Response contains consumerUserPhoneExists "true"

  Scenario: Consumer User Phone Number Doesn't Exists - happy path - gql
    Given Test with id 22285 added to Testrail run
    When ConsumerUserPhoneExists query is called with not existing P#
    Then Response status code is 200
    And Response contains consumerUserPhoneExists "false"