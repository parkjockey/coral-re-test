@gql @regression @ConsumerUserData
Feature: GQL Consumer User Data
  Will cover tests regarding Consumer User Data query

  Background:
    Given admin accessToken is obtained

  Scenario: Consumer User Data - happy path - gql
    Given Consumer with email is created
    And Test with id 27451 added to Testrail run
    When ConsumerUserData query is called
    Then Response status code is 200
    And Response contains consumer user data

  Scenario: Consumer User Data By Id - happy path - gql
    And Consumer with email is created
    And Test with id 35761 added to Testrail run
    When ConsumerUserDataById query is called
    Then Response status code is 200
    And Response contains consumer user data by id