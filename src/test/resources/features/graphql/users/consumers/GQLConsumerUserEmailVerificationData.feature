#@gqlConsumerEmailVerification @userGQL @consumer @gql @regression
#Feature: GQL Consumer User Email Verification Data
#  Contains tests for fetching consumer data - B2C users

#FOR NOW FEATURE IS DISABLED
#  Background:
#    Given admin accessToken is obtained
#
#  Scenario: Consumer User Email Verification Data - happy path - gql
#    Given OTP code and session are obtained for not existing email
#    And ConfirmPasswordlessConsumerUserSignUp mutation is called
#    And Consumer token and id are obtained
#    And Consumer update his profile with new email
#    And Verification code is obtained from DB
#    And Test with id 14478 added to Testrail run
#    When ConsumerUserEmailVerificationData query is called
#    Then Response status code is 200
#    And Response contains updated temporaryEmail
