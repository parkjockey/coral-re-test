@gqlConsumerUsersPage @gql @regression
Feature: GQL Consumer Users Page will cover tests regarding searching consumer users

  Background:
    Given admin accessToken is obtained

  Scenario: Consumer Users Page Query - find consumer via email - happy path
    And Consumer with email is created
    And Test with id 31414 added to Testrail run
    When Consumer Users Page Query is called with created email consumer
    Then Response contains email and other consumer information

  Scenario: Consumer Users Page Query - find consumer via phone# - happy path
    And Consumer with phone is created
    And Test with id 31430 added to Testrail run
    When Consumer Users Page Query is called with created phone# consumer
    Then Response contains phone# and other consumer information