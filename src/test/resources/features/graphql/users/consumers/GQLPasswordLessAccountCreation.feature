#@passwordLess @gql @regression
Feature: GQL PasswordLess account creation

  Scenario: PasswordLess Consumer User SignUp - happy path - gql
    Given Test with id 14001 added to Testrail run
    When PasswordLessConsumerUserSignUp mutation is called with not existing email
    Then Response status code is 200
    And Response contains session
    And Email is received with OTP code

 @failing1 @twilio @p#signup
  Scenario: PasswordLess Consumer User SignUp - P# happy path - gql
    Given Test with id 20798 added to Testrail run
    When PasswordLessConsumerUserSignUp mutation is called with not existing P#
    Then Response status code is 200
    And Response contains session
    And SMS is received with OTP code

  Scenario: PasswordLess Consumer User SignUp - obtain second OTP - gql
    Given First OTP code and session are obtained for not existing email
    And Test with id 14002 added to Testrail run
    When PasswordLessConsumerUserSignUp mutation is called again for the same email
    Then New OTP code is obtained

  @kafka
  Scenario: Confirm PasswordLess Consumer User SignUp - happy path - gql
    Given OTP code and session are obtained for not existing email
    And Test with id 14015 added to Testrail run
    When ConfirmPasswordlessConsumerUserSignUp mutation is called
    Then Response status code is 200
    And Response contains successfulAuthentication true and all tokens
    And Kafka message is obtained for newly created consumer user

 @failing1 @twilio
  Scenario: Confirm PasswordLess Consumer User SignUp - P# happy path - gql
    Given OTP code and session are obtained for not existing phone number
    And Test with id 21191 added to Testrail run
    When ConfirmPasswordlessConsumerUserSignUp mutation is called for P#
    Then Response status code is 200
    And Response contains successfulAuthentication true and all tokens

  Scenario: Confirm PasswordLess Consumer User SignUp - try to confirm user again - gql
    Given OTP code and session are obtained for not existing email
    And Test with id 14022 added to Testrail run
    And ConfirmPasswordlessConsumerUserSignUp mutation is called
    When ConfirmPasswordlessConsumerUserSignUp mutation is called again
    Then Response status code is 200
    And Response contains error messageKey "bad.request"


#  Scenario: Confirm PasswordLess Consumer User SignUp - happy path - gql
#    Given OTP code and session are obtained for not existing email
#    And ConfirmPasswordlessConsumerUserSignUp mutation is called
#    And Activated user obtains token

