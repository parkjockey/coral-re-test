#@passwordLessLogin @passwordLess @gql @regression
Feature: GQL PasswordLess Login
 @failing1 @jenkinsDemo1
  Scenario: PasswordLess Consumer User Login - happy path - gql
    Given OTP code and session are obtained for not existing email
    And ConfirmPasswordlessConsumerUserSignUp mutation is called
    And Test with id 19976 added to Testrail run
    When PasswordLessConsumerUserLogin query is called
    Then Response status code is 200
    And Response has session code
    And Login Email is received with OTP code

 @failing1 @twilio
  Scenario: PasswordLess Consumer User Login - P# happy path - gql
    Given OTP code and session are obtained for not existing phone number
    And ConfirmPasswordlessConsumerUserSignUp mutation is called for P#
    And Response contains successfulAuthentication true and all tokens
    And Test with id 21198 added to Testrail run
    When PasswordLessConsumerUserLogin query is called with phone number
    Then Response status code is 200
    And Response has session code
    And Login SMS is received with OTP code


  Scenario: PasswordLess Consumer User Login - Enterprise use can't login - gql
    Given Test with id 20468 added to Testrail run
    When ConfirmOrganizationUserInvite mutation is called with enterprize admin user credentials
    Then Response status code is 200
    And Response contains error messageKey "user.not.found"
@failing1
  Scenario: Confirm PasswordLess Consumer User Login - happy path - gql
    Given OTP code and session are obtained for not existing email
    And ConfirmPasswordlessConsumerUserSignUp mutation is called
    And PasswordLessConsumerUserLogin query is called
    And Response has session code
    And Login Email is received with OTP code
    And Test with id 20460 added to Testrail run
    When ConfirmPasswordLessConsumerUserLogin mutation is called
    Then Response status code is 200
    Then Confirm PasswordLess Login Response contains successfulAuthentication true and all tokens

 @failing1 @twilio @jenkinsDemo2
  Scenario: Confirm PasswordLess Consumer User Login - P# happy path - gql
    Given OTP code and session are obtained for not existing phone number
    And ConfirmPasswordlessConsumerUserSignUp mutation is called for P#
    And PasswordLessConsumerUserLogin query is called with phone number
    And Response has session code
    And Login SMS is received with OTP code
    And Test with id 21201 added to Testrail run
    When ConfirmPasswordLessConsumerUserLogin mutation is called for Phone number
    Then Response status code is 200
    Then Confirm PasswordLess Login Response contains successfulAuthentication true and all tokens
