@gqlSuspendConsumer @gql @regression @suspendConsumer
Feature: GQL Update Consumer Status feature
  Will cover test cases regarding suspending and activating consumers

  Background:
    Given admin accessToken is obtained

  Scenario: Updated Consumer Status - suspend consumer - happy path - gql
    And Consumer with email is created
    And Test with id 35054 added to Testrail run
    When  UpdateConsumerStatus mutation is called with status "SUSPENDED"
    Then Response status code is 200
    And Response contains updateConsumerStatus true
    And Suspended consumer can't login with received error message "user.is.suspended"

  Scenario: Updated Consumer Status - activate suspended consumer - happy path - gql
    And Consumer with email is created
    And Test with id 35055 added to Testrail run
    And  UpdateConsumerStatus mutation is called with status "SUSPENDED"
    When  UpdateConsumerStatus mutation is called with status "ACTIVE"
    Then Response status code is 200
    And Response contains updateConsumerStatus true
    And Reactivated consumer can obtain activation code again