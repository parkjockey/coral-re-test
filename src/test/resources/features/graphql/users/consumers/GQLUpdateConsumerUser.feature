@gql @regression @consumer @gqlUpdateConsumer
Feature: GQL Update Consumer User

  Background:
    Given admin accessToken is obtained

  Scenario: Update Consumer User - happy path - gql
    And Consumer with email is created
    And Test with id 35047 added to Testrail run
    When UpdateConsumerUser mutation is called with new first, last name phone and email
    Then Response status code is 200
    And Consumer's first, last name phone and email are updated

