#@gql @regression @consumer @updatePhoneNumber
Feature: GQL Update Phone Number for consumers
  will tests sending OTP for phone update and confirmation of newly added phone

 @failing1 @twilio
  Scenario: Update Phone Number for consumers - change existing phone number - happy path
    Given Consumer with phone is created
    And Test with id 27412 added to Testrail run
    When Update Phone Number gql is called for consumer user
    Then Response status code is 200
    And Response contains Update Phone Number true
    And Sms is received with OTP for phone update
    And Consumer user now has temporary phone number

 @failing1 @twilio
  Scenario: Update Phone Number for consumers - consumer has email without phone - happy path
    Given Consumer with email is created
    And Test with id 27432 added to Testrail run
    When Update Phone Number gql is called for consumer user
    Then Response status code is 200
    And Response contains Update Phone Number true
    And Sms is received with OTP for phone update

 @failing1 @twilio
  Scenario: Confirm Consumer User Phone Number With Otp - confirm phone update - happy path
    Given Consumer with email is created
    And Test with id 27435 added to Testrail run
    And Update Phone Number gql is called for consumer user
    And Sms is received with OTP for phone update
    When ConfirmConsumerUserPhoneNumberWithOtp mutation is called
    Then Response status code is 200
    And Response contains Confirm Consumer User Phone Number with OTP true
    And Consumer user now has new phone