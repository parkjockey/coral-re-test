@rest @restOtp @regression
Feature: Rest EP for obtaining OTP code in response

  Scenario: Rest Consumer SignUp with Otp in response - email - happy path
    Given Test with id 25328 added to Testrail run
    When ConsumerSingUp ep is called with email value
    Then Response status code is 200
    And Response contains session and otp

  Scenario: Rest Consumer SignUp with Otp in response - phone - happy path
    Given Test with id 25331 added to Testrail run
    When ConsumerSingUp ep is called with phone value
    Then Response status code is 200
    And Response contains session and otp

  Scenario: Rest Consumer SignUp with Otp in response - email and phone - happy path
    Given Test with id 25328 added to Testrail run
    When ConsumerSingUp ep is called with both email and phone values
    Then Response status code is 200
    And Response contains session and otp
@failing1
  Scenario: Rest Login with in response Otp - email - happy path
    Given OTP code and session are obtained for not existing email
    And ConfirmPasswordlessConsumerUserSignUp mutation is called
    And Test with id 25336 added to Testrail run
    When Login EP is called with email
    Then Response status code is 200
    And Response contains session and otp

 @failing1 @twilio
  Scenario: Rest Login with in response Otp - P# - happy path
    Given OTP code and session are obtained for not existing phone number
    And ConfirmPasswordlessConsumerUserSignUp mutation is called for P#
    And Response contains successfulAuthentication true and all tokens
    And Test with id 25340 added to Testrail run
    When Login EP is called with phone
    Then Response status code is 200
    And Response contains session and otp