@rest @getUsersByIdsRest @regression
Feature: Rest GetUsersByIds

  Background:
    Given admin accessToken is obtained

  Scenario: GetUsersByIds - happy path - rest
    Given Users Ids obtained
    And Test with id 29650 added to Testrail run
    When GetUsersByIds request is sent
    Then Response status code is 200
    And Response contains users data
