@rest @getConsumerUserByEmail @regression
Feature: Rest GetConsumerUserByEmailAddress

  Background:
    Given admin accessToken is obtained

  Scenario: Consumer Users Data by email address - happy path - rest
   And Consumer with email is created
   And Test with id 46843 added to Testrail run
   When I send GetConsumerUserByEmailAddress request with created email
   Then Response status code is 200
   And Response contains email and other consumer information details

  Scenario: Consumer Users Data by email address - negative path - rest
    And Consumer with email is created
    And Test with id 46844 added to Testrail run
    When I send GetConsumerUserByEmailAddress request with "non-existing" email
    Then Response status code is 400
    And Response contains error messageKeys "bad.request"

  Scenario: Consumer Users Data by email address - negative path - rest
    And Consumer with email is created
    And Test with id 46848 added to Testrail run
    When I send GetConsumerUserByEmailAddress request with email that is "non-existing"
    Then Response status code is 404
    And Response contains error messageKeys "user.not.found"

