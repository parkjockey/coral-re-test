@rest @getConsumerUserById  @regression

Feature: Rest GetConsumerUserByUserId

  Background:
    Given admin accessToken is obtained

  Scenario: Consumer User Data By Id - happy path - rest
  And Consumer with email is created
  And Test with id 46841 added to Testrail run
  When I send a request to getConsumerUserDataByUserId
  Then Response status code is 200
  And Response contains all the relevant data

  Scenario: Consumer User Data By Id - negative path - rest
    And Consumer with email is created
    And Test with id 46842 added to Testrail run
    When I send a request to getConsumerUserDataByUserId with non-existing id
    Then Response status code is 404
    And Response contains error messageKeys "user.not.found"

