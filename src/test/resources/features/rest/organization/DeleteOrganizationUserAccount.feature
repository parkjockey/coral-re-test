@rest @deleteUser @regression
  Feature: Delete Organization User - rest

    Background:
      Given admin accessToken is obtained

    Scenario: Delete Organization User Account - rest
      And Test with id 47908 added to Testrail run
      And Invite Not SSO user to Reef organization as Global Reef Admin
      And Get user by email request is sent
      And Invitation code is obtained
      And ConfirmOrganizationUserInvite mutation is called
      When DeleteOrganizationUser request is called
      Then Response status code is 200
      And Response contains list of deleted users