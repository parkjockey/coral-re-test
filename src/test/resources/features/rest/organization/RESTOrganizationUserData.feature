@rest @getUserData # @regression
Feature: Organization User Data - rest

  Background:
    Given admin accessToken is obtained

Scenario: Get user data for current authenticated user - rest
  And Test with id 46849 added to Testrail run
  And Invite Not SSO user to Reef organization as Global Reef Admin
  And Get user by email request is sent
  And Invitation code is obtained
  And ConfirmOrganizationUserInvite mutation is called
  And Activated user obtains token
  When GetUserDataForCurrentUser request is called with obtained token
  Then Response status code is 200
  And Response contains user's data and role for user which token is used in request
  And DeleteOrganizationUser request is called
  And Response status code is 200
  And Response contains list of deleted users
