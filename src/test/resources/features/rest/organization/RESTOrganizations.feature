@rest @getOrganizationUsers  @regression
Feature: Organizations REST EP

  This feature will test Organization REST EP

  Background:
    Given admin accessToken is obtained

  Scenario: Get Organization Users - rest
    And Test with id 46855 added to Testrail run
    When Get Organization Users request is sent
    Then Response status code is 200
    And The Response contains all relevant data

  Scenario: Get organization user by email - rest
    And Test with id 46858 added to Testrail run
    And New organization is created
    Then Invite 1 user to organization request is sent
    And Response status code is 200
    And Response contains 1 invited users
    And Get an user by email request is sent
    And Response contains user's data
    And DeleteOrganizationUser request is called
    And Response status code is 200
    And Response contains list of deleted users

 Scenario: Get organization users by IDs - rest
    And Test with id 46859 added to Testrail run
    And New organization is created
    Then Invite 1 user to organization request is sent
    And Response status code is 200
    And Response contains 1 invited users
    And Get user by email request is sent
    And Invitation code is obtained
    And ConfirmOrganizationUserInvite mutation is called
    And Get user by ids request is sent
    And The Response contains all relevant data
    And DeleteOrganizationUser request is called
    And Response status code is 200
    And Response contains list of deleted users

