@rest @updateOrganizationUsers  @regression
Feature: Organization user REST EP

  This feature will test update Organization user REST EP

  Background:
    Given admin accessToken is obtained

  Scenario: Update Organization User Account - rest
    And Test with id 49209 added to Testrail run
    And Invite Not SSO user to Reef organization as Global Reef Admin
    And Get user by email request is sent
    And Invitation code is obtained
    And ConfirmOrganizationUserInvite mutation is called
    And Activated user obtains token
    When User update his profile information
    Then Response status code is 204
    And Get user by phone request is sent
    And Response contains updated user's data
    And DeleteOrganizationUser request is called
    And Response status code is 200
    And Response contains list of deleted users