@authenticationUI @ui @regression
Feature: Authenticate admin users feature

  As a REEF admin, I must be able to login with my active directory (AD)
  credentials so that I don't have to create an account and so that my account can be administered by my organization.

  @grid
  Scenario: Authenticate admin users - sso user - Authenticate admin happy flow
    Given an administrator has already created an AD account for me
    And Test with id 5182 added to Testrail run
    When I try to log in with my AD credentials
    Then SSO User is logged in

  Scenario: Authenticate admin users - Not existing user
    Given my account doesn't exist in Active Directory
    And Test with id 5183 added to Testrail run
    When I try to insert Not existing username
    Then the system will display a Microsoft invalid username error message

  Scenario: Authenticate admin users - Invalid Password
    Given an administrator has already created an AD account for me
    And I log in with my AD
    And Test with id 5184 added to Testrail run
    And the system redirects me to Microsoft AD page
    When I insert wrong AD password
    Then the system will display a Microsoft invalid password error

  @disabledUser
  Scenario: Authenticate admin users - Disabled user
    Given disabled user in Active Directory
    And I insert disabled AD account
    And Test with id 5185 added to Testrail run
    When I insert AD password
    Then the system will display a Microsoft account locked error

  Scenario: Authenticate admin users - Login with no SSO user happy flow
    Given an administrator has already created an no SSO account for me
    And Test with id 30922 added to Testrail run
    When I try to log in with my No SSO credentials
    Then the system will take me to the home page

  Scenario: Authenticate admin users - no SSO Invalid Password
    Given an administrator has already created an no SSO account for me
    And I log in with my no SSO email
    And Test with id 30923 added to Testrail run
    When I insert wrong password
    Then the system will display a invalid password error
@UIRecoveryPassword
  Scenario: User recovery password
    Given admin accessToken is obtained
    And Invite Not SSO user to Reef organization as Global Reef Admin
    And Get user by email request is sent
    And Invitation code is obtained
    And ConfirmOrganizationUserInvite mutation is called
    And Test with id 32069 added to Testrail run
    And user go to option for recovery password and send recovery link to his email
    And User goes to Reset Password link
    And User set new password on recovery password page
    When User confirm recovery password
    Then the user should be able to log in with the new password
    And DeleteOrganizationUser request is called
    And Response status code is 200
    And Response contains list of deleted users

  Scenario: Password policy on confirm invited user form
    Given admin accessToken is obtained
    And Test with id 34388 added to Testrail run
    And Invite Not SSO user to Reef organization as Global Reef Admin
    And Get user by email request is sent
    And Invitation code is obtained
    And User goes to Create your account page
    And User enter first and last name on the confirmation form
    When User enters a password that meets all the conditions
    Then User should see that all the conditions in the list have turned green
    And DeleteOrganizationUser request is called
    And Response status code is 200
    And Response contains list of deleted users