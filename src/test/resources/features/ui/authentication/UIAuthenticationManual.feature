Feature: Feature will store manual scenarios for Authentication feature until they are not automated

  Scenario: UI Logout as SSO user
    Given SSO user is logged in
    And Users click on Logout link in Profile section
    When User click on his email in Microsoft's Pick an account form
    Then user is redirected to the Login Page

  Scenario: UI Logout as Not SSO user
    Given Not SSO user is logged in
    When Users click on Logout link in Profile section
    Then user is redirected to the Login Page