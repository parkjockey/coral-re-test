@ui @consumersAccountUI @consumers #@regression
Feature: Consumers Account feature - will assert cases related to consumer account


 # Scenario: UIConsumers - Search US consumer via email
   # Given admin accessToken is obtained
  #  And Consumer with "us" email is created
   # And NoSSO admin user is logged in
   # And user go to consumers page
   # And user select search via email option
   # And user enter consumer email in search field
   # And user open consumer profile pop-up
   # When user change consumer first and last name
   # Then user should see successfull toast message and on consumers page changed first and last name

   Scenario: UI consumers - search consumer via email
     Given admin accessToken is obtained
     And Consumer is created
     And NoSSO admin user is logged in
     And user go to consumers page
     And user select search via email option
     And user enter consumer email in search field
     When user open consumer profile pop-up
     Then user should see consumer's profile
