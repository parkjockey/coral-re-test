#@ui @consumersUI @consumers #@regression
Feature: Consumers feature - will assert cases related to consumers


  Scenario: UIConsumers - Search US consumer via phone number
    Given admin accessToken is obtained
    And Consumer with "us" phone is created
    And NoSSO admin user is logged in
    And user go to consumers page
    And Test with id 29128 added to Testrail run
    When user enter consumer mobile phone in search field
    Then User should see that consumer with this mobile phone exists in "Active" status

  Scenario: UIConsumers - Search Canada consumer via phone number
    Given admin accessToken is obtained
    And Consumer with "ca" phone is created
    And NoSSO admin user is logged in
    And user go to consumers page
    And user select option for Canada region
    And Test with id 29129 added to Testrail run
    When user enter consumer mobile phone in search field
    Then User should see that consumer with this mobile phone exists in "Active" status

  Scenario: UIConsumers - Search US consumer via email
    Given admin accessToken is obtained
    And Consumer with "us" email is created
    And NoSSO admin user is logged in
    And user go to consumers page
    And user select search via email option
    And Test with id 29132 added to Testrail run
    When user enter consumer email in search field
    Then User should see that consumer with this email exists in "Active" status

  Scenario: UIConsumers - Search Canada consumer via email
    Given admin accessToken is obtained
    And Consumer with "ca" email is created
    And NoSSO admin user is logged in
    And user go to consumers page
    And user select search via email option
    And user select option for Canada region
    And Test with id 29133 added to Testrail run
    When user enter consumer email in search field
    Then User should see that consumer with this email exists in "Active" status