@ui @consumersUI @consumers @regression
Feature: Consumers feature - will assert cases related to consumers

  Scenario: UIConsumers - Search consumer via phone number
    Given admin accessToken is obtained
    And Consumer is created
    And NoSSO admin user is logged in
    And user go to consumers page
    And Test with id 46839 added to Testrail run
    When user enters consumer mobile phone in search field
    Then User should see that consumer with this mobile phone exists

  Scenario: UIConsumers - Search consumer via email
    Given admin accessToken is obtained
    And Consumer is created
    And NoSSO admin user is logged in
    And user go to consumers page
    And user select search via email option
    And Test with id 46840 added to Testrail run
    When user enter consumer email in search field
    Then User should see that consumer with this email exists

  Scenario: UIConsumers - FAQ page
    Given NoSSO admin user is logged in
    And user go to consumers page
    And Test with id 47118 added to Testrail run
    When user clicks on FAQ link
    Then user should be able to see FAQ questions
