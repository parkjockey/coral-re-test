@ui @consumersUI @consumers @regression
Feature: Consumers feature - will assert cases related to consumer's payment methods

  Scenario: UIConsumers - update email and phone number - happy path
   Given Consumer is created
   And NoSSO admin user is logged in
   And user go to consumers page
   And Test with id 47112 added to Testrail run
   And user select search via email option
   And user enter consumer email in search field
   And user open consumer profile pop-up
   When user updates email and phone number
   Then user should see successful toast message and updated email and phone number

  Scenario: UIConsumers - update email and phone number - negative path
    Given Consumer is created
    And NoSSO admin user is logged in
    And user go to consumers page
    And Test with id 47333 added to Testrail run
    And user select search via email option
    And user enter consumer email in search field
    And user open consumer profile pop-up
    When user updates email and phone with invalid inputs
    Then user should see error messages

# this scenario is not showing payment methods, only Payment. Currently there is no way to add CC
  Scenario: UIConsumers - View consumer's payment methods
    Given Consumer is created
    And NoSSO admin user is logged in
    And user go to consumers page
    And Test with id 47113 added to Testrail run
    And user select search via email option
    And user enter consumer email in search field
    And user open consumer profile pop-up
    When user goes to Payment methods submodule
    Then user should see the list of all active payment methods


  Scenario: UIConsumers - View consumer's vehicles
    Given Consumer is created
    And mutation CreateVehicleForUser is sent
    And NoSSO admin user is logged in
    And user go to consumers page
    And Test with id 47114 added to Testrail run
    And user select search via email option
    And user enter consumer email in search field
    And user open consumer profile pop-up
    When user goes to Vehicles submodule
    Then user should see the list of all consumer's vehicles


  Scenario: UIConsumers - Add vehicle
    Given Consumer is created
    And NoSSO admin user is logged in
    And user go to consumers page
    And Test with id 47115 added to Testrail run
    And user select search via email option
    And user enter consumer email in search field
    And user open consumer profile pop-up
    When user goes to Vehicles submodule
    And user clicks add vehicle button
    And fills out required license plate and province fields and clicks add vehicle
    Then user should see successful toast message

  Scenario: UIConsumers - Add vehicle - try to add an existing vehicle
    Given Consumer is created
    And NoSSO admin user is logged in
    And user go to consumers page
    And Test with id 47115 added to Testrail run
    And user select search via email option
    And user enter consumer email in search field
    And user open consumer profile pop-up
    When user goes to Vehicles submodule
    And user clicks add vehicle button
    And fills out required license plate and province fields and clicks add vehicle
    And user should see successful toast message
    And user clicks add vehicle button
    And fills out required license plate and province fields
    Then user should see error message that license plate is already exists




  Scenario: UIConsumers - Delete vehicle
    Given Consumer is created
    And mutation CreateVehicleForUser is sent
    And NoSSO admin user is logged in
    And user go to consumers page
    And Test with id 47116 added to Testrail run
    And user select search via email option
    And user enter consumer email in search field
    And user open consumer profile pop-up
    And user goes to Vehicles submodule
    And user should see the list of all consumer's vehicles
    When user deletes an existing vehicle
    Then successful toast message is displayed

  Scenario: UIConsumers - Purchases screen
    Given Consumer is created
    And NoSSO admin user is logged in
    And user go to consumers page
    And Test with id 47117 added to Testrail run
    And user select search via email option
    And user enter consumer email in search field
    And user open consumer profile pop-up
    When user goes to Purchases submodule
    Then user should see Purchases submodule



