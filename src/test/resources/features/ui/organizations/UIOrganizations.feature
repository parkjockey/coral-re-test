@organizationUI @ui @organization @regression
Feature: UIOrganization feature - will cover E2E use case for Organization Creation/Update/Delete and Organization Searching

  Scenario: UIOrganization - Create Organization
    Given NoSSO admin user is logged in
    And user can reach Organizations page
    And user can open Organization Creation form
    And Test with id 10548 added to Testrail run
    When user insert all details on Organization Creation form and click on Create button
    Then new organization can be found


  Scenario: UIOrganization - create organization 51 character long name
    Given NoSSO admin user is logged in
    And user can reach Organizations page
    And user can open Organization Creation form
    And Test with id 13637 added to Testrail run
    When user insert 51 character long organization name
    Then user should see a message that he cannot enter over 50 characters for org name


    Scenario: UIOrganization - create organization 201 character long description
      Given NoSSO admin user is logged in
      And user can reach Organizations page
      And user can open Organization Creation form
      When user insert 201 character long organization description
      And Test with id 13638 added to Testrail run
      Then user should see a message that he cannot enter over 200 characters for org description
  @createOrgUI
  Scenario: UIOrganization - create organization when organization already exist
      Given NoSSO admin user is logged in
      And organization already exist
      And user can open Organization Creation form
      And Test with id 13639 added to Testrail run
      When user try to create organization with a name that already exists
      Then the user should see a message that the name already exists


    Scenario: UIOrganization - create organization when name contains special character
      Given NoSSO admin user is logged in
      And user can reach Organizations page
      And user can open Organization Creation form
      And Test with id 13640 added to Testrail run
      When user insert special character long organization name
      Then user should see a message that special character not allowed organization name
  @OrganizationPaginationUI
    Scenario: UIOrganization - pagination organizations - go to next page
      Given NoSSO admin user is logged in
      And user can reach Organizations page
      And Test with id 13641 added to Testrail run
      When user go to next organization page
      Then user should see that organizations exist on the page

  Scenario: UIOrganization select 40 rows per page
    Given NoSSO admin user is logged in
    And user can reach Organizations page
    And Test with id 16257 added to Testrail run
    When the user selects forty rows per page on organizations page
    Then user should see on page forty organizations

  Scenario: Search organization  - happy path
    Given NoSSO admin user is logged in
    And organization already exist
    And user can reach Organizations page
    And Test with id 17001 added to Testrail run
    When enter organization name in search field on organizations page
    Then user should see that this organization exists in search results

  Scenario: Search organization - the organization doesn't exist
    Given NoSSO admin user is logged in
    And user can reach Organizations page
    And organization doesn't exist in the Reef system
    And Test with id 17003 added to Testrail run
    When user enter not existing organization name in the search field on the organizations page
    Then the user should see no organization found message

  Scenario: UIOrganizations - Update Organization when name doesn't exists
    Given admin accessToken is obtained
    And organization is already exists
    And Test with id 30924 added to Testrail run
    And NoSSO admin user is logged in
    And user can reach Organizations page
    And user can edit created organization
    When user update created organization
    Then updated organization can be found via Search field on Organizations page

  Scenario: UIOrganizations - Update Organization when name already exists
    Given admin accessToken is obtained
    And two organizations already exists
    And Test with id 30925 added to Testrail run
    And NoSSO admin user is logged in
    And user can reach Organizations page
    And user can edit created organization
    When user tries to update the name of the first organization with the same name as the second organization
    Then user should see message that this organization name already used

  Scenario: UIOrganizations - Update Organization descriptions
    Given admin accessToken is obtained
    And organization is already exists
    And Test with id 30926 added to Testrail run
    And NoSSO admin user is logged in
    And user can reach Organizations page
    And user can edit created organization
    When user update organization description and save changes
    Then user should see new description on this organization
  @UIAssignModulesToOrg
  Scenario: UIOrganizations - Assign modules to organization
    Given admin accessToken is obtained
    And organization is already exists
    And Test with id 30927 added to Testrail run
    And NoSSO admin user is logged in
    And user can reach Organizations page
    And user can assign modules to organization
    When user assign "Identity Access" modules to organization
    And user click on Assign Modules button and open expand modules
    Then user should in expanding label that "Identity Access" modules is assigned on organization
  @container
  Scenario: UIOrganization - Switch Organization
    Given NoSSO admin user is logged in
    And Test with id 30928 added to Testrail run
    And organization with assigned modules already exist
    When user switch the organization
    Then user should see organization name in header bar and modules assigned on home page
  @container
  Scenario: UIOrganization - Admin role creation on Assign modules
    Given NoSSO admin user is logged in
    And Test with id 30929 added to Testrail run
    And organization with assigned modules already exist
    When user switch the organization
    Then user should see on roles page admin role is created

    Scenario: UIOrganization - Check organization logo
      Given NoSSO admin user is logged in
      And user can reach Organizations page
      And Test with id 25076 added to Testrail run
      When user find the "Reef" organization using the search field
      Then user should see Reef Enterprise logo

  Scenario: UIOrganization - Check organization header logo
    Given Test with id 30102 added to Testrail run
    When  NoSSO admin user is logged in
    Then user should see the Reef Enterprise logo in the header bar