@usersUI @ui @users @regression
Feature: Real Estate Admin - New Campus feature

  Scenario: UICampus - Real Estate Admin views the Campuses Page
    Given Real Estate admin user is logged in
    When user navigates to the Campuses Page
    Then user should view the Campus page elements on page

  Scenario: UICampus - Real Estate Admin views the Campus cards
    Given Real Estate admin user is logged in
    When user navigates to the Campuses Page
    Then user views the Campus cards

  Scenario: UICampus - Real Estate Admin views create new Campus modal
    Given Real Estate admin user is logged in
    When user navigates to the Campuses Page
    And user clicks on the ADD NEW CAMPUS button
    Then user should be able to view create campus modal

  Scenario: UICampus - Real Estate Admin Creates a new Campus
    Given Real Estate admin user is logged in
    When user navigates to the Campuses Page
    Then user be able to create a new campus

  Scenario: UICampus - Real Estate Admin views 25 Campus Cards
    Given Real Estate admin user is logged in
    When user navigates to the Campuses Page
    And user selects the pagination of 25 Campuses rows per page
    Then user views 25 Campus Cards rows per page

  Scenario: UICampus - Real Estate Admin views 50 Campus Cards
    Given Real Estate admin user is logged in
    When user navigates to the Campuses Page
    And user selects the pagination of 50 Campuses rows per page
    Then user views 50 Campus Cards rows per page

  Scenario: UICampus - Real Estate Admin views 100 Campus Cards
    Given Real Estate admin user is logged in
    When user navigates to the Campuses Page
    And user selects the pagination of 100 Campuses rows per page
    Then user views 100 Campus Cards rows per page
  @wip
  Scenario: UICampus - Real Estate Admin searches the campus
    Given Real Estate admin user is logged in
    When user navigates to the Campuses Page
    And user searches the already Created campus
    Then user should be able to view the campus details are available