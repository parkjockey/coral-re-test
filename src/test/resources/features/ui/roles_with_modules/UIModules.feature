@modulesUI @ui @modules @regression
Feature: UIModules feature - will cover E2E use case for Module assignment
  Note: Modules can be added to the role via role creation flow or later by clicking on modules section of specific role.

Scenario: UIModules - Assign Module to Role - all child/grandchild modules inherit parent permission level
  Given admin accessToken is obtained
  And New role is added to Reef Enterprise
  And NoSSO admin user is logged in
  And user can reach Roles page
  And user can find created role
  And user can open role's mange module form
  And Test with id 9071 added to Testrail run
  When I select Module "Test module" with level on that module: "VIEWER"
  Then all child and grandchild modules are checked also with the same permission level "VIEWER"
  And all child elements can't check "OWNER" permission level
  And number of assigned modules is 3
  And in Assign Module Form for "Test module" module number of total modules is 3 and assigned is 3


  Scenario: UIModules - Assign Module to Role - child modules can have the same or smaller permission level
    Given admin accessToken is obtained
    And New role is added to Reef Enterprise
    And NoSSO admin user is logged in
    And user can reach Roles page
    And user can find created role
    And user can open role's mange module form
    And user selected Module "Test module" with level on that module: "OWNER"
    And Test with id 5743 added to Testrail run
    When user select SubModule "Test child module" with level "VIEWER"
    Then parent Module "Test module" still has "OWNER" level
    But child Module "Test grandchild module" is changed to "VIEWER"

  Scenario: UIModules - Assign Module to Role - parent View level will override child Owner level
    Given admin accessToken is obtained
    And New role is added to Reef Enterprise
    And NoSSO admin user is logged in
    And user can reach Roles page
    And user can find created role
    And user can open role's mange module form
    And user selected Module "Test module" with level on that module: "OWNER"
    And user selected SubModule "Test child module" with level on that module: "OWNER"
    And Test with id 9080 added to Testrail run
    When I select Module "Test module" with level on that module: "VIEWER"
    Then all child and grandchild modules are checked also with the same permission level "VIEWER"



