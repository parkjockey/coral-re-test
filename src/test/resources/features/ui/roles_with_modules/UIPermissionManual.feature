Feature:  Feature will store manual scenarios for Permission features until they are not automated

  Scenario: UI Role module - Viewer Level
    Given User has Random Role that has Role module with VIEWER level
    When User reach Role page
    Then User can see Search field with user list
    And User can change View
    And User only can view Role's modules