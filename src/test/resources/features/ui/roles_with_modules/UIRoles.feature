@rolesUI @ui @roles @regression
Feature: UIRoles feature - will cover E2E use case for Role Creation/Update and Role Searching

  Scenario: UIRoles - Create Role and Search Role
    Given NoSSO admin user is logged in
    And user can reach Roles page
    And user can open Role Creation form
    And Test with id 5742 added to Testrail run
    When user insert all details on Role Creation form and click on Create button
    Then new role can be found via Search field on Roles page

  Scenario: UIRoles - Update Role
    Given NoSSO admin user is logged in
    And user can reach Roles page
    And the role is created with assigned module
    And user can edit created role
    And Test with id 5743 added to Testrail run
    When user update created role
    Then updated role can be found via Search field on Roles page


  Scenario: UIRoles - Delete Role
    Given NoSSO admin user is logged in
    And user can reach Roles page
    And the role is created with assigned module
    And user click on Delete role button
    And Test with id 8032 added to Testrail run
    When user click on Remove button in the popup
    Then role can't be found via Search field on Roles page


  Scenario: UIRoles - Delete Role - role can't be deleted if has at least one user
    Given NoSSO admin user is logged in
    And user can reach Roles page
    And user found role with assigned user
    And Test with id 9221 added to Testrail run
    When user clicked on Delete role button
    Then popup is shown with info Message that role can't be deleted right now

  Scenario: UIRoles - Create Role with same name as the deleted role
    Given admin accessToken is obtained
    And New role is added to Reef Enterprise
    And New role is deleted
    And NoSSO admin user is logged in
    And user can reach Roles page
    And Test with id 13642 added to Testrail run
    When user try to create role with same name
    Then the user should see success pop-up message

   Scenario: UIRoles - roles page lazy load
      Given NoSSO admin user is logged in
      And user can reach Roles page
      And Test with id 13674 added to Testrail run
      When user scroll down on roles page
      Then user should see roles loader
      And after loader user should see other roles

  Scenario: UIRoles - View Role Users
    Given NoSSO admin user is logged in
    And user can reach Roles page
    And user found role with assigned user
    And Test with id 29126 added to Testrail run
    When user clicked on role users label
    Then user should be redirected on users page and see only users with "Global REEF Administrator" role







