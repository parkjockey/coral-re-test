@ui @users @regression @userAccount

Feature: User account feature - will assert first and last name on the user account page

  Background:
    Given admin accessToken is obtained

  Scenario: Check first and last name of non-sso user
    And Test with id 13721 added to Testrail run
    And Invite Not SSO user to Reef organization as Global Reef Admin
    And Get user by email request is sent
    And Invitation code is obtained
    And ConfirmOrganizationUserInvite mutation is called
    When user is logged in
    Then User goes to user account page
    And User details are correct
    And DeleteOrganizationUser request is called
    And Response status code is 200
    And Response contains list of deleted users

  Scenario: Check first and last name of sso user
    And Test with id 13722 added to Testrail run
    And SSO user is created
    And Invite SSO user to Reef organization as Global Reef Admin
    And Get user by email request is sent
    And Invitation code is obtained
    And ConfirmOrganizationSsoUserInvite mutation is called
    And SSO user open activation link and login with his credentials
    When Home page is opened
    Then User goes to user account page
    And User details are correct
    And DeleteOrganizationUser request is called
    And Response status code is 200
    And Response contains list of deleted users

  Scenario: Update first and last name on user account tab
    And Invite Not SSO user to Reef organization as Global Reef Admin
    And Get user by email request is sent
    And Invitation code is obtained
    And ConfirmOrganizationUserInvite mutation is called
    And user is logged in
    And User goes to user account page
    And Test with id 13723 added to Testrail run
    When user update first and last name
    Then the user should see the updated first name and last name when again goes to the user account tab
    And DeleteOrganizationUser request is called
    And Response status code is 200
    And Response contains list of deleted users

  Scenario: Update password on user account tab
    And Invite Not SSO user to Reef organization as Global Reef Admin
    And Get user by email request is sent
    And Invitation code is obtained
    And ConfirmOrganizationUserInvite mutation is called
    And user is logged in
    And User goes to user account page
    And user go to security section
    And Test with id 13724 added to Testrail run
    When user update password
    Then the user should be able to log in with the new password
    And DeleteOrganizationUser request is called
    And Response status code is 200
    And Response contains list of deleted users
@container
  Scenario: Reset password for SSO user
    Given admin user is logged in
    And Test with id 24891 added to Testrail run
    And User go to security tab in account settings
    When user click reset password button
    Then user is redirected to the new tab - reef reset password
  @container
  Scenario: Upload user profile picture
    And Invite Not SSO user to Reef organization as Global Reef Admin
    And Get user by email request is sent
    And Invitation code is obtained
    And ConfirmOrganizationUserInvite mutation is called
    And user is logged in
    And User goes to user account page
    And User goes to upload profile picture
    When user upload profile picture
    Then User should see uploaded profile picture
    And DeleteOrganizationUser request is called
    And Response status code is 200
    And Response contains list of deleted users