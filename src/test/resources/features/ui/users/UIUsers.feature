@usersUI @ui @users @regression
Feature: Users feature - will cover e2e invite organization users and assign role to organization users

  Scenario: UIUser check organization user management page
    Given NoSSO admin user is logged in
    And Test with id 27110 added to Testrail run
    When user can reach users page
    Then user should see all elements on page

  Scenario: UIUser select rows per page
    Given NoSSO admin user is logged in
    And user can reach users page
    And Test with id 13675 added to Testrail run
    When the user selects ten rows per page
    Then user should see on page ten users

  Scenario: UIUser go to third page - pagination
    Given NoSSO admin user is logged in
    And user can reach users page
    And Test with id 13676 added to Testrail run
    When user go to third page
    Then user should see that users exist on the page

  Scenario: UIInvite user when user already exists in the organization
    Given NoSSO admin user is logged in
    And user can reach users page
    And Test with id 13677 added to Testrail run
    When user add an existing user to the organization and press enter
    Then user should see the user first and last name

  Scenario: UIInvite user when user doesn't exists in the reef system
    Given NoSSO admin user is logged in
    And user can reach users page
    And Test with id 13678 added to Testrail run
    When user add a user who does not exist in the Reef system to the organization and press enter
    Then user should see the user email

  Scenario: UIInvite user when email is not in valid format
    Given NoSSO admin user is logged in
    And user can reach users page
    And Test with id 13679 added to Testrail run
    When user add a email that is not in a valid format
    Then user should see a warning message

  Scenario: UIInvite user - send user invitation
    Given NoSSO admin user is logged in
    And user can reach users page
    And user go to invite users pop-up entered email in email invitation field
    And and the user has selected a role
    And Test with id 13680 added to Testrail run
    When user click on invite user button
    Then user should see success pop with appropriate messages
    And Email is received with subject: "Welcome to REEF"
    And DeleteOrganizationUser request is called
    And Response status code is 200
    And Response contains list of deleted users

 @failing  @container # failing because of recaptcha
  Scenario: UIConfirm Invited user - not sso user
    Given admin accessToken is obtained
    And Test with id 12715 added to Testrail run
    And Invite Not SSO user to Reef organization as Global Reef Admin
    And Get user by email request is sent
    And Invitation code is obtained
    And User goes to Create your account page
    And Test with id 12715 added to Testrail run
    When Not SSO user fill all necessary data
    Then CreateAccountPopUp is shown and user is redirected to HomePage
    And DeleteOrganizationUser request is called
    And Response status code is 200
    And Response contains list of deleted users

  @failing @container  # failing because of recaptcha
  Scenario: UIConfirm Invited user - sso user
    Given admin accessToken is obtained
    And Test with id 12613 added to Testrail run
    And SSO user is created
    And Invite SSO user to Reef organization as Global Reef Admin
    And Get user by email request is sent
    And Invitation code is obtained
    When SSO user open activation link and login with his credentials
    Then User is redirected to HomePage
    And DeleteOrganizationUser request is called
    And Response status code is 200
    And Response contains list of deleted users

  Scenario: Search organization users - happy path
    Given NoSSO admin user is logged in
    And user can reach users page
    And Test with id 14780 added to Testrail run
    When enter user email in search field on users page
    Then user should see that this user exists in search results

  Scenario: Search organization users - the user doesn't exist
    Given NoSSO admin user is logged in
    And user can reach users page
    And user email doesn't exist in the Reef system
    And Test with id 14794 added to Testrail run
    When user enter not existing email in the search field on the users page
    Then the user should see no user found message

  Scenario: UIInvite users - Add Roles to invited members - mandatory step
    Given NoSSO admin user is logged in
    And user can reach users page
    And Test with id 14800 added to Testrail run
    When user add a user who does not exist in the Reef system to the organization and press enter
    Then user should see that the invite button is disabled

  @UIAssignRoleToReefEnterpriseUser
  Scenario: UIAssign role to Reef enterprise user
    Given admin accessToken is obtained
    And Invite Not SSO user to Reef organization as Global Reef Admin
    And Get user by email request is sent
    And Invitation code is obtained
    And ConfirmOrganizationUserInvite mutation is called
    And New role is added to Reef Enterprise
    And NoSSO admin user is logged in
    And user can reach users page
    And Assign roles to user modal is opened for newly created user
    And Test with id 16917 added to Testrail run
    When First role in modal is checked and assign role button is clicked
    Then Success tostyfy message is displayed
    And DeleteOrganizationUser request is called
    And Response status code is 200
    And Response contains list of deleted users
   @UIB2BLogin
  Scenario: B2BUser - Login with B2B user
    Given admin accessToken is obtained
    And Random organization is created
    And "Identity Access" module is assigned to organization
    And New role is added to the new organization
    And "Identity Access" module is assigned to role with permission level "OWNER"
    And Invite user to new organization
    And Get user by email request is sent
    And Invitation code is obtained
    And ConfirmOrganizationUserInvite mutation is called
    And Activated user obtains token
    And Test with id 30930 added to Testrail run
    When B2B user login
    Then user should see organization name in header bar
    And DeleteOrganizationUser request is called
    And Response status code is 200
    And Response contains list of deleted users

  Scenario: Resend user invitation
    Given admin accessToken is obtained
    And Test with id 27446 added to Testrail run
    And Invite Not SSO user to Reef organization as Global Reef Admin
    And Get user by email request is sent
    And NoSSO admin user is logged in
    And user can reach users page
    And user can reach invited user
    When User resend invitation
    Then invitation pop-up message is displayed
    And DeleteOrganizationUser request is called
    And Response status code is 200
    And Response contains list of deleted users

  Scenario: Users statuses filter - Active status
    Given NoSSO admin user is logged in
    And user can reach users page
    And Test with id 30931 added to Testrail run
    When user select active status filter
    Then user should see on users page only active users

  Scenario: Users statuses filter - Invited status
    Given NoSSO admin user is logged in
    And user can reach users page
    And Test with id 30932 added to Testrail run
    When user select invited status filter
    Then user should see on users page only invited users

  Scenario: Users statuses filter - Suspended status
    Given NoSSO admin user is logged in
    And user can reach users page
    And Test with id 30933 added to Testrail run
    When user select suspended status filter
    Then user should see on users page only suspended users
#****This scenario is disabled until the moment when CORE-1820 is resolved****
# @failing @CORE-1820
  Scenario: Users Roles filter
    Given NoSSO admin user is logged in
    And user can reach users page
    And Test with id 30934 added to Testrail run
    When user select "Global REEF Administrator" role from roles filter
    Then user should see on users page only users with "Global REEF Administrator" role

  Scenario: Users Roles filter - multiple roles
    Given NoSSO admin user is logged in
    And user can reach users page
  #  And Test with id  added to Testrail run
    When user selects "Location Owner", "Location Approver" and "Location Viewer" roles from roles filter
    And user should see on users page users with selected roles
    And Assign roles to user modal is opened for first user
    And one role is unchecked and assign role button is clicked
    And Success tostyfy message is displayed
    And Assign roles to user modal is opened for first user
    And user scroll down the roles page
    Then user should see roles that are assigned to the first user

  @combinedFiltersUI
  Scenario: Users Roles filter
    Given NoSSO admin user is logged in
    And admin accessToken is obtained
    And New role is added to Reef Enterprise
    And Invite Not SSO user to Reef organization with random role
    And Get user by email request is sent
    And Invitation code is obtained
    And ConfirmOrganizationUserInvite mutation is called
    And Activated user obtains token
    And user can reach users page
    When user select new created role role from roles filter
    Then user should see on user only user with this new created role
    And DeleteOrganizationUser request is called
    And Response status code is 200
    And Response contains list of deleted users


  Scenario: Search organization users by Name - happy path
    Given NoSSO admin user is logged in
    And admin accessToken is obtained
    And Invite Not SSO user to Reef organization as Global Reef Admin
    And Get user by email request is sent
    And Invitation code is obtained
    And ConfirmOrganizationUserInvite mutation is called
    And user can reach users page
    And Test with id 31872 added to Testrail run
    When Admin search user by Name on users page
    Then user should see that this user exists in search results on users page
    And DeleteOrganizationUser request is called
    And Response status code is 200
    And Response contains list of deleted users
@container
Scenario: Suspend B2B user - happy path
  Given NoSSO admin user is logged in
  And Test with id 33034 added to Testrail run
  And admin accessToken is obtained
  And Random organization is created
  And "Identity Access" module is assigned to organization
  And New role is added to the new organization
  And "Identity Access" module is assigned to role with permission level "OWNER"
  And Invite user to new organization
  And Get user by email request is sent
  And Invitation code is obtained
  And ConfirmOrganizationUserInvite mutation is called
  And Activated user obtains token
  And user switch the organization
  And user go to users page
  And user can reach invited user
  And user suspend organization user
  And user is logged out
  When B2B user try to login
  Then user should see a message that account is suspended
  And DeleteOrganizationUser request is called
  And Response status code is 200
  And Response contains list of deleted users
  @container
  Scenario: Suspend B2E user who has the Global Reef Admin role
    Given NoSSO admin user is logged in
    And Test with id 33035 added to Testrail run
    And admin accessToken is obtained
    And Invite Not SSO user to Reef organization as Global Reef Admin
    And Get user by email request is sent
    And Invitation code is obtained
    And ConfirmOrganizationUserInvite mutation is called
    And user can reach users page
    And user can reach invited user
    When user suspend organization user
    Then user should see toast message Failed to update users status
    And DeleteOrganizationUser request is called
    And Response status code is 200
    And Response contains list of deleted users
  @container
  Scenario: Suspend B2E a user who doesn't have Global Reef Admin role
    Given NoSSO admin user is logged in
    And Test with id 33036 added to Testrail run
    And admin accessToken is obtained
    And New role is added to Reef Enterprise
    And Invite Not SSO user to Reef organization with random role
    And Get user by email request is sent
    And Invitation code is obtained
    And ConfirmOrganizationUserInvite mutation is called
    And Activated user obtains token
    And user can reach users page
    And user can reach invited user
    When user suspend organization user
    And user is logged out
    When B2B user try to login
    Then user should see a message that Enterprise account is suspended
    And DeleteOrganizationUser request is called
    And Response status code is 200
    And Response contains list of deleted users

  Scenario: Users Date range filter
    Given NoSSO admin user is logged in
    And Test with id 33243 added to Testrail run
    And user can reach users page
    When user select previous month in date range filter
    Then user should only see users who were updated last month

  #****This scenario is disabled until the moment when CORE-1820 is resolved****
  @failing @CORE-1820
  Scenario: Combined filters on Users page
    Given NoSSO admin user is logged in
    And user can reach users page
    And user can reach users page
    When user selects combined filters on users page
    Then user should only see users who match the filter criteria

  Scenario: Combined filters on Users page
    Given NoSSO admin user is logged in
    And admin accessToken is obtained
    And New role is added to Reef Enterprise
    And Invite Not SSO user to Reef organization with random role
    And Get user by email request is sent
    And Invitation code is obtained
    And ConfirmOrganizationUserInvite mutation is called
    And Activated user obtains token
    And user can reach users page
    When user selects a combined filter that correspond to this user
    Then user should only see user who match the filtered criteria
    And DeleteOrganizationUser request is called
    And Response status code is 200
    And Response contains list of deleted users